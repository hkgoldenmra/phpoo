<?php

/**
 * Description of \php\awt\Color
 */

namespace php\awt {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');

	/**
	 * The \php\awt\Color class is used to encapsulate colors.
	 */
	class Color extends \php\lang\PHPObject {

		/**
		 * Returns a standard black \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function BLACK() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0x00));
		}

		/**
		 * Returns a standard white \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function WHITE() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns a standard red \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function RED() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0x00));
		}

		/**
		 * Returns a standard green \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function GREEN() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0x00));
		}

		/**
		 * Returns a standard blue \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function BLUE() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns a standard yellow \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function YELLOW() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0x00));
		}

		/**
		 * Returns a standard magenta \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function MAGENTA() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns a standard cyan \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function CYAN() {
			return self::newInstanceByRGB(\php\lang\PHPNumber::newInstance(0x00), \php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns a transparent \php\awt\Color object.
		 * @return \php\awt\Color
		 */
		public static final function TRANSPARENT() {
			return self::newInstanceByARGB(\php\lang\PHPNumber::newInstance(0x7f), \php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0xff), \php\lang\PHPNumber::newInstance(0xff));
		}

		private static function RGBtoHSB($rgb) {
			$R = ($rgb >> 0x10) & 0xff;
			$G = ($rgb >> 0x08) & 0xff;
			$B = $rgb & 0xff;

			$HSL = array();
			$var_R = ($R / 0xff);
			$var_G = ($G / 0xff);
			$var_B = ($B / 0xff);

			$var_Min = min($var_R, $var_G, $var_B);
			$var_Max = max($var_R, $var_G, $var_B);
			$del_Max = $var_Max - $var_Min;

			$V = $var_Max;

			if ($del_Max == 0) {
				$H = 0;
				$S = 0;
			} else {
				$S = $del_Max / $var_Max;

				$del_R = ((($var_Max - $var_R) / 6) + ($del_Max / 2)) / $del_Max;
				$del_G = ((($var_Max - $var_G) / 6) + ($del_Max / 2)) / $del_Max;
				$del_B = ((($var_Max - $var_B) / 6) + ($del_Max / 2)) / $del_Max;

				if ($var_R == $var_Max) {
					$H = $del_B - $del_G;
				} else if ($var_G == $var_Max) {
					$H = (1 / 3) + $del_R - $del_B;
				} else if ($var_B == $var_Max) {
					$H = (2 / 3) + $del_G - $del_R;
				}

				if ($H < 0) {
					$H++;
				}
				if ($H > 1) {
					$H--;
				}
			}
			$HSL['H'] = $H * 360;
			$HSL['S'] = $S * 100;
			$HSL['B'] = $V * 100;
			return $HSL;
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\awt\Color object by color value.
		 * @param \php\lang\PHPNumber $value The color value.
		 * @return \php\awt\Color
		 */
		public static function newInstanceByValue(\php\lang\PHPNumber $value) {
			return new Color($value);
		}

		/**
		 * Returns a \php\awt\Color object by alpha, red, green, blue.
		 * @param \php\lang\PHPNumber $alpha The alpha channel, from 0 to 127. 127 as completely transparency. Default 0.
		 * @param \php\lang\PHPNumber $red The red channel, from 0 to 255. Default 0.
		 * @param \php\lang\PHPNumber $green The green channel, from 0 to 255. Default 0.
		 * @param \php\lang\PHPNumber $blue The blue channel, from 0 to 255. Default 0.
		 * @return \php\awt\Color
		 */
		public static function newInstanceByARGB(\php\lang\PHPNumber $alpha, \php\lang\PHPNumber $red, \php\lang\PHPNumber $green, \php\lang\PHPNumber $blue) {
			$alpha = $alpha->shiftLeft(\php\lang\PHPNumber::newInstance(0x18));
			$red = $red->shiftLeft(\php\lang\PHPNumber::newInstance(0x10));
			$green = $green->shiftLeft(\php\lang\PHPNumber::newInstance(0x08));
			return self::newInstanceByValue($alpha->addition($red)->addition($green)->addition($blue));
		}

		/**
		 * Returns a \php\awt\Color object by alpha, red, green, blue.
		 * @param \php\lang\PHPNumber $red The red channel, from 0 to 255. Default 0.
		 * @param \php\lang\PHPNumber $green The green channel, from 0 to 255. Default 0.
		 * @param \php\lang\PHPNumber $blue The blue channel, from 0 to 255. Default 0.
		 * @return \php\awt\Color
		 */
		public static function newInstanceByRGB(\php\lang\PHPNumber $red, \php\lang\PHPNumber $green, \php\lang\PHPNumber $blue) {
			return self::newInstanceByARGB(\php\lang\PHPNumber::newInstance(0x00), $red, $green, $blue);
		}

		/**
		 * Returns a \php\awt\Color object by alpha, hue, saturation, brightness.
		 * @param \php\lang\PHPNumber $alpha The alpha channel, from 0 to 127. 127 as completely transparency. Default 0.
		 * @param \php\lang\PHPNumber $hue The hue value, from 0 to 360. Default 0.
		 * @param \php\lang\PHPNumber $saturation The saturation value, from 0 to 100. Default 0.
		 * @param \php\lang\PHPNumber $brightness The brightness value, from 0 to 100. Default 0.
		 * @return \php\awt\Color
		 */
		public static function newInstanceByAHSB(\php\lang\PHPNumber $alpha, \php\lang\PHPNumber $hue, \php\lang\PHPNumber $saturation, \php\lang\PHPNumber $brightness) {
			$_0 = \php\lang\PHPNumber::newInstance(0x00);
			$_1 = \php\lang\PHPNumber::newInstance(1);
			$_100 = \php\lang\PHPNumber::newInstance(100);

			$hue = $hue->modulo(\php\lang\PHPNumber::newInstance(360), new \php\lang\PHPBoolean(true));

			if ($saturation->isNegative()->getBoolean()) {
				$saturation = $_0->copy();
			} else if ($saturation->isGreaterThan($_100)->getBoolean()) {
				$saturation = $_100->copy();
			}

			if ($brightness->isNegative()->getBoolean()) {
				$brightness = $_0->copy();
			} else if ($brightness->isGreaterThan($_100)->getBoolean()) {
				$brightness = $_100->copy();
			}

			$hue = $hue->dividedBy(\php\lang\PHPNumber::newInstance(60));
			$saturation = $saturation->dividedBy($_100);
			$brightness = $brightness->dividedBy($_100)->multiply(\php\lang\PHPNumber::newInstance(0xff));

			$i = $hue->floor();
			$f = $hue->subtraction($i);
			$m = $brightness->multiply($_1->subtraction($saturation));
			$n = $brightness->multiply($_1->subtraction($saturation->multiply($f)));
			$k = $brightness->multiply($_1->subtraction($saturation->multiply($_1->subtraction($f))));

			if ($i->isZero()->getBoolean()) {
				return self::newInstanceByARGB($alpha, $brightness, $k, $m);
			} else if ($i->isEqualTo($_1)->getBoolean()) {
				return self::newInstanceByARGB($alpha, $n, $brightness, $m);
			} else if ($i->isEqualTo(\php\lang\PHPNumber::newInstance(2))->getBoolean()) {
				return self::newInstanceByARGB($alpha, $m, $brightness, $k);
			} else if ($i->isEqualTo(\php\lang\PHPNumber::newInstance(3))->getBoolean()) {
				return self::newInstanceByARGB($alpha, $m, $n, $brightness);
			} else if ($i->isEqualTo(\php\lang\PHPNumber::newInstance(4))->getBoolean()) {
				return self::newInstanceByARGB($alpha, $k, $m, $brightness);
			} else if ($i->bewteen(\php\lang\PHPNumber::newInstance(5), \php\lang\PHPNumber::newInstance(6))->getBoolean()) {
				return self::newInstanceByARGB($alpha, $brightness, $m, $n);
			} else {
				return self::BLACK();
			}
		}

		/**
		 * Returns a \php\awt\Color object by hue, saturation, brightness.
		 * @param \php\lang\PHPNumber $hue The hue value, from 0 to 360. Default 0.
		 * @param \php\lang\PHPNumber $saturation The saturation value, from 0 to 100. Default 0.
		 * @param \php\lang\PHPNumber $brightness The brightness value, from 0 to 100. Default 0.
		 * @return \php\awt\Color
		 */
		public static function newInstanceByHSB(\php\lang\PHPNumber $hue, \php\lang\PHPNumber $saturation, \php\lang\PHPNumber $brightness) {
			return self::newInstanceByAHSB(\php\lang\PHPNumber::newInstance(0x00), $hue, $saturation, $brightness);
		}

		private $value;

		/**
		 * Constructs a \php\awt\Color object.
		 * @param \php\lang\PHPNumber $value
		 */
		protected function __construct(\php\lang\PHPNumber $value) {
			parent::__construct();
			$this->value = $value->getInteger()->getNumber();
		}

		public function equals(\php\lang\PHPObject $object = null) {
			$return = false;
			if ($object instanceof Color) {
				$return = $this->getARGB()->equals($object->getARGB())->getBoolean();
			}
			return \php\lang\PHPBoolean::newInstance($return);
		}

		/**
		 * Returns the RGB value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getARGB() {
			return \php\lang\PHPNumber::newInstance($this->value);
		}

		/**
		 * Returns the alpha value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getAlpha() {
			return $this->getARGB()->shiftRight(\php\lang\PHPNumber::newInstance(0x18)->bitwiseAnd(\php\lang\PHPNumber::newInstance(0x7F)));
		}

		/**
		 * Returns the RGB value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getRGB() {
			return $this->getARGB()->bitwiseAnd(\php\lang\PHPNumber::newInstance(0xffffff));
		}

		/**
		 * Returns the red value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getRed() {
			return $this->getARGB()->shiftRight(\php\lang\PHPNumber::newInstance(0x10))->bitwiseAnd(\php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns the green value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getGreen() {
			return $this->getARGB()->shiftRight(\php\lang\PHPNumber::newInstance(0x08))->bitwiseAnd(\php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns the blue value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getBlue() {
			return $this->getARGB()->bitwiseAnd(\php\lang\PHPNumber::newInstance(0xff));
		}

		/**
		 * Returns the hue value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getHue() {
			$hsb = self::RGBtoHSB($this->getARGB()->getNumber());
			return \php\lang\PHPNumber::newInstance($hsb['H']);
		}

		/**
		 * Returns the saturation value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getSaturation() {
			$hsb = self::RGBtoHSB($this->getARGB()->getNumber());
			return \php\lang\PHPNumber::newInstance($hsb['S']);
		}

		/**
		 * Returns the brightness value of this color.
		 * @return \php\lang\PHPNumber
		 */
		public function getBrightness() {
			$hsb = self::RGBtoHSB($this->getARGB()->getNumber());
			return \php\lang\PHPNumber::newInstance($hsb['B']);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance('alpha = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getAlpha()->toString()));
			$return = $return->append(\php\lang\PHPString::newInstance(', red = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getRed()->toString()));
			$return = $return->append(\php\lang\PHPString::newInstance(', green = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getGreen()->toString()));
			$return = $return->append(\php\lang\PHPString::newInstance(', blue = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getBlue()->toString()));
			$return = $return->append(\php\lang\PHPString::newInstance(', hue = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getHue()->toString()));
			$return = $return->append(\php\lang\PHPString::newInstance(', saturation = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getSaturation()->toString()));
			$return = $return->append(\php\lang\PHPString::newInstance(', brightness = '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->getBrightness()->toString()));
			return $return;
		}

	}

}