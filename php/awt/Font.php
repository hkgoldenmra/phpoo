<?php

/**
 * Description of \php\awt\Font
 */

namespace php\awt {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');

	/**
	 * Represents fonts such as size, name, type
	 */
	class Font extends \php\lang\PHPObject {

		/**
		 * Returns the value of bold type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function BOLD() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the value of italic type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function ITALIC() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\awt\Font object.
		 * @param \php\awt\Size $size the size of font.
		 * @param \php\lang\PHPString $name the name of font.
		 * @param \php\lang\PHPNumber $type use bold or italic of font, can use same time.
		 * @return \php\awt\Font
		 */
		public static function newInstanceByParameters(Size $size, \php\lang\PHPString $name = null, \php\lang\PHPNumber $type = null) {
			return new Font($size, $name, $type);
		}

		private $size;
		private $name;
		private $type;

		/**
		 * Constructs a \php\awt\Font object.
		 * @param \php\awt\Size $size the size of font
		 * @param \php\lang\PHPString $name the name of font
		 * @param \php\lang\PHPNumber $type use bold or italic of font, can use same time
		 */
		protected function __construct(Size $size, \php\lang\PHPString $name = null, \php\lang\PHPNumber $type = null) {
			parent::__construct();
			if ($name === null) {
				$name = \php\lang\PHPString::newInstance('');
			}
			if ($type === null) {
				$type = \php\lang\PHPNumber::ZERO();
			}
			$this->size = $size;
			$this->name = $name->getString();
			$this->type = $type->getNumber();
		}

		/**
		 * Returns the size of font.
		 * @return \php\awt\Size
		 */
		public function getSize() {
			return $this->size;
		}

		/**
		 * Returns the name of font.
		 * @return \php\lang\PHPString
		 */
		public function getName() {
			return \php\lang\PHPString::newInstance($this->name);
		}

		/**
		 * Returns the type of font.
		 * @return \php\lang\PHPNumber
		 */
		public function getType() {
			return \php\lang\PHPNumber::newInstance($this->type);
		}

	}

}