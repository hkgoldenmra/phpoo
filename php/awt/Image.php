<?php

/**
 * Description of \php\awt\Image
 */

namespace php\awt {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/FunctionNotFoundException.php');
	include_once('php/awt/Color.php');
	include_once('php/awt/Point.php');
	include_once('php/io/File.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 *  The class \php\awt\Image that represent graphical images.
	 */
	class Image extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a blank image object with given width and height in pixels.
		 * @param \php\lang\PHPNumber $width The image width in pixels.
		 * @param \php\lang\PHPNumber $height The height width in pixels.
		 * @return \php\awt\Image A blank image.
		 * @throws \php\lang\FunctionNotFoundException
		 */
		public static function newInstanceBySize(\php\lang\PHPNumber $width, \php\lang\PHPNumber $height = null) {
			if (function_exists('imagecreatetruecolor')) {
				if ($height === null) {
					$height = $width->copy();
				}
				return new Image(imagecreatetruecolor($width->getNumber(), $height->getNumber()));
			} else {
				throw new \php\lang\FunctionNotFoundException('gd library not installed');
			}
		}

		/**
		 * Returns an exist image object.
		 * @param \php\io\File $file A valid image file.
		 * @return \php\awt\Image An exist image.
		 * @throws \php\lang\IllegalArgumentException
		 * @throws \php\lang\FunctionNotFoundException
		 */
		public static function newInstanceByFile(\php\io\File $file) {
			if (function_exists('imagecreatetruecolor')) {
				$mimeType = $file->getMimeType();
				$path = $file->getPath()->getString();
				$handle = null;
				if ($mimeType->regexMatch(\php\lang\PHPString::newInstance('/^image\/.*jpeg$/'))->isEmpty()->not()->getBoolean()) {
					$handle = imagecreatefromjpeg($path);
				} else if ($mimeType->regexMatch(\php\lang\PHPString::newInstance('/^image\/.*png/'))->isEmpty()->not()->getBoolean()) {
					$handle = imagecreatefrompng($path);
				} else if ($mimeType->regexMatch(\php\lang\PHPString::newInstance('/^image\/.*gif$/'))->isEmpty()->not()->getBoolean()) {
					$handle = imagecreatefromgif($path);
				} else if ($mimeType->regexMatch(\php\lang\PHPString::newInstance('/^image\/.*bmp/'))->isEmpty()->not()->getBoolean()) {
					$handle = imagecreatefromwbmp($path);
				}
				if ($handle !== null) {
					return new Image($handle);
				} else {
					throw new \php\lang\IllegalArgumentException('mime type not match');
				}
			} else {
				throw new \php\lang\FunctionNotFoundException('gd library not installed');
			}
		}

		private $handle;

		/**
		 * Constructs a \php\awt\Image object.
		 * @param type $handle An image stream.
		 */
		protected function __construct($handle) {
			parent::__construct();
			$transparent = imagecolorallocatealpha($handle, 0xff, 0xff, 0xff, 0x7f);
			imagefill($handle, 0, 0, $transparent);
			imagesavealpha($handle, true);
			imagealphablending($handle, true);
			$this->handle = $handle;
		}

		/**
		 * The width of this image.
		 * @return \php\lang\PHPNumber
		 */
		public function getWidth() {
			return \php\lang\PHPNumber::newInstance(imagesx($this->handle));
		}

		/**
		 * The height of this image.
		 * @return \php\lang\PHPNumber
		 */
		public function getHeight() {
			return \php\lang\PHPNumber::newInstance(imagesy($this->handle));
		}

		/**
		 * Returns the color of the selected coordination.
		 * @param \php\awt\Point $point The coordination.
		 * @return \php\awt\Color
		 */
		public function getColorAt(Point $point) {
			return Color::newInstanceByValue(\php\lang\PHPNumber::newInstance(imagecolorat($this->handle, $point->getX()->getInteger()->getNumber(), $point->getY()->getInteger()->getNumber())));
		}

		/**
		 * A copy of this object.
		 * @return \php\lang\PHPObject
		 */
		public function copy() {
			$_0 = \php\lang\PHPNumber::ZERO();
			$return = self::newInstanceBySize($this->getWidth(), $this->getHeight());
			$return->merge($this, Point::newInstance($_0, $_0));
			return $return;
		}

		/**
		 * Sets the color to the selected coordination.
		 * @param \php\awt\Point $point The coordination.
		 * @param \php\awt\Color $color The color sets to the selected coordination
		 */
		public function setColorAt(Point $point, Color $color) {
			$_1 = \php\lang\PHPNumber::ONE();
			$this->fillRect(Point::newInstance($point->getX(), $point->getY()), $_1, $_1, $color);
		}

		private function toImageColor(Color $color) {
			return imagecolorallocatealpha($this->handle, $color->getRed()->getNumber(), $color->getGreen()->getNumber(), $color->getBlue()->getNumber(), $color->getAlpha()->getNumber());
		}

		/**
		 * Draws a rectangle border.
		 * @param \php\awt\Point $point1 The coordination of first corner.
		 * @param \php\awt\Point $point2 The opposite coordination of corner to the first corner.
		 * @param \php\awt\Color $color The border color of rectangle.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawRectangle(Point $point1, Point $point2, Color $color, \php\lang\PHPNumber $thickness = null) {
			if ($thickness === null) {
				$thickness = \php\lang\PHPNumber::ONE();
			}
			imagesetthickness($this->handle, $thickness->getNumber());
			imagerectangle($this->handle, $point1->getX()->getInteger()->getNumber(), $point1->getY()->getInteger()->getNumber(), $point2->getX()->getInteger()->getNumber(), $point2->getY()->getInteger()->getNumber(), $this->toImageColor($color));
			imagesetthickness($this->handle, 1);
		}

		/**
		 * Fills a rectangle.
		 * @param \php\awt\Point $point1 The coordination of first corner.
		 * @param \php\awt\Point $point2 The opposite coordination of corner to the first corner.
		 * @param \php\awt\Color $color The color of rectangle.
		 */
		public function fillRectangle(Point $point1, Point $point2, Color $color) {
			imagefilledrectangle($this->handle, $point1->getX()->getInteger()->getNumber(), $point1->getY()->getInteger()->getNumber(), $point2->getX()->getInteger()->getNumber(), $point2->getY()->getInteger()->getNumber(), $this->toImageColor($color));
		}

		/**
		 * Draws a ellipse border.
		 * @param \php\awt\Point $point The coordination of centre.
		 * @param \php\lang\PHPNumber $width The width of the ellipse.
		 * @param \php\lang\PHPNumber $height The height of the ellipse.
		 * @param \php\awt\Color $color The color of ellipse.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawEllipse(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color, \php\lang\PHPNumber $thickness = null) {
			if ($thickness === null) {
				$thickness = \php\lang\PHPNumber::ONE();
			}
			// Rejected. Try to improve later.
			//imagesetthickness($this->handle, $thickness->getNumber());
			//imagearc($this->handle, $point->getX()->getNumber(), $point->getY()->getNumber(), $width->getNumber(), $height->getNumber(), 0, 360, $color->getARGB()->getNumber());
			//imagesetthickness($this->handle, 1);
			$_2 = \php\lang\PHPNumber::newInstance(2);
			$centre = Point::newInstance($width->dividedBy($_2), $height->dividedBy($_2));
			$overlay = new Image(imagecreate($width->getNumber(), $height->getNumber()));
			$overlay->fillEllipse($centre, $width, $height, $color);
			$overlay->fillEllipse($centre, $width->subtraction($thickness->multiply($_2)), $height->subtraction($thickness->multiply($_2)), Color::newInstanceByValue(\php\lang\PHPNumber::newInstance(0x7FFFFFFF)));
			$point->setX($point->getX()->subtraction($width->dividedBy($_2)));
			$point->setY($point->getY()->subtraction($height->dividedBy($_2)));
			$this->merge($overlay, $point);
		}

		/**
		 * Fills a ellipse.
		 * @param \php\awt\Point $point The coordination of centre.
		 * @param \php\lang\PHPNumber $width The width of the ellipse.
		 * @param \php\lang\PHPNumber $height The height of the ellipse.
		 * @param \php\awt\Color $color The color of ellipse.
		 */
		public function fillEllipse(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color) {
			imagefilledellipse($this->handle, $point->getX()->getInteger()->getNumber(), $point->getY()->getInteger()->getNumber(), $width->getInteger()->getNumber(), $height->getInteger()->getNumber(), $this->toImageColor($color));
		}

		/**
		 * Draws a rectangle border.
		 * @param \php\awt\Point $point The top-left corner of the rectangle located at this image.
		 * @param \php\lang\PHPNumber $width The width of the rectangle.
		 * @param \php\lang\PHPNumber $height The height of the rectangle.
		 * @param \php\awt\Color $color The color of rectangle.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawRect(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color, \php\lang\PHPNumber $thickness = null) {
			$this->drawRectangle($point, $point->getX()->addition($width), $point->getY()->addition($height), $color, $thickness);
		}

		/**
		 * Fills a rectangle.
		 * @param \php\awt\Point $point The top-left corner of the rectangle located at this image.
		 * @param \php\lang\PHPNumber $width The width of the rectangle.
		 * @param \php\lang\PHPNumber $height The height of the rectangle.
		 * @param \php\awt\Color $color The color of rectangle.
		 */
		public function fillRect(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color) {
			$this->fillRectangle($point, Point::newInstance($point->getX()->addition($width), $point->getY()->addition($height)), $color);
		}

		/**
		 * Draws a ellipse border.
		 * @param \php\awt\Point $point The top-left corner of the ellipse located at this image.
		 * @param \php\lang\PHPNumber $width The width of the ellipse.
		 * @param \php\lang\PHPNumber $height The height of the ellipse.
		 * @param \php\awt\Color $color The color of ellipse.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawOval(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color, \php\lang\PHPNumber $thickness = null) {
			$_2 = \php\lang\PHPNumber::newInstance(2);
			$this->drawEllipse(Point::newInstance($point->getX()->addition($width->dividedBy($_2)), $point->getY()->addition($height->dividedBy($_2))), $width, $height, $color, $thickness);
		}

		/**
		 * Fills a ellipse.
		 * @param \php\awt\Point $point The top-left corner of the ellipse located at this image.
		 * @param \php\lang\PHPNumber $width The width of the ellipse.
		 * @param \php\lang\PHPNumber $height The height of the ellipse.
		 * @param \php\awt\Color $color The color of ellipse.
		 */
		public function fillOval(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color) {
			$_2 = \php\lang\PHPNumber::newInstance(2);
			$this->fillEllipse(Point::newInstance($point->getX()->addition($width->dividedBy($_2)), $point->getY()->addition($height->dividedBy($_2))), $width, $height, $color);
		}

		/**
		 * Draws a line between two points.
		 * @param \php\awt\Point $point1 The 1st point of line.
		 * @param \php\awt\Point $point2 The 2nd point of line.
		 * @param \php\awt\Color $color The color of line.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawLine(Point $point1, Point $point2, Color $color, \php\lang\PHPNumber $thickness = null) {
			if ($thickness === null) {
				$thickness = \php\lang\PHPNumber::ONE();
			}
			imagesetthickness($this->handle, $thickness->getNumber());
			imageline($this->handle, $point1->getX()->getInteger()->getNumber(), $point1->getY()->getInteger()->getNumber(), $point2->getX()->getInteger()->getNumber(), $point2->getY()->getInteger()->getNumber(), $this->toImageColor($color));
			imagesetthickness($this->handle, 1);
		}

		/**
		 * Draws a unclosed polygon border.
		 * @param \php\util\collections\LinkedList $points A list of points.
		 * @param \php\awt\Color $color The color of polyline.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawPolyline(\php\util\collections\LinkedList $points, Color $color, \php\lang\PHPNumber $thickness = null) {
			for ($i = \php\lang\PHPNumber::ZERO(); $points->isEmpty()->not()->getBoolean(); $i = $i->increase()) {
				$point1 = $points->getFirst();
				$points->deleteFirst();
				if ($points->isEmpty()->not()->getBoolean()) {
					$point2 = $points->getFirst();
					$this->drawLine($point1, $point2, $color, $thickness);
				} else {
					$this->drawLine($point1, $point1, $color, $thickness);
				}
			}
		}

		/**
		 * Draws a polygon border.
		 * @param \php\util\collections\LinkedList $points A list of points.
		 * @param \php\awt\Color $color The color of polygon.
		 * @param \php\lang\PHPNumber $thickness The thickness of the border. Default 1.
		 */
		public function drawPolygon(\php\util\collections\LinkedList $points, Color $color, \php\lang\PHPNumber $thickness = null) {
			if ($points->size()->isGreaterThan(\php\lang\PHPNumber::newInstance(2))->getBoolean()) {
				$array = array();
				for ($i = \php\lang\PHPNumber::ZERO(); $points->isEmpty()->not()->getBoolean(); $i = $i->increase()) {
					$point = $points->getFirst();
					$points->deleteFirst();
					array_push($array, $point->getX()->getInteger()->getNumber());
					array_push($array, $point->getY()->getInteger()->getNumber());
				}
				if ($thickness === null) {
					$thickness = \php\lang\PHPNumber::ONE();
				}
				imagesetthickness($this->handle, $thickness->getNumber());
				imagepolygon($this->handle, $array, count($array) / 2, $this->toImageColor($color));
				imagesetthickness($this->handle, 1);
			} else {
				$this->drawPolyline($points, $color, $thickness);
			}
		}

		/**
		 * Fills a polygon.
		 * @param \php\util\collections\LinkedList $points A list of points.
		 * @param \php\awt\Color $color The color of polygon.
		 */
		public function fillPolygon(\php\util\collections\LinkedList $points, Color $color) {
			if ($points->size()->isGreaterThan(\php\lang\PHPNumber::newInstance(2))->getBoolean()) {
				$array = array();
				for ($i = \php\lang\PHPNumber::ZERO(); $points->isEmpty()->not()->getBoolean(); $i = $i->increase()) {
					$point = $points->getFirst();
					$points->deleteFirst();
					array_push($array, $point->getX()->getInteger()->getNumber());
					array_push($array, $point->getY()->getInteger()->getNumber());
				}
				imagefilledpolygon($this->handle, $array, count($array) / 2, $this->toImageColor($color));
			} else {
				$this->drawPolyline($points, $color);
			}
		}

		/**
		 * Convert this image to negative.
		 */
		public function negative() {
			imagefilter($this->handle, IMG_FILTER_NEGATE);
		}

		/**
		 * Converts this image to grayscale.
		 */
		public function grayscale() {
			imagefilter($this->handle, IMG_FILTER_GRAYSCALE);
		}

		/**
		 * Converts this image to red channel.
		 */
		public function redChannel() {
			imagefilter($this->handle, IMG_FILTER_COLORIZE, 0x00, -0xff, -0xff);
		}

		/**
		 * Converts this image to green channel.
		 */
		public function greenChannel() {
			imagefilter($this->handle, IMG_FILTER_COLORIZE, -0xff, 0x00, -0xff);
		}

		/**
		 * Converts this image to green channel.
		 */
		public function blueChannel() {
			imagefilter($this->handle, IMG_FILTER_COLORIZE, -0xf, -0xff, 0x00);
		}

		/**
		 * Converts this image to cyan channel.
		 */
		public function cyanChannel() {
			imagefilter($this->handle, IMG_FILTER_COLORIZE, -0xff, 0x00, 0x00);
		}

		/**
		 * Converts this image to magenta channel.
		 */
		public function magentaChannel() {
			imagefilter($this->handle, IMG_FILTER_COLORIZE, 0x00, -0xff, 0x00);
		}

		/**
		 * Converts this image to yellow channel.
		 */
		public function yellowChannel() {
			imagefilter($this->handle, IMG_FILTER_COLORIZE, 0x00, 0x00, -0xff);
		}

		/**
		 * Rotates this image with a given angle.
		 * @param \php\lang\PHPNumber $angle The rotation angle to the image anticlockwisely.
		 * @param \php\awt\Color $color The color of the uncovered zone after the rotation. Default &lt;Color::TRANSPARENT()&gt;
		 */
		public function rotate(\php\lang\PHPNumber $angle, Color $color = null) {
			if ($color === null) {
				$color = Color::TRANSPARENT();
			}
			if (($handle = imagerotate($this->handle, $angle->getNumber(), $this->toImageColor($color))) !== false) {
				$this->handle = $handle;
			}
		}

		private function export($mimeType, $extension, \php\lang\PHPString $filename = null) {
			header(sprintf('Content-Type: %s; charset=UTF-8', $mimeType));
			if ($filename !== null) {
				$extension = \php\lang\PHPString::newInstance('.' . $extension);
				if ($filename->endsWith($extension)->not()->getBoolean()) {
					$filename = $filename->append($extension);
					header(sprintf('Content-Disposition: attachment; filename="%s"', $filename->getString()));
				}
			}
		}

		/**
		 * Exports this image as PNG. Default direct output on web page.
		 * @param \php\lang\PHPString $filename The exported filename. Default &lt;null&gt; as direct output on web page.
		 */
		public function exportPNG(\php\lang\PHPString $filename = null) {
			$this->export('image/png', 'png', $filename);
			imagepng($this->handle);
			imagedestroy($this->handle);
		}

		/**
		 * Exports this image as JPEG. Default direct output on web page.
		 * @param \php\lang\PHPString $filename The exported filename. Default &lt;null&gt; as direct output on web page.
		 */
		public function exportJPEG(\php\lang\PHPString $filename = null) {
			$this->export('image/jpeg', 'jpg', $filename);
			imagejpeg($this->handle);
			imagedestroy($this->handle);
		}

		private function toBase64String($contents) {
			ob_end_clean();
			return \php\lang\PHPString::newInstance(base64_encode($contents));
		}

		/**
		 * Returns a PNG base64 string of this image.
		 * @return \php\lang\PHPString
		 */
		public function toPNGBase64String() {
			ob_start();
			imagepng($this->handle);
			return $this->toBase64String(ob_get_contents());
		}

		/**
		 * Returns a JPEG base64 string of this image.
		 * @return \php\lang\PHPString
		 */
		public function toJPEGBase64String() {
			ob_start();
			imagejpeg($this->handle);
			return $this->toBase64String(ob_get_contents());
		}

		/**
		 * Resizes this image to new width and height.
		 * @param \php\lang\PHPNumber $width The width of resized image.
		 * @param \php\lang\PHPNumber $height The height of resized image.
		 */
		public function resize(\php\lang\PHPNumber $width, \php\lang\PHPNumber $height) {
			$image = Image::newInstanceBySize($width, $height);
			imagecopyresized($image->handle, $this->handle, 0, 0, 0, 0, $image->getWidth()->getNumber(), $image->getHeight()->getNumber(), $this->getWidth()->getNumber(), $this->getHeight()->getNumber());
			$this->handle = $image->handle;
		}

		/**
		 * Merges a image on the original image.
		 * @param \php\awt\Image $image A overlay image.
		 * @param \php\awt\Point $point The top-left corner of the overlay image located at the original image.
		 */
		public function merge(Image $image, Point $point) {
			imagecopyresized($this->handle, $image->handle, $point->getX()->getInteger()->getNumber(), $point->getY()->getInteger()->getNumber(), 0, 0, $image->getWidth()->getNumber(), $image->getHeight()->getNumber(), $image->getWidth()->getNumber(), $image->getHeight()->getNumber());
		}

		/**
		 * Pads an extra border to this image.
		 * @param \php\lang\PHPNumber $top The size of top border.
		 * @param \php\lang\PHPNumber $left The size of left border.
		 * @param \php\lang\PHPNumber $right The size of right border.
		 * @param \php\lang\PHPNumber $bottom The size of bottom border.
		 * @param \php\awt\Color $color The padded color.
		 */
		public function pad(\php\lang\PHPNumber $top, \php\lang\PHPNumber $left, \php\lang\PHPNumber $right, \php\lang\PHPNumber $bottom, Color $color) {
			$width = $this->getWidth()->addition($left)->addition($right);
			$height = $this->getHeight()->addition($top)->addition($bottom);
			$image = Image::newInstanceBySize($width, $height);
			$_0 = \php\lang\PHPNumber::ZERO();
			$image->fillRect(Point::newInstance($_0, $_0), $width, $height, $color);
			$image->merge($this, Point::newInstance($left, $top));
			$this->handle = $image->handle;
		}

		/**
		 * Trims the edge of this image.
		 * @param \php\lang\PHPNumber $top The size of top edge.
		 * @param \php\lang\PHPNumber $left The size of left edge.
		 * @param \php\lang\PHPNumber $right The size of right edge.
		 * @param \php\lang\PHPNumber $bottom The size of bottom edge.
		 */
		public function trim(\php\lang\PHPNumber $top, \php\lang\PHPNumber $left, \php\lang\PHPNumber $right, \php\lang\PHPNumber $bottom) {
			$this->pad($top->negative(), $left->negative(), $right->negative(), $bottom->negative(), Color::newInstanceByValue(\php\lang\PHPNumber::newInstance(0x7FFFFFFF)));
		}

		/**
		 * Crops the selected area from this image.
		 * @param \php\awt\Point $point The top-left corner of the overlay image located at the original image.
		 * @param \php\lang\PHPNumber $width The width of croped image.
		 * @param \php\lang\PHPNumber $height The height of croped image.
		 * @param \php\awt\Color $color The color if the croped area is out of the original image.
		 */
		public function crop(Point $point, \php\lang\PHPNumber $width, \php\lang\PHPNumber $height, Color $color) {
			$return = Image::newInstanceBySize($width, $height);
			$_0 = \php\lang\PHPNumber::ZERO();
			$return->fillRect($_0, $_0, $width, $height, $color);
			imagecopyresized($return->handle, $this->handle, 0, 0, $point->getX()->getInteger()->getNumber(), $point->getY()->getInteger()->getNumber(), $return->getWidth()->getNumber(), $return->getHeight()->getNumber(), $return->getWidth()->getNumber(), $return->getHeight()->getNumber());
			$this->handle = $return->handle;
		}

		/**
		 * Flips this image horizontally.
		 */
		public function flipHorizontal() {
			imageflip($this->handle, IMG_FLIP_HORIZONTAL);
		}

		/**
		 * Flips this image vertically.
		 */
		public function flipVertical() {
			imageflip($this->handle, IMG_FLIP_VERTICAL);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance('width = '));
			$return = $return->append($this->getWidth()->toString());
			$return = $return->append(\php\lang\PHPString::newInstance(', height = '));
			$return = $return->append($this->getHeight()->toString());
			return $return;
		}

	}

}
