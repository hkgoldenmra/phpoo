<?php

/**
 * Description of \php\awt\Point
 */

namespace php\awt {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');

	class Point extends \php\lang\PHPObject {

		/**
		 * Returns a \php\awt\Point object.
		 * @param \php\lang\PHPNumber $x X-axis. Default 0.
		 * @param \php\lang\PHPNumber $y Y-axis. Default 0.
		 * @return \php\awt\Point
		 */
		public static function newInstance(\php\lang\PHPNumber $x = null, \php\lang\PHPNumber $y = null) {
			return new Point($x, $y);
		}

		private $x;
		private $y;

		/**
		 * Constructs a \php\awt\Point object.
		 * @param \php\lang\PHPNumber $x X-axis. Default 0.
		 * @param \php\lang\PHPNumber $y Y-axis. Default 0.
		 */
		protected function __construct(\php\lang\PHPNumber $x = null, \php\lang\PHPNumber $y = null) {
			parent::__construct();
			if ($x === null) {
				$x = \php\lang\PHPNumber::ZERO();
			}
			if ($y === null) {
				$y = \php\lang\PHPNumber::ZERO();
			}
			$this->setXY($x, $y);
		}

		/**
		 * Sets the X-axis.
		 * @param \php\lang\PHPNumber $x X-axis.
		 */
		public function setX(\php\lang\PHPNumber $x) {
			$this->x = $x->getNumber();
		}

		/**
		 * Sets the Y-axis.
		 * @param \php\lang\PHPNumber $y Y-axis.
		 */
		public function setY(\php\lang\PHPNumber $y) {
			$this->y = $y->getNumber();
		}

		/**
		 * Sets the X-axis and Y-axis.
		 * @param \php\lang\PHPNumber $x X-axis.
		 * @param \php\lang\PHPNumber $y Y-axis.
		 */
		public function setXY(\php\lang\PHPNumber $x, \php\lang\PHPNumber $y) {
			$this->setX($x);
			$this->setY($y);
		}

		/**
		 * Returns the X-axis.
		 * @return \php\lang\PHPNumber
		 */
		public function getX() {
			return \php\lang\PHPNumber::newInstance($this->x);
		}

		/**
		 * Returns the Y-axis.
		 * @return \php\lang\PHPNumber
		 */
		public function getY() {
			return \php\lang\PHPNumber::newInstance($this->y);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance('x = ' . $this->getX()->getNumber()));
			$return = $return->append(\php\lang\PHPString::newInstance(', y = ' . $this->getY()->getNumber()));
			return $return;
		}

	}

}