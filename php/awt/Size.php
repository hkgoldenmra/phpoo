<?php

/**
 * Description of \php\awt\Size
 */

namespace php\awt {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');

	/**
	 * \php\awt\Size class represents the size and unit data.
	 */
	class Size extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\awt\Size object.
		 * @param \php\lang\PHPNumber $size The size data.
		 * @param \php\lang\PHPString $unit The unit data.
		 * @return \php\awt\Size
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $size, \php\lang\PHPString $unit = null) {
			return new Size($size, $unit);
		}

		private $size;
		private $unit;

		/**
		 * Constructs a \php\awt\Size object.
		 * @param \php\lang\PHPNumber $size The size data.
		 * @param \php\lang\PHPString $unit The unit data.
		 */
		protected function __construct(\php\lang\PHPNumber $size, \php\lang\PHPString $unit = null) {
			parent::__construct();
			if ($unit === null) {
				$unit = \php\lang\PHPString::newInstance('');
			}
			$this->size = $size->getNumber();
			$this->unit = $unit->getString();
		}

		/**
		 * Returns the size.
		 * @return \php\lang\PHPNumber
		 */
		public function getSize() {
			return \php\lang\PHPNumber::newInstance($this->size);
		}

		/**
		 * Returns the unit.
		 * @return \php\lang\PHPString
		 */
		public function getUnit() {
			return \php\lang\PHPString::newInstance($this->unit);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append($this->getSize()->toString());
			$return = $return->append($this->getUnit()->toString());
			return $return;
		}

	}

}