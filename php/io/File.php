<?php

/**
 * Description of \php\io\File
 */

namespace php\io {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/io/FileNotFoundException.php');
	include_once('php/io/IOException.php');
	include_once('php/io/filefilters/FileFilter.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 * An abstract representation of file and directory path.
	 */
	class File extends \php\lang\PHPObject {

		/**
		 * The system-dependent directory-separator character.
		 * @return \php\lang\PHPCharacter
		 */
		public static final function DIRECTORY_SEPARATOR() {
			return \php\lang\PHPCharacter::newInstanceByCharacter(DIRECTORY_SEPARATOR);
		}

		/**
		 * The system-dependent path-separator character.
		 * @return \php\lang\PHPCharacter
		 */
		public static final function PATH_SEPARATOR() {
			return \php\lang\PHPCharacter::newInstanceByCharacter(PATH_SEPARATOR);
		}

		/**
		 * The total space of the root drive.
		 * @param \php\lang\PHPString $root The root path of a file system. Default &lt;install root path&gt;.
		 * @return \php\lang\PHPNumber
		 */
		public static function getTotalSpace(\php\lang\PHPString $root = null) {
			if ($root === null) {
				$root = \php\lang\PHPString::newInstance('.');
			}
			return \php\lang\PHPNumber::newInstance(disk_total_space($root->getString()));
		}

		/**
		 * The free space of the root drive.
		 * @param \php\lang\PHPString $root The root path of a file system. Default &lt;install root path&gt;.
		 * @return \php\lang\PHPNumber
		 */
		public static function getFreeSpace(\php\lang\PHPString $root = null) {
			if ($root === null) {
				$root = \php\lang\PHPString::newInstance('.');
			}
			return \php\lang\PHPNumber::newInstance(disk_free_space($root->getString()));
		}

		/**
		 * The used space of the root drive.
		 * @param \php\lang\PHPString $root The root path of a file system. Default &lt;install root path&gt;.
		 * @return \php\lang\PHPNumber
		 */
		public static function getUsedSpace(\php\lang\PHPString $root = null) {
			return self::getTotalSpace($root)->subtraction(File::getFreeSpace($root));
		}

		/**
		 * Lists drivers in local server.
		 * @return \php\util\collections\Vector
		 */
		public static function listDrives() {
			$return = \php\util\collections\Vector::newInstance();
			if (DIRECTORY_SEPARATOR == '/') {
				$return->add(File::newInstance(\php\lang\PHPString::newInstance('/')));
			} else {
				$fso = new COM('Scripting.FileSystemObject');
				foreach ($fso->Drives as $drive) {
					$return->add(File::newInstance(\php\lang\PHPString::newInstance($fso->GetDrive($drive)->DriveLetter)));
				}
			}
			return $return;
		}

		/**
		 * Returns an instance of \php\io\File object.
		 * @param \php\lang\PHPString $path The file path.
		 * @return \php\io\File
		 */
		public static function newInstance(\php\lang\PHPString $path = null) {
			return new File($path);
		}

		private $path;

		/**
		 * Constructs a \php\io\File object.
		 * @param \php\lang\PHPString $path The file path
		 */
		protected function __construct(\php\lang\PHPString $path = null) {
			parent::__construct();
			$path = $path->getString();
			$path = str_replace('\\', '/', $path);
			$path = preg_replace('/\/+/', '/', $path);
			$fullpath = realpath($path);
			if ($fullpath === false) {
				if (preg_match('/^([A-Z]+:)?\//', $path) > 0) {
					echo 1;
					$fullpath = $path;
				} else {
					echo 2;
					$fullpath = realpath('.') . DIRECTORY_SEPARATOR . $path;
				}
			} else if (is_link($path)) {
				$fullpath = $path;
			}
			if (is_dir($fullpath)) {
				$fullpath .= '/';
			}
			$fullpath = preg_replace('/\/+/', '/', $fullpath);
			$this->path = str_replace('/', DIRECTORY_SEPARATOR, $fullpath);
		}

		/**
		 * Tests two object of \php\io\File is the same or not.
		 * @param \php\lang\PHPObject $object The test object.
		 * @return type
		 */
		public function equals(\php\lang\PHPObject $object = null) {
			$return = false;
			if ($object instanceof File) {
				$return = $this->getPath()->equals($object->getPath())->getBoolean();
			}
			return PHPBoolean::newInstance($return);
		}

		/**
		 * The file path.
		 * @return \php\lang\PHPString
		 */
		public function getPath() {
			return \php\lang\PHPString::newInstance($this->path);
		}

		/**
		 * Checks the file exist or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function exist() {
			return \php\lang\PHPBoolean::newInstance(file_exists($this->getPath()->getString()));
		}

		private function throwFileNotFoundException() {
			if ($this->exist()->not()->getBoolean()) {
				throw new FileNotFoundException(sprintf('File "%s" is not exist.', $this->getPath()->getString()));
			}
		}

		/**
		 * Creates a directory.
		 * @throws \php\io\IOException
		 */
		public function mkdir() {
			if ($this->exist()->getBoolean()) {
				throw new IOException('Directory exist');
			} else {
				mkdir($this->getPath()->getString());
			}
		}

		/**
		 * Deletes a file or directory
		 * @throws \php\io\FileNotFoundException
		 */
		public function delete() {
			try {
				$this->throwFileNotFoundException();
				unlink($this->getPath()->getString());
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * The file size.
		 * @return \php\lang\PHPNumber
		 * @throws FileNotFoundException
		 */
		public function length() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPNumber::newInstance(filesize($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is root or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isRoot() {
			return \php\lang\PHPBoolean::newInstance(strlen(basename($this->getPath()->getString())) == 0);
		}

		/**
		 * The name of this file.
		 * @return \php\lang\PHPString
		 */
		public function getName() {
			if ($this->isRoot()->getBoolean()) {
				return $this->getPath();
			} else {
				$path = $this->getPath();
				$dirname = \php\lang\PHPString::newInstance(dirname($path->getString()));
				return $path->substring($dirname->length()->increase());
			}
		}

		/**
		 * The extension of this file.
		 * @return \php\lang\PHPString
		 */
		public function getExtension() {
			if ($this->isDirectory()->getBoolean()) {
				return null;
			} else {
				$name = $this->getName();
				$dot = \php\lang\PHPString::newInstance('.');
				if ($name->contains($dot)->getBoolean()) {
					$index = $name->lastIndexOf($dot);
					return $name->substr($index->addition($dot->length()));
				} else {
					return null;
				}
			}
		}

		/**
		 * The name of this file without extension.
		 * @return \php\lang\PHPString
		 */
		public function getNameWithoutExtension() {
			$name = $this->getName();
			if ($this->isDirectory()->getBoolean()) {
				return $name;
			} else {
				$dot = \php\lang\PHPString::newInstance('.');
				if ($name->contains($dot)->getBoolean()) {
					$index = $name->lastIndexOf($dot);
					return $name->substr(\php\lang\PHPNumber::ZERO(), $index);
				} else {
					return $name;
				}
			}
		}

		/**
		 * The MIME content type for a file as determined.
		 * @return \php\lang\PHPString
		 * @throws \php\io\FileNotFoundException
		 */
		public function getMimeType() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPString::newInstance(mime_content_type($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * The parent directory.
		 * @return \php\io\File
		 */
		public function getParent() {
			if ($this->isRoot()->getBoolean()) {
				return null;
			} else {
				return File::newInstance(\php\lang\PHPString::newInstance(dirname($this->getPath()->getString())));
			}
		}

		/**
		 * Checks the file is link or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isLink() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPBoolean::newInstance(is_link($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * The link source of this path.
		 * @return \php\io\File
		 * @throws \php\io\FileNotFoundException
		 */
		public function getLinkSource() {
			try {
				if ($this->isLink()->getBoolean()) {
					return File::newInstance(\php\lang\PHPString::newInstance(realpath($this->getPath()->getString())));
				} else {
					return null;
				}
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		private function isFileType($value) {
			try {
				$this->throwFileNotFoundException();
				$perms = fileperms($this->getPath()->getString());
				return \php\lang\PHPBoolean::newInstance(($perms & 0xf000) == $value);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is socket or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isSocket() {
			try {
				return $this->isFileType(0xc000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is block or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isBlock() {
			try {
				return $this->isFileType(0x6000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is character or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isCharacter() {
			try {
				return $this->isFileType(0x2000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is pipe or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isPipe() {
			try {
				return $this->isFileType(0x1000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is directory or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isDirectory() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPBoolean::newInstance(is_dir($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is regular file or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isFile() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPBoolean::newInstance(is_file($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		private function isFilePermission($value) {
			try {
				$this->throwFileNotFoundException();
				$perms = fileperms($this->getPath()->getString());
				return \php\lang\PHPBoolean::newInstance($this->isLink()->getBoolean() || ($perms & $value) > 0);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		private function isFileSticky($value) {
			try {
				$this->throwFileNotFoundException();
				$perms = fileperms($this->getPath()->getString());
				return \php\lang\PHPBoolean::newInstance(!$this->isLink()->getBoolean() && ($perms & $value) > 0);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is owner readable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOwnerReadable() {
			try {
				return $this->isFilePermission(00400);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is owner writable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOwnerWritable() {
			try {
				return $this->isFilePermission(00200);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is owner executable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOwnerExecutable() {
			try {
				return $this->isFilePermission(00100);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is owner sticky or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOwnerSticky() {
			try {
				return $this->isFileSticky(04000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is group readable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isGroupReadable() {
			try {
				return $this->isFilePermission(00040);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is group writable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isGroupWritable() {
			try {
				return $this->isFilePermission(00020);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is group executable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isGroupExecutable() {
			try {
				return $this->isFilePermission(00010);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is group sticky or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isGroupSticky() {
			try {
				return $this->isFileSticky(01000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is other readable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOtherReadable() {
			try {
				return $this->isFilePermission(00004);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is other writable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOtherWritable() {
			try {
				return $this->isFilePermission(00002);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is other executable or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOtherExecutable() {
			try {
				return $this->isFilePermission(00001);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Checks the file is other sticky or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\io\FileNotFoundException
		 */
		public function isOtherSticky() {
			try {
				return $this->isFileSticky(01000);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns the owner id of this file.
		 * @return \php\lang\PHPNumber
		 * @throws \php\io\FileNotFoundException
		 */
		public function getOwnerId() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPNumber::newInstance(fileowner($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns the owner name of this file.
		 * @return \php\lang\PHPString
		 * @throws \php\io\FileNotFoundException
		 */
		public function getOwnerName() {
			try {
				$fields = posix_getpwuid($this->getOwnerId()->getNumber());
				return \php\lang\PHPString::newInstance($fields['name']);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns the group id of this file.
		 * @return \php\lang\PHPNumber
		 * @throws \php\io\FileNotFoundException
		 */
		public function getGroupId() {
			try {
				$this->throwFileNotFoundException();
				return \php\lang\PHPNumber::newInstance(filegroup($this->getPath()->getString()));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns the group name of this file.
		 * @return \php\lang\PHPString
		 * @throws \php\io\FileNotFoundException
		 */
		public function getGroupName() {
			try {
				$fields = posix_getgrgid($this->getOwnerId()->getNumber());
				return \php\lang\PHPString::newInstance($fields['name']);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * The modified time of this file.
		 * @return \php\util\Calendar
		 */
		public function lastModified() {
			try {
				$this->throwFileNotFoundException();
				return \php\util\Calendar::newInstanceByTimestamp(\php\lang\PHPNumber::newInstance(filemtime($this->getPath()->getString())));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * The accessed time of this file.
		 * @return \php\util\Calendar
		 */
		public function lastAccessed() {
			try {
				$this->throwFileNotFoundException();
				return \php\util\Calendar::newInstanceByTimestamp(\php\lang\PHPNumber::newInstance(fileatime($this->getPath()->getString())));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * The INode changed of this file.
		 * @return \php\util\Calendar
		 */
		public function iNodeCreated() {
			try {
				$this->throwFileNotFoundException();
				return \php\util\Calendar::newInstanceByTimestamp(\php\lang\PHPNumber::newInstance(filectime($this->getPath()->getString())));
			} catch (FileNotFoundException $ex) {
				throw $ex;
			}
		}

		/**
		 * returns the array contains files or directories
		 * @param \php\io\filefilters\FileFilter $filter the file filter
		 * @return \php\util\collections\LinkedList
		 */
		public function listFiles(filefilters\FileFilter $filter = null) {
			$return = \php\util\collections\LinkedList::newInstance();
			if ($this->isDirectory()->getBoolean()) {
				$directories = array();
				$files = array();
				if (($handle = @opendir($this->getPath()->getString())) !== false) {
					while (($name = readdir($handle)) !== false) {
						if ($name != '.' && $name != '..') {
							$path = realpath($this->getPath()->getString() . DIRECTORY_SEPARATOR . $name);
							$file = File::newInstance(\php\lang\PHPString::newInstance($path));
							if ($filter === null || $filter->accept($file)->getBoolean()) {
								if (is_dir($path)) {
									array_push($directories, $path);
								} else {
									array_push($files, $path);
								}
							}
						}
					}
					sort($directories);
					sort($files);
					foreach (array_merge($directories, $files) as $v) {
						$return->addLast(File::newInstance(\php\lang\PHPString::newInstance($v)));
					}
					closedir($handle);
				}
			}
			return $return;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$source = \php\lang\PHPString::newInstance('');
			if ($this->isLink()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('l'));
				$source = $source->append(\php\lang\PHPString::newInstance(' -> '));
				$source = $source->append($this->getLinkSource()->getPath());
			} else if ($this->isSocket()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('s'));
			} else if ($this->isBlock()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('b'));
			} else if ($this->isCharacter()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('c'));
			} else if ($this->isPipe()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('p'));
			} else if ($this->isDirectory()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('d'));
			} else if ($this->isFile()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('-'));
			} else {
				$return = $return->append(\php\lang\PHPString::newInstance('u'));
			}
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isOwnerReadable()->getBoolean()) ? 'r' : '-'));
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isOwnerWritable()->getBoolean()) ? 'w' : '-'));
			$sticky = $this->isOwnerSticky()->getBoolean();
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isOwnerExecutable()->getBoolean()) ? (($sticky) ? 's' : 'x') : (($sticky) ? 'S' : '-')));
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isGroupReadable()->getBoolean()) ? 'r' : '-'));
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isGroupWritable()->getBoolean()) ? 'w' : '-'));
			$sticky = $this->isGroupSticky()->getBoolean();
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isGroupExecutable()->getBoolean()) ? (($sticky) ? 's' : 'x') : (($sticky) ? 'S' : '-')));
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isOtherReadable()->getBoolean()) ? 'r' : '-'));
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isOtherWritable()->getBoolean()) ? 'w' : '-'));
			$sticky = $this->isOtherSticky()->getBoolean();
			$return = $return->append(\php\lang\PHPString::newInstance(($this->isOtherExecutable()->getBoolean()) ? (($sticky) ? 's' : 'x') : (($sticky) ? 'S' : '-')));
			$return = $return->append(\php\lang\PHPString::newInstance("\t"));
			$return = $return->append(\php\lang\PHPNumber::newInstance(count($this->listFiles()))->toString());
			$return = $return->append(\php\lang\PHPString::newInstance("\t"));
			$return = $return->append($this->getOwnerName());
			$return = $return->append(\php\lang\PHPString::newInstance("\t"));
			$return = $return->append($this->getGroupName());
			$return = $return->append(\php\lang\PHPString::newInstance("\t"));
			$lastModified = $this->lastModified();
			$return = $return->append($lastModified->toDateString());
			$return = $return->append(\php\lang\PHPString::newInstance('-'));
			$return = $return->append($lastModified->toTimeString());
			$return = $return->append(\php\lang\PHPString::newInstance("\t"));
			$return = $return->append($this->getPath());
			if ($this->isDirectory()->_and($this->isRoot()->not())->getBoolean()) {
				$return = $return->append(File::DIRECTORY_SEPARATOR());
			}
			$return = $return->append($source);
			return $return;
		}

	}

}