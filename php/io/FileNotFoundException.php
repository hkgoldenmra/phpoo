<?php

/**
 * Description of \php\io\FileNotFoundException
 */

namespace php\io {

	/**
	 * Signals that an attempt to open the file denoted by a specified path has failed.
	 */
	class FileNotFoundException extends \Exception {

		/**
		 * Constructs a \php\io\FileNotFoundException object.
		 * @param \php\lang\PHPString $message An error message
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}