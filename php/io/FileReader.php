<?php

/**
 * Description of \php\io\FileReader
 */

namespace php\io {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/io/File.php');
	include_once('php/io/FileNotFoundException.php');
	include_once('php/io/IOException.php');

	/**
	 * Convenience class for reading character files.
	 */
	class FileReader extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\io\FileReader object.
		 * @param \php\io\File $file The File to read from.
		 * @return \php\io\FileReader
		 * @throws \php\io\FileNotFoundException
		 * @throws \php\io\IOException
		 */
		public static function newInstanceByFile(File $file) {
			try {
				return new FileReader($file);
			} catch (FileNotFoundException $ex) {
				throw $ex;
			} catch (IOException $ex) {
				throw $ex;
			}
		}

		private $handle;

		/**
		 * Constructs a \php\io\FileReader object.
		 * @param \php\io\File $file The File to read from.
		 * @throws \php\io\FileNotFoundException
		 * @throws \php\io\IOException
		 */
		protected function __construct(File $file) {
			parent::__construct();
			if ($file->exist()->getBoolean()) {
				$this->handle = @fopen($file->getPath()->getString(), 'r');
				if ($this->handle === false) {
					$error = error_get_last();
					throw new IOException($error['message']);
				}
			} else {
				throw new FileNotFoundException(sprintf('File "%s" is not exist.', $file->getPath()->getString()));
			}
		}

		/**
		 * Reads a single character.
		 * @return \php\lang\Character
		 * @throws \php\io\IOException
		 */
		public function readChar() {
			if (@feof($this->handle) === false) {
				$return = @fgetc($this->handle);
				return \php\lang\PHPCharacter::newInstanceByCharacter($return);
			} else {
				return null;
			}
		}

		/**
		 * Reads up to $length of data from this reader.
		 * @param \php\lang\PHPNumber $length The length of data to read.
		 * @return \php\lang\PHPString
		 * @throws \php\io\IOException
		 */
		public function readData(\php\lang\PHPNumber $length) {
			if (@feof($this->handle) === false) {
				$return = @fgets($this->handle, $length->getNumber());
				return \php\lang\PHPString::newInstance($return);
			} else {
				return null;
			}
		}

		/**
		 * Reads a line of text.
		 * @return \php\lang\PHPString
		 */
		public function readLine() {
			if (@feof($this->handle) === false) {
				$return = fgets($this->handle);
				return \php\lang\PHPString::newInstance(str_replace("\n", '', $return));
			} else {
				return null;
			}
		}

		/**
		 * Resets the stream.
		 * @throws \php\io\IOException
		 */
		public function reset() {
			if (@rewind($this->handle) === false) {
				$error = error_get_last();
				throw new IOException($error['message']);
			}
		}

		/**
		 * Closes the stream and releases any system resources associated with it.
		 * @throws \php\io\IOException
		 */
		public function close() {
			if (@fclose($this->handle) === false) {
				$error = error_get_last();
				throw new IOException($error['message']);
			}
		}

	}

}