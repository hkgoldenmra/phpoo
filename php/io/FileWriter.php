<?php

/**
 * Description of \php\io\FileWriter
 */

namespace php\io {
	include_once('php/lang/PHPObject.php');
	include_once('php/io/File.php');

	/**
	 * A class for writing to object streams.
	 */
	class FileWriter extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		public static function newInstanceByFile(File $file) {
			return new FileWriter($file);
		}

		private $handle;

		/**
		 * Constructs a \php\io\FileWriter object.
		 * @param \php\io\File $file The File to write to.
		 */
		protected function __construct(File $file, \php\lang\PHPBoolean $append = null) {
			parent::__construct();
			if ($append === null) {
				$append = \php\lang\PHPBoolean::newInstance(true);
			}
			$append = (($append->getBoolean()) ? 'a' : 'w');
			$this->handle = @fopen($file->getPath()->getString(), $append);
			if ($this->handle === false) {
				$error = error_get_last();
				throw new IOException($error['message']);
			}
		}

		/**
		 * Writes object to writer.
		 * @param \php\lang\PHPObject $data Data to be written.
		 * @throws IOException
		 */
		public function write(\php\lang\PHPObject $data) {
			if (@fwrite($this->handle, $data->toString()->getString()) === false) {
				$error = error_get_last();
				throw new IOException($error['message']);
			}
		}

		/**
		 * Closes the stream.
		 * @throws \php\io\IOException
		 */
		public function flush() {
			if (@fflush($this->handle) === false) {
				$error = error_get_last();
				throw new IOException($error['message']);
			}
		}

		/**
		 * Closes the stream and releases any system resources associated with it.
		 * @throws \php\io\IOException
		 */
		public function close() {
			if (@fclose($this->handle) === false) {
				$error = error_get_last();
				throw new IOException($error['message']);
			}
		}

	}

}