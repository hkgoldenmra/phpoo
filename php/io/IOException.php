<?php

/**
 * Description of \php\io\IOException
 */

namespace php\io {

	/**
	 * Signals that an I/O exception of some sort has occurred. This is the general exception of exceptions produced by failed or interrupted I/O operations.
	 */
	class IOException extends \Exception {

		/**
		 * Constructs a \php\io\IOException object.
		 * @param \php\lang\PHPString $message An error message
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}