<?php

/**
 * Description of \php\io\filefilters\AudioFileFilter
 */

namespace php\io\filefilters {
	include_once('php/lang/PHPObject.php');
	include_once('php/io/File.php');
	include_once('php/io/filefilters/FileFilter.php');

	/**
	 * The implementation of \php\io\filefilters\FileFilter for filting audio files.
	 */
	class AudioFileFilter extends \php\lang\PHPObject implements FileFilter {

		/**
		 * Constructs a \php\io\filefilters\AudioFileFilter object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns a \php\io\filefilters\AudioFileFilter object.
		 * @return \php\io\filefilters\AudioFileFilter
		 */
		public static function newInstance() {
			return new AudioFileFilter();
		}

		/**
		 * Tests whether or not the specified file path should be included in a file list.
		 * @param \php\io\File $file The file to be tested.
		 * @return \php\lang\PHPBoolean
		 */
		public function accept(\php\io\File $file) {
			return $file->getMimeType()->regexMatch(\php\lang\PHPString::newInstance('/^audio\/.+$/'))->size()->isPositive();
		}

	}

}