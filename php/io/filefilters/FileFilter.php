<?php

/**
 * Description of \php\io\filefilters\FileFilter
 */

namespace php\io\filefilters {
	include_once('php/io/File.php');

	/**
	 * A filter for file path.
	 */
	interface FileFilter {

		/**
		 * Tests whether or not the specified file path should be included in a file list.
		 * @param \php\io\File $file The file to be tested.
		 * @return \php\lang\PHPBoolean
		 */
		public function accept(\php\io\File $file);
	}

}