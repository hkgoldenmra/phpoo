<?php

/**
 * Description of \php\io\filefilters\TextFileFilter
 */

namespace php\io\filefilters {
	include_once('php/lang/PHPObject.php');
	include_once('php/io/File.php');
	include_once('php/io/filefilters/FileFilter.php');

	/**
	 * The implementation of \php\io\filefilters\FileFilter for filting text files.
	 */
	class TextFileFilter extends \php\lang\PHPObject implements FileFilter {

		/**
		 * Constructs a \php\io\filefilters\TextFileFilter object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns a \php\io\filefilters\TextFileFilter object.
		 * @return \php\io\filefilters\TextFileFilter
		 */
		public static function newInstance() {
			return new TextFileFilter();
		}

		/**
		 * Tests whether or not the specified file path should be included in a file list.
		 * @param \php\io\File $file The file to be tested.
		 * @return \php\lang\PHPBoolean
		 */
		public function accept(\php\io\File $file) {
			return $file->getMimeType()->regexMatch(\php\lang\PHPString::newInstance('/^text\/.+$/'))->size()->isPositive();
		}

	}

}