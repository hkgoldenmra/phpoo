<?php

/**
 * Description of \php\io\filefilters\VideoFileFilter
 */

namespace php\io\filefilters {
	include_once('php/lang/PHPObject.php');
	include_once('php/io/File.php');
	include_once('php/io/filefilters/FileFilter.php');

	/**
	 * The implementation of \php\io\filefilters\FileFilter for filting video files.
	 */
	class VideoFileFilter extends \php\lang\PHPObject implements FileFilter {

		/**
		 * Consttructs a \php\io\filefilters\VideoFileFilter object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns a \php\io\filefilters\VideoFileFilter object.
		 * @return \php\io\filefilters\VideoFileFilter
		 */
		public static function newInstance() {
			return new VideoFileFilter();
		}

		/**
		 * Tests whether or not the specified file path should be included in a file list.
		 * @param \php\io\File $file The file to be tested.
		 * @return \php\lang\PHPBoolean
		 */
		public function accept(\php\io\File $file) {
			return $file->getMimeType()->regexMatch(\php\lang\PHPString::newInstance('/^video\/.+$/'))->size()->isPositive();
		}

	}

}