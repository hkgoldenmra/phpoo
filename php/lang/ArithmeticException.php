<?php

/**
 * Description of \php\lang\ArithmeticException
 */

namespace php\lang {

	/**
	 * Thrown when an exceptional arithmetic condition has occurred.
	 */
	class ArithmeticException extends \Exception {

		/**
		 * Constructs a \php\lang\ArithmeticException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}