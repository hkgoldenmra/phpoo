<?php

namespace php\lang {

	interface CharSequence {

		public function toString();
	}

}