<?php

/**
 * Description of \php\lang\FunctionNotFoundException
 */

namespace php\lang {

	/**
	 * Thrown when a function is not exist.
	 */
	class FunctionNotFoundException extends \Exception {

		/**
		 * Constructs a \php\lang\FunctionNotFoundException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}