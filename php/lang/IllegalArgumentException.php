<?php

/**
 * Description of \php\lang\IllegalArgumentException
 */

namespace php\lang {

	/**
	 * Thrown to indicate that a method has been passed an illegal or inappropriate argument.
	 */
	class IllegalArgumentException extends \Exception {

		/**
		 * Constructs a \php\lang\IllegalArgumentException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}