<?php

/**
 * Description of \php\lang\IndexOutOfBoundsException
 */

namespace php\lang {

	/**
	 * Thrown to indicate that an index of some sort is out of range.
	 */
	class IndexOutOfBoundsException extends \Exception {

		/**
		 * Constructs a \php\lang\IndexOutOfBoundsException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}