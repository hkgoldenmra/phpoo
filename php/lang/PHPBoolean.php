<?php

/**
 * Description of \php\lang\PHPBoolean
 */

namespace php\lang {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');

	/**
	 * The \php\lang\PHPBoolean class represents boolean values.
	 */
	class PHPBoolean extends PHPObject {

		/**
		 * Returns an instance of \php\lang\PHPBoolean object.
		 * @param boolean $boolean Force any data to be a boolean. Default false.
		 * @return \php\lang\PHPBoolean
		 */
		public static function newInstance($boolean = false) {
			return new PHPBoolean($boolean);
		}

		private $boolean;

		/**
		 * Constructs a \php\lang\PHPBoolean object.
		 * @param boolean $boolean Force any data to be a boolean.
		 */
		protected function __construct($boolean = false) {
			parent::__construct();
			$this->boolean = (($boolean) ? true : false);
		}

		/**
		 * The boolean of this object.
		 * @return boolean
		 */
		public function getBoolean() {
			return $this->boolean;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return PHPString::newInstance((($this->getBoolean()) ? 'true' : 'false'));
		}

		/**
		 * Checks two booleans are equal or not.
		 * @param \php\lang\PHPObject $object Another \php\lang\PHPBoolean object.
		 * @return \php\lang\PHPBoolean
		 */
		public function equals(PHPObject $object = null) {
			$return = false;
			if ($object instanceof PHPBoolean) {
				$return = ($this->getBoolean() === $object->getBoolean());
			}
			return PHPBoolean::newInstance($return);
		}

		/**
		 * Reverses the boolean value.
		 * @return \php\lang\PHPBoolean
		 */
		public function not() {
			return PHPBoolean::newInstance(!$this->getBoolean());
		}

		/**
		 * Compares other \php\lang\PHPBoolean with and.
		 * @param \php\lang\PHPBoolean $boolean Other \php\lang\PHPBoolean object.
		 * @return \php\lang\PHPBoolean
		 */
		public function _and(PHPBoolean $boolean) {
			return PHPBoolean::newInstance($this->getBoolean() && $boolean->getBoolean());
		}

		/**
		 * Compares other \php\lang\PHPBoolean with or.
		 * @param \php\lang\PHPBoolean $boolean Other \php\lang\PHPBoolean object.
		 * @return \php\lang\PHPBoolean
		 */
		public function _or(PHPBoolean $boolean) {
			return PHPBoolean::newInstance($this->getBoolean() || $boolean->getBoolean());
		}

	}

}