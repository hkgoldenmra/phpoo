<?php

/**
 * Description of \php\lang\PHPCharacter
 */

namespace php\lang {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/CharSequence.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/IllegalArgumentException.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 * The \php\lang\PHPCharacter class represents a characater.
	 */
	class PHPCharacter extends PHPObject implements CharSequence {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * The default charset of any \php\lang\PHPString objects.
		 * @return \php\lang\PHPString
		 */
		public static final function DEFAULT_CHARSET() {
			return PHPString::newInstance('UTF-8');
		}

		/**
		 * Returns an instance of \php\lang\Character object by character.
		 * @param \php\lang\PHPString $character Force any data to be a string and extract the first character only.
		 * @return \php\lang\PHPCharacter
		 * @throws \php\lang\IllegealArgumentException
		 */
		public static function newInstanceByCharacter($character) {
			$defaultCharset = self::DEFAULT_CHARSET()->getString();
			if (mb_strlen($character, $defaultCharset) < 1) {
				throw new IllegealArgumentException('length() < 1');
			} else {
				$character = mb_substr($character, 0, 1, $defaultCharset);
				return new PHPCharacter($character);
			}
		}

		/**
		 * Returns an instance of \php\lang\Character object by unicode.
		 * @param \php\lang\PHPNumber $unicode Using unicode converts to character.
		 * @return \php\lang\Character
		 * @throws \php\lang\IllegealArgumentException
		 */
		public static function newInstanceByUnicode($unicode) {
			$unicode = '&#' . $unicode . ';';
			$character = mb_convert_encoding($unicode, self::DEFAULT_CHARSET()->getString(), 'HTML-ENTITIES');
			if ($unicode == $character) {
				throw new IllegealArgumentException('cannot convert unicode to character');
			} else {
				return self::newInstanceByCharacter($character);
			}
		}

		private $character;

		/**
		 * Constructs a \php\lang\Character object.
		 * @param string $character Force any data to be a string and extract the first character only.
		 */
		protected function __construct($character) {
			parent::__construct();
			$this->character = $character;
		}

		/**
		 * The character of this object.
		 * @return string
		 */
		public function getCharacter() {
			return $this->character;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return PHPString::newInstance($this->getCharacter());
		}

		/**
		 * Checks two character are equal or not.
		 * @param \php\lang\PHPObject $object Another \php\lang\Character object.
		 * @return \php\lang\PHPBoolean
		 */
		public function equals(PHPObject $object = null) {
			$return = false;
			if ($object instanceof PHPCharacter) {
				$return = ($this->getCharacter() === $object->getCharacter());
			}
			return PHPBoolean::newInstance($return);
		}

		/**
		 * The Unicode of this character.
		 * @return \php\lang\PHPNumber
		 */
		public function toUnicode() {
			list(, $unicode) = unpack('N', mb_convert_encoding($this->getCharacter(), 'UCS-4BE', self::DEFAULT_CHARSET()->getString()));
			return PHPNumber::newInstance($unicode);
		}

		public function toURLEncodes() {
			$codes = \php\util\collections\LinkedList::newInstance();
			$unicode = $this->toUnicode();
			if ($unicode->isGreaterThan(PHPNumber::newInstance(0xFF))->getBoolean()) {
				$urlencodes = explode('%', urlencode($this->getCharacter()));
				array_shift($urlencodes);
				foreach ($urlencodes as $urlencode) {
					$codes->addLast(PHPNumber::newInstance(hexdec($urlencode)));
				}
			} else {
				$codes->addLast($unicode);
			}
			return $codes;
		}

	}

}
