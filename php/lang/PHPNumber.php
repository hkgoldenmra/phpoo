<?php

/**
 * Description of \php\lang\PHPNumber
 */

namespace php\lang {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/ArithmeticException.php');
	include_once('php/lang/IllegalArgumentException.php');
	include_once('php/util/collections/Set.php');

	/**
	 * The \php\lang\PHPNumber class represents number values.
	 */
	class PHPNumber extends PHPObject {

		/**
		 * Returns the constant value of E.
		 * @return \php\lang\PHPNumber
		 */
		public static final function E() {
			return PHPNumber::newInstance(M_E);
		}

		/**
		 * Returns the constant value of PI.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PI() {
			return PHPNumber::newInstance(M_PI);
		}

		/**
		 * Returns the constant value of 0.
		 * @return \php\lang\PHPNumber
		 */
		public static final function ZERO() {
			return PHPNumber::newInstance(0);
		}

		/**
		 * Returns the constant value of 1.
		 * @return \php\lang\PHPNumber
		 */
		public static final function ONE() {
			return PHPNumber::newInstance(1);
		}

		/**
		 * Returns an instance of \php\lang\PHPNumber object.
		 * @param float $number Force any data to be a float. Default 0.
		 * @return \php\lang\PHPNumber
		 */
		public static function newInstance($number = 0) {
			return new PHPNumber($number);
		}

		private $number;

		/**
		 * Constructs a \php\lang\PHPNumber object.
		 * @param float $number Force any data to be a float.
		 */
		protected function __construct($number = 0) {
			parent::__construct();
			if (is_numeric($number)) {
				$this->number = $number + 0;
			} else {
				throw new IllegalArgumentException('Non-numeric');
			}
		}

		/**
		 * The number of this object.
		 * @return float
		 */
		public function getNumber() {
			return $this->number;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return PHPString::newInstance(sprintf('%s', $this->getNumber()));
		}

		/**
		 * Tests two number are equal or unequal identified by their value.
		 * @param \php\lang\PHPObject $object
		 * @return \php\lang\PHPBoolean
		 */
		public function equals(PHPObject $object = null) {
			$return = false;
			if ($object instanceof PHPNumber) {
				$return = ($this->getNumber() === $object->getNumber());
			}
			return PHPBoolean::newInstance($return);
		}

		/**
		 * Converts the number to negative.
		 * @return \php\lang\PHPNumber
		 */
		public function negative() {
			return PHPNumber::newInstance(-$this->getNumber());
		}

		/**
		 * Forces the number to be absolute.
		 * @return \php\lang\PHPNumber
		 */
		public function absolute() {
			return PHPNumber::newInstance(abs($this->getNumber()));
		}

		/**
		 * Forces the number to be an integer.
		 * @return \php\lang\PHPNumber
		 */
		public function getInteger() {
			return PHPNumber::newInstance(intval($this->getNumber()));
		}

		/**
		 * Force the number to be a float.
		 * @return \php\lang\PHPNumber
		 */
		public function getFloat() {
			return PHPNumber::newInstance(floatval($this->getNumber()));
		}

		/**
		 * Tests this number is integer or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isInteger() {
			return PHPBoolean::newInstance(($this->getNumber() - $this->getInteger()->getNumber()) == 0);
		}

		/**
		 * Tests this number is float or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isFloat() {
			return PHPBoolean::newInstance(($this->getNumber() - $this->getInteger()->getNumber()) != 0);
		}

		/**
		 * Returns the value of the binary value.
		 * @return \php\lang\PHPString
		 */
		public function toBinString() {
			return PHPString::newInstance(sprintf('%s', decbin($this->getInteger()->getNumber())));
		}

		/**
		 * Returns the value of the octal value.
		 * @return \php\lang\PHPString
		 */
		public function toOctString() {
			return PHPString::newInstance(sprintf('%s', decoct($this->getInteger()->getNumber())));
		}

		/**
		 * Returns the value of the hexadecimal value.
		 * @return \php\lang\PHPString
		 */
		public function toHexString() {
			return PHPString::newInstance(sprintf('%s', dechex($this->getInteger()->getNumber())));
		}

		private function shift($direction, PHPNumber $number) {
			if ($direction == 'right') {
				return $this->getInteger()->getNumber() >> $number->getInteger()->getNumber();
			} else {
				return $this->getInteger()->getNumber() << $number->getInteger()->getNumber();
			}
		}

		/**
		 * Shifts this number to left with a number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function shiftLeft(PHPNumber $number) {
			return PHPNumber::newInstance($this->shift('left', $number));
		}

		/**
		 * Shifts this number to right with a number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function shiftRight(PHPNumber $number) {
			return PHPNumber::newInstance($this->shift('right', $number));
		}

		/**
		 * Converts this number with bitwise or from a number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function bitwiseOr(PHPNumber $number) {
			return PHPNumber::newInstance($this->getInteger()->getNumber() | $number->getInteger()->getNumber());
		}

		/**
		 * Converts this number with bitwise and from a number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function bitwiseAnd(PHPNumber $number) {
			return PHPNumber::newInstance($this->getInteger()->getNumber() & $number->getInteger()->getNumber());
		}

		/**
		 * Converts this number with bitwise xor from a number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function bitwiseXor(PHPNumber $number) {
			return PHPNumber::newInstance($this->getInteger()->getNumber() ^ $number->getInteger()->getNumber());
		}

		/**
		 * Checks the number is negative or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isNegative() {
			return $this->isLessThan(PHPNumber::ZERO());
		}

		/**
		 * Checks the number is positive or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isPositive() {
			return $this->isGreaterThan(PHPNumber::ZERO());
		}

		/**
		 * Checks the number is zero or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isZero() {
			return $this->isEqualTo(PHPNumber::ZERO());
		}

		/**
		 * Checks two numbers are not equal or not.
		 * @param \php\lang\PHPObject $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function notEqual(PHPNumber $number) {
			return PHPBoolean::newInstance($this->getNumber() != $number->getNumber());
		}

		/**
		 * Checks two numbers are equal or not.
		 * @param \php\lang\PHPObject $object Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function equal(PHPObject $object) {
			if ($object instanceof \php\lang\PHPNumber) {
				return $this->notEqual($object)->not();
			} else {
				return PHPBoolean::newInstance(false);
			}
		}

		/**
		 * Checks two numbers are equal or not.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function isEqualTo(PHPNumber $number) {
			return $this->equals($number);
		}

		/**
		 * Checks this numbers is less than another number or not.
		 * @param \php\lang\PHPObject $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function isLessThan(PHPNumber $number) {
			return PHPBoolean::newInstance($this->getNumber() < $number->getNumber());
		}

		/**
		 * Checks this numbers is greater than another number or not.
		 * @param \php\lang\PHPObject $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function isGreaterThan(PHPNumber $number) {
			return PHPBoolean::newInstance($this->getNumber() > $number->getNumber());
		}

		/**
		 * Checks this numbers is less or equal to another number or not.
		 * @param \php\lang\PHPObject $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function isLessOrEqual(PHPNumber $number) {
			return PHPBoolean::newInstance($this->isEqualTo($number)->getBoolean() || $this->isLessThan($number)->getBoolean());
		}

		/**
		 * Checks this numbers is greater or equal to another number or not.
		 * @param \php\lang\PHPObject $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function isGreaterOrEqual(PHPNumber $number) {
			return PHPBoolean::newInstance($this->isEqualTo($number)->getBoolean() || $this->isGreaterThan($number)->getBoolean());
		}

		/**
		 * Checks this numbers is between these two numbers.
		 * @param \php\lang\PHPNumber $number1 First \php\lang\PHPNumber object.
		 * @param \php\lang\PHPNumber $number2 Second \php\lang\PHPNumber object.
		 * @return \php\lang\PHPBoolean
		 */
		public function bewteen(PHPNumber $number1, PHPNumber $number2) {
			if ($number1->isGreaterThan($number2)->getBoolean()) {
				$temp = $number1;
				$number1 = $number2;
				$number2 = $temp;
			}
			return $this->isGreaterOrEqual($number1)->_and($this->isLessOrEqual($number2));
		}

		/**
		 * Adds a number to this number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function addition(PHPNumber $number) {
			return PHPNumber::newInstance($this->getNumber() + $number->getNumber());
		}

		/**
		 * Subtracts a number to this number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function subtraction(PHPNumber $number) {
			return PHPNumber::newInstance($this->getNumber() - $number->getNumber());
		}

		/**
		 * Multiplies a number to this number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function multiply(PHPNumber $number) {
			return PHPNumber::newInstance($this->getNumber() * $number->getNumber());
		}

		/**
		 * Divided by a number to this number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 * @throws \php\lang\ArithmeticException
		 */
		public function dividedBy(PHPNumber $number) {
			if ($number->isZero()->getBoolean()) {
				throw new ArithmeticException('divided by 0');
			} else {
				return PHPNumber::newInstance($this->getNumber() / $number->getNumber());
			}
		}

		/**
		 * Counts the number of digits.
		 * @return \php\lang\PHPNumber
		 */
		public function digits() {
			$number = $this->getNumber();
			$i = 1;
			$digits = 0;
			while ($number != intval($number)) {
				$number *= 10;
				$i *= 10;
				$digits++;
			}
			return PHPNumber::newInstance($digits);
		}

		/**
		 * Returns which number is greater.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function max(PHPNumber $number) {
			return PHPNumber::newInstance(max($this->getNumber(), $number->getNumber()));
		}

		/**
		 * Returns which number is smaller.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function min(PHPNumber $number) {
			return PHPNumber::newInstance(min($this->getNumber(), $number->getNumber()));
		}

		/**
		 * Returns the power of this number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @return \php\lang\PHPNumber
		 */
		public function pow(PHPNumber $number) {
			return PHPNumber::newInstance(pow($this->getNumber(), $number->getNumber()));
		}

		/**
		 * Returns the log value of the base number.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object. Default E().
		 * @return ty\php\lang\PHPNumberpe
		 */
		public function log(PHPNumber $number = null) {
			if ($number === null) {
				$number = self::E();
			}
			return PHPNumber::newInstance(log($this->getNumber(), $number->getNumber()));
		}

		/**
		 * Converts the number to over.
		 * @return \php\lang\PHPNumber
		 */
		public function over() {
			return PHPNumber::newInstance(1)->dividedBy($this);
		}

		/**
		 * Converts the number to square root.
		 * @return \php\lang\PHPNumber
		 */
		public function sqrt() {
			return $this->pow(PHPNumber::newInstance(2)->over());
		}

		/**
		 * Converts the number to cube root.
		 * @return \php\lang\PHPNumber
		 */
		public function cbrt() {
			return $this->pow(PHPNumber::newInstance(3)->over());
		}

		/**
		 * Returns the number of the modulo.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @param \php\lang\PHPBoolean $mustPositive Auto converts the result to positive.
		 * @return \php\lang\PHPNumber
		 * @throws \php\lang\ArithmeticException
		 */
		public function modulo(PHPNumber $number, PHPBoolean $mustPositive = null) {
			if ($number->isZero()->getBoolean()) {
				throw new ArithmeticException('divided by 0');
			} else {
				$max = $this->digits()->max($number->digits());
				$multiply = PHPNumber::newInstance(10);
				$multiply = $multiply->pow($max);
				$modulo = PHPNumber::newInstance($this->multiply($multiply)->getNumber() % $number->multiply($multiply)->getNumber());
				$modulo = $modulo->dividedBy($multiply);
				if ($mustPositive === null) {
					$mustPositive = PHPBoolean::newInstance(false);
				}
				if ($mustPositive->_and($modulo->isNegative())->getBoolean()) {
					$modulo = $modulo->addition($number);
				}
				return $modulo;
			}
		}

		/**
		 * Returns the number of the quotient.
		 * @param \php\lang\PHPNumber $number Another \php\lang\PHPNumber object.
		 * @param \php\lang\PHPBoolean $mustPositive Auto converts the result to positive.
		 * @return \php\lang\PHPNumber
		 */
		public function quotient(PHPNumber $number, PHPBoolean $mustPositive = null) {
			$modulo = $this->modulo($number, $mustPositive);
			return $this->subtraction($modulo)->dividedBy($number);
		}

		/**
		 * Calculates the factorial of this number.
		 * @return \php\lang\PHPNumber
		 * @throws \php\lang\ArithmeticException
		 */
		public function factorial() {
			if ($this->isInteger()->not()->getBoolean()) {
				throw new ArithmeticException('not an integer');
			} else if ($this->isNegative()->getBoolean()) {
				throw new ArithmeticException('number < 0');
			} else {
				$product = new PHPNumber(1);
				for ($i = $product->copy(); $i->isLessOrEqual($this)->getBoolean(); $i = $i->increase()) {
					$product = $product->multiply($i);
				}
				return $product;
			}
		}

		/**
		 * Increases the number by 1.
		 * @return \php\lang\PHPNumber
		 */
		public function increase() {
			return $this->addition(PHPNumber::ONE());
		}

		/**
		 * Descreases the number by 1.
		 * @return \php\lang\PHPNumber
		 */
		public function descrease() {
			return $this->subtraction(PHPNumber::ONE());
		}

		/**
		 * Forces round up the value if digit exist.
		 * @return \php\lang\PHPNumber
		 */
		public function ceil() {
			return new PHPNumber(ceil($this->getNumber()));
		}

		/**
		 * Forces round down the value even digit exist.
		 * @return \php\lang\PHPNumber
		 */
		public function floor() {
			return new PHPNumber(floor($this->getNumber()));
		}

		/**
		 * Checks the number is prime or not.
		 * @return \php\lang\PHPBoolean
		 * @throws \php\lang\ArithmeticException
		 */
		public function isPrime() {
			$_1 = PHPNumber::newInstance(1);
			if ($this->isInteger()->not()->getBoolean()) {
				throw new ArithmeticException('not an integer');
			} else if ($this->isLessThan($_1)->getBoolean()) {
				throw new ArithmeticException('value < 1');
			} else {
				$return = true;
				if ($this->isEqualTo($_1)->getBoolean()) {
					$return = false;
				} else {
					for ($max = $this->sqrt(), $i = PHPNumber::newInstance(2); $i->isLessOrEqual($max)->getBoolean(); $i = $i->increase()) {
						if ($i->isPrime()->getBoolean()) {
							if ($this->modulo($i)->isZero()->getBoolean()) {
								$return = false;
								break;
							}
						} else {
							continue;
						}
					}
				}
				return PHPBoolean::newInstance($return);
			}
		}

		/**
		 * Lists the factors of this number.
		 * @return \php\util\collections\Set
		 * @throws \php\lang\ArithmeticException
		 */
		public function factors() {
			$_1 = PHPNumber::newInstance(1);
			if ($this->isInteger()->not()->getBoolean()) {
				throw new ArithmeticException('not an integer');
			} else if ($this->isLessThan($_1)->getBoolean()) {
				throw new ArithmeticException('value < 1');
			} else {
				$factors = \php\util\collections\Set::newInstance();
				for ($max = $this->sqrt(), $i = PHPNumber::newInstance(2); $i->isLessOrEqual($max)->getBoolean(); $i = $i->increase()) {
					if ($this->modulo($i)->isZero()->getBoolean()) {
						$factors->add($i);
						$factors->add($this->dividedBy($i));
					}
				}
				$factors->add(PHPNumber::newInstance(1));
				$factors->add($this);
				$factors = \php\util\Arrays::newInstance(\php\util\comparators\PHPNumberComparator::newInstance(PHPBoolean::newInstance(true)))->sort($factors);
				return $factors;
			}
		}

	}

}