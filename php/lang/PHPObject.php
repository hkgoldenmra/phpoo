<?php

/**
 * Description of \php\lang\PHPObject
 */

namespace php\lang {
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/lang/UnsupportOperationException.php');

	/**
	 * \php\lang\PHPObject is the root of the class hierarchy. Every classes have \php\lang\PHPObject as a superclass.
	 */
	class PHPObject {

		/**
		 * A predefined function for unreachable functions.
		 * @throws \Exception
		 */
		protected static function unsupportedFunction() {
			throw new UnsupportOperationException('Unsupported function');
		}

		/**
		 * Returns a \php\lang\PHPObject object.
		 * @return \php\lang\PHPObject
		 */
		public static function newInstance() {
			return new PHPObject();
		}

		private $hashCode;

		/**
		 * Constructs a \php\lang\PHPObject object.
		 */
		protected function __construct() {
			$this->generateHashCode();
		}

		private function generateHashCode() {
			//$this->hashCode = spl_object_hash($this);
			//$uniqid = explode('.', uniqid('', true));
			//$this->hashCode = sprintf('%s%014x', $uniqid[0], ($uniqid[1]));
			$this->hashCode = uniqid();
		}

		/**
		 * The full name of this object.
		 * @return \php\lang\PHPString
		 */
		public function getFullName() {
			return PHPString::newInstance('\\' . get_class($this));
		}

		/**
		 * The package name of this object.
		 * @return \php\lang\PHPString
		 */
		public function getPackageName() {
			$fullName = $this->getFullName();
			$index = $fullName->lastIndexOf(PHPString::newInstance('\\'));
			return $fullName->substr(PHPNumber::newInstance(0), $index);
		}

		/**
		 * The class name of this object.
		 * @return \php\lang\PHPString
		 */
		public function getClassName() {
			$fullName = $this->getFullName();
			$index = $fullName->lastIndexOf(PHPString::newInstance('\\'));
			return $fullName->substr($index->addition(PHPNumber::newInstance(1)));
		}

		/**
		 * The hash code of this object.
		 * @return \php\lang\PHPString
		 */
		public function getHashCode() {
			return PHPString::newInstance($this->hashCode);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = PHPString::newInstance('');
			$return = $return->append($this->getFullName());
			$return = $return->append(PHPString::newInstance('@'));
			$return = $return->append($this->getHashCode());
			return $return;
		}

		/**
		 * Checks two objects are equal or not.
		 * @param \php\lang\PHPObject $object Another \php\lang\PHPObject object.
		 * @return \php\lang\PHPBoolean
		 */
		public function equals(PHPObject $object) {
			return $this->getHashCode()->toString()->equals($object->getHashCode()->toString());
		}

		/**
		 * A copy of this object.
		 * @return \php\lang\PHPObject
		 */
		public function copy() {
			$return = clone $this;
			$return->generateHashCode();
			return $return;
		}

		/**
		 * Represents this object.
		 * @return string
		 */
		public function __toString() {
			return $this->toString()->getString();
		}

	}

}