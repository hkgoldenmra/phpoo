<?php

/**
 * Description of \php\lang\PHPString
 */

namespace php\lang {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/CharSequence.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/lang/StringIndexOutOfBoundsException.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/collections/Map.php');

	/**
	 * The \php\lang\PHPString class represents character strings.
	 */
	class PHPString extends PHPObject implements CharSequence {

		/**
		 * Returns an instance of \php\lang\PHPString object.
		 * @param string $string Force any data to be a string. Default &lt;empty string&gt;.
		 * @return \php\lang\PHPString
		 */
		public static function newInstance($string = '') {
			return new PHPString($string);
		}

		private $string;

		/**
		 * Constructs a \php\lang\PHPString object.
		 * @param string $string Force any data to be a string. Default &lt;empty string&gt;.
		 */
		protected function __construct($string = '') {
			parent::__construct();
			$this->string = sprintf('%s', $string);
		}

		/**
		 * The string of this object.
		 * @return string
		 */
		public function getString() {
			return $this->string;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return PHPString::newInstance($this->getString());
		}

		/**
		 * Tests two string are equal or unequal identified by their basic string.
		 * @param \php\lang\PHPObject $object
		 * @return \php\lang\PHPBoolean
		 */
		public function equals(PHPObject $object = null) {
			$return = false;
			if ($object instanceof PHPString) {
				$return = ($this->getString() === $object->getString());
			}
			return PHPBoolean::newInstance($return);
		}

		/**
		 * Tests two string are equal or unequal identified by their basic string but ignore case.
		 * @param \php\lang\PHPString $string
		 * @return \php\lang\PHPBoolean
		 */
		public function equalsIgnoreCase(PHPString $string) {
			return $this->toUpperCase()->equals($string->toUpperCase());
		}

		/**
		 * Returns a trimmed string.
		 * @param \php\lang\CharSequence $mask Determine a trim string or character.
		 * @return \php\lang\PHPString
		 */
		public function trim(CharSequence $mask = null) {
			if ($mask === null) {
				return new PHPString(trim($this->getString()));
			} else {
				return new PHPString(trim($this->getString(), $mask->toString()->getString()));
			}
		}

		/**
		 * Tests the string is numeric or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isNumeric() {
			return PHPBoolean::newInstance(is_numeric($this->getString()));
		}

		/**
		 * Returns a number if it can.
		 * @return \php\lang\PHPNumber
		 */
		public function toNumber() {
			return PHPNumber::newInstance($this->getString());
		}

		/**
		 * Converts all characters into lower case in this string.
		 * @return \php\lang\PHPString
		 */
		public function toLowerCase() {
			return PHPString::newInstance(mb_strtolower($this->getString(), PHPCharacter::DEFAULT_CHARSET()->getString()));
		}

		/**
		 * Converts all characters into upper case in this string.
		 * @return \php\lang\PHPString
		 */
		public function toUpperCase() {
			return PHPString::newInstance(mb_strtoupper($this->getString(), PHPCharacter::DEFAULT_CHARSET()->getString()));
		}

		/**
		 * The length of this string.
		 * @return \php\lang\PHPNumber
		 */
		public function length() {
			return PHPNumber::newInstance(mb_strlen($this->getString(), PHPCharacter::DEFAULT_CHARSET()->getString()));
		}

		/**
		 * Checks this string is empty or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isEmpty() {
			return $this->length()->isZero();
		}

		/**
		 * Returns the character at specific index.
		 * @param \php\lang\PHPNumber $index
		 * @return \php\lang\PHPCharacter
		 */
		public function charAt(PHPNumber $index) {
			return PHPCharacter::newInstanceByCharacter($this->substr($index, PHPNumber::newInstance(1))->getString());
		}

		/**
		 * Returns an array of characters.
		 * @return \php\util\Vector
		 */
		public function toCharArray() {
			$return = \php\util\collections\Vector::newInstance();
			for ($i = PHPNumber::ZERO(); $i->isLessThan($this->length())->getBoolean(); $i = $i->increase()) {
				$return->add($this->charAt($i));
			}
			return $return;
		}

		/**
		 * Returns a padded string specific length and character.
		 * @param \php\lang\PHPCharacter $character
		 * @param \php\lang\PHPNumber $length
		 * @return \php\lang\PHPBoolean
		 */
		public function padChar(PHPCharacter $character, PHPNumber $length) {
			$return = $this->toString();
			while ($return->length()->isLessThan($length)->getBoolean()) {
				$return = $return->insert($character);
			}
			return $return;
		}

		/**
		 * A substring in this string. If $length not set, extract to the end.
		 * @param \php\lang\PHPNumber $start The start position of a substring extract for.
		 * @param \php\lang\PHPNumber $length The length of a substring extract for.
		 * @return \php\lang\PHPString
		 * @throws \php\lang\StringIndexOutOfBoundsException
		 */
		public function substr(PHPNumber $start, PHPNumber $length = null) {
			if ($start->isNegative()->getBoolean()) {
				throw new StringIndexOutOfBoundsException('start < 0');
			}
			if ($start->isGreaterThan($this->length())->getBoolean()) {
				throw new StringIndexOutOfBoundsException('start > length()');
			}
			if ($length === null) {
				return PHPString::newInstance(mb_substr($this->getString(), $start->getInteger()->getNumber(), null, PHPCharacter::DEFAULT_CHARSET()));
			} else {
				if ($length->isNegative()->getBoolean()) {
					throw new StringIndexOutOfBoundsException('length < 0');
				}
				if ($start->addition($length)->isGreaterThan($this->length())->getBoolean()) {
					throw new StringIndexOutOfBoundsException('start + length > length()');
				}
				return PHPString::newInstance(mb_substr($this->getString(), $start->getInteger()->getNumber(), $length->getInteger()->getNumber(), PHPCharacter::DEFAULT_CHARSET()));
			}
		}

		/**
		 * A substring in this string. If $end not set, extract to the end.
		 * @param \php\lang\PHPNumber $start The start position of a substring extract for.
		 * @param \php\lang\PHPNumber $end The end position of a substring extract for.
		 * @return \php\lang\PHPString
		 * @throws \php\lang\StringIndexOutOfBoundsException
		 */
		public function substring(PHPNumber $start, PHPNumber $end = null) {
			if ($end === null) {
				return $this->substr($start);
			} else {
				if ($end->isNegative()->getBoolean()) {
					throw new StringIndexOutOfBoundsException('end < 0');
				}
				if ($end->isGreaterThan($this->length())->getBoolean()) {
					throw new StringIndexOutOfBoundsException('end > length()');
				}
				return $this->substr($start, $end->subtraction($start));
			}
		}

		/**
		 * Returns a substring begin from left.
		 * @param \php\lang\PHPNumber $length The length of substring.
		 * @return \php\lang\PHPString
		 */
		public function left(PHPNumber $length) {
			return $this->substr(PHPNumber::ZERO(), $length);
		}

		/**
		 * Returns a substring begin from right.
		 * @param \php\lang\PHPNumber $length The length of substring.
		 * @return \php\lang\PHPString
		 */
		public function right(PHPNumber $length) {
			return $this->substr($this->length()->subtraction($length));
		}

		/**
		 * The number of substrings stored in this string.
		 * @param \php\lang\CharSequence $substring A substring in this string.
		 * @return \php\lang\PHPNumber
		 */
		public function substringCount(CharSequence $substring) {
			return PHPNumber::newInstance(substr_count($this->getString(), $substring->toString()->getString()));
		}

		/**
		 * Appends a string at the end of this string.
		 * @param \php\lang\CharSequence $string A string will be appended to.
		 * @param \php\lang\PHPNumber $offset Skipped offset. Default length().
		 * @return \php\lang\PHPString
		 */
		public function append(CharSequence $string, PHPNumber $offset = null) {
			if ($offset === null) {
				return PHPString::newInstance($this->getString() . $string->toString()->getString());
			} else {
				$head = $this->substr(PHPNumber::ZERO(), $offset);
				$tail = $this->substr($offset);
				return $head->append($string)->append($tail);
			}
		}

		/**
		 * Inserts a string at the beginning of this string.
		 * @param \php\lang\CharSequence $string A string will be inserted to.
		 * @param \php\lang\PHPNumber $offset Skipped offset. Default 0.
		 * @return \php\lang\PHPString
		 */
		public function insert(CharSequence $string, PHPNumber $offset = null) {
			if ($offset === null) {
				$offset = PHPNumber::ZERO();
			}
			return $this->append($string, $offset);
		}

		private function _indexOf($direction, CharSequence $substring, PHPNumber $offset = null) {
			if ($substring->toString()->isEmpty()->getBoolean()) {
				throw new StringIndexOutOfBoundsException('length() == 0');
			}
			if ($offset === null) {
				$offset = PHPNumber::ZERO();
			}
			if ($direction == 'left') {
				$index = mb_strpos($this->getString(), $substring->toString(), $offset->getInteger()->getNumber(), PHPCharacter::DEFAULT_CHARSET()->getString());
			} else if ($direction == 'right') {
				$index = mb_strrpos($this->getString(), $substring->toString(), $offset->getInteger()->getNumber(), PHPCharacter::DEFAULT_CHARSET()->getString());
			}
			if ($index === false) {
				$index = -1;
			}
			return PHPNumber::newInstance($index);
		}

		/**
		 * The position of the matched substring in this string from the beginning to the end. If $offset not set, scan from the beginning. If not found, returns -1.
		 * @param \php\lang\CharSequence $substring A substring in this string.
		 * @param \php\lang\PHPNumber $offset Skipped offset. Default 0.
		 * @return \php\lang\PHPNumber
		 * @throws \php\lang\StringIndexOutOfBoundsException
		 */
		public function indexOf(CharSequence $substring, PHPNumber $offset = null) {
			return $this->_indexOf('left', $substring, $offset);
		}

		/**
		 * Returns the index of substring at last, if substring not present, is -1
		 * @param \php\lang\PHPString $substring A substring in this string.
		 * @param \php\lang\PHPNumber $offset Skipped offset. Default length().
		 * @return \php\lang\PHPNumber
		 * @throws \php\lang\StringIndexOutOfBoundsException
		 */
		public function lastIndexOf(CharSequence $substring, PHPNumber $offset = null) {
			return $this->_indexOf('right', $substring, $offset);
		}

		/**
		 * Checks this substring is contained or not in this string.
		 * @param \php\lang\CharSequence $substring A substring in this string.
		 * @return \php\lang\PHPBoolean
		 */
		public function contains(CharSequence $substring) {
			return $this->indexOf($substring)->isPositive();
		}

		/**
		 * Checks the substring is started in this string.
		 * @param \php\lang\CharSequence $substring A substring in this string.
		 * @return \php\lang\PHPBoolean
		 */
		public function startsWith(CharSequence $substring) {
			return $this->indexOf($substring)->isZero();
		}

		/**
		 * Checks the substring is end in this string.
		 * @param \php\lang\CharSequence $substring A substring in this string.
		 * @return \php\lang\PHPBoolean
		 */
		public function endsWith(CharSequence $substring) {
			$index = $this->lastIndexOf($substring);
			if ($this->contains($substring)->getBoolean()) {
				return $index->isEqualTo($this->length()->subtraction($substring->length()));
			} else {
				return PHPBoolean::newInstance(false);
			}
		}

		/**
		 * Replaces the first substring in this string.
		 * @param \php\lang\CharSequence $old The substring will be replaced for.
		 * @param \php\lang\CharSequence $new The string will replace to.
		 * @return \php\lang\PHPString
		 */
		public function replaceFirst(CharSequence $old, CharSequence $new) {
			if ($this->contains($old)->getBoolean()) {
				$index = $this->indexOf($old);
				return PHPString::newInstance(substr_replace($this->getString(), $new->toString()->getString(), $index->getNumber()));
			} else {
				return $this->copy();
			}
		}

		/**
		 * Replaces the last substring in this string.
		 * @param \php\lang\CharSequence $old The substring will be replaced for.
		 * @param \php\lang\CharSequence $new The string will replace to.
		 * @return \php\lang\PHPString
		 */
		public function replaceLast(CharSequence $old, CharSequence $new) {
			if ($this->contains($old)->getBoolean()) {
				$index = $this->lastIndexOf($old);
				return PHPString::newInstance(substr_replace($this->getString(), $new->toString()->getString(), $index->getNumber()));
			} else {
				return $this->copy();
			}
		}

		/**
		 * Replaces all substring in this string.
		 * @param \php\lang\CharSequence $old The substring will be replaced for.
		 * @param \php\lang\CharSequence $new The string will replace to.
		 * @return \php\lang\PHPString
		 */
		public function replaceAll(CharSequence $old, CharSequence $new) {
			return new PHPString(str_replace($old->toString()->getString(), $new->toString()->getString(), $this->getString()));
		}

		public function split(CharSequence $delimiter, PHPNumber $limit = null) {
			$array = array();
			if ($limit === null) {
				$array = explode($delimiter->toString()->getString(), $this->getString());
			} else {
				$array = explode($delimiter->toString()->getString(), $this->getString(), $limit->getInteger()->getNumber());
			}
			$return = \php\util\collections\Vector::newInstance();
			foreach ($array as $v) {
				$return->add(PHPString::newInstance($v));
			}
			return $return;
		}

		/**
		 * Reverses the string.
		 * @return \php\lang\PHPString
		 */
		public function reverse() {
			return PHPString::newInstance(strrev($this->getString()));
		}

		/**
		 * Returns the match pattern capture groups.
		 * @param \php\lang\PHPString $pattern The regular expression pattern.
		 * @return \php\util\collections\Map
		 */
		public function regexMatch(PHPString $pattern) {
			$return = \php\util\collections\Map::newInstance();
			if (preg_match($pattern->getString(), $this->getString(), $matches) > 0) {
				foreach ($matches as $k => $v) {
					$return->put(PHPString::newInstance($k), PHPString::newInstance($v));
				}
			}
			return $return;
		}

		/**
		 * Replaces the matched regular expression pattern in this string.
		 * @param \php\lang\PHPString $pattern The regular expression pattern will be replaced for.
		 * @param \php\lang\PHPString $new The string will replace to.
		 * @return \php\lang\PHPString
		 */
		public function regexReplace(PHPString $pattern, CharSequence $new) {
			return PHPString::newInstance(preg_replace($pattern->getString(), $new->toString()->getString(), $this->getString()));
		}

	}

}
