<?php

/**
 * Description of \php\lang\StringIndexOutOfBoundsException
 */

namespace php\lang {
	include_once('php/lang/IndexOutOfBoundsException.php');

	/**
	 * Thrown by String methods to indicate that an index is either negative or greater than the size of the string.
	 */
	class StringIndexOutOfBoundsException extends IndexOutOfBoundsException {

		/**
		 * Constructs a \php\lang\StringIndexOutOfBoundsException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}