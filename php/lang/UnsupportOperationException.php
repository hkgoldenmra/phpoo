<?php

/**
 * Description of \php\lang\UnsupportOperationException
 */

namespace php\lang {

	/**
	 * Thrown to indicate that the requested operation is not supported.
	 */
	class UnsupportOperationException extends \Exception {

		/**
		 * Constructs a \php\lang\UnsupportOperationException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}