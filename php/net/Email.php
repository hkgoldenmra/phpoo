<?php

namespace php\net {
	include_once('php/io/IOException.php');
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/io/File.php');
	include_once('php/io/FileReader.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/collections/Vector.php');

	class Email extends \php\lang\PHPObject {

		public static function newInstanceByParameters(\php\lang\PHPString $smtpServer, \php\lang\PHPString $username, \php\lang\PHPString $password, \php\lang\PHPBoolean $useSSl = null, \php\lang\PHPNumber $port = null) {
			return new Email($smtpServer, $username, $password, $useSSl, $port);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private function checkReponseCode($code) {
			$response = fgets($this->handle);
			$this->responseBody .= $response;
			if (substr($response, 3, 1) != ' ') {
				$this->checkReponseCode($code);
			} else {
				if (substr($response, 0, 3) != $code) {
					throw new \php\io\IOException($response);
				}
			}
		}

		private function sendRequest($request, $code, $hidden_request = false) {
			$this->responseBody .= (($hidden_request) ? "***** Hidden Request *****\r\n" : $request);
			fwrite($this->handle, $request);
			$this->checkReponseCode($code);
		}

		private $eof = "\r\n";
		private $handle;
		private $email;
		private $name;
		private $tos;
		private $ccs;
		private $bccs;
		private $subject;
		private $body;
		private $attachments;
		private $responseBody;

		/**
		 * Constructs a Email object to send E-Mail
		 * @param \php\lang\PHPString $smtpServer A SMTP Server address
		 * @param \php\lang\PHPString $username The login username of SMTP Server
		 * @param \php\lang\PHPString $password The login password of SMTP Server
		 * @param \php\lang\PHPBoolean $useSSl The SMTP Server requires SSL or not, default false
		 * @param \php\lang\PHPNumber $port The port of SMTP Server uses, default 25 if SSL is not required, default 465 if SSL is required
		 * @throws \php\io\IOException
		 */
		protected function __construct(\php\lang\PHPString $smtpServer, \php\lang\PHPString $username, \php\lang\PHPString $password, \php\lang\PHPBoolean $useSSl = null, \php\lang\PHPNumber $port = null) {
			parent::__construct();
			$this->tos = \php\util\collections\Map::newInstance();
			$this->ccs = \php\util\collections\Map::newInstance();
			$this->bccs = \php\util\collections\Map::newInstance();
			$this->attachments = \php\util\collections\Map::newInstance();
			if ($useSSl === null) {
				$useSSl = \php\lang\PHPBoolean::newInstance(false);
			}
			$useSSl = $useSSl->getBoolean();
			if ($port === null) {
				$port = \php\lang\PHPNumber::newInstance(($useSSl) ? 465 : 25);
			}
			$port = $port->getNumber();
			$smtpServer = $smtpServer->getString();
			$server = $smtpServer;
			if ($useSSl) {
				$server = 'ssl://' . $server;
			}
			$errno = 0;
			$error = '';
			if ($this->handle = @fsockopen($server, $port, $errno, $error)) {
				$this->checkReponseCode(220);
				$this->sendRequest('EHLO ' . $smtpServer . $this->eof, 250);
				$this->sendRequest('AUTH LOGIN' . $this->eof, 334);
				$this->sendRequest(base64_encode($username->getString()) . $this->eof, 334, true);
				$this->sendRequest(base64_encode($password->getString()) . $this->eof, 235, true);
			} else {
				echo $error;
				$error = explode(': ', $error);
				throw new \php\io\IOException(end($error));
			}
		}

		/**
		 * Sets the sender from this Email object
		 * @param \php\lang\PHPString $email The sender E-Mail address
		 * @param \php\lang\PHPString $name The sender alias, default &lt;not present&gt;
		 */
		public function setSender(\php\lang\PHPString $email, \php\lang\PHPString $name = null) {
			$this->email = $email->getString();
			$this->name = (($name === null) ? null : $name->getString());
		}

		/**
		 * Adds the recipient to this Email object
		 * @param \php\lang\PHPString $email The recipient E-Mail address
		 * @param \php\lang\PHPString $name The recipient alias, default &lt;not present&gt;
		 */
		public function addTo(\php\lang\PHPString $email, \php\lang\PHPString $name = null) {
			$this->tos->put($email, $name);
		}

		/**
		 * Adds the recipient to this Email object signed as Cc
		 * @param \php\lang\PHPString $email The recipient E-Mail address
		 * @param \php\lang\PHPString $name The recipient alias, default &lt;not present&gt;
		 */
		public function addCc(\php\lang\PHPString $email, \php\lang\PHPString $name = null) {
			$this->ccs->put($email, $name);
		}

		/**
		 * Adds the recipient to this Email object signed as Bcc
		 * @param \php\lang\PHPString $email The recipient E-Mail address
		 * @param \php\lang\PHPString $name The recipient alias, default &lt;not present&gt;
		 */
		public function addBcc(\php\lang\PHPString $email, \php\lang\PHPString $name = null) {
			$this->bccs->put($email, $name);
		}

		/**
		 * Sets the subject of this Email object
		 * @param \php\lang\PHPString $subject The subject of this Email object, default &lt;not present&gt;
		 */
		public function setSubject(\php\lang\PHPString $subject = null) {
			$this->subject = (($subject === null) ? null : $subject->getString());
		}

		/**
		 * Sets the body of this Email object
		 * @param \php\lang\PHPString $body The body of this Email object
		 */
		public function setBody(\php\lang\PHPString $body) {
			$this->body = (($body === null) ? null : $body->getString());
		}

		/**
		 * Adds an attachment to this Email object
		 * @param \php\io\File $attachment A file attach to this Email object
		 * @param \php\lang\PHPString $name The name of attachment, default &lt;not present&gt;
		 */
		public function addAttachment(\php\io\File $attachment, \php\lang\PHPString $name = null) {
			$this->attachments->put($attachment, $name);
		}

		/**
		 * Sends an E-Mail from this Email object
		 */
		public function send() {
			$email = null;
			if ($this->email !== null) {
				$email = '<' . $this->email . '>';
				$this->sendRequest('MAIL FROM: ' . $email . $this->eof, 250);
			}
			$tos = array();
			$ccs = array();
			$keys = $this->tos->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->tos->get($key);
				$key = '<' . $key->getString() . '>';
				$this->sendRequest('RCPT TO: ' . $key . $this->eof, 250);
				if ($value !== null) {
					$key = $value->getString() . ' ' . $key;
				}
				array_push($tos, $key);
			}
			$keys = $this->ccs->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->ccs->get($key);
				$key = '<' . $key->getString() . '>';
				$this->sendRequest('RCPT TO: ' . $key . $this->eof, 250);
				if ($value !== null) {
					$key = $value->getString() . ' ' . $key;
				}
				array_push($ccs, $key);
			}
			$keys = $this->bccs->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->bccs->get($key);
				$key = '<' . $key->getString() . '>';
				$this->sendRequest('RCPT TO: ' . $key . $this->eof, 250);
			}
			$this->sendRequest('DATA' . $this->eof, 354);
			$calendar = \php\util\Calendar::newInstanceByDateTime();
			$request = '';
			if ($this->subject !== null) {
				$request .= 'Subject: ' . $this->subject . $this->eof;
			}
			if ($email !== null) {
				if ($this->name !== null) {
					$email = $this->name . ' ' . $email;
				}
				$request .= 'From: ' . $email . $this->eof;
			}
			if (count($tos) > 0) {
				$request .= 'To: ' . implode(', ', $tos) . $this->eof;
			}
			if (count($ccs) > 0) {
				$request .= 'Cc: ' . implode(', ', $ccs) . $this->eof;
			}
			$request .= 'Content-Type: multipart/mixed; boundary="' . $calendar->toString()->getString() . '"' . $this->eof;
			$request .= 'MIME-Version: 1.0' . $this->eof;
			$request .= $this->eof;
			$request .= '--' . $calendar->toString()->getString() . $this->eof;
			$request .= 'Content-Type: text/plain; charset=UTF-8; format=flowed' . $this->eof;
			$request .= $this->eof;
			$request .= $this->body . $this->eof;
			$this->responseBody .= $request;
			$keys = $this->attachments->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->attachments->get($key);
				$subrequest = '';
				$subrequest .= '--' . $calendar->toString()->getString() . $this->eof;
				$subrequest .= 'Content-Type: ' . $key->getMimeType()->getString() . '; charset=UTF-8' . $this->eof;
				$subrequest .= 'Content-Transfer-Encoding: base64' . $this->eof;
				$subrequest .= 'Content-Disposition: attachment; filename="' . (($value === null) ? $key->getName()->getString() : $value->getString()) . '"' . $this->eof;
				$subrequest .= $this->eof;
				$contents = \php\lang\PHPString::newInstance('');
				$fileReader = \php\io\FileReader::newInstanceByFile($key);
				while (($data = $fileReader->readData(\php\lang\PHPNumber::newInstance(1024))) !== null) {
					$contents = $contents->append($data);
				}
				$fileReader->close();
				$request .= $subrequest . chunk_split(base64_encode($contents->getString()), 1024, $this->eof);
				$this->responseBody .= $subrequest . '***** base64 encoded file contents "' . $key->getPath()->getString() . '" *****' . $this->eof;
			}
			$subrequest = '';
			$subrequest .= '--' . $calendar->toString()->getString() . '--' . $this->eof;
			$subrequest .= "." . $this->eof;
			$request .= $subrequest;
			$this->responseBody .= $subrequest;
			fwrite($this->handle, $request);
			$this->checkReponseCode(250);
			$this->sendRequest('QUIT' . $this->eof, 221);
		}

		/**
		 * Returns the information between client and server
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return \php\lang\PHPString::newInstance($this->responseBody);
		}

	}

}
