<?php

namespace php\net {
	include_once('php/net/URI.php');
	include_once('php/lang/PHPString.php');

	class File extends URI {

		public static function newInstanceByParameters(\php\lang\PHPString $file) {
			return new File($file);
		}

		private $file;

		/**
		 * Constructs a File protocol URI
		 * @param \php\lang\PHPString $file
		 */
		protected function __construct(\php\lang\PHPString $file) {
			parent::__construct(\php\lang\PHPString::newInstance('file'));
			$this->file = $file->getString();
		}

		/**
		 * Returns the file of this File protocol URI
		 * @return \php\lang\PHPString
		 */
		public function getFile() {
			return (($this->file === null) ? null : \php\lang\PHPString::newInstance($this->file));
		}

		/**
		 * Returns an exist file present from this File protocol URI
		 * @return \php\io\File
		 */
		public function getRealFile() {
			return (($this->file === null) ? null : \php\io\File::newInstance($this->getFile()));
		}

		/**
		 * Returns a \php\lang\PHPString represent this File object
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			$return = $return->append(\php\lang\PHPString::newInstance('//'));
			$return = $return->append($this->getFile());
			return $return;
		}

	}

}