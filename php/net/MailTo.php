<?php

namespace php\net {
	include_once('php/net/URI.php');
	include_once('php/lang/PHPString.php');

	class MailTo extends URI {

		public static function newInstanceByParameters(\php\lang\PHPString $name, \php\lang\PHPString $domain) {
			return new MailTo($name, $domain);
		}

		private $name;
		private $domain;

		/**
		 * Constructs a MailTo protocol URI
		 * @param \php\lang\PHPString $name The name of this MailTo object
		 * @param \php\lang\PHPString $domain The domain of this MailTo object
		 */
		protected function __construct(\php\lang\PHPString $name, \php\lang\PHPString $domain) {
			parent::__construct(\php\lang\PHPString::newInstance('mailto'));
			$this->name = $name->getString();
			$this->domain = $domain->getString();
		}

		/**
		 * Returns the name of this MailTo protocol URI
		 * @return \php\lang\PHPString
		 */
		public function getName() {
			return (($this->name === null) ? null : \php\lang\PHPString::newInstance($this->name));
		}

		/**
		 * Returns the domain of this MailTo protocol URI
		 * @return \php\lang\PHPString
		 */
		public function getDomain() {
			return (($this->domain === null) ? null : \php\lang\PHPString::newInstance($this->domain));
		}

		/**
		 * Returns a \php\lang\PHPString represent this MailTo object
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			$return = $return->append($this->getName());
			$return = $return->append(\php\lang\PHPString::newInstance('@'));
			$return = $return->append($this->getDomain());
			return $return;
		}

	}

}