<?php

/**
 * Description of \php\net\URI
 */

namespace php\net {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');

	/**
	 * Represents a Uniform Resource Identifier (URI) reference.
	 */
	abstract class URI extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		public static function parseURI(\php\lang\PHPString $uri) {
			$uri = $uri->getString();
			$regex = '/^(?P<protocol>[A-Za-z\~\-\_\.][0-9A-Za-z\~\-\_\.]*):(?P<source>.+)$/';
			if (preg_match($regex, $uri, $matches) > 0) {
				$protocol = \php\lang\PHPString::newInstance($matches['protocol']);
				$source = $matches['source'];
				switch ($protocol->getString()) {
					case 'ftp':
					case 'http':
					case 'https': {
							$regex = '/^\/\/(?P<account>(?P<username>[^:\/@\?#]+)(?P<password>:[^:\/@\?#]*)?@)?(?P<host>[^:\/@\?#]+)(?P<port>:[^:\/@\?#]+)?(?P<path>\/[^\?#]*)(?P<query>\?[^#]*)?(?P<anchor>#.*)?$/';
							if (preg_match($regex, $source, $matches) > 0) {
								$username = null;
								$password = null;
								$port = \php\lang\PHPNumber::newInstance(-1);
								$query = null;
								$anchor = null;
								if (isset($matches['account']) && strlen($matches['account']) > 0) {
									$username = \php\lang\PHPString::newInstance($matches['username']);
								}
								if (isset($matches['password']) && strlen($matches['password']) > 0) {
									$password = \php\lang\PHPString::newInstance(sprintf('%s', substr($matches['password'], 1)));
								}
								$host = \php\lang\PHPString::newInstance($matches['host']);
								if (isset($matches['port']) && strlen($matches['port']) > 0) {
									$port = \php\lang\PHPNumber::newInstance(substr($matches['port'], 1));
								}
								$path = \php\lang\PHPString::newInstance($matches['path']);
								if (isset($matches['query'])) {
									$query = \php\lang\PHPString::newInstance(sprintf('%s', substr($matches['query'], 1)));
								}
								if (isset($matches['anchor'])) {
									$anchor = \php\lang\PHPString::newInstance(sprintf('%s', substr($matches['anchor'], 1)));
								}
								return URL::newInstanceByParameters($protocol, $host, $path, $username, $password, $port, $query, $anchor);
							} else {
								
							}
						} break;
					case 'mailto': {
							$regex = '/^(?P<name>[^@]+)@(?P<domain>.+)$/';
							if (preg_match($regex, $source, $matches) > 0) {
								$name = \php\lang\PHPString::newInstance($matches['name']);
								$domain = \php\lang\PHPString::newInstance($matches['domain']);
								return MailTo::newInstanceByParameters($name, $domain);
							} else {
								
							}
						} break;
					case 'file': {
							$regex = '/^\/\/(?P<file>\/.*)?$/';
							if (preg_match($regex, $source, $matches) > 0) {
								$file = \php\lang\PHPString::newInstance($matches['file']);
								return File::newInstanceByParameters($file);
							} else {
								
							}
						} break;
				}
			} else {
				throw new \Exception('no protocol');
			}
		}

		private $protocol;

		protected function __construct(\php\lang\PHPString $protocol) {
			parent::__construct();
			$this->protocol = $protocol->getString();
		}

		/**
		 * The URI protocol.
		 * @return \php\lang\PHPString
		 */
		public function getProtocol() {
			return \php\lang\PHPString::newInstance($this->protocol);
		}

		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append($this->getProtocol());
			$return = $return->append(\php\lang\PHPString::newInstance(':'));
			return $return;
		}

	}

}