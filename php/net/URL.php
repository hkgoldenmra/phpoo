<?php

/**
 * Description of \php\net\URL
 */

namespace php\net {
	include_once('php/net/URI.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');

	class URL extends URI {

		/**
		 * Returns a \php\net\URL object.
		 * @param \php\lang\PHPString $protocol Only http, https, ftp are accepted.
		 * @param \php\lang\PHPString $host A host server.
		 * @param \php\lang\PHPString $path A path in host server.
		 * @param \php\lang\PHPString $username The login username of this host server, default &lt;null&gt;.
		 * @param \php\lang\PHPString $password The login password of this host server, default &lt;null&gt;.
		 * @param \php\lang\PHPNumber $port The port connects to this host server.
		 * @param \php\lang\PHPString $query The string between ? and #.
		 * @param \php\lang\PHPString $anchor The string after #.
		 * @return \php\net\URL
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $protocol, \php\lang\PHPString $host, \php\lang\PHPString $path, \php\lang\PHPString $username = null, \php\lang\PHPString $password = null, \php\lang\PHPNumber $port = null, \php\lang\PHPString $query = null, \php\lang\PHPString $anchor = null) {
			return new URL($protocol, $host, $path, $username, $password, $port, $query, $anchor);
		}

		private $username;
		private $password;
		private $host;
		private $port = -1;
		private $path;
		private $query;
		private $anchor;

		/**
		 * Constructs a \php\net\URL object.
		 * @param \php\lang\PHPString $protocol Only http, https, ftp are accepted.
		 * @param \php\lang\PHPString $host A host server.
		 * @param \php\lang\PHPString $path A path in host server.
		 * @param \php\lang\PHPString $username The login username of this host server, default &lt;null&gt;.
		 * @param \php\lang\PHPString $password The login password of this host server, default &lt;null&gt;.
		 * @param \php\lang\PHPNumber $port The port connects to this host server.
		 * @param \php\lang\PHPString $query The string between ? and #.
		 * @param \php\lang\PHPString $anchor The string after #.
		 */
		protected function __construct(\php\lang\PHPString $protocol, \php\lang\PHPString $host, \php\lang\PHPString $path, \php\lang\PHPString $username = null, \php\lang\PHPString $password = null, \php\lang\PHPNumber $port = null, \php\lang\PHPString $query = null, \php\lang\PHPString $anchor = null) {
			parent::__construct($protocol);
			$this->host = $host->getString();
			$this->path = $path->getString();
			if ($username !== null) {
				$this->username = $username->getString();
			}
			if ($password !== null) {
				$this->password = $password->getString();
			}
			if ($port !== null) {
				$this->port = $port->getNumber();
			}
			if ($query !== null) {
				$this->query = $query->getString();
			}
			if ($anchor !== null) {
				$this->anchor = $anchor->getString();
			}
		}

		/**
		 * Returns the username of this URL.
		 * @return \php\lang\PHPString
		 */
		public function getUsername() {
			return (($this->username === null) ? null : \php\lang\PHPString::newInstance($this->username));
		}

		/**
		 * Returns the password of this URL.
		 * @return \php\lang\PHPString
		 */
		public function getPassword() {
			return (($this->password === null) ? null : \php\lang\PHPString::newInstance($this->password));
		}

		/**
		 * Returns the host of this URL.
		 * @return \php\lang\PHPString
		 */
		public function getHost() {
			return (($this->host === null) ? null : \php\lang\PHPString::newInstance($this->host));
		}

		/**
		 * Returns the port of this URL.
		 * @return \php\lang\PHPNumber
		 */
		public function getPort() {
			return \php\lang\PHPNumber::newInstance($this->port);
		}

		/**
		 * Returns the path of this URL.
		 * @return \php\lang\PHPString
		 */
		public function getPath() {
			return \php\lang\PHPString::newInstance($this->path);
		}

		/**
		 * Returns the query of this URL.
		 * @return \php\lang\PHPString
		 */
		public function getQuery() {
			return (($this->query === null) ? null : \php\lang\PHPString::newInstance($this->query));
		}

		/**
		 * Returns the anchor of this URL.
		 * @return \php\lang\PHPString
		 */
		public function getAnchor() {
			return (($this->anchor === null) ? null : \php\lang\PHPString::newInstance($this->anchor));
		}

		/**
		 * Returns host with port if port is presented.
		 * @return \php\lang\PHPString
		 */
		public function getFullHost() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append($this->getHost());
			$temp = $this->getPort();
			if ($temp->isNegative()->not()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance(':'));
				$return = $return->append($temp->toString());
			}
			return $return;
		}

		/**
		 * Returns path with query and anchor if they are presented.
		 * @return \php\lang\PHPString
		 */
		public function getFullPath() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append($this->getPath());
			if (($temp = $this->getQuery()) !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance('?'));
				$return = $return->append($temp);
			}
			if (($temp = $this->getAnchor()) !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance('#'));
				$return = $return->append($temp);
			}
			return $return;
		}

		/**
		 * Returns the complete URL of this URI.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			$return = $return->append(\php\lang\PHPString::newInstance('//'));
			if (($temp = $this->getUsername()) !== null) {
				$return = $return->append($temp);
				if (($temp = $this->getPassword()) !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(':'));
					$return = $return->append($temp);
				}
				$return = $return->append(\php\lang\PHPString::newInstance('@'));
			}
			$return = $return->append($this->getFullHost());
			$return = $return->append($this->getFullPath());
			return $return;
		}

	}

}