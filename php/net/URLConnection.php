<?php

/**
 * Description of \php\net\URLConnection
 */

namespace php\net {
	include_once('php/io/IOException.php');
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/io/FileReader.php');
	include_once('php/net/URL.php');
	include_once('php/util/collections/Map.php');

	/**
	 * The \php\net\URLConnection send a HTTP request and retrieve the relatve HTTP response.
	 */
	class URLConnection extends \php\lang\PHPObject {

		public static final function PROTOCOL_HTTP() {
			return \php\lang\PHPString::newInstance('HTTP');
		}

		public static final function HTTP_VERSION() {
			return \php\lang\PHPString::newInstance('1.1');
		}

		/**
		 * Returns the GET method string.
		 * @return \php\lang\PHPString
		 */
		public static final function METHOD_GET() {
			return \php\lang\PHPString::newInstance('GET');
		}

		/**
		 * Returns the POST method string.
		 * @return \php\lang\PHPString
		 */
		public static final function METHOD_POST() {
			return \php\lang\PHPString::newInstance('POST');
		}

		/**
		 * Returns the PUT method string.
		 * @return \php\lang\PHPString
		 */
		public static final function METHOD_PUT() {
			return \php\lang\PHPString::newInstance('PUT');
		}

		/**
		 * Returns the PATCH method string.
		 * @return \php\lang\PHPString
		 */
		public static final function METHOD_PATCH() {
			return \php\lang\PHPString::newInstance('PATCH');
		}

		/**
		 * Returns this DELETE method string.
		 * @return \php\lang\PHPString
		 */
		public static final function METHOD_DELETE() {
			return \php\lang\PHPString::newInstance('DELETE');
		}

		/**
		 * Returns the string of "Continue" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_CONTINUE() {
			return \php\lang\PHPString::newInstance('100');
		}

		/**
		 * Returns the string of "Switching Protocols" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_SWITCHING_PROTOCOLS() {
			return \php\lang\PHPString::newInstance('101');
		}

		/**
		 * Returns the string of "Processing" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PROCESSING() {
			return \php\lang\PHPString::newInstance('102');
		}

		/**
		 * Returns the string of "Early Hints" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_EARLY_HINTS() {
			return \php\lang\PHPString::newInstance('103');
		}

		/**
		 * Returns the string of "OK" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_OK() {
			return \php\lang\PHPString::newInstance('200');
		}

		/**
		 * Returns the string of "Created" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_CREATED() {
			return \php\lang\PHPString::newInstance('201');
		}

		/**
		 * Returns the string of "Accepted" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_ACCEPTED() {
			return \php\lang\PHPString::newInstance('202');
		}

		/**
		 * Returns the string of "Non-Authoritative Information" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NON_AUTHORITATIVE_INFORMATION() {
			return \php\lang\PHPString::newInstance('203');
		}

		/**
		 * Returns the string of "No Content" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NO_CONTENT() {
			return \php\lang\PHPString::newInstance('204');
		}

		/**
		 * Returns the string of "Reset Content" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_RESET_CONTENT() {
			return \php\lang\PHPString::newInstance('205');
		}

		/**
		 * Returns the string of "Partial Content" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PARTIAL_CONTENT() {
			return \php\lang\PHPString::newInstance('206');
		}

		/**
		 * Returns the string of "Multi-Status" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_MULTI_STATUS() {
			return \php\lang\PHPString::newInstance('207');
		}

		/**
		 * Returns the string of "Already Reported" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_ALREADY_REPORTED() {
			return \php\lang\PHPString::newInstance('208');
		}

		/**
		 * Returns the string of "I'm Used" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_IM_USED() {
			return \php\lang\PHPString::newInstance('226');
		}

		/**
		 * Returns the string of "Multiple Choices" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_MULTIPLE_CHOICES() {
			return \php\lang\PHPString::newInstance('300');
		}

		/**
		 * Returns the string of "Moved Permanently" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_MOVED_PERMANENTLY() {
			return \php\lang\PHPString::newInstance('301');
		}

		/**
		 * Returns the string of "Found" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_FOUND() {
			return \php\lang\PHPString::newInstance('302');
		}

		/**
		 * Returns the string of "See Other" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_SEE_OTHER() {
			return \php\lang\PHPString::newInstance('303');
		}

		/**
		 * Returns the string of "Not Modified" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NOT_MODIFIED() {
			return \php\lang\PHPString::newInstance('304');
		}

		/**
		 * Returns the string of "Use Proxy" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_USE_PROXY() {
			return \php\lang\PHPString::newInstance('305');
		}

		/**
		 * Returns the string of "Switch Proxy" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_SWITCH_PROXY() {
			return \php\lang\PHPString::newInstance('306');
		}

		/**
		 * Returns the string of "Temporary Redirect" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_TEMPORARY_REDIRECT() {
			return \php\lang\PHPString::newInstance('307');
		}

		/**
		 * Returns the string of "Permanent Redirect" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PERMANENT_REDIRECT() {
			return \php\lang\PHPString::newInstance('308');
		}

		/**
		 * Returns the string of "Bad Request" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_BAD_REQUEST() {
			return \php\lang\PHPString::newInstance('400');
		}

		/**
		 * Returns the string of "Unauthorzied" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_UNAUTHORIZED() {
			return \php\lang\PHPString::newInstance('401');
		}

		/**
		 * Returns the string of "Payment Required" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PAYMENT_REQUIRED() {
			return \php\lang\PHPString::newInstance('402');
		}

		/**
		 * Returns the string of "Forbidden" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_FORBIDDEN() {
			return \php\lang\PHPString::newInstance('403');
		}

		/**
		 * Returns the string of "Not Found" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NOT_FOUND() {
			return \php\lang\PHPString::newInstance('404');
		}

		/**
		 * Returns the string of "Method Not Allowed" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_METHOD_NOT_ALLOWED() {
			return \php\lang\PHPString::newInstance('405');
		}

		/**
		 * Returns the string of "Not Acceptable" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NOT_ACCEPTABLE() {
			return \php\lang\PHPString::newInstance('406');
		}

		/**
		 * Returns the string of "Proxy Authentication Required" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PROXY_AUTHENTICATION_REQUIRED() {
			return \php\lang\PHPString::newInstance('407');
		}

		/**
		 * Returns the string of "Request Timeout" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_REQUEST_TIMEOUT() {
			return \php\lang\PHPString::newInstance('408');
		}

		/**
		 * Returns the string of "Conflict" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_CONFLICT() {
			return \php\lang\PHPString::newInstance('409');
		}

		/**
		 * Returns the string of "Gone" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_GONE() {
			return \php\lang\PHPString::newInstance('410');
		}

		/**
		 * Returns the string of "Length Required" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_LENGTH_REQUIRED() {
			return \php\lang\PHPString::newInstance('411');
		}

		/**
		 * Returns the string of "Precondition Failed" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PRECONDITION_FAILED() {
			return \php\lang\PHPString::newInstance('412');
		}

		/**
		 * Returns the string of "Request Entity Too Large" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_REQUEST_ENTITY_TOO_LARGE() {
			return \php\lang\PHPString::newInstance('413');
		}

		/**
		 * Returns the string of "Request URI Too Long" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_REQUEST_URI_TOO_LONG() {
			return \php\lang\PHPString::newInstance('414');
		}

		/**
		 * Returns the string of "Unsupported Media Type" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_UNSUPPORTED_MEDIA_TYPE() {
			return \php\lang\PHPString::newInstance('415');
		}

		/**
		 * Returns the string of "Requested Range Not Satisfiable" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_REQUESTED_RANGE_NOT_SATISFIABLE() {
			return \php\lang\PHPString::newInstance('416');
		}

		/**
		 * Returns the string of "Expectation Failed" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_EXPECTATION_FAILED() {
			return \php\lang\PHPString::newInstance('417');
		}

		/**
		 * Returns the string of "I'm A Teapot" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_I_M_A_TEAPOT() {
			return \php\lang\PHPString::newInstance('418');
		}

		/**
		 * Returns the string of "Misdirected Request" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_MISDIRECTED_REQUEST() {
			return \php\lang\PHPString::newInstance('421');
		}

		/**
		 * Returns the string of "Unprocessable Entity" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_UNPROCESSABLE_ENTITY() {
			return \php\lang\PHPString::newInstance('422');
		}

		/**
		 * Returns the string of "Locked" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_LOCKED() {
			return \php\lang\PHPString::newInstance('423');
		}

		/**
		 * Returns the string of "Failed Dependency" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_FAILED_DEPENDENCY() {
			return \php\lang\PHPString::newInstance('424');
		}

		/**
		 * Returns the string of "Upgrade Required" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_UPGRADE_REQUIRED() {
			return \php\lang\PHPString::newInstance('426');
		}

		/**
		 * Returns the string of "Precondition Requred" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_PRECONDITION_REQUIRED() {
			return \php\lang\PHPString::newInstance('428');
		}

		/**
		 * Returns the string of "Too Many Requests" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_TOO_MANY_REQUESTS() {
			return \php\lang\PHPString::newInstance('429');
		}

		/**
		 * Returns the string of "Request Header Fields Too Large" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_REQUEST_HEADER_FIELDS_TOO_LARGE() {
			return \php\lang\PHPString::newInstance('431');
		}

		/**
		 * Returns the string of "Unavailable For Legal Reasons" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_UNAVAILABLE_FOR_LEGAL_REASONS() {
			return \php\lang\PHPString::newInstance('451');
		}

		/**
		 * Returns the string of "Request Header Too Large" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_REQUEST_HEADER_TOO_LARGE() {
			return \php\lang\PHPString::newInstance('494');
		}

		/**
		 * Returns the string of "Internal Server Error" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_INTERNAL_SERVER_ERROR() {
			return \php\lang\PHPString::newInstance('500');
		}

		/**
		 * Returns the string of "Not Implemeneted" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NOT_IMPLEMENTED() {
			return \php\lang\PHPString::newInstance('501');
		}

		/**
		 * Returns the string of "Bad Gateway" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_BAD_GATEWAY() {
			return \php\lang\PHPString::newInstance('502');
		}

		/**
		 * Returns the string of "Service Unavailable" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_SERVICE_UNAVAILABLE() {
			return \php\lang\PHPString::newInstance('503');
		}

		/**
		 * Returns the string of "Gateway Timeout" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_GATEWAY_TIMEOUT() {
			return \php\lang\PHPString::newInstance('504');
		}

		/**
		 * Returns the string of "HTTP Version Not Supported" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_HTTP_VERSION_NOT_SUPPORTED() {
			return \php\lang\PHPString::newInstance('505');
		}

		/**
		 * Returns the string of "Variant Also Negotiates" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_VARIANT_ALSO_NEGOTIATES() {
			return \php\lang\PHPString::newInstance('506');
		}

		/**
		 * Returns the string of "Insufficient Storage" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_INSUFFICIENT_STORAGE() {
			return \php\lang\PHPString::newInstance('507');
		}

		/**
		 * Returns the string of "Loop Detected" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_LOOP_DETECTED() {
			return \php\lang\PHPString::newInstance('508');
		}

		/**
		 * Returns the string of "Not Extended" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NOT_EXTENDED() {
			return \php\lang\PHPString::newInstance('510');
		}

		/**
		 * Returns the string of "Network AUthentication Required" response code.
		 * @return \php\lang\PHPString
		 */
		public static final function STATUS_NETWORK_AUTHENTICATION_REQUIRED() {
			return \php\lang\PHPString::newInstance('511');
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\net\HttpConnection object.
		 * @param \php\net\URL $url The URL will send HTTP request.
		 * @param \php\lang\PHPString $requestMethod The HTTP request method, Default GET.
		 */
		public static function newInstanceByURL(URL $url, \php\lang\PHPString $requestMethod = null) {
			return new URLConnection($url, $requestMethod);
		}

		private $handle;
		private $head;
		private $headers;
		private $body = '';
		private $responseCode = null;
		private $responseMessage;
		private $responseHeaders;
		private $responseBody;

		/**
		 * Constructs a \php\net\HttpConnection object.
		 * @param \php\net\URL $url The URL will send HTTP request.
		 * @param \php\lang\PHPString $requestMethod The HTTP request method, Default GET.
		 */
		protected function __construct(URL $url, \php\lang\PHPString $requestMethod = null) {
			parent::__construct();
			$this->headers = \php\util\collections\Map::newInstance();
			$this->responseHeaders = \php\util\collections\Map::newInstance();
			if ($requestMethod === null) {
				$requestMethod = \php\lang\PHPString::newInstance('GET');
			}
			$protocol = $url->getProtocol()->getString();
			$host = $url->getFullHost();
			$path = $url->getFullPath();
			$port = $url->getPort()->getNumber();
			if ($protocol == 'https') {
				$protocol = 'ssl://';
				if ($port < 0) {
					$port = 443;
				}
			} else if ($protocol == 'http') {
				$protocol = '';
				if ($port < 0) {
					$port = 80;
				}
			} else if ($protocol == 'ftp') {
				$protocol = '';
				if ($port < 0) {
					$port = 21;
				}
			}
			$server = $protocol . $url->getHost()->getString();
			$errno = 0;
			$error = '';
			if ($this->handle = @fsockopen($server, $port, $errno, $error)) {
				$this->head = sprintf("%s %s %s/%s", $requestMethod->getString(), $path, self::PROTOCOL_HTTP()->getString(), self::HTTP_VERSION()->getString());
				$os_name = null;
				$os_version = null;
				if (1) {
					$output = @shell_exec('sw_vers 2>/dev/null');
					if ($output !== null) {
						$output = explode("\n", $output);
						foreach ($output as $v) {
							$v = trim($v);
							if (preg_match('/^ProductName:\\s+(?P<name>.+)/', $v, $matches) > 0) {
								$os_name = $matches['name'];
							} else if (preg_match('/^ProductVersion:\\s+(?P<version>.+)/', $v, $matches) > 0) {
								$os_version = $matches['version'];
							}
						}
					}
					$output = @shell_exec('cat /etc/*release 2>/dev/null');
					if ($output !== null) {
						$output = explode("\n", $output);
						foreach ($output as $v) {
							$v = trim($v);
							if ($os_name === null && (preg_match('/^NAME="(?P<name>[^"]+)"/', $v, $matches) > 0 || preg_match('/^DISTRIB_ID=(?P<name>.+)/', $v, $matches) > 0)) {
								$os_name = $matches['name'];
							} else if ($os_version === null && (preg_match('/^VERSION="(?P<version>[0-9.-_]+)"/', $v, $matches) > 0 || preg_match('/^DISTRIB_RELEASE=(?P<version>[0-9.-_]+)/', $v, $matches) > 0)) {
								$os_version = $matches['version'];
							} else if ($os_name === null && $os_version === null && preg_match('/^(?P<name>.+) (?P<version>[0-9.-_]+)/', $v, $matches) > 0) {
								$os_name = $matches['name'];
								$os_version = $matches['version'];
							}
						}
					}
				}
				if ($os_name === null) {
					$os_name = php_uname('s');
				}
				if ($os_version === null) {
					$os_version = php_uname('r');
				}
				$os_arch = php_uname('m');
				$browser_version = explode('-', phpversion());
				$browser_version = $browser_version[0];
				$engine = explode(' ', filter_input(INPUT_SERVER, 'SERVER_SOFTWARE'));
				if (count(explode('/', $engine[0])) == 1) {
					$engine[0] .= '/*';
				}
				$engine = $engine[0];
				$this->addHeader(\php\lang\PHPString::newInstance('Host'), $host);
				$this->addHeader(\php\lang\PHPString::newInstance('User-Agent'), \php\lang\PHPString::newInstance(sprintf('Mozilla/5.0 (%s %s %s) %s PHP/%s', $os_name, $os_version, $os_arch, $engine, $browser_version)));
				$this->addHeader(\php\lang\PHPString::newInstance('Connection'), \php\lang\PHPString::newInstance('close'));
			} else {
				$error = explode(': ', $error);
				throw new \php\io\IOException(end($error));
			}
		}

		/**
		 * Adds extra HTTP request header to this HTTP request.
		 * @param \php\lang\PHPString $key The HTTP request header key.
		 * @param \php\lang\PHPObject $value  The HTTP request header value.
		 */
		public function addHeader(\php\lang\PHPString $key, \php\lang\PHPObject $value) {
			$this->headers->put($key, $value);
		}

		/**
		 * Adds extra content HTTP request body to this HTTP request.
		 * @param \php\lang\PHPString $body The HTTP request body.
		 */
		public function addBodyString(\php\lang\PHPObject $body) {
			$this->body .= $body->toString()->getString();
		}

		/**
		 * Add request body from a file contents.
		 * @param \php\io\File $file The file of request.
		 */
		public function addBodyFile(\php\io\File $file) {
			$reader = \php\io\FileReader::newInstanceByFile($file);
			while (($string = $reader->readData(\php\lang\PHPNumber::newInstance(1024 * 1024))) !== null) {
				$this->addBodyString($string);
			}
			$reader->close();
		}

		private function toHeader(\php\util\collections\Map $headers) {
			$return = '';
			$keys = $headers->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $headers->get($key);
				$return .= sprintf("\n%s: %s", $key->getString(), $value->getString());
			}
			return $return;
		}

		/**
		 * Sends HTTP request from this connection.
		 * @param \php\lang\PHPBoolean $headerOnly Send HTTP request with headers only, default false.
		 */
		public function send(\php\lang\PHPBoolean $headerOnly = null) {
			$crlf = "\r\n";
			if ($headerOnly === null) {
				$headerOnly = \php\lang\PHPBoolean::newInstance(false);
			}
			$headerOnly = $headerOnly->getBoolean();
			$this->addHeader(\php\lang\PHPString::newInstance('Content-Length'), \php\lang\PHPString::newInstance(strlen($this->body)));
			fwrite($this->handle, $this->head);
			fwrite($this->handle, $this->toHeader($this->headers));
			fwrite($this->handle, "\n\n");
			fwrite($this->handle, $this->body);
			$return = '';
			while (($string = fgets($this->handle)) !== false) {
				$return .= $string;
				if ($headerOnly && (strpos($return, $crlf . $crlf) !== false)) {
					break;
				}
			}
			fclose($this->handle);
			$responses = explode($crlf . $crlf, $return, 2);
			if (!$headerOnly) {
				$this->responseBody = $responses[1];
			}
			$headers = explode($crlf, $responses[0]);
			$heads = explode(' ', array_shift($headers), 3);
			$this->responseCode = $heads[1];
			$this->responseMessage = $heads[2];
			foreach ($headers as $header) {
				$fields = explode(': ', $header);
				$this->responseHeaders->put(\php\lang\PHPString::newInstance($fields[0]), \php\lang\PHPString::newInstance($fields[1]));
				if (!$headerOnly && $fields[0] == 'Transfer-Encoding' && $fields[1] == 'chunked') {
					$this->responseBody = '';
					$contentLength = 0;
					$length = 1;
					while ($length > 0) {
						$fields = explode($crlf, $responses[1], 2);
						$length = hexdec($fields[0]);
						$contentLength += $length;
						$this->responseBody .= substr($fields[1], 0, $length);
						$responses[1] = substr($fields[1], $length + 2);
					}
					$this->responseHeaders->put(\php\lang\PHPString::newInstance('Content-Length'), \php\lang\PHPString::newInstance($contentLength));
				}
			}
			//echo $this->toString()->getString();
		}

		/**
		 * Returns the HTTP response code.
		 * @return \php\lang\PHPString
		 */
		public function getResponseCode() {
			return \php\lang\PHPString::newInstance($this->responseCode);
		}

		/**
		 * Returns the HTTP response message.
		 * @return \php\lang\PHPString
		 */
		public function getResponseMessage() {
			return \php\lang\PHPString::newInstance($this->responseMessage);
		}

		/**
		 * Returns the HTTP response headers.
		 * @return \php\util\collections\Map
		 */
		public function getResponseHeaders() {
			return $this->responseHeaders;
		}

		/**
		 * Returns the HTTP response body.
		 * @return \php\lang\PHPString
		 */
		public function getResponseBody() {
			return \php\lang\PHPString::newInstance($this->responseBody);
		}

		public function copy() {
			$return = parent::copy();
			$return->headers = $return->headers->copy();
			$return->responseHeaders = $return->responseHeaders->copy();
			return $return;
		}

		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance($this->head));
			$return = $return->append(\php\lang\PHPString::newInstance($this->toHeader($this->headers)));
			$return = $return->append(\php\lang\PHPString::newInstance("\n\n"));
			$return = $return->append(\php\lang\PHPString::newInstance($this->body));
			if ($this->responseCode !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance("\n\n"));
				$return = $return->append(self::PROTOCOL_HTTP());
				$return = $return->append(\php\lang\PHPString::newInstance("/"));
				$return = $return->append(self::HTTP_VERSION());
				$return = $return->append(\php\lang\PHPString::newInstance(" "));
				$return = $return->append($this->getResponseCode()->toString());
				$return = $return->append(\php\lang\PHPString::newInstance(" "));
				$return = $return->append($this->getResponseMessage());
				$return = $return->append(\php\lang\PHPString::newInstance($this->toHeader($this->getResponseHeaders())));
				$return = $return->append(\php\lang\PHPString::newInstance("\n\n"));
				$return = $return->append(\php\lang\PHPString::newInstance($this->getResponseBody()));
			}
			return $return;
		}

	}

}
