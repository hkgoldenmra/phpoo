<?php

/**
 * Description of \php\sql\AliasField
 */

namespace php\sql {
	include_once('php/lang/PHPString.php');
	include_once('php/sql/Field.php');

	/**
	 * The \php\sql\Field class is the fields with alias for a SQL query string.
	 */
	class AliasField extends Field {

		/**
		 * Returns a \php\sql\AliasField object.
		 * @param \php\lang\PHPString $field The SQL field.
		 * @param \php\lang\PHPString $alias The alias of SQL field.
		 * @return \php\sql\AliasField
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field, \php\lang\PHPString $alias = null) {
			return new AliasField($field, $alias);
		}

		private $alias;

		/**
		 * Constructs a \php\sql\AliasField object.
		 * @param \php\lang\PHPString $field The SQL field.
		 * @param \php\lang\PHPString $alias The alias of SQL field.
		 */
		protected function __construct(\php\lang\PHPString $field, \php\lang\PHPString $alias = null) {
			parent::__construct($field);
			if ($alias !== null) {
				$this->alias = $alias->getString();
			}
		}

		/**
		 * Returns the alias value.
		 * @return \php\lang\PHPString
		 */
		public function getAlias() {
			return (($this->alias === null) ? null : \php\lang\PHPString::newInstance($this->alias));
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append($this->getField());
			if (($alias = $this->getAlias()) !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance(' AS '));
				$return = $return->append($alias);
			}
			return $return;
		}

	}

}