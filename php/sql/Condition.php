<?php

/**
 * Description of \php\sql\Condition
 */

namespace php\sql {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');

	/**
	 * The \php\sql\Condition class is the condition for a SQL query string.
	 */
	abstract class Condition extends \php\lang\PHPObject {

		public static function newInstance() {
			self::unsupportedFunction();
		}

		private $field;
		private $isAnd;
		private $hasNot;

		/**
		 * Constructs a \php\sql\Condition object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 */
		protected function __construct(\php\lang\PHPString $field, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			parent::__construct();
			if ($isAnd === null) {
				$isAnd = \php\lang\PHPBoolean::newInstance(true);
			}
			if ($hasNot === null) {
				$hasNot = \php\lang\PHPBoolean::newInstance(false);
			}
			$this->field = $field->getString();
			$this->isAnd = $hasNot->getBoolean();
			$this->hasNot = $hasNot->getBoolean();
		}

		/**
		 * Returns the select field.
		 * @return \php\lang\PHPString
		 */
		public function getField() {
			return \php\lang\PHPString::newInstance($this->field);
		}

		/**
		 * Is true (AND) or false (NO).
		 * @return \php\lang\PHPBoolean
		 */
		public function isAnd() {
			return \php\lang\PHPBoolean::newInstance($this->isAnd);
		}

		/**
		 * Returns the condition has &quot;NOT&quot; or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function hasNot() {
			return \php\lang\PHPBoolean::newInstance($this->hasNot);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance(($this->hasNot()->getBoolean()) ? 'NOT ' : ''));
			return $return;
		}

	}

}