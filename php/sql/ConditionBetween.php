<?php

/**
 * Description of \php\sql\ConditionBetween
 */

namespace php\sql {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/sql/Condition.php');

	/**
	 * The \php\sql\ConditionBetween class is the condition of the field in a range for a SQL query string.
	 */
	class ConditionBetween extends Condition {

		/**
		 * Returns a \php\sql\ConditionBetween object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPString $lower The lower value.
		 * @param \php\lang\PHPString $upper The upper value.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 * @return \php\sql\ConditionNull
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field, \php\lang\PHPString $lower, \php\lang\PHPString $upper, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			return new ConditionNull($field, $lower, $upper, $isAnd, $hasNot);
		}

		private $lower;
		private $upper;

		/**
		 * Constructs a \php\sql\ConditionBetween object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPString $lower The lower value.
		 * @param \php\lang\PHPString $upper The upper value.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 */
		protected function __construct(\php\lang\PHPString $field, \php\lang\PHPString $lower, \php\lang\PHPString $upper, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			parent::__construct($field, $isAnd, $hasNot);
			$this->lower = $lower->getString();
			$this->upper = $upper->getString();
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = parent::toString();
			$return = $return->append(\php\lang\PHPString::newInstance(' (('));
			$return = $return->append(\php\lang\PHPString::newInstance($this->lower));
			$return = $return->append(\php\lang\PHPString::newInstance(' >= '));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' AND '));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' >= '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->upper));
			$return = $return->append(\php\lang\PHPString::newInstance(') OR ('));
			$return = $return->append(\php\lang\PHPString::newInstance($this->lower));
			$return = $return->append(\php\lang\PHPString::newInstance(' <= '));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' AND '));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' <= '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->upper));
			$return = $return->append(\php\lang\PHPString::newInstance('))'));
			return $return;
		}

	}

}