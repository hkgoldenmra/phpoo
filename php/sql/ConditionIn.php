<?php

/**
 * Description of \php\sql\ConditionIn
 */

namespace php\sql {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/collections/LinkedList.php');
	include_once('php/sql/Condition.php');

	/**
	 * The \php\sql\ConditionIn class is the condition of the field in the list for a SQL query string.
	 */
	class ConditionIn extends Condition {

		/**
		 * Returns a \php\sql\ConditionIn object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\util\collections\LinkedList $list The list to check for.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 * @return \php\sql\ConditionIn
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field, \php\util\collections\LinkedList $list, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			return new ConditionIn($field, $list, $hasNot);
		}

		private $list;

		/**
		 * Constructs a \php\sql\ConditionIn object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\util\collections\LinkedList $list The list to check for.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 */
		protected function __construct(\php\lang\PHPString $field, \php\util\collections\LinkedList $list, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			parent::__construct($field, $isAnd, $hasNot);
			$this->list = $list;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = parent::toString();
			$return = $return->append(\php\lang\PHPString::newInstance(' ('));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' IN ('));
			$string = $this->list->toString();
			$string = $string->substring(\php\lang\PHPNumber::ONE(), $string->length()->descrease());
			$return = $return->append($string);
			$return = $return->append(\php\lang\PHPString::newInstance('))'));
			return $return;
		}

	}

}