<?php

/**
 * Description of \php\sql\ConditionNull
 */

namespace php\sql {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/sql/Condition.php');

	/**
	 * The \php\sql\ConditionBetween class is the condition of the field is null for a SQL query string.
	 */
	class ConditionNull extends Condition {

		/**
		 * Returns a \php\sql\ConditionNull object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 * @return \php\sql\ConditionNull
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			return new ConditionNull($field, $hasNot);
		}

		/**
		 * Constructs a \php\sql\ConditionNull object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 */
		protected function __construct(\php\lang\PHPString $field, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			parent::__construct($field, $isAnd, $hasNot);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = parent::toString();
			$return = $return->append(\php\lang\PHPString::newInstance(' ('));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' IS NULL)'));
			return $return;
		}

	}

}