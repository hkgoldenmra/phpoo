<?php

/**
 * Description of \php\sql\ConditionOperator
 */

namespace php\sql {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/sql/Condition.php');

	/**
	 * The \php\sql\ConditionOperator class is the condition of the field operates the value for a SQL query string.
	 */
	class ConditionOperator extends Condition {

		/**
		 * Returns the equal operator.
		 * @return \php\lang\PHPNumber
		 */
		public static final function EQUAL() {
			return \php\lang\PHPNumber::newInstance(0);
		}

		/**
		 * Returns the unequal operator.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNEQUAL() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the greator operator.
		 * @return \php\lang\PHPNumber
		 */
		public static final function GREATER() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the greator or equal operator.
		 * @return \php\lang\PHPNumber
		 */
		public static final function GREATER_OR_EQUAL() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the less operator.
		 * @return \php\lang\PHPNumber
		 */
		public static final function LESS() {
			return \php\lang\PHPNumber::newInstance(4);
		}

		/**
		 * Returns the less or equal operator.
		 * @return \php\lang\PHPNumber
		 */
		public static final function LESS_OR_EQUAL() {
			return \php\lang\PHPNumber::newInstance(5);
		}

		private static $operators = array(
			0 => '=',
			1 => '<>',
			2 => '>',
			3 => '>=',
			4 => '<',
			5 => '<=',
		);

		/**
		 * Returns a \php\sql\ConditionOperator object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPNumber $operator The checked operator.
		 * @param \php\lang\PHPObject $value The value to check for.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 * @return \php\sql\ConditionOperator
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field, \php\lang\PHPNumber $operator, \php\lang\PHPObject $value, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			return new ConditionOperator($field, $operator, $value, $isAnd, $hasNot);
		}

		private $operator;
		private $value;

		/**
		 * Constructs a \php\sql\ConditionOperator object.
		 * @param \php\lang\PHPString $field The field to be checked.
		 * @param \php\lang\PHPNumber $operator The checked operator.
		 * @param \php\lang\PHPObject $value The value to check for.
		 * @param \php\lang\PHPBoolean $isAnd The connection to the previous condition when true is AND and false is OR. Default &lt;true&gt;.
		 * @param \php\lang\PHPBoolean $hasNot The condition has &quot;NOT&quot; or not. Default &lt;false&gt;.
		 */
		protected function __construct(\php\lang\PHPString $field, \php\lang\PHPNumber $operator, \php\lang\PHPObject $value, \php\lang\PHPBoolean $isAnd = null, \php\lang\PHPBoolean $hasNot = null) {
			parent::__construct($field, $isAnd, $hasNot);
			if (!in_array($operator->getNumber(), array_keys(self::$operators))) {
				$operator = self::EQUAL();
			}
			$this->operator = $operator->getNumber();
			$this->value = $value->toString()->getString();
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = parent::toString();
			$return = $return->append(\php\lang\PHPString::newInstance('('));
			$return = $return->append($this->getField());
			$return = $return->append(\php\lang\PHPString::newInstance(' '));
			$return = $return->append(\php\lang\PHPString::newInstance(self::$operators[$this->operator]));
			$return = $return->append(\php\lang\PHPString::newInstance(' '));
			$return = $return->append(\php\lang\PHPString::newInstance($this->value));
			$return = $return->append(\php\lang\PHPString::newInstance(')'));
			return $return;
		}

	}

}