<?php

/**
 * Description of \php\sql\Field
 */

namespace php\sql {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');

	/**
	 * The \php\sql\Field class is the fields for a SQL query string.
	 */
	class Field extends \php\lang\PHPObject {

		/**
		 * Returns a \php\sql\Field object.
		 * @param \php\lang\PHPString $field The SQL field.
		 * @return \php\sql\Field
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field) {
			return new Field($field);
		}

		public static function newInstance() {
			self::unsupportedFunction();
		}

		private $field;

		/**
		 * Constructs a \php\sql\Field object.
		 * @param \php\lang\PHPString $field The SQL field.
		 */
		protected function __construct(\php\lang\PHPString $field) {
			parent::__construct();
			$this->field = $field->getString();
		}

		/**
		 * Returns the field value.
		 * @return \php\lang\PHPString
		 */
		public function getField() {
			return (($this->field === null) ? null : \php\lang\PHPString::newInstance($this->field));
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return $this->getField();
		}

	}

}