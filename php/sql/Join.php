<?php

/**
 * Description of \php\sql\Join
 */

namespace php\sql {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/sql/AliasField.php');

	/**
	 * 
	 */
	class Join extends \php\lang\PHPObject {

		/**
		 * Returns &lt;empty&gt; join type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function NONE() {
			return \php\lang\PHPNumber::newInstance(0);
		}

		/**
		 * Returns LEFT join type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function LEFT() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns RIGHT join type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function RIGHT() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns INNER join type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function INNER() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns OUTER join type.
		 * @return \php\lang\PHPNumber
		 */
		public static final function OUTER() {
			return \php\lang\PHPNumber::newInstance(4);
		}

		private static $types = array(
			0 => null,
			1 => 'LEFT',
			2 => 'RIGHT',
			3 => 'INNER',
			4 => 'OUTER'
		);

		/**
		 * Returns a \php\sql\Join object.
		 * @param \php\sql\Field $from A joined table.
		 * @param \php\sql\Condition $condition A condition joins two tables.
		 * @param \php\lang\PHPNumber $type The join type. Default &lt;empty&gt;.
		 * @return \php\sql\Join
		 */
		public static function newInstanceByParameters(Field $from, Condition $condition, \php\lang\PHPNumber $type = null) {
			return new Join($from, $condition, $type);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private $from;
		private $type;
		private $conditions;

		/**
		 * Constructs a \php\sql\Join object.
		 * @param \php\sql\Field $from A joined table.
		 * @param \php\sql\Condition $condition A condition joins two tables.
		 * @param \php\lang\PHPNumber $type The join type. Default &lt;empty&gt;.
		 */
		protected function __construct(Field $from, Condition $condition, \php\lang\PHPNumber $type = null) {
			parent::__construct();
			$this->from = $from;
			if ($type === null || !in_array($type->getNumber(), array_keys(self::$types))) {
				$type = self::NONE();
			}
			$this->type = $type->getNumber();
			$this->conditions = \php\util\collections\Map::newInstance();
			$this->addCondition($condition);
		}

		/**
		 * Adds extra condition joins two tables.
		 * @param \php\sql\Condition $condition A condition joins two tables.
		 * @param \php\lang\PHPNumber $key The group of conditions.
		 */
		public function addCondition(Condition $condition, \php\lang\PHPNumber $key = null) {
			if ($this->conditions->containsKey($key)->not()->getBoolean()) {
				$this->conditions->put($key, \php\util\collections\Vector::newInstance());
			}
			$this->conditions->get($key)->add($condition);
		}

		/**
		 * Returns the joined table.
		 * @return \php\sql\Field
		 */
		public function getFrom() {
			return $this->from;
		}

		/**
		 * Returns the join type.
		 * @return \php\lang\PHPNumber
		 */
		public function getType() {
			return \php\lang\PHPNumber::newInstance($this->type);
		}

		/**
		 * Returns the group of conditions.
		 * @return \php\util\collections\Map
		 */
		public function getConditions() {
			return $this->conditions;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			if (($type = self::$types[$this->getType()->getNumber()]) !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance($type . ' '));
			}
			$return = $return->append(\php\lang\PHPString::newInstance('JOIN '));
			$return = $return->append($this->getFrom()->toString());
			$return = $return->append(\php\lang\PHPString::newInstance(' ON '));
			$keys = $this->conditions->keySet();
			$size = $keys->size();
			if ($size->isPositive()->getBoolean()) {
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$key = $keys->get($i);
					$values = $this->conditions->get($key);
					$vsize = $values->size();
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n" . (($values->get(\php\lang\PHPNumber::ZERO())->isAnd()->getBoolean()) ? 'AND ' : 'OR ')));
					}
					$return = $return->append(\php\lang\PHPString::newInstance('('));
					for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($vsize)->getBoolean(); $j = $j->increase()) {
						$value = $values->get($j);
						if ($j->isPositive()->getBoolean()) {
							$return = $return->append(\php\lang\PHPString::newInstance(' ' . (($value->isAnd()) ? 'AND' : 'OR') . ' '));
						}
						$return = $return->append($value->toString());
					}
					$return = $return->append(\php\lang\PHPString::newInstance(')'));
				}
			}
			return $return;
		}

	}

}