<?php

/**
 * Description of \php\sql\QueryBuilder
 */

namespace php\sql {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/collections/Map.php');
	include_once('php/sql/Field.php');
	include_once('php/sql/AliasField.php');
	include_once('php/sql/SortField.php');
	include_once('php/sql/Join.php');
	include_once('php/sql/Condition.php');

	/**
	 * The \php\sql\QueryBuilder class helps to create a standard SQL query string.
	 */
	class QueryBuilder extends \php\lang\PHPObject {

		/**
		 * Returns a \php\sql\QueryBuilder object.
		 * @return \php\sql\QueryBuilder
		 */
		public static function newInstance() {
			return new QueryBuilder();
		}

		private $selects;
		private $froms;
		private $joins;
		private $wheres;
		private $groupBys;
		private $havings;
		private $orderBys;
		private $start;
		private $limit;
		private $fields;

		/**
		 * Constructs a \php\sql\QueryBuilder object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->selects = \php\util\collections\Vector::newInstance();
			$this->froms = \php\util\collections\Vector::newInstance();
			$this->joins = \php\util\collections\Vector::newInstance();
			$this->wheres = \php\util\collections\Map::newInstance();
			$this->groupBys = \php\util\collections\Vector::newInstance();
			$this->havings = \php\util\collections\Map::newInstance();
			$this->orderBys = \php\util\collections\Vector::newInstance();
			$this->fields = \php\util\collections\Map::newInstance();
		}

		/**
		 * Adds a select field to this builder.
		 * @param \php\sql\AliasField $select The table field, string, function.
		 */
		public function addSelect(Field $select) {
			$this->selects->add($select);
		}

		/**
		 * Adds a table to this builder.
		 * @param \php\sql\AliasField $from The table or sub-query.
		 */
		public function addFrom(Field $from) {
			$this->froms->add($from);
		}

		/**
		 * Adds a join table to this builder.
		 * @param \php\sql\Join $join The join table.
		 */
		public function addJoin(Join $join) {
			$this->joins->add($join);
		}

		/**
		 * Add a where condition to this builder.
		 * @param \php\sql\Condition $where The condition.
		 * @param \php\lang\PHPNumber $key The group of conditions.
		 */
		public function addWhere(Condition $where, \php\lang\PHPNumber $key = null) {
			if ($this->wheres->containsKey($key)->not()->getBoolean()) {
				$this->wheres->put($key, \php\util\collections\Vector::newInstance());
			}
			$this->wheres->get($key)->add($where);
		}

		/**
		 * Add a group by field to this builder.
		 * @param \php\lang\PHPString $groupBy The field to group.
		 */
		public function addGroupBy(\php\lang\PHPString $groupBy) {
			$this->groupBys->add($groupBy);
		}

		/**
		 * Add a having condition to this builder.
		 * @param \php\sql\Condition $having The condition.
		 * @param \php\lang\PHPNumber $key The group of conditions.
		 */
		public function addHaving(Condition $having, \php\lang\PHPNumber $key = null) {
			if ($this->havings->containsKey($key)->not()->getBoolean()) {
				$this->havings->put($key, \php\util\collections\Vector::newInstance());
			}
			$this->havings->get($key)->add($having);
		}

		/**
		 * Adds a order by field to this builder.
		 * @param \php\sql\SortField $orderBy The field to sort.
		 */
		public function addOrderBy(SortField $orderBy) {
			$this->orderBys->add($orderBy);
		}

		/**
		 * Sets the start index to this builder.
		 * @param \php\lang\PHPNumber $start Maximum entries of the query.
		 */
		public function setStart(\php\lang\PHPNumber $start) {
			$start = $start->getInteger()->getNumber();
			if ($start >= 0) {
				$this->start = $start;
			}
		}

		/**
		 * Sets this limit offset to this builder.
		 * @param \php\lang\PHPNumber $limit The start index of the query.
		 */
		public function setLimit(\php\lang\PHPNumber $limit) {
			$limit = $limit->getInteger()->getNumber();
			if ($limit > 0) {
				$this->limit = $limit;
			}
		}

		public function setField(\php\lang\PHPString $field, \php\lang\PHPObject $value = null) {
			$this->fields->put($field, $value);
		}

		/**
		 * Returns the select query string.
		 * @return \php\lang\PHPString
		 */
		public function toSelectString() {
			$return = \php\lang\PHPString::newInstance('');
			$size = $this->selects->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance('SELECT '));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n, "));
					}
					$return = $return->append($this->selects->get($i)->toString());
				}
			}
			$size = $this->froms->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nFROM"));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n, "));
					}
					$return = $return->append($this->froms->get($i)->toString());
				}
			}
			$size = $this->joins->size();
			if ($size->isPositive()->getBoolean()) {
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$return = $return->append(\php\lang\PHPString::newInstance("\n"));
					$return = $return->append($this->joins->get($i)->toString());
				}
			}
			$keys = $this->wheres->keySet();
			$size = $keys->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nWHERE "));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$key = $keys->get($i);
					$values = $this->wheres->get($key);
					$vsize = $values->size();
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n" . (($values->get(\php\lang\PHPNumber::ZERO())->isAnd()->getBoolean()) ? 'AND ' : 'OR ')));
					}
					$return = $return->append(\php\lang\PHPString::newInstance('('));
					for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($vsize)->getBoolean(); $j = $j->increase()) {
						$value = $values->get($j);
						if ($j->isPositive()->getBoolean()) {
							$return = $return->append(\php\lang\PHPString::newInstance(' ' . (($value->isAnd()) ? 'AND' : 'OR') . ' '));
						}
						$return = $return->append($value->toString());
					}
					$return = $return->append(\php\lang\PHPString::newInstance(')'));
				}
			}
			$size = $this->groupBys->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nGROUP BY "));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n, "));
					}
					$return = $return->append($this->groupBys->get($i)->toString());
				}
			}
			$keys = $this->havings->keySet();
			$size = $keys->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nHAVING "));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$key = $keys->get($i);
					$values = $this->havings->get($key);
					$vsize = $values->size();
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n" . (($values->get(\php\lang\PHPNumber::ZERO())->isAnd()->getBoolean()) ? 'AND ' : 'OR ')));
					}
					$return = $return->append(\php\lang\PHPString::newInstance('('));
					for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($vsize)->getBoolean(); $j = $j->increase()) {
						$value = $values->get($j);
						if ($j->isPositive()->getBoolean()) {
							$return = $return->append(\php\lang\PHPString::newInstance(' ' . (($value->isAnd()) ? 'AND' : 'OR') . ' '));
						}
						$return = $return->append($value->toString());
					}
					$return = $return->append(\php\lang\PHPString::newInstance(')'));
				}
			}
			$size = $this->orderBys->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nORDER BY "));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n, "));
					}
					$return = $return->append($this->orderBys->get($i)->toString());
				}
			}
			if ($this->limit !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nLIMIT " . $this->limit));
			}
			if ($this->start !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance("\nOFFSET " . $this->start));
			}
			$return = $return->append(\php\lang\PHPString::newInstance("\n;"));
			return $return;
		}

		public function toInsertString(\php\lang\PHPString $table) {
			$return = \php\lang\PHPString::newInstance('');
			$keys = $this->fields->keySet();
			$size = $keys->size();
			if ($size->isPositive()->getBoolean()) {
				$fieldString = \php\lang\PHPString::newInstance('');
				$valueString = \php\lang\PHPString::newInstance('');
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$key = $keys->get($i);
					$value = $this->fields->get($key);
					$fieldString = $fieldString->append(\php\lang\PHPString::newInstance("\n"));
					$valueString = $valueString->append(\php\lang\PHPString::newInstance("\n"));
					if ($i->isPositive()->getBoolean()) {
						$fieldString = $fieldString->append(\php\lang\PHPString::newInstance(", "));
						$valueString = $valueString->append(\php\lang\PHPString::newInstance(", "));
					}
					$fieldString = $fieldString->append($key);
					$valueString = $valueString->append($value === null ? \php\lang\PHPString::newInstance('null') : $value->toString());
				}
				$return = $return->append(\php\lang\PHPString::newInstance("INSERT INTO  "));
				$return = $return->append($table);
				$return = $return->append(\php\lang\PHPString::newInstance(" ("));
				$return = $return->append($fieldString);
				$return = $return->append(\php\lang\PHPString::newInstance("\n) VALUES ("));
				$return = $return->append($valueString);
				$return = $return->append(\php\lang\PHPString::newInstance("\n);"));
			}
			return $return;
		}

		public function toUpdateString(\php\lang\PHPString $table) {
			$return = \php\lang\PHPString::newInstance('');
			$keys = $this->fields->keySet();
			$size = $keys->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("UPDATE "));
				$return = $return->append($table);
				$return = $return->append(\php\lang\PHPString::newInstance(" SET "));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$key = $keys->get($i);
					$value = $this->fields->get($key);
					$return = $return->append(\php\lang\PHPString::newInstance("\n"));
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance(", "));
					}
					$return = $return->append($key);
					$return = $return->append(\php\lang\PHPString::newInstance(" = "));
					$return = $return->append($value === null ? \php\lang\PHPString::newInstance('null') : $value->toString());
				}
				$keys = $this->wheres->keySet();
				$size = $keys->size();
				if ($size->isPositive()->getBoolean()) {
					$return = $return->append(\php\lang\PHPString::newInstance("\nWHERE "));
					for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
						$key = $keys->get($i);
						$values = $this->wheres->get($key);
						$vsize = $values->size();
						if ($i->isPositive()->getBoolean()) {
							$return = $return->append(\php\lang\PHPString::newInstance("\n" . (($values->get(\php\lang\PHPNumber::ZERO())->isAnd()->getBoolean()) ? 'AND ' : 'OR ')));
						}
						$return = $return->append(\php\lang\PHPString::newInstance('('));
						for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($vsize)->getBoolean(); $j = $j->increase()) {
							$value = $values->get($j);
							if ($j->isPositive()->getBoolean()) {
								$return = $return->append(\php\lang\PHPString::newInstance(' ' . (($value->isAnd()) ? 'AND' : 'OR') . ' '));
							}
							$return = $return->append($value->toString());
						}
						$return = $return->append(\php\lang\PHPString::newInstance(')'));
					}
				}
				$return = $return->append(\php\lang\PHPString::newInstance("\n;"));
			}
			return $return;
		}

		public function toDeleteString(\php\lang\PHPString $table) {
			$return = \php\lang\PHPString::newInstance('');
			$keys = $this->wheres->keySet();
			$size = $keys->size();
			if ($size->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance("DELETE FROM "));
				$return = $return->append($table);
				$return = $return->append(\php\lang\PHPString::newInstance("\nWHERE "));
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$key = $keys->get($i);
					$values = $this->wheres->get($key);
					$vsize = $values->size();
					if ($i->isPositive()->getBoolean()) {
						$return = $return->append(\php\lang\PHPString::newInstance("\n" . (($values->get(\php\lang\PHPNumber::ZERO())->isAnd()->getBoolean()) ? 'AND ' : 'OR ')));
					}
					$return = $return->append(\php\lang\PHPString::newInstance('('));
					for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($vsize)->getBoolean(); $j = $j->increase()) {
						$value = $values->get($j);
						if ($j->isPositive()->getBoolean()) {
							$return = $return->append(\php\lang\PHPString::newInstance(' ' . (($value->isAnd()) ? 'AND' : 'OR') . ' '));
						}
						$return = $return->append($value->toString());
					}
					$return = $return->append(\php\lang\PHPString::newInstance(')'));
				}
				$return = $return->append(\php\lang\PHPString::newInstance(';'));
			}
			return $return;
		}

	}

}