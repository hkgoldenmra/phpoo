<?php

namespace php\sql {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/sql/SQLException.php');

	class SQLConnection extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\sql\SQLConnection object.
		 * @param \php\lang\PHPString $host The database host IP or domain.
		 * @param \php\lang\PHPString $username The database login user name.
		 * @param \php\lang\PHPString $password The database login password.
		 * @param \php\lang\PHPNumber $port The database socket port.
		 * @return \php\sql\SQLConnection
		 */
		public static function newConnection(\php\lang\PHPString $host, \php\lang\PHPString $username, \php\lang\PHPString $password, \php\lang\PHPNumber $port = null) {
			return new SQLConnection($host, $username, $password);
		}

		/**
		 *
		 * @var \MySQLi 
		 */
		private $connection;

		/**
		 * Constructs a \php\sql\SQLConnection object.
		 * @param \php\lang\PHPString $host The database host IP or domain.
		 * @param \php\lang\PHPString $username The database login user name.
		 * @param \php\lang\PHPString $password The database login password.
		 * @param \php\lang\PHPNumber $port The database socket port.
		 */
		protected function __construct(\php\lang\PHPString $host, \php\lang\PHPString $username, \php\lang\PHPString $password, \php\lang\PHPNumber $port = null) {
			parent::__construct();
			$host = $host->getString();
			$username = $username->getString();
			$password = $password->getString();
			if ($port === null) {
				$this->connection = mysqli_connect($host, $username, $password);
			} else {
				$this->connection = mysqli_connect($host, $username, $password, '', $port->getNumber());
			}
		}

		/**
		 * Sends a SQL query string to database.
		 * @param \php\lang\PHPString $sql The SQL query string.
		 * @return \php\util\collections\Vector
		 * @throws \php\sql\SQLException
		 */
		public function query(\php\lang\PHPString $sql) {
			$result = $this->connection->query($sql->getString());
			if ($result === true) {
				return null;
			} else if ($result === false) {
				throw new SQLException(sprintf('%d: %s', $this->connection->errno, $this->connection->error));
			} else {
				$return = \php\util\collections\Vector::newInstance();
				while (($assoc = $result->fetch_assoc()) !== null) {
					$map = \php\util\collections\Map::newInstance();
					foreach ($assoc as $k => $v) {
						$k = \php\lang\PHPString::newInstance($k);
						if ($v === null) {
							$map->put($k, null);
						} else {
							$map->put($k, \php\lang\PHPString::newInstance($v));
						}
					}
					$return->add($map);
				}
				$result->free();
				return $return;
			}
		}

		/**
		 * Closes the database connection.
		 */
		public function close() {
			$this->connection->close();
		}

	}

}