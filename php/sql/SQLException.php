<?php

/**
 * Description of \php\lang\SQLException
 */

namespace php\sql {

	/**
	 * An exception that provides information on a database access error or other errors.
	 */
	class SQLException extends \Exception {

		/**
		 * Constructs a \php\lang\SQLException object.
		 * @param string $message An error message.
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}