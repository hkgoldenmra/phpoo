<?php

/**
 * Description of \php\sql\SortField
 */

namespace php\sql {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/sql/Field.php');

	/**
	 * The \php\sql\SortField class is the field to sort for a SQL query string.
	 */
	class SortField extends Field {

		/**
		 * Returns a \php\sql\SortField object.
		 * @param \php\lang\PHPString $field The field.
		 * @param \php\lang\PHPBoolean $isAscending The order is true (asscending) or false (descending).
		 * @return \php\sql\SortField
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $field, \php\lang\PHPBoolean $isAscending = null) {
			return new SortField($field, $isAscending);
		}

		private $isAscending;

		/**
		 * Constructs a \php\sql\SortField object.
		 * @param \php\lang\PHPString $field The field.
		 * @param \php\lang\PHPBoolean $isAscending The order is true (asscending) or false (descending).
		 */
		protected function __construct(\php\lang\PHPString $field, \php\lang\PHPBoolean $isAscending = null) {
			parent::__construct($field, $isAscending);
			if ($isAscending === null) {
				$isAscending = \php\lang\PHPBoolean::newInstance(true);
			}
			$this->isAscending = $isAscending->getBoolean();
		}

		/**
		 * Is the order true (asscending) or false (descending).
		 * @return \php\lang\PHPBoolean
		 */
		public function isAscending() {
			return \php\lang\PHPBoolean::newInstance($this->isAscending);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append($this->getField());
			if ($isAscending = $this->isAscending()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance(($isAscending) ? ' ASC' : ' DESC'));
			}
			return $return;
		}

	}

}