<?php

/**
 * Description of \php\util\Calendar
 */

namespace php\util {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');

	/**
	 * The class \php\util\Calendar represents a specific instant in time, with millisecond precision.
	 */
	class Calendar extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\util\Calendar object created by date time fields.
		 * @param \php\lang\PHPNumber $year The year field. Default current year.
		 * @param \php\lang\PHPNumber $month The month field. Default current month.
		 * @param \php\lang\PHPNumber $day The day of month field. Default current day of month.
		 * @param \php\lang\PHPNumber $hour The hour field. Default current hours.
		 * @param \php\lang\PHPNumber $minute The minute field. Default current minutes.
		 * @param \php\lang\PHPNumber $second The second field. Default current seconds.
		 * @param \php\lang\PHPNumber $nanosecond The nanosecond field. Default current nanoseconds.
		 * @param \php\lang\PHPNumber $timezone The timezone field. Default current timezone.
		 * @return \php\util\Calendar
		 */
		public static function newInstanceByDateTime(\php\lang\PHPNumber $year = null, \php\lang\PHPNumber $month = null, \php\lang\PHPNumber $day = null, \php\lang\PHPNumber $hour = null, \php\lang\PHPNumber $minute = null, \php\lang\PHPNumber $second = null, \php\lang\PHPNumber $nanosecond = null, \php\lang\PHPNumber $timezone = null) {
			$fields = getdate(time());
			if ($year === null) {
				$year = \php\lang\PHPNumber::newInstance($fields['year']);
			}
			if ($month === null) {
				$month = \php\lang\PHPNumber::newInstance($fields['mon']);
			}
			if ($day === null) {
				$day = \php\lang\PHPNumber::newInstance($fields['mday']);
			}
			if ($hour === null) {
				$hour = \php\lang\PHPNumber::newInstance($fields['hours']);
			}
			if ($minute === null) {
				$minute = \php\lang\PHPNumber::newInstance($fields['minutes']);
			}
			if ($second === null) {
				$second = \php\lang\PHPNumber::newInstance($fields['seconds']);
			}
			return self::newInstanceByTimestamp(\php\lang\PHPNumber::newInstance(mktime($hour->getNumber(), $minute->getNumber(), $second->getNumber(), $month->getNumber(), $day->getNumber(), $year->getNumber())), $nanosecond, $timezone);
		}

		/**
		 * Returns a \php\util\Calendar object created by timestamp.
		 * @param \php\lang\PHPNumber $timestamp The timestamp. Default current timestamp.
		 * @param \php\lang\PHPNumber $nanosecond The nanosecond field. Default current nanoseconds.
		 * @param \php\lang\PHPNumber $timezone The timezone field. Default current timezone.
		 * @return \php\util\Calendar
		 */
		public static function newInstanceByTimestamp(\php\lang\PHPNumber $timestamp = null, \php\lang\PHPNumber $nanosecond = null, \php\lang\PHPNumber $timezone = null) {
			$fields = explode('.', sprintf('%.9f', microtime(true)));
			if ($nanosecond === null) {
				$nanosecond = \php\lang\PHPNumber::newInstance(intval($fields[1]));
			} else {
				$nanosecond = $nanosecond->bitwiseAnd(\php\lang\PHPNumber::newInstance(999999999));
			}
			if ($timezone === null) {
				$offset = timezone_offset_get(new \DateTimeZone(date_default_timezone_get()), new \DateTime());
				$timezone = \php\lang\PHPNumber::newInstance(intval($offset / 3600) + (abs($offset % 3600) / 60));
			}
			if ($timestamp === null) {
				$timestamp = \php\lang\PHPNumber::newInstance(time());
			}
			return new Calendar($timestamp, $nanosecond, $timezone);
		}

		private $timestamp;
		private $nanosecond;
		private $timezone;

		/**
		 * Constructs a \php\util\Calendar object created by timestamp.
		 * @param \php\lang\PHPNumber $timestamp The timestamp. Default current timestamp.
		 * @param \php\lang\PHPNumber $nanosecond The nanosecond field. Default current nanoseconds.
		 * @param \php\lang\PHPNumber $timezone The timezone field. Default current timezone.
		 */
		protected function __construct(\php\lang\PHPNumber $timestamp, \php\lang\PHPNumber $nanosecond, \php\lang\PHPNumber $timezone) {
			parent::__construct();
			$this->timestamp = $timestamp->getNumber();
			$this->nanosecond = $nanosecond->getNumber();
			$this->timezone = $timezone->getNumber();
		}

		/**
		 * Checks two calendars are equal or not.
		 * @param \php\lang\PHPObject $object Another \php\util\Calendar object.
		 * @return \php\lang\PHPBoolean
		 */
		public function equals(\php\lang\PHPObject $object = null) {
			$return = false;
			if ($object instanceof Calendar) {
				$return = $this->toString()->equals($object->toString())->getBoolean();
			}
			return PHPBoolean::newInstance($return);
		}

		/**
		 * The timestamp of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getTimestamp() {
			return \php\lang\PHPNumber::newInstance($this->timestamp);
		}

		/**
		 * The nanosecond of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getNanosecond() {
			return \php\lang\PHPNumber::newInstance($this->nanosecond);
		}

		/**
		 * The timezone of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getTimezone() {
			return \php\lang\PHPNumber::newInstance($this->timezone);
		}

		/**
		 * The weekday field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getWeekday() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['wday']);
		}

		/**
		 * The year field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getYear() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['year']);
		}

		/**
		 * The month field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getMonth() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['mon']);
		}

		/**
		 * The day of month field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getDayOfMonth() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['mday']);
		}

		/**
		 * The hours field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getHour() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['hours']);
		}

		/**
		 * The minutes field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getMinute() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['minutes']);
		}

		/**
		 * The seconds field of this calendar.
		 * @return \php\lang\PHPNumber
		 */
		public function getSecond() {
			$fields = getdate($this->getTimestamp()->getNumber());
			return \php\lang\PHPNumber::newInstance($fields['seconds']);
		}

		/**
		 * The timezone of this calendar.
		 * @return \php\lang\PHPString
		 */
		public function toTimezoneString() {
			$timezone = abs($this->getTimezone()->getNumber());
			$hour = intval($timezone);
			$minute = ($timezone - $hour) * 60;
			return \php\lang\PHPString::newInstance(sprintf('%s%02d:%02d', ($this->getTimezone()->isNegative()->getBoolean() ? '-' : '+'), $hour, $minute));
		}

		/**
		 * The date of this calendar.
		 * @return \php\lang\PHPString
		 */
		public function toDateString() {
			return \php\lang\PHPString::newInstance(sprintf('%04d-%02d-%02d', $this->getYear()->getNumber(), $this->getMonth()->getNumber(), $this->getDayOfMonth()->getNumber()));
		}

		/**
		 * The time of this calendar.
		 * @return \php\lang\PHPString
		 */
		public function toTimeString() {
			return \php\lang\PHPString::newInstance(sprintf('%02d:%02d:%02d', $this->getHour()->getNumber(), $this->getMinute()->getNumber(), $this->getSecond()->getNumber()));
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			return \php\lang\PHPString::newInstance(str_replace(':', '-', sprintf('%s-%s-%09d%s', $this->toDateString()->getString(), $this->toTimeString()->getString(), $this->getNanosecond()->getNumber(), $this->toTimezoneString()->getString())));
		}

	}

}