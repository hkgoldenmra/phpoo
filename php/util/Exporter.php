<?php

/**
 * Description of \php\util\Exporter
 */

namespace php\util {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');

	/**
	 * A class provides headers for exporting.
	 */
	class Exporter extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Exports the streamed data on the fly on the page.
		 * @param \php\lang\PHPString $mimeType The exported MIME type.
		 * @param \php\lang\PHPString $filename The default file name of the exported data.
		 */
		public static function export(\php\lang\PHPString $mimeType, \php\lang\PHPString $filename) {
			header(sprintf('Content-Type: %s; charset=%s', $mimeType->getString(), \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			header(sprintf('Content-Disposition: attachment; filename="%s"', $filename->getString()));
		}

		protected function __construct() {
			parent::__construct();
		}

	}

}