<?php

/**
 * Description of \php\util\System
 */

namespace php\util {
	include_once('php/lang/PHPObject.php');
	include_once('php/util/collections/Map.php');

	/**
	 * A class provides a global static methods to retrieve HTTP data.
	 */
	class System extends \php\lang\PHPObject {

		/**
		 * 
		 * @param array $parameters
		 * @return \php\util\collections\Map
		 */
		private static function newParameters($parameters) {
			$map = \php\util\collections\Map::newInstance();
			if (is_array($parameters)) {
				foreach ($parameters as $k => $v) {
					$k = \php\lang\PHPString::newInstance($k);
					if (is_array($v)) {
						$map->put($k, self::newParameters(Map::newInstance(), $v));
					} else {
						$map->put($k, \php\lang\PHPString::newInstance($v));
					}
				}
			}
			return $map;
		}

		/**
		 * Returns the data retrieved from GET method.
		 * @return \php\util\collections\Map
		 */
		public static function newInstanceByGet() {
			return self::newParameters(filter_input_array(INPUT_GET));
		}

		/**
		 * Returns the data retrieved from POST method.
		 * @return \php\util\collections\Map
		 */
		public static function newInstanceByPost() {
			return self::newParameters(filter_input_array(INPUT_POST));
		}

		/**
		 * Returns the data retrieved from PUT method.
		 * @return \php\util\collections\Map
		 */
		public static function newInstanceByPut() {
			$input = '';
			$handle = fopen('php://input', 'r');
			while (!feof($handle)) {
				$input .= fread($handle, 1024 * 1024);
			}
			fclose($handle);
			$_PUT = array();
			parse_str($input, $_PUT);
			return self::newParameters($_PUT);
		}

		protected function __construct() {
			parent::__construct();
		}

	}

}