<?php

/**
 * Description of \php\util\Uploader
 */

namespace php\util {
	include_once('php/lang/PHPObject.php');
	include_once('php/io/File.php');
	include_once('php/io/IOException.php');

	/**
	 * A class provides a file upload process.
	 */
	class Uploader extends \php\lang\PHPObject {

		public static function newInstance() {
			return new Uploader();
		}

		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Moves the uploaded file to destination directory.
		 * @param \php\io\File $directory The destination directory.
		 * @throws \php\io\IOException
		 */
		public function move(\php\io\File $directory) {
			if (count($_FILES) > 0 && $directory->isDirectory()->getBoolean()) {
				$files = array();
				foreach ($_FILES as $file) {
					if (array_key_exists('name', $file)) {
						if (is_array($file['name'])) {
							foreach (array_keys($file['name']) as $i) {
								$f = array();
								$f['name'] = $file['name'][$i];
								$f['type'] = $file['type'][$i];
								$f['tmp_name'] = $file['tmp_name'][$i];
								$f['error'] = $file['error'][$i];
								$f['size'] = $file['size'][$i];
								array_push($files, $f);
							}
						} else {
							array_push($files, $file);
						}
					}
				}
				foreach ($files as $file) {
					if ($file['error'] === 0) {
						if (!@move_uploaded_file($file['tmp_name'], $directory->getPath()->getString() . '/' . $file['name'])) {
							throw new \php\io\IOException("Unable to move uploaded file to target directory");
						}
					}
				}
			}
		}

	}

}