<?php

/**
 * Description of \php\util\UserAgent
 */

namespace php\util {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');

	/**
	 * A class to examine the user agent string
	 */
	class UserAgent extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\UserAgent object.
		 * @param \php\lang\PHPString $userAgentString The user agent string. Default &lt;filter_input(INPUT_SERVER, 'HTTP_USER_AGENT')&gt;
		 * @return \php\util\UserAgent
		 */
		public static function newInstance(\php\lang\PHPString $userAgentString = null) {
			return new UserAgent($userAgentString);
		}

		private function convertWindowsVersion($version) {
			$array = array(
				'5.0' => 'ME',
				'5.1' => 'XP',
				'6.0' => 'VISTA',
				'6.1' => '7',
				'6.2' => '8',
				'6.3' => '8.1',
				'10.0' => '10',
			);
			if (array_key_exists($version, $array)) {
				$version = $array[$version];
			}
			return $version;
		}

		private $user_agent_string;
		private $os_name = 'Unknown OS';
		private $os_version;
		private $browser_name = 'Unknown Browser';
		private $browser_version;
		private $engine_name = 'Unknown Engine';
		private $engine_version;

		/**
		 * Returns a \php\util\UserAgent object.
		 * @param \php\lang\PHPString $userAgentString The user agent string. Default &lt;filter_input(INPUT_SERVER, 'HTTP_USER_AGENT')&gt;
		 */
		protected function __construct(\php\lang\PHPString $userAgentString = null) {
			parent::__construct();
			if ($userAgentString === null) {
				$this->user_agent_string = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');
			} else {
				$this->user_agent_string = $userAgentString->getString();
			}
			$version = '[-(: \/]+(?P<version>[^ );]+)';
			$array = array(
				'CYGWIN_NT' . $version => 'Windows',
				'Windows NT' . $version => 'Windows',
				'WinNT' . $version => 'Windows',
				'iPad.+?OS' . $version => 'iPad iOS',
				'iPhone.+?OS' . $version => 'iPhone iOS',
				'Macintosh; Intel Mac OS X' . $version => 'Mac OS X',
				'FreeBSD' . $version => 'FreeBSD',
				'OpenBSD' . $version => 'OpenBSD',
				'SunOS' . $version => 'Sun OS',
				'Android' . $version => 'Android',
				'Ubuntu; Linux' . $version => 'Ubuntu',
				'Valve Steam Tenfoot' . $version => 'Steam OS',
				'Linux' . $version => 'GNU/Linux',
				'X11' . $version => 'GNU/Linux',
			);
			foreach ($array as $k => $v) {
				if (preg_match('/' . $k . '/i', $this->user_agent_string, $matches) > 0) {
					$this->os_name = $v;
					$this->os_version = preg_replace('/[_\/]/', '.', $matches['version']);
					if ($v == 'Windows') {
						$this->os_version = $this->convertWindowsVersion($this->os_version);
					}
					break;
				}
			}
			$array = array(
				'Chromium' . $version => 'Chromium',
				'OPR' . $version => 'Opera',
				'Maxthon' . $version => 'Maxthon',
				'Line' . $version => 'Line',
				'SamsungBrowser' . $version => 'Samsung Browser',
				'Chrome' . $version . ' Mobile' => 'Google Chrome Mobile',
				'Chrome' . $version . ' Iron' => 'Iron',
				'QupZilla' . $version => 'QupZilla',
				'Chrome' . $version => 'Google Chrome',
				'CriOS' . $version . ' Mobile' => 'Google Chrome for iOS',
				'FxiOS' . $version . ' Mobile' => 'Firefox for iOS',
				'Version' . $version . ' Mobile.+?Safari' => 'Safari Mobile',
				'Version' . $version . ' Safari' => 'Safari',
				'K-Meleon' . $version => 'K-Meleon',
				'SeaMonkey' . $version => 'SeaMonkey',
				'Lightning' . $version => 'Lightning',
				'Mobile.+?Firefox' . $version => 'Firefox Mobile',
				'Firefox' . $version => 'Firefox',
				'Trident.+?; rv' . $version => 'Internet Explorer',
				'MSIE' . $version => 'Internet Explorer',
				'Links' . $version => 'Links',
				'Konqueror' . $version => 'Konqueror',
			);
			foreach ($array as $k => $v) {
				if (preg_match('/' . $k . '/i', $this->user_agent_string, $matches) > 0) {
					$this->browser_name = $v;
					$this->browser_version = preg_replace('/[_\/]/', '.', $matches['version']);
					break;
				}
			}
			$array = array(
				'Blink' . $version => 'Blink',
				'Presto' . $version => 'Presto',
				'AppleWebKit' . $version => 'WebKit',
				'KHTML' . $version => 'KHTML',
				'Gecko' . $version => 'Gecko',
				'Trident' . $version => 'Trident',
				'GNU C' . $version => 'GNU C',
			);
			foreach ($array as $k => $v) {
				if (preg_match('/' . $k . '/i', $this->user_agent_string, $matches) > 0) {
					$this->engine_name = $v;
					$this->engine_version = preg_replace('/[_\/]/', '.', $matches['version']);
					break;
				}
			}
		}

		/**
		 * Returns the original user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getUserAgentString() {
			return \php\lang\PHPString::newInstance($this->user_agent_string);
		}

		/**
		 * Returns the name of operating system parsed from the user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getOSName() {
			return \php\lang\PHPString::newInstance($this->os_name);
		}

		/**
		 * Returns the version of operating system parsed from the user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getOSVersion() {
			return \php\lang\PHPString::newInstance($this->os_version);
		}

		/**
		 * Returns the name of browser parsed from the user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getBrowserName() {
			return \php\lang\PHPString::newInstance($this->browser_name);
		}

		/**
		 * Returns the version of browser parsed from the user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getBrowserVersion() {
			return \php\lang\PHPString::newInstance($this->browser_version);
		}

		/**
		 * Returns the name of engine parsed from the user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getEngineName() {
			return \php\lang\PHPString::newInstance($this->engine_name);
		}

		/**
		 * Returns the version of engine parsed from the user agent string.
		 * @return \php\lang\PHPString
		 */
		public function getEngineVersion() {
			return \php\lang\PHPString::newInstance($this->engine_version);
		}

	}

}