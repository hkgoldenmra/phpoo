<?php

/**
 * Description of \php\util\Zip
 */

namespace php\util {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/Calendar.php');

	/**
	 * A file archive, compressed with Zip.
	 */
	class Zip extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\Zip object.
		 * @return \php\util\Zip
		 */
		public static function newInstance() {
			return new Zip();
		}

		private $datasec = array();
		private $ctrl_dir = array();
		private $eof_ctrl_dir = "\x50\x4b\x05\x06\x00\x00\x00\x00";
		private $old_offset = 0;

		/**
		 * Constructs a \php\util\Zip object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		private function unix2DosTime($unixtime = 0) {
			$timearray = (($unixtime == 0) ? getdate() : getdate($unixtime));
			if ($timearray['year'] < 1980) {
				$timearray['year'] = 1980;
				$timearray['mon'] = 1;
				$timearray['mday'] = 1;
				$timearray['hours'] = 0;
				$timearray['minutes'] = 0;
				$timearray['seconds'] = 0;
			}

			return (($timearray['year'] - 1980) << 25) |
				($timearray['mon'] << 21) |
				($timearray['mday'] << 16) |
				($timearray['hours'] << 11) |
				($timearray['minutes'] << 5) |
				($timearray['seconds'] >> 1);
		}

		/**
		 * Adds a file to a ZIP archive from the given file.
		 * @param \php\lang\PHPString $filename The file name in this Zip archive.
		 * @param \php\lang\PHPString $data The data of the file.
		 * @param \php\util\Calendar $datetime The date time of the file add into this Zip archive.
		 */
		public function addFile(\php\lang\PHPString $filename, \php\lang\PHPString $data = null, Calendar $datetime = null) {
			if ($data === null) {
				$data = \php\lang\PHPString::newInstance('');
			}
			if ($datetime === null) {
				$datetime = Calendar::newInstanceByTimestamp();
			}
			$name = $filename->getString();
			$data = $data->getString();
			$time = $datetime->getTimestamp()->getNumber();
			$name = str_replace('\\', '/', $name);

			$dtime = substr('00000000' . dechex($this->unix2DosTime($time)), -8);
			$hexdtime = '\x' . $dtime[6] . $dtime[7] . '\x' . $dtime[4] . $dtime[5]
				. '\x' . $dtime[2] . $dtime[3]
				. '\x' . $dtime[0] . $dtime[1];
			eval('$hexdtime = "' . $hexdtime . '";');

			$fr = "\x50\x4b\x03\x04";
			$fr .= "\x14\x00";
			$fr .= "\x00\x00";
			$fr .= "\x08\x00";
			$fr .= $hexdtime;

			$unc_len = strlen($data);
			$crc = crc32($data);
			$zdata = gzcompress($data);
			$zdata = substr(substr($zdata, 0, strlen($zdata) - 4), 2);
			$c_len = strlen($zdata);
			$fr .= pack('V', $crc);
			$fr .= pack('V', $c_len);
			$fr .= pack('V', $unc_len);
			$fr .= pack('v', strlen($name));
			$fr .= pack('v', 0);
			$fr .= $name;
			$fr .= $zdata;

			$this->datasec[] = $fr;

			$cdrec = "\x50\x4b\x01\x02";
			$cdrec .= "\x00\x00";
			$cdrec .= "\x14\x00";
			$cdrec .= "\x00\x00";
			$cdrec .= "\x08\x00";
			$cdrec .= $hexdtime;
			$cdrec .= pack('V', $crc);
			$cdrec .= pack('V', $c_len);
			$cdrec .= pack('V', $unc_len);
			$cdrec .= pack('v', strlen($name));
			$cdrec .= pack('v', 0);
			$cdrec .= pack('v', 0);
			$cdrec .= pack('v', 0);
			$cdrec .= pack('v', 0);
			$cdrec .= pack('V', 32);

			$cdrec .= pack('V', $this->old_offset);
			$this->old_offset += strlen($fr);

			$cdrec .= $name;
			$this->ctrl_dir[] = $cdrec;
		}

		/**
		 * Exports this Zip archive on the fly.
		 * @param \php\lang\PHPString $filename File name of exported data.
		 * @param \php\lang\PHPString $extension File extension of exported data.
		 * @param \php\lang\PHPString $mimeType MIME type of exported data.
		 */
		public function export(\php\lang\PHPString $filename = null, \php\lang\PHPString $extension = null, \php\lang\PHPString $mimeType = null) {
			if ($mimeType === null) {
				$$mimeType = \php\lang\PHPString::newInstance('application/zip');
			}
			if ($extension === null) {
				$extension = \php\lang\PHPString::newInstance('zip');
			}
			if ($filename === null) {
				$datetime = Calendar::newInstanceByTimestamp();
				$filename = $datetime->toString();
			}
			if ($filename === null || $filename->endsWith($extension)->not()->getBoolean()) {
				$filename = $filename->append(\php\lang\PHPString::newInstance('.'));
				$filename = $filename->append($extension);
			}
			Exporter::export($mimeType, $filename);
			$data = implode('', $this->datasec);
			$ctrldir = implode('', $this->ctrl_dir);

			print($data . $ctrldir . $this->eof_ctrl_dir .
				pack('v', sizeof($this->ctrl_dir)) . pack('v', sizeof($this->ctrl_dir)) .
				pack('V', strlen($ctrldir)) . pack('V', strlen($data)) . "\x00\x00");
		}

	}

}