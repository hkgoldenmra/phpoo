<?php

/**
 * Description of \php\util\collections\ArrayIndexOutOfBoundsException
 */

namespace php\util\collections {
	include_once('php/lang/IndexOutOfBoundsException.php');

	/**
	 * Thrown to indicate that an array has been accessed with an illegal index. The index is either negative or greater than or equal to the size of the array.
	 */
	class ArrayIndexOutOfBoundsException extends \php\lang\IndexOutOfBoundsException {

		/**
		 * Constructs a \php\lang\ArrayIndexOutOfBoundsException object.
		 * @param \php\lang\PHPString $message An error message
		 */
		public function __construct($message = "", $code = 0, \Exception $previous = null) {
			parent::__construct($message, $code, $previous);
		}

	}

}