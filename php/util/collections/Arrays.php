<?php

/**
 * Description of \php\util\collections\Arrays
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/util/comparators/Comparator.php');
	include_once('php/util/comparators/PHPObjectComparator.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 * This class contains various methods for manipulating arrays.
	 */
	class Arrays extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\collections\Arrays object.
		 * @param \php\util\comparators\Comparator $comparator The comparator to determine the order of the list.
		 * @return \php\util\collections\Arrays
		 */
		public static function newInstance(\php\util\comparators\Comparator $comparator = null) {
			return new Arrays($comparator);
		}

		private $comparator;

		/**
		 * Constructs a \php\util\collections\Arrays object.
		 * @param \php\util\comparators\Comparator $comparator
		 */
		protected function __construct(\php\util\comparators\Comparator $comparator = null) {
			parent::__construct();
			$this->comparator = $comparator;
		}

		/**
		 * Sorts the specified list according to the order induced by the specified comparator.
		 * @param \php\util\collections\LinkedList $list The list to be sorted.
		 * @return type
		 */
		public function sort(LinkedList $list) {
			$isBooleans = true;
			$isCharacters = true;
			$isNumbers = true;
			$isStrings = true;
			$size = $list->size();
			$objects = array();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
				$object = $list->get($i);
				if ($isBooleans && !($object instanceof \php\lang\PHPBoolean)) {
					$isBooleans = false;
				}
				if ($isCharacters && !($object instanceof \php\lang\PHPCharacter)) {
					$isCharacters = false;
				}
				if ($isNumbers && !($object instanceof \php\lang\PHPNumber)) {
					$isNumbers = false;
				}
				if ($isStrings && !($object instanceof \php\lang\PHPString)) {
					$isStrings = false;
				}
				array_push($objects, $object);
			}
			if ($this->comparator === null) {
				if ($isBooleans) {
					$this->comparator = \php\util\comparators\PHPBooleanComparator::newInstance(\php\lang\PHPBoolean::newInstance(true));
				} else if ($isCharacters) {
					$this->comparator = \php\util\comparators\PHPCharacterComparator::newInstance(\php\lang\PHPBoolean::newInstance(true));
				} else if ($isNumbers) {
					$this->comparator = \php\util\comparators\PHPNumberComparator::newInstance(\php\lang\PHPBoolean::newInstance(true));
				} else if ($isStrings) {
					$this->comparator = \php\util\comparators\PHPStringComparator::newInstance(\php\lang\PHPBoolean::newInstance(true));
				} else {
					$this->comparator = \php\util\comparators\PHPObjectComparator::newInstance(\php\lang\PHPBoolean::newInstance(true));
				}
			}
			usort($objects, array($this->comparator, 'compare'));
			$list->clear();
			$list = $list->copy();
			foreach ($objects as $v) {
				$list->addLast($v);
			}
			return $list;
		}

	}

}