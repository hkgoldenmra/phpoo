<?php

/**
 * Description of \php\util\collections\Iterator
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');

	/**
	 * An iterator for lists that traverses the list in either direction.
	 */
	class Iterator extends Vector {

		/**
		 * 
		 * @return \php\util\collections\Vector
		 */
		public static function newInstance() {
			return new Iterator();
		}

		private $pointer;

		/**
		 * 
		 */
		protected function __construct() {
			parent::__construct();
			$this->reset();
		}

		/**
		 * Resets the pointer at the first position.
		 */
		public function reset() {
			$this->pointer = 0;
		}

		/**
		 * Removes the current element in this iterator.
		 */
		public function remove() {
			$this->delete(\php\lang\PHPNumber::newInstance($this->pointer));
		}

		/**
		 * Modifies the current element in this iterator.
		 * @param \php\lang\PHPObject $element The new element replaces the current element.
		 */
		public function modify(\php\lang\PHPObject $element = null) {
			$this->delete(\php\lang\PHPNumber::newInstance($this->pointer));
		}

		/**
		 * Checks this iterator has more elements.
		 * @return \php\lang\PHPBoolean
		 */
		public function hasNext() {
			return \php\lang\PHPBoolean::newInstance($this->pointer < $this->size()->getNumber());
		}

		/**
		 * Returns the next element in this iterator. 
		 * @return \php\lang\PHPObject
		 * @throws ArrayIndexOutOfBoundsException
		 */
		public function next() {
			if ($this->pointer < $this->size()->getNumber()) {
				$data = $this->get(\php\lang\PHPNumber::newInstance($this->pointer));
				$this->pointer++;
				return $data;
			} else {
				throw new ArrayIndexOutOfBoundsException("No more elements.");
			}
		}

	}

}
	