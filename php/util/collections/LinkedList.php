<?php

/**
 * Description of \php\util\collections\LinkedList
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Node.php');
	include_once('php/util/collections/ArrayIndexOutOfBoundsException.php');

	/**
	 * Operations that index into the list will traverse the list from the beginning or the end, whichever is closer to the specified index.
	 */
	class LinkedList extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\collections\LinkedList object.
		 * @return \php\util\collections\LinkedList
		 */
		public static function newInstance() {
			return new LinkedList();
		}

		/**
		 *
		 * @var \php\util\collections\Node
		 */
		protected $head;

		/**
		 *
		 * @var \php\util\collections\Node
		 */
		protected $tail;
		protected $count;

		/**
		 * Constructs a \php\util\collections\LinkedList object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->clear();
		}

		public function clear() {
			$this->head = null;
			$this->tail = null;
			$this->count = 0;
		}

		/**
		 * The number of elements in this list.
		 * @return \php\lang\PHPNumber
		 */
		public function size() {
			return \php\lang\PHPNumber::newInstance($this->count);
		}

		/**
		 * Checks the list is empty or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isEmpty() {
			return $this->size()->isZero();
		}

		/**
		 * Returns the first element in this list.
		 * @return \php\lang\PHPObject
		 * @throws \php/util/collections/ArrayIndexOutOfBoundsException
		 */
		public function getFirst() {
			if ($this->isEmpty()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('Empty array');
			} else {
				return $this->head->getData();
			}
		}

		/**
		 * Returns the last element in this list.
		 * @return \php\lang\PHPObject
		 * @throws \php/util/collections/ArrayIndexOutOfBoundsException
		 */
		public function getLast() {
			if ($this->isEmpty()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('Empty array');
			} else {
				return $this->tail->getData();
			}
		}

		/**
		 * Inserts the specified element at the beginning of this list.
		 * @param \php\lang\PHPObject $element The data to insert to this list. Default &lt;null&gt;.
		 */
		public function addFirst(\php\lang\PHPObject $data = null) {
			$data = Node::newInstance($data);
			if ($this->isEmpty()->getBoolean()) {
				$this->head = $data;
				$this->tail = $data;
			} else {
				$node = $this->head;
				$data->setTail($node);
				$node->setHead($data);
				$this->head = $data;
			}
			$this->count++;
		}

		/**
		 * Appends the specified element to the end of this list.
		 * @param \php\lang\PHPObject $element The data to append to this list. Default &lt;null&gt;.
		 */
		public function addLast(\php\lang\PHPObject $data = null) {
			$data = Node::newInstance($data);
			if ($this->isEmpty()->getBoolean()) {
				$this->head = $data;
				$this->tail = $data;
			} else {
				$node = $this->tail;
				$data->setHead($node);
				$node->setTail($data);
				$this->tail = $data;
			}
			$this->count++;
		}

		/**
		 * Removes the first element from this list.
		 * @throws \php\lang\ArrayIndexOutOfBoundsException
		 */
		public function deleteFirst() {
			if ($this->isEmpty()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('empty array');
			} else {
				if ($this->head->equals($this->tail)->getBoolean()) {
					$this->clear();
				} else {
					$this->head = $this->head->getTail();
					$this->count--;
				}
			}
		}

		/**
		 * Removes the last element from this list.
		 * @throws \php\lang\ArrayIndexOutOfBoundsException
		 */
		public function deleteLast() {
			if ($this->isEmpty()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('empty array');
			} else {
				if ($this->tail->equals($this->head)->getBoolean()) {
					$this->clear();
				} else {
					$this->tail = $this->tail->getHead();
					$this->count--;
				}
			}
		}

		public function reverse() {
			$linkedList = LinkedList::newInstance();
			if ($this->isEmpty()->not()->getBoolean()) {
				$head = $this->tail;
				while ($head->getHead() !== null) {
					$linkedList->addLast($head->getData());
					$head = $head->getHead();
				}
				$linkedList->addLast($head->getData());
			}
			return $linkedList;
		}

		public function copy() {
			$linkedList = parent::copy();
			$linkedList->clear();
			if ($this->isEmpty()->not()->getBoolean()) {
				$tail = $this->head;
				while ($tail->equals($this->tail)->not()->getBoolean()) {
					$data = $tail->getData();
					if ($data === null) {
						$linkedList->addLast($data);
					} else {
						$linkedList->addLast($data->copy());
					}
					$tail = $tail->getTail();
				}
				$data = $this->getLast();
				if ($data === null) {
					$linkedList->addLast($data);
				} else {
					$linkedList->addLast($data->copy());
				}
			}
			return $linkedList;
		}

		public function toString() {
			$return = \php\lang\PHPString::newInstance('[ ');
			if ($this->isEmpty()->not()->getBoolean()) {
				$tail = $this->head;
				while ($tail->equals($this->tail)->not()->getBoolean()) {
					$data = $tail->getData();
					if ($data === null) {
						$data = \php\lang\PHPString::newInstance('null');
					}
					$return = $return->append($data->toString());
					$return = $return->append(\php\lang\PHPString::newInstance(', '));
					$tail = $tail->getTail();
				}
				$data = $this->getLast();
				if ($data === null) {
					$data = \php\lang\PHPString::newInstance('null');
				}
				$return = $return->append($data->toString());
				$return = $return->append(\php\lang\PHPString::newInstance(' '));
			}
			$return = $return->append(\php\lang\PHPString::newInstance(']'));
			return $return;
		}

	}

}