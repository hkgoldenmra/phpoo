<?php

/**
 * Description of \php\util\collections\Map
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/collections/Set.php');

	class Map extends \php\lang\PHPObject {

		public static function newInstance() {
			return new Map();
		}

		/**
		 * @var \php\util\collections\Set
		 */
		private $keys;

		/**
		 * @var \php\util\collections\Vector
		 */
		private $values;

		protected function __construct() {
			parent::__construct();
			$this->keys = Set::newInstance();
			$this->values = Vector::newInstance();
		}

		public function size() {
			return $this->keys->size();
		}

		public function isEmpty() {
			return $this->keys->isEmpty();
		}

		public function indexOf(\php\lang\PHPObject $key = null) {
			return $this->keys->indexOf($key);
		}

		public function indexOfValue(\php\lang\PHPObject $value = null) {
			return $this->values->indexOf($value);
		}

		public function lastIndexOfValue(\php\lang\PHPObject $value = null) {
			return $this->values->lastIndexOf($value);
		}

		public function containsKey(\php\lang\PHPObject $key = null) {
			return $this->keys->contains($key);
		}

		public function containsValue(\php\lang\PHPObject $value = null) {
			return $this->values->contains($value);
		}

		public function put(\php\lang\PHPObject $key = null, \php\lang\PHPObject $value = null) {
			$index = $this->indexOf($key);
			if ($index->isNegative()->getBoolean()) {
				$this->keys->put($key);
				$this->values->add($value);
			} else {
				$this->keys->put($key);
				$this->values->set($index, $value);
			}
		}

		public function keySet() {
			return $this->keys;
		}

		public function get(\php\lang\PHPObject $key = null) {
			$index = $this->indexOf($key);
			return $this->values->get($index);
		}

		public function delete(\php\lang\PHPObject $key = null) {
			$index = $this->indexOf($key);
			$this->keys->delete($index);
			$this->values->delete($index);
		}

		public function toString() {
			$return = \php\lang\PHPString::newInstance('{');
			$keys = $this->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				if ($i->isPositive()->getBoolean()) {
					$return = $return->append(\php\lang\PHPString::newInstance(','));
				}
				$key = $keys->get($i);
				$value = $this->get($key);
				if ($key === null) {
					$key = \php\lang\PHPString::newInstance('null');
				}
				if ($value === null) {
					$value = \php\lang\PHPString::newInstance('null');
				}
				$return = $return->append(\php\lang\PHPString::newInstance(' '));
				$return = $return->append($key->toString());
				$return = $return->append(\php\lang\PHPString::newInstance(': '));
				$return = $return->append($value->toString());
			}
			$return = $return->append(\php\lang\PHPString::newInstance(' }'));
			return $return;
		}

	}

}