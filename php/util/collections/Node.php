<?php

/**
 * Description of \php\util\collections\Node
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');

	/**
	 * The \php\util\collections\Node class is a class to store a data and indicate the chain of nodes for the data.
	 */
	class Node extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\collections\Node object.
		 * @param \php\lang\PHPObject $data The data to store.
		 * @param \php\util\collections\Node $head The previoud \php\util\collections\Node object. Default &lt;null&gt;.
		 * @param \php\util\collections\Node $tail The next \php\util\collections\Node object. Default null.
		 * @return \php\util\collections\Node
		 */
		public static function newInstance(\php\lang\PHPObject $data = null, Node $head = null, Node $tail = null) {
			return new Node($data, $head, $tail);
		}

		private $data;
		private $head;
		private $tail;

		/**
		 * Constructs a \php\util\collections\Node object.
		 * @param \php\lang\PHPObject $data The data to store.
		 * @param \php\util\collections\Node $head The previoud \php\util\collections\Node object. Default &lt;null&gt;.
		 * @param \php\util\collections\Node $tail The next \php\util\collections\Node object. Default null.
		 */
		protected function __construct(\php\lang\PHPObject $data = null, Node $head = null, Node $tail = null) {
			parent::__construct();
			$this->setData($data);
			$this->setHead($head);
			$this->setTail($tail);
		}

		/**
		 * Sets the data of this node.
		 * @param \php\lang\PHPObject $data The data to store.
		 */
		public function setData(\php\lang\PHPObject $data = null) {
			$this->data = $data;
		}

		/**
		 * Sets the head node.
		 * @param \php\util\collections\Node $head The previous node.
		 */
		public function setHead(Node $head = null) {
			$this->head = $head;
		}

		/**
		 * Sets the tail node.
		 * @param \php\util\collections\Node $tail The next node.
		 */
		public function setTail(Node $tail = null) {
			$this->tail = $tail;
		}

		/**
		 * Gets the data of this node.
		 * @return \php\lang\PHPObject
		 */
		public function getData() {
			return $this->data;
		}

		/**
		 * Gets the head node.
		 * @return \php\util\collections\Node
		 */
		public function getHead() {
			return $this->head;
		}

		/**
		 * Gets the tail node.
		 * @return \php\util\collections\Node
		 */
		public function getTail() {
			return $this->tail;
		}

		/**
		 * A copy of this object.
		 * @return \php\util\collections\Node
		 */
		public function copy() {
			$return = parent::copy();
			if (($data = $this->getData()) !== null) {
				$return->setData($data->copy());
			}
			if (($node = $this->getHead()) !== null && ($data = $node->getData()) !== null) {
				$return->setHead(Node::newInstance($data->copy()));
			}
			if (($node = $this->getTail()) !== null && ($data = $node->getData()) !== null) {
				$return->setTail(Node::newInstance($data->copy()));
			}
			return $return;
		}

	}

}