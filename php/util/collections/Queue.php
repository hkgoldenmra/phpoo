<?php

/**
 * Description of \php\util\collections\Queue
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/util/collections/Vector.php');

	/**
	 * The \php\util\collections\Queue class represents a first-in-first-out (FIFO) objects.
	 */
	class Queue extends Vector {

		/**
		 * Returns a \php\util\collections\Queue object.
		 */
		public static function newInstance() {
			return new Stack();
		}

		/**
		 * Constructs a \php\util\collections\Queue object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Pushes an item onto the top of this queue.
		 * @param \php\lang\PHPObject $data The element will be pushed in stack.
		 */
		public function enqueue(\php\lang\PHPObject $data = null) {
			parent::addFirst($data);
		}

		/**
		 * Looks at the object at the bottom of this queue without removing it from the queue.
		 * @return \php\lang\PHPObject
		 */
		public function getFront() {
			return parent::getFirst();
		}

		/**
		 * Removes the object at the bottom of this queue and returns that object as the value of this function.
		 * @return \php\lang\PHPObject
		 */
		public function dequeue() {
			return parent::getAndDelete(\php\lang\PHPNumber::ZERO());
		}

	}

}