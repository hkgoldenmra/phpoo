<?php

/**
 * Description of \php\util\collections\Map
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');

	class Set extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\collections\Set object.
		 */
		public static function newInstance() {
			return new Set();
		}

		/**
		 * @var \php\util\collections\Vector
		 */
		private $array;

		/**
		 * Constructs a \php\util\collections\Set object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->array = Vector::newInstance();
		}

		/**
		 * The number of elements in this list.
		 * @return \php\lang\PHPNumber
		 */
		public function size() {
			return $this->array->size();
		}

		/**
		 * Checks the list is empty or not.
		 * @return \php\lang\PHPBoolean
		 */
		public function isEmpty() {
			return $this->array->isEmpty();
		}

		public function indexOf(\php\lang\PHPObject $data = null) {
			return $this->array->indexOf($data);
		}

		/**
		 * Checks this element is contained or not in this list.
		 * @param \php\lang\PHPString $data Data in this list. Default &lt;null&gt;
		 * @return \php\lang\PHPBoolean
		 */
		public function contains(\php\lang\PHPObject $data = null) {
			return $this->array->contains($data);
		}

		public function put(\php\lang\PHPObject $data = null) {
			$index = $this->indexOf($data);
			if ($index->isNegative()->getBoolean()) {
				$this->array->add($data);
			} else {
				$this->array->set($index, $data);
			}
		}

		/**
		 * Gets the element at the selected index.
		 * @param \php\lang\PHPNumber $index The index of selected element.
		 */
		public function get(\php\lang\PHPNumber $index) {
			return $this->array->get($index);
		}

		public function delete(\php\lang\PHPNumber $index) {
			$this->array->delete($index);
		}

		public function toString() {
			return $this->array->toString();
		}

	}

}