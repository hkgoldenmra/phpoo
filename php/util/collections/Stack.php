<?php

/**
 * Description of \php\util\collections\Stack
 */

namespace php\util\collections {
	include_once('php/lang/PHPObject.php');
	include_once('php/util/collections/Vector.php');

	/**
	 * The \php\util\collections\Stack class represents a last-in-first-out (LIFO) objects.
	 */
	class Stack extends Vector {

		/**
		 * Returns a \php\util\collections\Stack object.
		 */
		public static function newInstance() {
			return new Stack();
		}

		/**
		 * Constructs a \php\util\collections\Stack object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Pushes an item onto the top of this stack.
		 * @param \php\lang\PHPObject $data The element will be pushed in stack.
		 */
		public function push(\php\lang\PHPObject $data = null) {
			parent::addLast($data);
		}

		/**
		 * Looks at the object at the top of this stack without removing it from the stack.
		 * @return \php\lang\PHPObject
		 */
		public function peek() {
			return parent::getLast();
		}

		/**
		 * Removes the object at the top of this stack and returns that object as the value of this function.
		 * @return \php\lang\PHPObject
		 */
		public function pop() {
			return parent::getAndDelete(parent::size()->descrease());
		}

	}

}