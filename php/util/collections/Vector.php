<?php

/**
 * Description of \php\util\collections\Vector
 */

namespace php\util\collections {
	include_once('php/lang/IllegalArgumentException.php');
	include_once('php/util/collections/ArrayIndexOutOfBoundsException.php');
	include_once('php/lang/PHPObject.php');
	include_once('php/util/collections/LinkedList.php');

	class Vector extends LinkedList {

		/**
		 * Returns a \php\util\collections\Vector object.
		 * @return \php\util\collections\Vector
		 */
		public static function newInstance() {
			return new Vector();
		}

		/**
		 * Constructs a \php\util\collections\Vector object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		private function getNode(\php\lang\PHPNumber $index) {
			if ($index->isNegative()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('index < 0');
			} else if ($index->isGreaterOrEqual($this->size())->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('index >= size()');
			} else {
				$tail = $this->head;
				while ($tail->equals($this->tail)->not()->getBoolean() && $index->isPositive()->getBoolean()) {
					$tail = $tail->getTail();
					$index = $index->descrease();
				}
				return $tail;
			}
		}

		/**
		 * Gets the element at the selected index.
		 * @param \php\lang\PHPNumber $index The index of selected element.
		 * @throws \php\lang\ArrayIndexOutOfBoundsException
		 * @throws \php\lang\ArrayIndexOutOfBoundsException
		 */
		public function get(\php\lang\PHPNumber $index) {
			$data = $this->getNode($index);
			if ($data !== null) {
				$data = $data->getData();
			}
			return $data;
		}

		/**
		 * Inserts the specified element at the specific position of this list.
		 * @param \php\lang\PHPObject $data The data to insert to this list. Default &lt;null&gt;.
		 * @param \php\lang\PHPNumber $index The index of selected position.
		 */
		public function add(\php\lang\PHPObject $data = null, \php\lang\PHPNumber $index = null) {
			if ($index === null) {
				$index = parent::size();
			}
			if ($index->isLessOrEqual(\php\lang\PHPNumber::newInstance(0))->getBoolean()) {
				parent::addFirst($data);
			} else if ($index->isGreaterOrEqual($this->size())->getBoolean()) {
				parent::addLast($data);
			} else {
				$tail = $this->head;
				while ($tail->equals($this->tail)->not()->getBoolean() && $index->isPositive()->getBoolean()) {
					$tail = $tail->getTail();
					$index = $index->descrease();
				}
				$data = Node::newInstance($data);
				$head = $tail->getHead();
				$data->setHead($head);
				$head->setTail($data);
				$data->setTail($tail);
				$tail->setHead($data);
				$this->count++;
			}
		}

		/**
		 * Deletes the element at the selected index.
		 * @param \php\lang\PHPNumber $index The index of selected element.
		 * @throws \php\lang\ArrayIndexOutOfBoundsException
		 */
		public function delete(\php\lang\PHPNumber $index) {
			if ($index->isNegative()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('index < 0');
			} else if ($index->isGreaterOrEqual($this->size())->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('index >= size()');
			} else if ($index->isZero()->getBoolean()) {
				parent::deleteFirst();
			} else if ($index->equals(parent::size()->descrease())->getBoolean()) {
				parent::deleteLast();
			} else {
				$tail = $this->head;
				while ($tail->equals($this->tail)->not()->getBoolean() && $index->isPositive()->getBoolean()) {
					$tail = $tail->getTail();
					$index = $index->descrease();
				}
				$head = $tail->getHead();
				$tail = $tail->getTail();
				$head->setTail($tail);
				$tail->setHead($head);
				$this->count--;
			}
		}

		public function getAndDelete(\php\lang\PHPNumber $index) {
			if ($index->isNegative()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('index < 0');
			} else if ($index->isGreaterOrEqual($this->size())->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('index >= size()');
			} else if ($index->isZero()->getBoolean()) {
				$data = parent::getFirst();
				parent::deleteFirst();
				return $data;
			} else if ($index->equals(parent::size()->descrease())->getBoolean()) {
				$data = parent::getLast();
				parent::deleteLast();
				return $data;
			} else {
				$node = $this->head;
				while ($node->equals($this->tail)->not()->getBoolean() && $index->isPositive()->getBoolean()) {
					$node = $node->getTail();
					$index = $index->descrease();
				}
				$head = $node->getHead();
				$tail = $node->getTail();
				$head->setTail($tail);
				$tail->setHead($head);
				$this->count--;
				return $node->getData();
			}
		}

		/**
		 * Sets the element at the selected index.
		 * @param \php\lang\PHPNumber $index The index of selected element.
		 * @param \php\lang\PHPObject $data The set element.
		 * @throws \php\lang\ArrayIndexOutOfBoundsException
		 */
		public function set(\php\lang\PHPNumber $index, \php\lang\PHPObject $data = null) {
			if ($index->isGreaterOrEqual(parent::size())->getBoolean()) {
				$this->resize($index->increase());
			}
			$this->getNode($index)->setData($data);
		}

		public function resize(\php\lang\PHPNumber $length) {
			if ($length->isNegative()->getBoolean()) {
				throw new ArrayIndexOutOfBoundsException('length < 0');
			} else if ($length->isZero()->getBoolean()) {
				parent::clear();
			} else if ($length->isLessThan(parent::size())->getBoolean()) {
				$this->tail = $this->getNode($length->descrease());
				$this->count = $length->getNumber();
			} else {
				while (parent::size()->isLessThan($length)->getBoolean()) {
					parent::addLast(null);
				}
			}
		}

		/**
		 * Returns the index of the first occurrence of the specified element in this list, if not exist returns -1.
		 * @param \php\lang\PHPString $data An element in this list. Default &lt;null&gt;
		 * @param \php\lang\PHPNumber $offset Skipped offset. Default 0.
		 * @return \php\lang\PHPNumber
		 */
		public function indexOf(\php\lang\PHPObject $data = null, \php\lang\PHPNumber $offset = null) {
			if ($offset === null) {
				$offset = \php\lang\PHPNumber::ZERO();
			}
			$return = \php\lang\PHPNumber::newInstance(-1);
			if (parent::isEmpty()->getBoolean()) {
				return $return;
			} else {
				$tail = $this->head;
				while ($tail !== null) {
					$return = $return->increase();
					if ($offset->isPositive()->not()->getBoolean()) {
						$object = $tail->getData();
						if ($object === $data || ($object !== null && $object->equals($data)->getBoolean())) {
							return $return;
						}
					}
					$tail = $tail->getTail();
					$offset = $offset->descrease();
				}
				return \php\lang\PHPNumber::newInstance(-1);
			}
		}

		/**
		 * Returns the index of the first occurrence of the specified element in this list, if not exist returns -1.
		 * @param \php\lang\PHPString $data An element in this list. Default &lt;null&gt;
		 * @param \php\lang\PHPNumber $offset Skipped offset. Default size().
		 * @return \php\lang\PHPNumber
		 */
		public function lastIndexOf(\php\lang\PHPString $data = null, \php\lang\PHPNumber $offset = null) {
			if ($offset === null) {
				$offset = \php\lang\PHPNumber::ZERO();
			}
			$return = \php\lang\PHPNumber::newInstance(-1);
			if (parent::isEmpty()->getBoolean()) {
				return $return;
			} else {
				$head = $this->tail;
				while ($head !== null) {
					$return = $return->increase();
					if ($offset->isPositive()->not()->getBoolean()) {
						$object = $head->getData();
						if ($object === $data || ($object !== null && $object->equals($data)->getBoolean())) {
							return $return;
						}
					}
					$head = $head->getTail();
					$offset = $offset->descrease();
				}
				return \php\lang\PHPNumber::newInstance(-1);
			}
		}

		/**
		 * Checks this element is contained or not in this list.
		 * @param \php\lang\PHPString $data A substring in this string. Default &lt;null&gt;
		 * @return \php\lang\PHPBoolean
		 */
		public function contains(\php\lang\PHPObject $data = null) {
			return $this->indexOf($data)->isNegative()->not();
		}

		public function trimFirsts() {
			if (parent::isEmpty()->not()->getBoolean()) {
				while (parent::getFirst() === null) {
					parent::deleteFirst();
				}
			}
		}

		public function trimLasts() {
			if (parent::isEmpty()->not()->getBoolean()) {
				while (parent::getLast() === null) {
					parent::deleteLast();
				}
			}
		}

		public function trim() {
			$this->trimFirsts();
			$this->trimLasts();
		}

	}

}