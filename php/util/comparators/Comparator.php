<?php

/**
 * Description of \php\util\comparators\Comparator
 */

namespace php\util\comparators {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPBoolean.php');

	/**
	 * A \php\util\comparators\Comparator class imposes a total ordering on some collection of objects.
	 */
	abstract class Comparator extends \php\lang\PHPObject {

		protected $ascending;

		/**
		 * Constructs a \php\util\comparators\Comparator object.
		 * @param \php\lang\PHPBoolean $ascending The ordering direction. Default &lt;true&gt; as ascending.
		 */
		protected function __construct(\php\lang\PHPBoolean $ascending = null) {
			parent::__construct();
			if ($ascending === null) {
				$ascending = \php\lang\PHPBoolean::newInstance(true);
			}
			$this->ascending = $ascending->getBoolean();
		}

		/**
		 * Compares its two element in a list for order. Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
		 * @param \php\lang\PHPObject $object1 The first object to be compared.
		 * @param \php\lang\PHPObject $object2 The second object to be compared.
		 * @return int
		 */
		public function compare(\php\lang\PHPObject $object1 = null, \php\lang\PHPObject $object2 = null) {
			$return = 0;
			if ($this->ascending) {
				if ($object1 === null) {
					$return += -1;
				} else if ($object2 === null) {
					$return += 1;
				}
			} else {
				if ($object2 === null) {
					$return += -1;
				} else if ($object1 === null) {
					$return += 1;
				}
			}
			return $return;
		}

	}

}