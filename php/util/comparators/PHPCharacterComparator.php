<?php

/**
 * Description of \php\util\comparators\PHPCharacterComparator
 */

namespace php\util\comparators {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/comparators/Comparator.php');

	/**
	 * The implementation of \php\util\comparators\Comparator for ordering \php\lang\PHPCharacter objects.
	 */
	class PHPCharacterComparator extends Comparator {

		/**
		 * Returns a \php\util\comparators\PHPCharacterComparator object.
		 * @param \php\lang\PHPBoolean $ascending The ordering direction. Default &lt;true&gt; as ascending.
		 * @return \php\util\comparators\PHPCharacterComparator
		 */
		public static function newInstance(\php\lang\PHPBoolean $ascending = null) {
			return new PHPCharacterComparator($ascending);
		}

		/**
		 * Constructs a \php\util\comparators\PHPCharacterComparator object.
		 * @param \php\lang\PHPBoolean $ascending The ordering direction. Default &lt;true&gt; as ascending.
		 */
		protected function __construct(\php\lang\PHPBoolean $ascending = null) {
			parent::__construct($ascending);
		}

		/**
		 * Compares its two element in a list for order. Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
		 * @param \php\lang\PHPObject $object1 The first object to be compared.
		 * @param \php\lang\PHPObject $object2 The second object to be compared.
		 * @return int
		 */
		public function compare(\php\lang\PHPObject $object1 = null, \php\lang\PHPObject $object2 = null) {
			$return = parent::compare($object1, $object2);
			if ($return === 0) {
				if ($this->ascending) {
					$return += strcmp($object1->getCharacter(), $object2->getCharacter());
				} else {
					$return += strcmp($object2->getCharacter(), $object1->getCharacter());
				}
			}
			return $return;
		}

	}

}