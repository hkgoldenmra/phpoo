<?php

/**
 * Description of \php\util\google\OAuth2
 */

namespace php\util\google {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/io/IOException.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 * The \php\util\google\OAuth2 object is the API for Google OAuth2.
	 */
	class OAuth2 extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\google\OAuth2 object.
		 * @param \php\lang\PHPString $clientId The client ID generated in Google API Console.
		 * @param \php\lang\PHPString $clientSecret The client secret generated in Google API Console.
		 * @param \php\lang\PHPString $apiKey The API Key generated in Google API Console.
		 * @return \php\util\google\OAuth2
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $clientId, \php\lang\PHPString $clientSecret, \php\lang\PHPString $apiKey) {
			return new OAuth2($clientId, $clientSecret, $apiKey);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private $clientId;
		private $clientSecret;
		private $apiKey;
		private $tokenType;
		private $accessToken;
		private $refreshToken;

		/**
		 * Constructs a \php\util\google\OAuth2 object.
		 * @param \php\lang\PHPString $clientId The client ID generated in Google API Console.
		 * @param \php\lang\PHPString $clientSecret The client secret generated in Google API Console.
		 * @param \php\lang\PHPString $apikey The API Key generated in Google API Console.
		 */
		protected function __construct(\php\lang\PHPString $clientId, \php\lang\PHPString $clientSecret, \php\lang\PHPString $apikey) {
			parent::__construct();
			$this->clientId = $clientId->getString();
			$this->clientSecret = $clientSecret->getString();
			$this->apiKey = $apikey->getString();
		}

		/**
		 * Generates the URL of the given scopes.
		 * @param \php\util\collections\LinkedList $scopes The list of scopes need to verify.
		 * @return \php\net\URL
		 */
		public function generateURL(\php\util\collections\LinkedList $scopes) {
			$queryString = \php\lang\PHPString::newInstance('');
			$queryString = $queryString->append(\php\lang\PHPString::newInstance('response_type=code&redirect_uri=urn:ietf:wg:oauth:2.0:oob&client_id=' . $this->clientId . '&scope='));
			while ($scopes->size()->isPositive()->getBoolean()) {
				$scope = $scopes->getFirst();
				$scopes->deleteFirst();
				$queryString = $queryString->append($scope->toString());
				if ($scopes->size()->isPositive()->getBoolean()) {
					$queryString = $queryString->append(\php\lang\PHPString::newInstance('+'));
				}
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance('https')
					, \php\lang\PHPString::newInstance('accounts.google.com')
					, \php\lang\PHPString::newInstance('/o/oauth2/v2/auth')
					, null
					, null
					, null
					, $queryString
			);
			return $url;
		}

		/**
		 * Retrieves the tokens to this object.
		 * @param \php\lang\PHPString $code The verify code to retrieve the tokens.
		 * @throws \php\io\IOException
		 */
		public function retrieveTokens(\php\lang\PHPString $code) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance('https')
					, \php\lang\PHPString::newInstance('www.googleapis.com')
					, \php\lang\PHPString::newInstance('/oauth2/v4/token')
			);
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/x-www-form-urlencoded'));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('grant_type=authorization_code&redirect_uri=urn:ietf:wg:oauth:2.0:oob'));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('&code=' . $code->getString()));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('&client_id=' . $this->clientId));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('&client_secret=' . $this->clientSecret));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				$this->tokenType = $json['token_type'];
				$this->accessToken = $json['access_token'];
				$this->refreshToken = $json['refresh_token'];
			}
		}

		/**
		 * Refresh the tokens from this object.
		 * @throws \php\io\IOException
		 */
		public function refreshTokens() {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance('https')
					, \php\lang\PHPString::newInstance('www.googleapis.com')
					, \php\lang\PHPString::newInstance('/oauth2/v4/token')
			);
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/x-www-form-urlencoded'));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('grant_type=refresh_token'));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('&client_id=' . $this->clientId));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('&client_secret=' . $this->clientSecret));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance('&refresh_token=' . $this->refreshToken));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				$this->tokenType = $json['token_type'];
				$this->accessToken = $json['access_token'];
			}
		}

		/**
		 * Returns the token type of this object.
		 * @return \php\lang\PHPString
		 */
		public function getTokenType() {
			return \php\lang\PHPString::newInstance($this->tokenType);
		}

		/**
		 * Returns the access token of this object.
		 * @return \php\lang\PHPString
		 */
		public function getAccessToken() {
			return \php\lang\PHPString::newInstance($this->accessToken);
		}

	}

}