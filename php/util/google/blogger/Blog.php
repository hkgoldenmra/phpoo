<?php

/**
 * Description of \php\util\google\blogger\Blog
 */

namespace php\util\google\blogger {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Arrays.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\blogger\Blog object is the API for Google Blogger.
	 */
	class Blog extends \php\lang\PHPObject {

		/**
		 * Returns the string of full permission of scope for Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_FULL() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/blogger');
		}

		/**
		 * Returns the string of readonly permission of scope for Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_READONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/blogger.readonly');
		}

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/blogger/v3';

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toEntry($entry) {
			$return = new Blog();
			$return->id = $entry['id'];
			$return->title = $entry['name'];
			$return->description = $entry['description'];
			$return->url = \php\net\URI::parseURI(\php\lang\PHPString::newInstance($entry['url']));
			return $return;
		}

		/**
		 * Returns a list of Blog objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listBlogs(\php\util\google\OAuth2 $oAuth2) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/users/self/blogs')
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				$return = \php\util\collections\Vector::newInstance();
				foreach ($json['items'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				return $return;
			}
		}

		/**
		 * Returns a Blog object of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Blog object.
		 * @return \php\util\google\blogger\Blog
		 * @throws \php\io\IOException
		 */
		public static function getBlog(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		private $id;
		private $title;
		private $description;
		private $url;

		/**
		 * Constructs a \php\util\google\blogger\Blog objcet.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Blog object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the title of Blog object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

		/**
		 * Returns the description of Blog object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getDescription() {
			return \php\lang\PHPString::newInstance($this->description);
		}

		/**
		 * Returns the URL of Blog object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getURL() {
			return \php\lang\PHPString::newInstance($this->url);
		}

	}

}