<?php

/**
 * Description of \php\util\google\blogger\Comment
 */

namespace php\util\google\blogger {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Arrays.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\blogger\Comment object is the API for Google Blogger.
	 */
	class Comment extends \php\lang\PHPObject {

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/blogger/v3';

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toEntry($entry) {
			$return = new Comment();
			$return->blogId = $entry['blog']['id'];
			$return->postId = $entry['post']['id'];
			$return->id = $entry['id'];
			$return->message = $entry['content'];
			$return->author = $entry['author']['displayName'];
			$return->status = $entry['status'];
			return $return;
		}

		/**
		 * Returns a list of Comment objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listComments(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listComments($return, $oAuth2, $blogId);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listComments(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('maxResults=500&status=emptied&status=live&status=pending&status=spam');
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/comments')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['items'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listComments($return, $oAuth2, $blogId, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		/**
		 * Returns a list of Comment objects in a Post object of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $postId The ID of Post object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listCommentsInPost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $postId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listCommentsInPost($return, $oAuth2, $blogId, $postId);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listCommentsInPost(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $postId, \php\lang\PHPString $pageToken = null) {
			$query = \php\lang\PHPString::newInstance('maxResults=500&status=emptied&status=live&status=pending&status=spam');
			if ($pageToken !== null) {
				$query = $query->append(\php\lang\PHPString::newInstance('&pageToken='));
				$query = $query->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $postId->getString() . '/comments')
					, null
					, null
					, null
					, $query
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['items'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listCommentsInPost($return, $oAuth2, $blogId, $postId, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		/**
		 * Returns a Comment objects in a Post object of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $postId The ID of Post object.
		 * @param \php\lang\PHPString $id The ID of Comment object.
		 * @return \php\util\google\blogger\Comment
		 * @throws \php\io\IOException
		 */
		public static function getComment(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $postId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $postId->getString() . '/comments/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Deletes a Comment objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $postId The ID of Post object.
		 * @param \php\lang\PHPString $id The ID of Comment object.
		 * @throws \php\io\IOException
		 */
		public static function deleteComment(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $postId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $postId->getString() . '/comments/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero() && is_array($json)) {
				if (array_key_exists('error_description', $json)) {
					$body = $json['error_description'];
				} else if (array_key_exists('error', $json)) {
					$body = $json['error']['message'];
				} else {
					$body = $urlConnection->getResponseMessage()->getString();
				}
				throw new \php\io\IOException($body);
			}
		}

		private $blogId;
		private $postId;
		private $id;
		private $message;
		private $author;
		private $status;

		/**
		 * Constructs a \php\util\google\blogger\Comment objcet.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Blog object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getBlogId() {
			return \php\lang\PHPString::newInstance($this->blogId);
		}

		/**
		 * Returns the ID of Post object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getPostId() {
			return \php\lang\PHPString::newInstance($this->postId);
		}

		/**
		 * Returns the ID of Comment object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the message of Comment object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getMessage() {
			return \php\lang\PHPString::newInstance($this->message);
		}

		/**
		 * Returns the author of Comment object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getAuthor() {
			return \php\lang\PHPString::newInstance($this->author);
		}

		/**
		 * Returns the status of Comment object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getStatus() {
			return \php\lang\PHPString::newInstance($this->status);
		}

	}

}