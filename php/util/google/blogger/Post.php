<?php

/**
 * Description of \php\util\google\blogger\Post
 */

namespace php\util\google\blogger {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Arrays.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\blogger\Post object is the API for Google Blogger.
	 */
	class Post extends \php\lang\PHPObject {

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/blogger/v3';

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toEntry($entry) {
			$return = new Post();
			$return->blogId = $entry['blog']['id'];
			$return->id = $entry['id'];
			$return->title = $entry['title'];
			$return->content = $entry['content'];
			$return->url = \php\net\URI::parseURI(\php\lang\PHPString::newInstance($entry['url']));
			$return->tags = \php\util\collections\Vector::newInstance();
			if (array_key_exists('labels', $entry)) {
				foreach ($entry['labels'] as $label) {
					$return->tags->add(\php\lang\PHPString::newInstance($label));
				}
			}
			$return->status = $entry['status'];
			return $return;
		}

		private static function toEntryString(\php\lang\PHPString $title, \php\lang\PHPString $content, \php\util\collections\LinkedList $tags = null, \php\lang\PHPBoolean $isDraft = null) {
			$json = array();
			$json['kind'] = 'blogger#post';
			$json['title'] = $title->getString();
			$json['content'] = $content->getString();
			if ($tags !== null) {
				$json['labels'] = array();
				for ($i = \php\lang\PHPNumber::ZERO(); $tags->size()->isPositive()->getBoolean(); $i = $i->increase()) {
					$label = $tags->getFirst();
					$tags->deleteFirst();
					array_push($json['labels'], $label->toString()->getString());
				}
			}
			return trim(json_encode($json));
		}

		/**
		 * Adds a Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $title The title of the Post object.
		 * @param \php\lang\PHPString $content The content of the Post object.
		 * @param \php\util\collections\LinkedList $tags The list of tags of the Post object. Default &lt;empty&gt;.
		 * @param \php\lang\PHPBoolean $isDraft This Post object will not publish immediately. Default true.
		 * @return \php\util\google\blogger\Post
		 * @throws \php\io\IOException
		 */
		public static function addPost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $title, \php\lang\PHPString $content, \php\util\collections\LinkedList $tags = null, \php\lang\PHPBoolean $isDraft = null) {
			if ($isDraft === null) {
				$isDraft = \php\lang\PHPBoolean::newInstance(true);
			}
			$queryString = \php\lang\PHPString::newInstance('isDraft=' . (($isDraft->getBoolean()) ? 'true' : 'false'));
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title, $content, $tags, $isDraft)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Updates a Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $id The ID of Post object.
		 * @param \php\lang\PHPString $title The title of the Post object.
		 * @param \php\lang\PHPString $content The content of the Post object.
		 * @param \php\util\collections\LinkedList $tags The list of tags of the Post object. Default &lt;empty&gt;.
		 * @return \php\util\google\blogger\Post
		 * @throws \php\io\IOException
		 */
		public static function updatePost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $id, \php\lang\PHPString $title, \php\lang\PHPString $content, \php\util\collections\LinkedList $tags = null) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PUT());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title, $content, $tags)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Returns a list of Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listPosts(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listPosts($return, $oAuth2, $blogId);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listPosts(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $pageToken = null) {
			$query = \php\lang\PHPString::newInstance('maxResults=500&orderBy=published&status=draft&status=live&status=scheduled');
			if ($pageToken !== null) {
				$query = $query->append(\php\lang\PHPString::newInstance('&pageToken='));
				$query = $query->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts')
					, null
					, null
					, null
					, $query
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['items'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listPosts($return, $oAuth2, $blogId, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		/**
		 * Returns a list of Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $id The ID of Post object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function getPost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Deletes a Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $id The ID of Post object.
		 * @throws \php\io\IOException
		 */
		public static function deletePost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			}
		}

		/**
		 * Publishes a Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $id The ID of Post object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function publishPost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $id->getString() . '/publish')
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Drafts a Post objects of Google Blogger.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $blogId The ID of Blog object.
		 * @param \php\lang\PHPString $id The ID of Post object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function draftPost(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $blogId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/blogs/' . $blogId->getString() . '/posts/' . $id->getString() . '/revert')
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		private $blogId;
		private $id;
		private $title;
		private $content;
		private $url;
		private $tags;
		private $status;

		/**
		 * Constructs a \php\util\google\blogger\Post objcet.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Blog object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getBlogId() {
			return \php\lang\PHPString::newInstance($this->blogId);
		}

		/**
		 * Returns the ID of Post object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the title of Post object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

		/**
		 * Returns the content of Post object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getContent() {
			return \php\lang\PHPString::newInstance($this->content);
		}

		/**
		 * Returns the URL of Post object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getURL() {
			return $this->url;
		}

		/**
		 * Returns the list of tags of the Post object of Google Blogger.
		 * @return \php\
		 */
		public function listTags() {
			return $this->tags;
		}

		/**
		 * Returns the status of Post object of Google Blogger.
		 * @return \php\lang\PHPString
		 */
		public function getStatus() {
			return \php\lang\PHPString::newInstance($this->status);
		}

	}

}