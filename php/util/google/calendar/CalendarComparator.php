<?php

namespace php\util\google\calendar {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/comparators/Comparator.php');

	class CalendarComparator extends \php\util\comparators\Comparator {

		public static function newInstance(\php\lang\PHPBoolean $ascending = null) {
			return new CalendarComparator($ascending);
		}

		protected function __construct(\php\lang\PHPBoolean $ascending = null) {
			parent::__construct($ascending);
		}

		public function compare(\php\lang\PHPObject $object1 = null, \php\lang\PHPObject $object2 = null) {
			$return = parent::compare($object1, $object2);
			if ($return === 0) {
				if ($this->ascending) {
					$return += strcmp($object1->getTitle()->getString(), $object2->getTitle()->getString());
				} else {
					$return += strcmp($object2->getTitle()->getString(), $object1->getTitle()->getString());
				}
			}
			return $return;
		}

	}

}