<?php

/**
 * Description of \php\util\google\blogger\Event
 */

namespace php\util\google\calendar {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/io/IOException.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/collections/Arrays.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');
	include_once('php/util/google/calendar/EventComparator.php');

	/**
	 * The \php\util\google\blogger\Event object is the API for Google Calendar.
	 */
	class Event extends \php\lang\PHPObject {

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/calendar/v3/calendars/%s/events';
		private static $regexDateTimeUTC = '/^(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2})T(?P<hour>[0-9]{2}):(?P<minute>[0-9]{2}):(?P<second>[0-9]{2})(?P<utcpn>[\-\+])(?P<utchour>[0-9]{2}):(?P<utcminute>[0-9]{2})$/';

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toCalendarFromUTC($fields, $key) {
			if (array_key_exists($key, $fields) && array_key_exists('dateTime', $fields[$key]) && preg_match(self::$regexDateTimeUTC, $fields[$key]['dateTime'], $matches) > 0) {
				$timezone = intval($matches['utchour']) + intval($matches['utcminute']) / 60;
				if ($matches['utcpn'] == '-') {
					$timezone = -$timezone;
				}
				return \php\util\Calendar::newInstanceByDateTime(
						\php\lang\PHPNumber::newInstance($matches['year'])
						, \php\lang\PHPNumber::newInstance($matches['month'])
						, \php\lang\PHPNumber::newInstance($matches['day'])
						, \php\lang\PHPNumber::newInstance($matches['hour'])
						, \php\lang\PHPNumber::newInstance($matches['minute'])
						, \php\lang\PHPNumber::newInstance($matches['second'])
						, \php\lang\PHPNumber::ZERO()
						, \php\lang\PHPNumber::newInstance($timezone)
				);
			} else {
				return null;
			}
		}

		private static function toEntry($entry) {
			$return = new Event();
			$return->calendarId = $entry['organizer']['email'];
			$return->id = $entry['id'];
			if (array_key_exists('summary', $entry)) {
				$return->title = $entry['summary'];
			}
			if (array_key_exists('description', $entry)) {
				$return->description = $entry['description'];
			}
			$return->start = self::toCalendarFromUTC($entry, 'start');
			$return->end = self::toCalendarFromUTC($entry, 'end');
			$return->url = \php\net\URI::parseURI(\php\lang\PHPString::newInstance($entry['htmlLink']));
			return $return;
		}

		private static function toDateTimeString(\php\util\Calendar $calendar) {
			$return = '';
			$return .= $calendar->toDateString()->getString();
			$return .= 'T' . $calendar->toTimeString()->getString();
			$return .= $calendar->toTimezoneString()->getString();
			return $return;
		}

		private static function toEntryString(\php\util\Calendar $start, \php\util\Calendar $end, \php\lang\PHPString $title, \php\lang\PHPString $description) {
			$return = array();
			$return['start'] = array();
			$return['start']['dateTime'] = self::toDateTimeString($start);
			$return['end'] = array();
			$return['end']['dateTime'] = self::toDateTimeString($end);
			$return['summary'] = $title->getString();
			$return['description'] = $description->getString();
			return trim(json_encode($return));
		}

		/**
		 * Deletes an Event object of Google Calendar.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $calendarId The ID of Calendar object.
		 * @param \php\lang\PHPString $id The ID of Event object.
		 * @throws \php\io\IOException
		 */
		public static function deleteEvent(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $calendarId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(sprintf(self::$defaultPath, $calendarId->getString()) . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('If-Match'), \php\lang\PHPString::newInstance('*'));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			}
		}

		/**
		 * Returns an Event object of Google Calendar.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $calendarId The ID of Calendar object.
		 * @param \php\lang\PHPString $id The ID of Event object.
		 * @return \php\util\google\calendar\Event
		 * @throws \php\io\IOException
		 */
		public static function getEvent(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $calendarId, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(sprintf(self::$defaultPath, $calendarId->getString()) . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Returns a list of Event object of Google Calendar.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $calendarId The ID of Calendar object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listEvents(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $calendarId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listEvents($return, $oAuth2, $calendarId);
				$comparator = \php\util\collections\Arrays::newInstance(EventComparator::newInstance());
				return $comparator->sort($return);
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listEvents(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $calendarId, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('max-results=1000');
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(sprintf(self::$defaultPath, $calendarId->getString()))
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['items'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listEvents($return, $oAuth2, $calendarId, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		/**
		 * Adds a list of Event object of Google Calendar.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $calendarId The ID of Calendar object.
		 * @param \php\util\Calendar $start The start date time of Event object.
		 * @param \php\util\Calendar $end The end date time of Event object.
		 * @param \php\lang\PHPString $title The title of Event object. Default &quot;&quot;.
		 * @param \php\lang\PHPString $description The description of Event object. Default &quot;&quot;.
		 * @return \php\util\google\calendar\Event
		 * @throws \php\io\IOException
		 */
		public static function addEvent(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $calendarId, \php\util\Calendar $start, \php\util\Calendar $end, \php\lang\PHPString $title = null, \php\lang\PHPString $description = null) {
			if ($title === null) {
				$title = \php\lang\PHPString::newInstance('');
			}
			if ($description === null) {
				$description = \php\lang\PHPString::newInstance('');
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(sprintf(self::$defaultPath, $calendarId->getString()))
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($start, $end, $title, $description)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Updates a list of Event object of Google Calendar.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $calendarId The ID of Calendar object.
		 * @param \php\lang\PHPString $id  The ID of Event object.
		 * @param \php\util\Calendar $start The start date time of Event object. Default &lt;original start date time&gt;.
		 * @param \php\util\Calendar $end The end date time of Event object. Default &lt;original end date time&gt;.
		 * @param \php\lang\PHPString $title The title of Event object. Default &lt;original title&gt;.
		 * @param \php\lang\PHPString $description The description of Event object. Default &lt;original description&gt;.
		 * @return \php\util\google\calendar\Event
		 * @throws \php\io\IOException
		 */
		public static function updateEvent(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $calendarId, \php\lang\PHPString $id, \php\util\Calendar $start = null, \php\util\Calendar $end = null, \php\lang\PHPString $title = null, \php\lang\PHPString $description = null) {
			$event = self::getEvent($oAuth2, $calendarId, $id);
			if ($start === null) {
				$start = $event->getStart();
			}
			if ($end === null) {
				$end = $event->getEnd();
			}
			if ($title === null) {
				$title = $event->getTitle();
			}
			if ($description === null) {
				$description = $event->getDescription();
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(sprintf(self::$defaultPath, $calendarId->getString()) . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PUT());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('If-Match'), \php\lang\PHPString::newInstance('*'));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($start, $end, $title, $description)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		private $calendarId;
		private $id;
		private $title;
		private $description;
		private $start;
		private $end;
		private $url;

		/**
		 * Constructs a \php\util\google\calendar\Event objcet.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Calendar object of Google Calendar.
		 * @return \php\lang\PHPString
		 */
		public function getCalendarId() {
			return \php\lang\PHPString::newInstance($this->calendarId);
		}

		/**
		 * Returns the ID of Event object of Google Calendar.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the title of Event object of Google Calendar.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

		/**
		 * Returns the description of Event object of Google Calendar.
		 * @return \php\lang\PHPString
		 */
		public function getDescription() {
			return \php\lang\PHPString::newInstance($this->description);
		}

		/**
		 * Returns the starting date time of Event object of Google Calendar.
		 * @return \php\util\Calendar
		 */
		public function getStart() {
			return $this->start;
		}

		/**
		 * Returns the ending date time of Event object of Google Calendar.
		 * @return \php\util\Calendar
		 */
		public function getEnd() {
			return $this->end;
		}

		/**
		 * Returns the URL of Event object of Google Calendar.
		 * @return \php\net\URL
		 */
		public function getURL() {
			return $this->url;
		}

	}

}