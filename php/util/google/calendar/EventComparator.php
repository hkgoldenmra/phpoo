<?php

namespace php\util\google\calendar {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/comparators/Comparator.php');

	class EventComparator extends \php\util\comparators\Comparator {

		public static function newInstance(\php\lang\PHPBoolean $ascending = null) {
			return new EventComparator($ascending);
		}

		protected function __construct(\php\lang\PHPBoolean $ascending = null) {
			parent::__construct($ascending);
		}

		public function compare(\php\lang\PHPObject $object1 = null, \php\lang\PHPObject $object2 = null) {
			$return = parent::compare($object1, $object2);
			if ($return === 0) {
				$start1 = $object1->getStart();
				if ($start1 !== null) {
					$start1 = $start1->getTimestamp()->getNumber();
				}
				$start2 = $object2->getStart();
				if ($start2 !== null) {
					$start2 = $start2->getTimestamp()->getNumber();
				}
				if ($this->ascending) {
					$return += strcmp($start1, $start2);
				} else {
					$return += strcmp($start2, $start1);
				}
			}
			return $return;
		}

	}

}