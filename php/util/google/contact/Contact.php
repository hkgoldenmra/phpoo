<?php

/**
 * Description of \php\util\google\contact\Contact
 */

namespace php\util\google\contact {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Arrays.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/google/OAuth2.php');
	include_once('php/util/google/contact/ContactComparator.php');

	/**
	 * The \php\util\google\contact\Contact object is the API for Google Contact.
	 */
	class Contact extends \php\lang\PHPObject {

		/**
		 * Returns the string of full permission of scope for Google Contact.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_FULL() {
			return \php\lang\PHPString::newInstance('https://www.google.com/m8/feeds');
		}

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.google.com';
		private static $defaultPath = '/m8/feeds/contacts/default/full';

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/atom+xml; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toEntry(\DOMElement $entry, $namespaces) {
			$return = new Contact();
			if (($temps = $entry->getElementsByTagNameNS($namespaces[''], 'id')) !== null && $temps->length > 0) {
				$return->id = explode('/', urldecode($temps->item(0)->textContent));
				$return->id = end($return->id);
			}
			if (($temps = $entry->getElementsByTagNameNS($namespaces['gd'], 'name')) !== null && $temps->length > 0) {
				$name = $temps->item(0);
				if (($temp = $name->getElementsByTagNameNS($namespaces['gd'], 'namePrefix')) !== null && $temp->length > 0) {
					$return->namePrefix = $temp->item(0)->textContent;
				}
				if (($temp = $name->getElementsByTagNameNS($namespaces['gd'], 'givenName')) !== null && $temp->length > 0) {
					$return->givenName = $temp->item(0)->textContent;
				}
				if (($temp = $name->getElementsByTagNameNS($namespaces['gd'], 'additionalName')) !== null && $temp->length > 0) {
					$return->additionalName = $temp->item(0)->textContent;
				}
				if (($temp = $name->getElementsByTagNameNS($namespaces['gd'], 'familyName')) !== null && $temp->length > 0) {
					$return->familyName = $temp->item(0)->textContent;
				}
				if (($temp = $name->getElementsByTagNameNS($namespaces['gd'], 'nameSuffix')) !== null && $temp->length > 0) {
					$return->nameSuffix = $temp->item(0)->textContent;
				}
			}
			if (($temps = $entry->getElementsByTagNameNS($namespaces['gd'], 'email')) !== null) {
				for ($k = 0; $k < $temps->length; $k++) {
					$temp = $temps->item($k);
					$attributes = $temp->attributes;
					$textContent = $attributes->getNamedItem('address')->textContent;
					if (($primary = $attributes->getNamedItem('primary')) !== null && $primary == 'true') {
						$return->emails->addFirst(\php\lang\PHPString::newInstance($textContent));
					} else {
						$return->emails->addLast(\php\lang\PHPString::newInstance($textContent));
					}
				}
			}
			if (($temps = $entry->getElementsByTagNameNS($namespaces['gd'], 'phoneNumber')) !== null) {
				for ($k = 0; $k < $temps->length; $k++) {
					$temp = $temps->item($k);
					$attributes = $temp->attributes;
					$textContent = $temp->textContent;
					if (($primary = $attributes->getNamedItem('primary')) !== null && $primary == 'true') {
						$return->telephones->addFirst(\php\lang\PHPString::newInstance($textContent));
					} else {
						$return->telephones->addLast(\php\lang\PHPString::newInstance($textContent));
					}
				}
			}
			if (($temps = $entry->getElementsByTagNameNS($namespaces['gd'], 'structuredPostalAddress')) !== null) {
				for ($k = 0; $k < $temps->length; $k++) {
					$address = $temps->item($k);
					$map = \php\util\collections\Map::newInstance();
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'street')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('street'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'pobox')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('pobox'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'neighborhood')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('neighborhood'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'city')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('city'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'region')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('region'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'postcode')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('postcode'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					if (($temp = $address->getElementsByTagNameNS($namespaces['gd'], 'country')) !== null && $temp->length > 0) {
						$map->put(\php\lang\PHPString::newInstance('country'), \php\lang\PHPString::newInstance($temp->item(0)->textContent));
					}
					$attributes = $address->attributes;
					if (($primary = $attributes->getNamedItem('primary')) !== null && $primary == 'true') {
						$return->addresses->addFirst($map);
					} else {
						$return->addresses->addLast($map);
					}
				}
			}
			return $return;
		}

		private static function toEntryString(\php\lang\PHPString $title = null) {
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$entry = $document->createElement('entry');
			$entry->setAttribute('xmlns', 'http://www.w3.org/2005/Atom');
			if ($title !== null) {
				$entry->appendChild($document->createElement('title', $title->getString()));
			}
			$document->appendChild($entry);
			return trim($document->saveXML());
		}

		/**
		 * Deletes a Contact object of Google Contact.
		 */
		public static function deleteContact(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('If-Match'), \php\lang\PHPString::newInstance('*'));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			if (strlen($body) > 0) {
				$json = json_decode($body, true);
				if (array_key_exists('error_description', $json)) {
					throw new \php\io\IOException($json['error_description']);
				} else if (array_key_exists('error', $json)) {
					throw new \php\io\IOException($json['error']['message']);
				} else {
					throw new \php\io\IOException('Unknown Error Occurs.');
				}
			}
		}

		/**
		 * Returns a Contact object of Google Contact.
		 */
		public static function getContact(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id = null) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . (($id === null) ? '?max-results=1000' : '/' . $id->getString()))
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$sxml = simplexml_load_string($body);
			$namespaces = $sxml->getNamespaces(true);
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$document->loadXML($body);
			if ($id === null) {
				$return = \php\util\collections\Vector::newInstance();
				if (($feeds = $document->getElementsByTagNameNS($namespaces[''], 'feed')) !== null && $feeds->length > 0) {
					$feed = $feeds->item(0);
					if (($entries = $feed->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
						for ($i = 0; $i < $entries->length; $i++) {
							$return->add(self::toEntry($entries->item($i), $namespaces));
						}
					}
				}
				$comparator = \php\util\collections\Arrays::newInstance(ContactComparator::newInstance());
				return $comparator->sort($return);
			} else {
				if (($entries = $document->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
					return self::toEntry($entries->item(0), $namespaces);
				} else {
					throw new \Exception();
				}
			}
		}

		/**
		 * Adds a Contact object of Google Contact.
		 */
		public static function addContact(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $title) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$sxml = simplexml_load_string($body);
			$namespaces = $sxml->getNamespaces(true);
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$document->loadXML($body);
			if (($entries = $document->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
				return self::toEntry($entries->item(0), $namespaces);
			} else {
				throw new \Exception();
			}
		}

		/**
		 * Updates a Contact object of Google Contact.
		 */
		public static function updateContact(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPString $title) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PUT());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('If-Match'), \php\lang\PHPString::newInstance('*'));
			$xml = self::toEntryString($title);
			$urlConnection->addBodyString($xml);
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$sxml = simplexml_load_string($body);
			$namespaces = $sxml->getNamespaces(true);
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$document->loadXML($body);
			if (($entries = $document->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
				return self::toEntry($entries->item(0), $namespaces);
			} else {
				throw new \Exception();
			}
		}

		private $id;
		private $namePrefix;
		private $givenName;
		private $additionalName;
		private $familyName;
		private $nameSuffix;
		private $emails;
		private $telephones;
		private $addresses;

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\util\google\contact\Contact objcet.
		 */
		protected function __construct() {
			parent::__construct();
			$this->emails = \php\util\collections\Vector::newInstance();
			$this->telephones = \php\util\collections\Vector::newInstance();
			$this->addresses = \php\util\collections\Vector::newInstance();
		}

		/**
		 * Returns the ID of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the name prefix of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getNamePrefix() {
			return \php\lang\PHPString::newInstance($this->namePrefix);
		}

		/**
		 * Returns the given name of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getGivenName() {
			return \php\lang\PHPString::newInstance($this->givenName);
		}

		/**
		 * Returns the additional name of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getAdditionalName() {
			return \php\lang\PHPString::newInstance($this->additionalName);
		}

		/**
		 * Returns the family name of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getFamilyName() {
			return \php\lang\PHPString::newInstance($this->familyName);
		}

		/**
		 * Returns the name suffix of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getNameSuffix() {
			return \php\lang\PHPString::newInstance($this->nameSuffix);
		}

		/**
		 * Returns the emails of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getEmails() {
			return $this->emails;
		}

		/**
		 * Returns the telephones of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getTelephones() {
			return $this->telephones;
		}

		/**
		 * Returns the addresses of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getAddresses() {
			return $this->addresses;
		}

	}

}