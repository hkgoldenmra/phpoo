<?php

namespace php\util\google\contact {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/comparators/Comparator.php');

	class ContactComparator extends \php\util\comparators\Comparator {

		public static function newInstance(\php\lang\PHPBoolean $ascending = null) {
			return new ContactComparator($ascending);
		}

		protected function __construct(\php\lang\PHPBoolean $ascending = null) {
			parent::__construct($ascending);
		}

		public function compare(\php\lang\PHPObject $object1 = null, \php\lang\PHPObject $object2 = null) {
			$return = parent::compare($object1, $object2);
			if ($return === 0) {
				if ($this->ascending) {
					$return += strcmp($object1->getNamePrefix()->getString(), $object2->getNamePrefix()->getString());
//					$return += strcmp($object1->getGivenName()->getString(), $object2->getGivenName()->getString());
//					$return += strcmp($object1->getAdditionalName()->getString(), $object2->getAdditionalName()->getString());
//					$return += strcmp($object1->getFamilyName()->getString(), $object2->getFamilyName()->getString());
//					$return += strcmp($object1->getNameSuffix()->getString(), $object2->getNameSuffix()->getString());
				} else {
					$return += strcmp($object2->getNamePrefix()->getString(), $object1->getNamePrefix()->getString());
//					$return += strcmp($object2->getGivenName()->getString(), $object1->getGivenName()->getString());
//					$return += strcmp($object2->getAdditionalName()->getString(), $object1->getAdditionalName()->getString());
//					$return += strcmp($object2->getFamilyName()->getString(), $object1->getFamilyName()->getString());
//					$return += strcmp($object2->getNameSuffix()->getString(), $object1->getNameSuffix()->getString());
				}
			}
			return $return;
		}

	}

}