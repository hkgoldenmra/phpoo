<?php

/**
 * Description of \php\util\google\contact\Group
 */

namespace php\util\google\contact {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Arrays.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');
	include_once('php/util/google/contact/GroupComparator.php');

	/**
	 * The \php\util\google\contact\Group object is the API for Google Contact.
	 */
	class Group extends \php\lang\PHPObject {

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.google.com';
		private static $defaultPath = '/m8/feeds/groups/default/full';

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/atom+xml; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toEntry(\DOMElement $entry, $namespaces) {
			$return = new Group();
			if (($temps = $entry->getElementsByTagNameNS($namespaces[''], 'id')) !== null && $temps->length > 0) {
				$return->id = explode('/', $temps->item(0)->textContent);
				$return->id = end($return->id);
			}
			if (($temps = $entry->getElementsByTagNameNS($namespaces[''], 'title')) !== null && $temps->length > 0) {
				$return->title = $temps->item(0)->textContent;
			}
			return $return;
		}

		private static function toEntryString(\php\lang\PHPString $title = null) {
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$entry = $document->createElement('entry');
			$entry->setAttribute('xmlns', 'http://www.w3.org/2005/Atom');
			if ($title !== null) {
				$entry->appendChild($document->createElement('title', $title->getString()));
			}
			$document->appendChild($entry);
			return trim($document->saveXML());
		}

		/**
		 * Deletes a Group object of Google Contact.
		 */
		public static function deleteGroup(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('If-Match'), \php\lang\PHPString::newInstance('*'));
			$urlConnection->send();
			if ($urlConnection->getResponseCode()->equals(\php\lang\PHPNumber::newInstance(200))->not()->getBoolean()) {
				throw new \Exception();
			}
		}

		/**
		 * Returns a Group object of Google Contact.
		 */
		public static function getGroup(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id = null) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . (($id === null) ? '?max-results=1000' : '/' . $id->getString()))
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$sxml = simplexml_load_string($body);
			$namespaces = $sxml->getNamespaces(true);
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$document->loadXML($body);
			if ($id === null) {
				$return = \php\util\collections\Vector::newInstance();
				if (($feeds = $document->getElementsByTagNameNS($namespaces[''], 'feed')) !== null && $feeds->length > 0) {
					$feed = $feeds->item(0);
					if (($entries = $feed->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
						for ($i = 0; $i < $entries->length; $i++) {
							$return->add(self::toEntry($entries->item($i), $namespaces));
						}
					}
				}
				$comparator = \php\util\collections\Arrays::newInstance(GroupComparator::newInstance());
				return $comparator->sort($return);
			} else {
				if (($entries = $document->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
					return self::toEntry($entries->item(0), $namespaces);
				} else {
					throw new \Exception();
				}
			}
		}

		/**
		 * Adds a Group object of Google Contact.
		 */
		public static function addGroup(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $title) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$sxml = simplexml_load_string($body);
			$namespaces = $sxml->getNamespaces(true);
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$document->loadXML($body);
			if (($entries = $document->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
				return self::toEntry($entries->item(0), $namespaces);
			} else {
				throw new \Exception();
			}
		}

		/**
		 * Updates a Group object of Google Contact.
		 */
		public static function updateGroup(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPString $title) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PUT());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('If-Match'), \php\lang\PHPString::newInstance('*'));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$sxml = simplexml_load_string($body);
			$namespaces = $sxml->getNamespaces(true);
			$document = new \DOMDocument('1.0', \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString());
			$document->loadXML($body);
			if (($entries = $document->getElementsByTagNameNS($namespaces[''], 'entry')) !== null && $entries->length > 0) {
				return self::toEntry($entries->item(0), $namespaces);
			} else {
				throw new \Exception();
			}
		}

		private $id;
		private $title;

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\util\google\contact\Group objcet.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Group object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the title of Contact object of Google Contact.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

	}

}