<?php

/**
 * Description of \php\util\google\drive\File
 */

namespace php\util\google\drive {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/io/IOException.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\drive\File object is the API for Google Drive.
	 */
	class File extends \php\lang\PHPObject {

		/**
		 * Returns the string of full permission of scope for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_FULL() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive');
		}

		/**
		 * Returns the string of readonly permission of scope for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_READONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.readonly');
		}

		/**
		 * Returns the string of application full permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_APPLICATION() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.appfolder');
		}

		/**
		 * Returns the string of application readonly permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_APPLICATION_READONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.apps.readonly');
		}

		/**
		 * Returns the string of file permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_FILE() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.file');
		}

		/**
		 * Returns the string of install permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_INSTALL() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.install');
		}

		/**
		 * Returns the string of metadata full permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_METADATA() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.metadata');
		}

		/**
		 * Returns the string of metadata readonly permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_METADATA_READONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.metadata.readonly');
		}

		/**
		 * Returns the string of script permission for Google Drive.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_SCRIPT() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/drive.scripts');
		}

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/drive/v3/files';
		private static $fields = 'kind,id,name,mimeType,starred,trashed,explicitlyTrashed,parents,spaces,version,webContentLink,webViewLink,iconLink,hasThumbnail,thumbnailLink,thumbnailVersion,viewedByMe,viewedByMeTime,createdTime,modifiedTime,modifiedByMeTime,modifiedByMe,owners,lastModifyingUser,shared,ownedByMe,capabilities,viewersCanCopyContent,copyRequiresWriterPermission,writersCanShare,permissions,permissionIds,originalFilename,fullFileExtension,fileExtension,md5Checksum,size,quotaBytesUsed,headRevisionId,isAppAuthorized';
		private static $regexDateTime = '/^(?P<year>[0-9]{4})-(?P<month>[0-9]{2})-(?P<day>[0-9]{2})T(?P<hour>[0-9]{2}):(?P<minute>[0-9]{2}):(?P<second>[0-9]{2})\.(?P<millisecond>[0-9]{3})Z$/';

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('GData-Version'), \php\lang\PHPString::newInstance('3'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private static function toCalendarFromUTC($fields, $key) {
			if (array_key_exists($key, $fields) && preg_match(self::$regexDateTime, $fields[$key], $matches) > 0) {
				return \php\util\Calendar::newInstanceByDateTime(
						\php\lang\PHPNumber::newInstance($matches['year'])
						, \php\lang\PHPNumber::newInstance($matches['month'])
						, \php\lang\PHPNumber::newInstance($matches['day'])
						, \php\lang\PHPNumber::newInstance($matches['hour'])
						, \php\lang\PHPNumber::newInstance($matches['minute'])
						, \php\lang\PHPNumber::newInstance($matches['second'])
						, \php\lang\PHPNumber::newInstance($matches['millisecond'] * 1000000)
				);
			} else {
				return null;
			}
		}

		private static function toEntry($entry) {
			$return = new File();
			$return->id = $entry['id'];
			$return->name = $entry['name'];
			if (array_key_exists('originalFilename', $entry)) {
				$return->filename = $entry['originalFilename'];
			}
			if (array_key_exists('fileExtension', $entry)) {
				$return->extension = $entry['fileExtension'];
			}
			$return->mime = $entry['mimeType'];
			if (array_key_exists('size', $entry)) {
				$return->size = $entry['size'];
			}
			if (array_key_exists('starred', $entry)) {
				$return->starred = $entry['starred'];
			}
			if (array_key_exists('trashed', $entry)) {
				$return->trashed = $entry['trashed'];
			}
			if (array_key_exists('shared', $entry)) {
				$return->shared = $entry['shared'];
			}
			$return->createdTime = self::toCalendarFromUTC($entry, 'createdTime');
			$return->modifiedTime = self::toCalendarFromUTC($entry, 'modifiedTime');
			return $return;
		}

		private static function toEntryString(\php\lang\PHPString $name, \php\lang\PHPBoolean $starred) {
			$return = array();
			$return['name'] = $name->getString();
			$return['starred'] = $starred->getBoolean();
			return trim(json_encode($return));
		}

		/**
		 * Returns a File object of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of File object.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function getFile(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			if ($id === null) {
				$id = \php\lang\PHPString::newInstance('root');
			}
			$queryString = \php\lang\PHPString::newInstance('fields=' . self::$fields);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Returns the root folder of File object of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function getRootFolder(\php\util\google\OAuth2 $oAuth2) {
			try {
				return self::getFile($oAuth2, \php\lang\PHPString::newInstance('root'));
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		public static function listFiles(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listFiles($return, $oAuth2);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns a list of File objects of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of folder.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		private static function _listFiles(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $q = null, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('fields=nextPageToken,files&orderBy=folder,name&pageSize=1000');
			if ($q !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&q='));
				$queryString = $queryString->append($q);
			}
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['files'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listFiles($return, $oAuth2, $q, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		/**
		 * Returns a list of File objects from specific folder of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of folder.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function listFilesInFolder(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listFiles($return, $oAuth2, \php\lang\PHPString::newInstance(sprintf("'%s'+in+parents+and+trashed=false", $id->getString())));
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns a list of File objects from trash bin of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function listTrashed(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listFiles($return, $oAuth2, \php\lang\PHPString::newInstance('trashed=true'));
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns a list of starred File objects of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function listStarred(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listFiles($return, $oAuth2, \php\lang\PHPString::newInstance('starred=true'));
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns a list of shared File objects of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function listShared(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listFiles($return, $oAuth2, \php\lang\PHPString::newInstance('sharedWithMe=true'));
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Copys a File objects with given name of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of File object.
		 * @param \php\lang\PHPString $name The new file name.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function copyFile(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPString $name = null) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString() . '/copy')
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$json = self::toEntryString($name);
			$urlConnection->addBodyString($json);
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		private static function setFileTrashed(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPBoolean $trashed) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PATCH());
			$json = array(
				'trashed' => $trashed->getBoolean()
			);
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(json_encode($json)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Trashes a File object of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The file ID.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function trashFile(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			try {
				return self::setFileTrashed($oAuth2, $id, \php\lang\PHPBoolean::newInstance(true));
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Restores a File object of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The file ID.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function untrashFile(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			try {
				return self::setFileTrashed($oAuth2, $id, \php\lang\PHPBoolean::newInstance(false));
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Deletes a File object of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The file ID.
		 * @throws \php\io\IOException
		 */
		public static function deleteFile(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			}
		}

		/**
		 * Clears the File objects in trash bin of Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @throws \php\io\IOException
		 */
		public static function emptyTrash(\php\util\google\OAuth2 $oAuth2) {
			try {
				self::deleteFile($oAuth2, \php\lang\PHPString::newInstance('trash'));
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Uploads a file to Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\io\File $file A file will be uploaded.
		 * @param \php\lang\PHPString $name The uploaded file name. Default &original file name&lt;.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function uploadFile(\php\util\google\OAuth2 $oAuth2, \php\io\File $file, \php\lang\PHPString $name = null, \php\lang\PHPBoolean $starred = null) {
			$boundary = md5(\php\util\Calendar::newInstanceByTimestamp()->toString()->getString());
			if ($name === null) {
				$name = $file->getName();
			}
			if ($starred === null) {
				$starred = \php\lang\PHPBoolean::newInstance(false);
			}
			$queryString = \php\lang\PHPString::newInstance('uploadType=multipart&fields=' . self::$fields);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance('/upload' . self::$defaultPath)
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance(sprintf('multipart/related; boundary="%s"', $boundary)));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(sprintf("--%s\n", $boundary)));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(sprintf("Content-Type: application/json; charset=%s\n", \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString())));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance("\n"));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($name . $starred) . "\n"));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(sprintf("--%s\n", $boundary)));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(sprintf("Content-Type: %s; charset=%s\n", $file->getMimeType()->getString(), \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString())));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance("\n"));
			$urlConnection->addBodyFile($file);
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance("\n"));
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(sprintf("--%s--", $boundary)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Updates a file to Google Drive.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of File object.
		 * @param \php\lang\PHPString $name The name of File object. Default &original file name&lt;.
		 * @return \php\util\google\drive\File
		 * @throws \php\io\IOException
		 */
		public static function updateFile(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPString $name = null, \php\lang\PHPBoolean $starred = null) {
			$entry = self::getFile($oAuth2, $id);
			if ($name === null) {
				$name = $entry->getName();
			}
			if ($starred === null) {
				$starred = $entry->isStarred();
			}
			$queryString = \php\lang\PHPString::newInstance('fields=' . self::$fields);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PATCH());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($name, $starred)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		private $id;
		private $name;
		private $filename;
		private $extension;
		private $mime;
		private $size = 0;
		private $starred = false;
		private $trashed = false;
		private $shared = false;
		private $createdTime;
		private $modifiedTime;

		/**
		 * Constructs a \php\util\google\drive\File objcet.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of File object of Google Drive.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the display name of File object of Google Drive.
		 * @return \php\lang\PHPString
		 */
		public function getName() {
			return \php\lang\PHPString::newInstance($this->name);
		}

		/**
		 * Returns the file name of File object of Google Drive.
		 * @return \php\lang\PHPString
		 */
		public function getFilename() {
			return (($this->filename === null) ? null : \php\lang\PHPString::newInstance($this->filename));
		}

		/**
		 * Returns the file extension of File object of Google Drive.
		 * @return \php\lang\PHPString
		 */
		public function getExtension() {
			return (($this->extension === null) ? null : \php\lang\PHPString::newInstance($this->extension));
		}

		/**
		 * Returns the MIME type of File object of Google Drive.
		 * @return \php\lang\PHPString
		 */
		public function getMime() {
			return \php\lang\PHPString::newInstance($this->mime);
		}

		/**
		 * Returns the file size of File object of Google Drive.
		 * @return \php\lang\PHPString
		 */
		public function getSize() {
			return \php\lang\PHPNumber::newInstance($this->size);
		}

		/**
		 * Returns the File object is starred or not of Google Drive.
		 * @return \php\lang\PHPBoolean
		 */
		public function isStarred() {
			return \php\lang\PHPBoolean::newInstance($this->starred);
		}

		/**
		 * Returns the File object is trashed or not of Google Drive.
		 * @return \php\lang\PHPBoolean
		 */
		public function isTrashed() {
			return \php\lang\PHPBoolean::newInstance($this->trashed);
		}

		/**
		 * Returns the File object is shared or not of Google Drive.
		 * @return \php\lang\PHPBoolean
		 */
		public function isShared() {
			return \php\lang\PHPBoolean::newInstance($this->shared);
		}

		/**
		 * Returns the created time of the File object of Google Drive.
		 * @return \php\util\Calendar
		 */
		public function getCreatedTime() {
			return $this->createdTime;
		}

		/**
		 * Returns the modified time of the File object of Google Drive.
		 * @return \php\util\Calendar
		 */
		public function getModifiedTime() {
			return $this->modifiedTime;
		}

	}

}