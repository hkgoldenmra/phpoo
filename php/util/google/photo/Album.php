<?php

/**
 * Description of \php\util\google\photo\Album
 */

namespace php\util\google\photo {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/io/IOException.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\photo\Album object is the API for Google Photo.
	 */
	class Album extends \php\lang\PHPObject {

		/**
		 * Returns the string of full permission of scope for Google Photo.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_FULL() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/photoslibrary');
		}

		/**
		 * Returns the string of readonly permission of scope for Google Photo.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_READONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/photoslibrary.readonly');
		}

		/**
		 * Returns the string of sharing permission of scope for Google Photo.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_SHARING() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/photoslibrary.sharing');
		}

		/**
		 * Returns the string of location permission of scope for Google Photo.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_LOCATION() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/photoslibrary.location');
		}

		/**
		 * Returns the string of append only permission of scope for Google Photo.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_APPEND_ONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/photoslibrary.appendonly');
		}

		/**
		 * Returns the string of readonly application create data permission of scope for Google Photo.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_READONLY_APPCREATEDDATA() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata');
		}

		public static function toEntryString(\php\lang\PHPString $title) {
			$return = array();
			$return['album'] = array();
			$return['album']['title'] = $title->getString();
			return trim(json_encode($return));
		}

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'photoslibrary.googleapis.com';
		private static $defaultPath = '/v1/albums';

		/**
		 * Adds a Album object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $title The title of Album object.
		 * @return \php\util\google\photo\Album
		 * @throws \php\io\IOException
		 */
		public static function addAlbum(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $title) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Updates a Album object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Album object.
		 * @param \php\lang\PHPString $title The title of Album object.
		 * @return \php\util\google\photo\Album
		 * @throws \Exception
		 * @throws \php\io\IOException
		 * @deprecated
		 */
		public static function updateAlbum(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPString $title = null) {
			throw new \Exception('Method under construction');
			$entry = self::getAlbum($oAuth2, $id);
			if ($title === null) {
				$title = $entry->getTitle();
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PUT());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(self::toEntryString($title)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Deletes a Album object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Album object.
		 * @throws \Exception
		 * @throws \php\io\IOException
		 * @deprecated
		 */
		public static function deleteAlbum(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			throw new \Exception('Method under construction');
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			}
		}

		/**
		 * Returns a Album object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Album object.
		 * @return \php\util\google\photo\Album
		 * @throws \php\io\IOException
		 */
		public static function getAlbum(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Returns a list of Album objects of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listAlbums(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listAlbums($return, $oAuth2);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listAlbums(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('pageSize=50');
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['albums'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listAlbums($return, $oAuth2, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		private static function toEntry($entry) {
			$return = new Album();
			$return->id = $entry['id'];
			$return->title = $entry['title'];
			return $return;
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private $id;
		private $title;

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\util\google\photo\Album object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Album object of Google Photo.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the title of Album object of Google Photo.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

	}

}