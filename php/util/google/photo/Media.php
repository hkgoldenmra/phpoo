<?php

/**
 * Description of \php\util\google\photo\Media
 */

namespace php\util\google\photo {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/io/IOException.php');
	include_once('php/io/File.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\photo\Media object is the API for Google Photo.
	 */
	class Media extends \php\lang\PHPObject {

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'photoslibrary.googleapis.com';
		private static $defaultPath = '/v1';

		/**
		 * Uploads a Media object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\io\File $file A file will be uploaded.
		 * @param \php\lang\PHPString $name The name of Media object. Default &original file name&lt;.
		 * @param \php\lang\PHPString $description The description of Media object.
		 * @return \php\util\google\photo\Media
		 * @throws \php\io\IOException
		 */
		public static function uploadMedia(\php\util\google\OAuth2 $oAuth2, \php\io\File $file, \php\lang\PHPString $name = null, \php\lang\PHPString $description = null, \php\lang\PHPString $albumId = null) {
			if ($name === null) {
				$name = $file->getName();
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/uploads')
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/octet-stream'));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('X-Goog-Upload-File-Name'), $name);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('X-Goog-Upload-Protocol'), \php\lang\PHPString::newInstance('raw'));
			$urlConnection->addBodyFile($file);
			$urlConnection->send();
			$map = $urlConnection->getResponseHeaders();
			if ($map->containsKey(\php\lang\PHPString::newInstance('X-GUploader-UploadID'))->not()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				$uploadToken = $urlConnection->getResponseBody();
				$url = \php\net\URL::newInstanceByParameters(
						\php\lang\PHPString::newInstance(self::$defaultProtocol)
						, \php\lang\PHPString::newInstance(self::$defaultHost)
						, \php\lang\PHPString::newInstance(self::$defaultPath . '/mediaItems:batchCreate')
				);
				$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
				if ($albumId !== null) {
					$json['albumId'] = $albumId->getString();
				}
				$json['newMediaItems'] = array();
				$entry['description'] = $description->getString();
				$entry['simpleMediaItem']['uploadToken'] = $uploadToken->getString();
				array_push($json['newMediaItems'], $entry);
				$urlConnection->addBodyString(\php\lang\PHPString::newInstance(json_encode($json)));
				$urlConnection->send();
				$body = $urlConnection->getResponseBody()->getString();
				$json = json_decode($body, true);
				if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
					if (is_array($json)) {
						if (array_key_exists('error_description', $json)) {
							$body = $json['error_description'];
						} else if (array_key_exists('error', $json)) {
							$body = $json['error']['message'];
						} else {
							$body = $urlConnection->getResponseMessage()->getString();
						}
					}
					throw new \php\io\IOException($body);
				} else {
					return self::toEntry($json['newMediaItemResults'][0]['mediaItem']);
				}
			}
		}

		/**
		 * Updates a Media object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Media object.
		 * @throws \Exception
		 */
		public static function updateMedia(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			throw new \Exception('Method under construction');
		}

		/**
		 * Deletes a Media object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Media object.
		 * @throws \Exception
		 * @deprecated
		 */
		public static function deleteMedia(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			throw new \Exception('Method under construction');
		}

		/**
		 * Returns a Media object of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Media object.
		 * @return \php\util\google\photo\Media
		 */
		public static function getMedia(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/mediaItems/' . $id->getString())
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Returns a list of Media objects of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listMedias(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listMedias($return, $oAuth2);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listMedias(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('pageSize=100');
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/mediaItems')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['mediaItems'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listMedias($return, $oAuth2, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		/**
		 * Returns a list of Media objects in album of Google Photo.
		 * @param \php\util\google\OAuth2 $oAuth2
		 * @param \php\lang\PHPString $albumId
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listMediasInAlbum(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $albumId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listMediasInAlbum($return, $oAuth2, $albumId);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listMediasInAlbum(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPString $pageToken = null) {
			$json = array();
			$json['albumId'] = $id->getString();
			$json['pageSize'] = 100;
			if ($pageToken !== null) {
				$json['pageToken'] = $pageToken;
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/mediaItems:search')
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(json_encode($json)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['mediaItems'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listAlbums($return, $oAuth2, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		private static function toEntry($entry) {
			$return = new Media();
			$return->id = $entry['id'];
			$return->description = $entry['description'];
			$return->width = $entry['mediaMetadata']['width'];
			$return->height = $entry['mediaMetadata']['height'];
			return $return;
		}

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		private $id;
		private $description;
		private $width;
		private $height;

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\util\google\photo\Media object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Media object of Google Photo.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the description of Media object of Google Photo.
		 * @return \php\lang\PHPString
		 */
		public function getDescription() {
			return \php\lang\PHPString::newInstance($this->description);
		}

		/**
		 * Returns the width of Media object of Google Photo.
		 * @return \php\lang\PHPNumber
		 */
		public function getWidth() {
			return \php\lang\PHPNumber::newInstance($this->width);
		}

		/**
		 * Returns the height of Media object of Google Photo.
		 * @return \php\lang\PHPNumber
		 */
		public function getHeight() {
			return \php\lang\PHPNumber::newInstance($this->height);
		}

	}

}