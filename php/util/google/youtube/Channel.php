<?php

/**
 * Description of \php\util\google\youtube\Channel
 */

namespace php\util\google\youtube {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/io/IOException.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\youtube\Channel object is the API for Google Youtube.
	 */
	class Channel extends \php\lang\PHPObject {

		/**
		 * Returns the string of full permission of scope for Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_FULL() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/youtube');
		}

		/**
		 * Returns the string of SSL permission of scope for Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_SSL() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/youtube.force-ssl');
		}

		/**
		 * Returns the string of readonly permission of scope for Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_READONLY() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/youtube.readonly');
		}

		/**
		 * Returns the string of upload permission of scope for Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_UPLOAD() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/youtube.upload');
		}

		/**
		 * Returns the string of partner permission of scope for Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_PARTNER() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/youtubepartner');
		}

		/**
		 * Returns the string of audit permission of scope for Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public static final function SCOPE_AUDIT() {
			return \php\lang\PHPString::newInstance('https://www.googleapis.com/auth/youtubepartner-channel-audit');
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/youtube/v3/channels';
		private static $part = 'auditDetails,brandingSettings,contentDetails,contentOwnerDetails,id,localizations,snippet,statistics,status,topicDetails';

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		/**
		 * Returns a Channel object of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Channel object.
		 * @return \php\util\google\youtube\Channel
		 * @throws \php\io\IOException
		 */
		public static function getChannel(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$queryString = \php\lang\PHPString::newInstance('id=' . $id->getString() . '&part=' . self::$part);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json['items'][0]);
			}
		}

		/**
		 * Returns a list of Channel objects of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listChannels(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listChannels($return, $oAuth2);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listChannels(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('maxResults=50&mine=true&part=' . self::$part);
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath)
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				foreach ($json['items'] as $entry) {
					$return->add(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listChannels($return, $oAuth2, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		private static function toEntry($entry) {
			$return = new Channel();
			$return->id = $entry['id'];
			$return->title = $entry['snippet']['title'];
			$return->description = $entry['snippet']['description'];
			return $return;
		}

		private $id;
		private $title;
		private $description;

		/**
		 * Constructs a \php\util\google\youtube\Channel object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Channel object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the title of Channel object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

		/**
		 * Returns the description of Channel object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getDescription() {
			return \php\lang\PHPString::newInstance($this->description);
		}

	}

}