<?php

/**
 * Description of \php\util\google\youtube\Video
 */

namespace php\util\google\youtube {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPCharacter.php');
	include_once('php/io/File.php');
	include_once('php/io/IOException.php');
	include_once('php/net/URL.php');
	include_once('php/net/URLConnection.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/google/OAuth2.php');

	/**
	 * The \php\util\google\youtube\Video object is the API for Google Youtube.
	 */
	class Video extends \php\lang\PHPObject {

		private static $categories = array(
			1 => 'Film and Animation',
			2 => 'Autos and Vehicles',
			10 => 'Music',
			15 => 'Pets and Animals',
			17 => 'Sports',
			19 => 'Travel and Events',
			20 => 'Gaming',
			21 => 'Video Blogging',
			22 => 'People and Blogs',
			23 => 'Comedy',
			24 => 'Entertainment',
			25 => 'News and Politics',
			26 => 'How to and Style',
			27 => 'Education',
			28 => 'Science and Technology',
			29 => 'Nonprofits and Activism',
		);

		public static final function CATEGORY_FILM_AND_ANIMATION() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		public static final function CATEGORY_AUTOS_AND_VEHICLES() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		public static final function CATEGORY_MUSIC() {
			return \php\lang\PHPNumber::newInstance(10);
		}

		public static final function CATEGORY_PETS_AND_ANIMALS() {
			return \php\lang\PHPNumber::newInstance(15);
		}

		public static final function CATEGORY_SPORTS() {
			return \php\lang\PHPNumber::newInstance(17);
		}

		public static final function CATEGORY_TRAVEL_AND_EVENTS() {
			return \php\lang\PHPNumber::newInstance(19);
		}

		public static final function CATEGORY_GAMING() {
			return \php\lang\PHPNumber::newInstance(20);
		}

		public static final function CATEGORY_VIDEO_BLOGGING() {
			return \php\lang\PHPNumber::newInstance(21);
		}

		public static final function CATEGORY_PEOPLE_AND_BLOGS() {
			return \php\lang\PHPNumber::newInstance(22);
		}

		public static final function CATEGORY_COMEDY() {
			return \php\lang\PHPNumber::newInstance(23);
		}

		public static final function CATEGORY_ENTERTAINMENT() {
			return \php\lang\PHPNumber::newInstance(24);
		}

		public static final function CATEGORY_NEWS_AND_POLITICS() {
			return \php\lang\PHPNumber::newInstance(25);
		}

		public static final function CATEGORY_HOWTO_AND_STYLE() {
			return \php\lang\PHPNumber::newInstance(26);
		}

		public static final function CATEGORY_EDUCATION() {
			return \php\lang\PHPNumber::newInstance(27);
		}

		public static final function CATEGORY_SCIENCE_AND_TECHNOLOGY() {
			return \php\lang\PHPNumber::newInstance(28);
		}

		public static final function CATEGORY_NONPROFITS_AND_ACTIVISM() {
			return \php\lang\PHPNumber::newInstance(29);
		}

		public static final function VISIBILITY_PRIVATE() {
			return \php\lang\PHPString::newInstance('private');
		}

		public static final function VISIBILITY_PUBLIC() {
			return \php\lang\PHPString::newInstance('public');
		}

		public static final function VISIBILITY_UNLISTED() {
			return \php\lang\PHPString::newInstance('unlisted');
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		private static $defaultProtocol = 'https';
		private static $defaultHost = 'www.googleapis.com';
		private static $defaultPath = '/youtube/v3';
		private static $part = 'contentDetails,id,liveStreamingDetails,localizations,player,recordingDetails,snippet,statistics,status,topicDetails';

		private static function createURLConnection(\php\util\google\OAuth2 $oAuth2, \php\net\URL $url, \php\lang\PHPString $method) {
			$urlConnection = \php\net\URLConnection::newInstanceByURL($url, $method);
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), \php\lang\PHPString::newInstance('application/json; charset=' . \php\lang\PHPCharacter::DEFAULT_CHARSET()->getString()));
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Authorization'), \php\lang\PHPString::newInstance(sprintf('%s %s', $oAuth2->getTokenType()->getString(), $oAuth2->getAccessToken()->getString())));
			return $urlConnection;
		}

		/**
		 * Returns a Video object of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Video object.
		 * @throws \php\io\IOException
		 */
		public static function deleteVideo(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			$queryString = \php\lang\PHPString::newInstance('id=' . $id->getString());
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/videos')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_DELETE());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			}
		}

		/**
		 * Uploads a Video object of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\io\File $file A file will be uploaded.
		 * @param \php\lang\PHPNumber $categoryId The ID of Video category.
		 * @param \php\lang\PHPString $title The title of Video. Default &lt;original file name&gt;.
		 * @param \php\lang\PHPString $description The description of Video. Default ''.
		 * @param \php\lang\PHPString $visibility The visibility of Video. Default &lt;VISIBILITY_PRIVATE()&gt;.
		 * @return \php\util\google\youtube\Video
		 * @throws \php\io\IOException
		 */
		public static function uploadVideo(\php\util\google\OAuth2 $oAuth2, \php\io\File $file, \php\lang\PHPNumber $categoryId, \php\lang\PHPString $title = null, \php\lang\PHPString $description = null, \php\lang\PHPString $visibility = null) {
			$queryString = \php\lang\PHPString::newInstance('part=' . self::$part);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance('/upload' . self::$defaultPath . '/videos')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_POST());
			$urlConnection->addHeader(\php\lang\PHPString::newInstance('Content-Type'), $file->getMimeType());
			$urlConnection->addBodyFile($file);
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				if ($title === null) {
					$title = $file->getName();
				}
				if ($description === null) {
					$description = \php\lang\PHPString::newInstance('');
				}
				if ($visibility === null) {
					$visibility = self::VISIBILITY_PRIVATE();
				}
				return self::updateVideo($oAuth2, \php\lang\PHPString::newInstance($json['id']), $categoryId, $title, $description, $visibility);
			}
		}

		/**
		 * Updates a Video object of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Video object.
		 * @param \php\lang\PHPNumber $categoryId The ID of Video category. Default &lt;original category ID&gt;.
		 * @param \php\lang\PHPString $title The title of Video. Default &lt;original title&gt;.
		 * @param \php\lang\PHPString $description The description of Video. Default &lt;original description&gt;.
		 * @param \php\lang\PHPString $visibility The visibility of Video. Default &lt;original visibility&gt;.
		 * @return \php\util\google\youtube\Video
		 * @throws \php\io\IOException
		 */
		public static function updateVideo(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id, \php\lang\PHPNumber $categoryId = null, \php\lang\PHPString $title = null, \php\lang\PHPString $description = null, \php\lang\PHPString $visibility = null) {
			$entry = self::getVideo($oAuth2, $id);
			$json['id'] = $id->getString();
			$json['snippet']['categoryId'] = (($categoryId !== null) ? $categoryId->getNumber() : $entry->categoryId);
			$json['snippet']['title'] = (($title !== null) ? $title->getString() : $entry->getTitle()->getString());
			if ($description !== null) {
				$json['snippet']['description'] = $description->getString();
			}
			if ($visibility !== null) {
				$json['status']['privacyStatus'] = $visibility->getString();
			}
			$queryString = \php\lang\PHPString::newInstance('part=' . self::$part);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/videos')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_PUT());
			$urlConnection->addBodyString(\php\lang\PHPString::newInstance(json_encode($json)));
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return self::toEntry($json);
			}
		}

		/**
		 * Returns a Video object of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $id The ID of Video object.
		 * @return \php\util\google\youtube\Video
		 * @throws \php\io\IOException
		 */
		public static function getVideo(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $id) {
			try {
				$return = self::_getVideos($oAuth2, $id);
				return self::toEntry($return[0]);
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _getVideos(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $idChain) {
			$queryString = \php\lang\PHPString::newInstance('id=' . $idChain->getString() . '&part=' . self::$part);
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/videos')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				return $json['items'];
			}
		}

		/**
		 * Returns a list of Video objects of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listVideos(\php\util\google\OAuth2 $oAuth2) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listVideos($return, $oAuth2);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listVideos(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $pageToken = null, \php\lang\PHPString $channelId = null) {
			$queryString = \php\lang\PHPString::newInstance('maxResults=50&type=video&part=id');
			if ($channelId === null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&forMine=true'));
			} else {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&channelId='));
				$queryString = $queryString->append($channelId);
			}
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/search')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				$ids = array();
				foreach ($json['items'] as $entry) {
					array_push($ids, $entry['id']['videoId']);
				}
				$videos = self::_getVideos($oAuth2, \php\lang\PHPString::newInstance(implode(',', $ids)));
				foreach ($videos as $entry) {
					$return->addLast(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listVideos($return, $oAuth2, \php\lang\PHPString::newInstance($json['nextPageToken']), $channelId);
				}
			}
		}

		/**
		 * Returns a list of Video objects of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $channelId The ID of Channel object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listVideosInChannel(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $channelId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listVideos($return, $oAuth2, null, $channelId);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		/**
		 * Returns a list of Video objects of Google Youtube.
		 * @param \php\util\google\OAuth2 $oAuth2 The Google OAuth2 token.
		 * @param \php\lang\PHPString $playlistId The ID of Playlist object.
		 * @return \php\util\collections\Vector
		 * @throws \php\io\IOException
		 */
		public static function listVideosInPlaylist(\php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $playlistId) {
			try {
				$return = \php\util\collections\Vector::newInstance();
				self::_listVideosInPlaylist($return, $oAuth2, $playlistId);
				return $return;
			} catch (\php\io\IOException $ex) {
				throw $ex;
			}
		}

		private static function _listVideosInPlaylist(\php\util\collections\Vector &$return, \php\util\google\OAuth2 $oAuth2, \php\lang\PHPString $playlistId, \php\lang\PHPString $pageToken = null) {
			$queryString = \php\lang\PHPString::newInstance('maxResults=50&part=snippet&playlistId=');
			$queryString = $queryString->append($playlistId);
			if ($pageToken !== null) {
				$queryString = $queryString->append(\php\lang\PHPString::newInstance('&pageToken='));
				$queryString = $queryString->append($pageToken);
			}
			$url = \php\net\URL::newInstanceByParameters(
					\php\lang\PHPString::newInstance(self::$defaultProtocol)
					, \php\lang\PHPString::newInstance(self::$defaultHost)
					, \php\lang\PHPString::newInstance(self::$defaultPath . '/playlistItems')
					, null
					, null
					, null
					, $queryString
			);
			$urlConnection = self::createURLConnection($oAuth2, $url, \php\net\URLConnection::METHOD_GET());
			$urlConnection->send();
			$body = $urlConnection->getResponseBody()->getString();
			$json = json_decode($body, true);
			if ($urlConnection->getResponseCode()->regexMatch(\php\lang\PHPString::newInstance('/^[23][0-9]{2}$/'))->size()->isZero()->getBoolean()) {
				if (is_array($json)) {
					if (array_key_exists('error_description', $json)) {
						$body = $json['error_description'];
					} else if (array_key_exists('error', $json)) {
						$body = $json['error']['message'];
					} else {
						$body = $urlConnection->getResponseMessage()->getString();
					}
				}
				throw new \php\io\IOException($body);
			} else {
				$ids = array();
				foreach ($json['items'] as $entry) {
					array_push($ids, $entry['snippet']['resourceId']['videoId']);
				}
				$videos = self::_getVideos($oAuth2, \php\lang\PHPString::newInstance(implode(',', $ids)));
				foreach ($videos as $entry) {
					$return->addLast(self::toEntry($entry));
				}
				if (array_key_exists('nextPageToken', $json)) {
					self::_listVideosInPlaylist($return, $oAuth2, $playlistId, \php\lang\PHPString::newInstance($json['nextPageToken']));
				}
			}
		}

		private static function toEntry($entry) {
			$return = new Video();
			$return->id = $entry['id'];
			$return->categoryId = $entry['snippet']['categoryId'];
			$return->title = $entry['snippet']['title'];
			$return->description = $entry['snippet']['description'];
			$return->tags = \php\util\collections\Vector::newInstance();
			foreach ($entry['snippet']['tags'] as $tag) {
				$return->tags->addLast(\php\lang\PHPString::newInstance($tag));
			}
			$return->visibility = $entry['status']['privacyStatus'];
			$return->duration = 0;
			if (preg_match('/^P(?:(?P<day>[0-9]*)D)?T(?:(?P<hour>[0-9]*)H)?(?:(?P<minute>[0-9]*)M)?(?:(?P<second>[0-9]*)S)?$/', $entry['contentDetails']['duration'], $matches) > 0) {
				if (array_key_exists('day', $matches)) {
					$return->duration += intval($matches['day']) * 60 * 60 * 24;
				}
				if (array_key_exists('hour', $matches)) {
					$return->duration += intval($matches['hour']) * 60 * 60;
				}
				if (array_key_exists('minute', $matches)) {
					$return->duration += intval($matches['minute']) * 60;
				}
				if (array_key_exists('second', $matches)) {
					$return->duration += intval($matches['second']);
				}
			}
			$return->viewCount = $entry['statistics']['viewCount'];
			$return->likeCount = $entry['statistics']['likeCount'];
			$return->dislikeCount = $entry['statistics']['dislikeCount'];
			$return->favoriteCount = $entry['statistics']['favoriteCount'];
			return $return;
		}

		private $id;
		private $categoryId;
		private $title;
		private $description;
		private $tags;
		private $visibility;
		private $duration;
		private $viewCount;
		private $likeCount;
		private $dislikeCount;
		private $favoriteCount;

		/**
		 * Constructs a \php\util\google\youtube\Video object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Returns the ID of Video object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getId() {
			return \php\lang\PHPString::newInstance($this->id);
		}

		/**
		 * Returns the category of Video object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getCategory() {
			return \php\lang\PHPString::newInstance(self::$categories[$this->categoryId]);
		}

		/**
		 * Returns the title of Video object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getTitle() {
			return \php\lang\PHPString::newInstance($this->title);
		}

		/**
		 * Returns the description of Video object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getDescription() {
			return \php\lang\PHPString::newInstance($this->description);
		}

		/**
		 * Returns the tags of Video object of Google Youtube.
		 * @return \php\util\collections\Vector
		 */
		public function getTags() {
			return $this->tags;
		}

		/**
		 * Returns the visibility of Video object of Google Youtube.
		 * @return \php\lang\PHPString
		 */
		public function getVisibility() {
			return \php\lang\PHPString::newInstance($this->visibility);
		}

		/**
		 * Returns the duration in seconds of Video object of Google Youtube.
		 * @return \php\lang\PHPNumber
		 */
		public function getDuration() {
			return \php\lang\PHPNumber::newInstance($this->duration);
		}

		/**
		 * Returns the view count of Video object of Google Youtube.
		 * @return \php\lang\PHPNumber
		 */
		public function getViewCount() {
			return \php\lang\PHPNumber::newInstance($this->viewCount);
		}

		/**
		 * Returns the like count of Video object of Google Youtube.
		 * @return \php\lang\PHPNumber
		 */
		public function getLikeCount() {
			return \php\lang\PHPNumber::newInstance($this->likeCount);
		}

		/**
		 * Returns the dislike count of Video object of Google Youtube.
		 * @return \php\lang\PHPNumber
		 */
		public function getDislikeCount() {
			return \php\lang\PHPNumber::newInstance($this->dislikeCount);
		}

		/**
		 * Returns the favorite count of Video object of Google Youtube.
		 * @return \php\lang\PHPNumber
		 */
		public function getFavoriteCountCount() {
			return \php\lang\PHPNumber::newInstance($this->favoriteCount);
		}

	}

}