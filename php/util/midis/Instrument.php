<?php

/**
 * Description of \php\util\midis\Instrument
 */

namespace php\util\midis {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');

	/**
	 * This class represents the instrument to play the musical note.
	 */
	class Instrument extends \php\lang\PHPObject {

		private static $instrumentNames = array(
			0 => 'Acoustic Grand Piano',
			1 => 'Bright Acoustic Piano',
			2 => 'Electric Grand Piano',
			3 => 'Honky-Tonk Piano',
			4 => 'Electric Piano 1',
			5 => 'Electric Piano 2',
			6 => 'Harpsichord',
			7 => 'Clavinet',
			8 => 'Celesta',
			9 => 'Glockenspiel',
			10 => 'Music Box',
			11 => 'Vibraphone',
			12 => 'Marimba',
			13 => 'Xylophone',
			14 => 'Tubular Bells',
			15 => 'Dulcimer',
			16 => 'Drawbar Organ',
			17 => 'Percussive Organ',
			18 => 'Rock Organ',
			19 => 'Church Organ',
			20 => 'Reed Organ',
			21 => 'Accordion',
			22 => 'Harmonica',
			23 => 'Tango Accordion',
			24 => 'Acoustic Guitar (Nylon)',
			25 => 'Acoustic Guitar (Steel)',
			26 => 'Electric Guitar (Jazz)',
			27 => 'Electric Guitar (Clean)',
			28 => 'Electric Guitar (Muted)',
			29 => 'Overdriven Guitar',
			30 => 'Distortion Guitar',
			31 => 'Guitar Harmonics',
			32 => 'Acoustic Bass',
			33 => 'Electric Bass (Finger)',
			34 => 'Electric Bass (Pick)',
			35 => 'Fretless Bass',
			36 => 'Slap Bass 1',
			37 => 'Slap Bass 2',
			38 => 'Synth Bass 1',
			39 => 'Synth Bass 2',
			40 => 'Violin',
			41 => 'Viola',
			42 => 'Cello',
			43 => 'Contrabass',
			44 => 'Tremolo Strings',
			45 => 'Pizzicato Strings',
			46 => 'Orchestral Harp',
			47 => 'Timpani',
			48 => 'String Ensemble 1',
			49 => 'String Ensemble 2',
			50 => 'Synth Strings 1',
			51 => 'Synth Strings 2',
			52 => 'Choir Aahs',
			53 => 'Voice Oohs',
			54 => 'Synth Choir',
			55 => 'Orchestra Hit',
			56 => 'Trumpet',
			57 => 'Trombone',
			58 => 'Tuba',
			59 => 'Muted Trumpet',
			60 => 'French Horn',
			61 => 'Brass Section',
			62 => 'Synth Brass 1',
			63 => 'Synth Brass 2',
			64 => 'Soprano Sax',
			65 => 'Alto Sax',
			66 => 'Tenor Sax',
			67 => 'Baritone Sax',
			68 => 'Oboe',
			69 => 'English Horn',
			70 => 'Bassoon',
			71 => 'Clarinet',
			72 => 'Piccolo',
			73 => 'Flute',
			74 => 'Recorder',
			75 => 'Pan Flute',
			76 => 'Blown Bottle',
			77 => 'Shakuhachi',
			78 => 'Whistle',
			79 => 'Ocarina',
			80 => 'Lead 1 (Square)',
			81 => 'Lead 2 (Sawtooth)',
			82 => 'Lead 3 (Calliope)',
			83 => 'Lead 4 (Chiff)',
			84 => 'Lead 5 (Charang)',
			85 => 'Lead 6 (Voice)',
			86 => 'Lead 7 (Fifths)',
			87 => 'Lead 8 (Bass + Lead)',
			88 => 'Pad 1 (New Age)',
			89 => 'Pad 2 (Warm)',
			90 => 'Pad 3 (Polysynth)',
			91 => 'Pad 4 (Choir)',
			92 => 'Pad 5 (Bowed)',
			93 => 'Pad 6 (Metallic)',
			94 => 'Pad 7 (Halo)',
			95 => 'Pad 8 (Sweep)',
			96 => 'Fx 1 (Rain)',
			97 => 'Fx 2 (Soundtrack)',
			98 => 'Fx 3 (Crystal)',
			99 => 'Fx 4 (Atmosphere)',
			100 => 'Fx 5 (Brightness)',
			101 => 'Fx 6 (Goblins)',
			102 => 'Fx 7 (Echoes)',
			103 => 'Fx 8 (Sci-Fi)',
			104 => 'Sitar',
			105 => 'Banjo',
			106 => 'Shamisen',
			107 => 'Koto',
			108 => 'Kalimba',
			109 => 'Bagpipe',
			110 => 'Fiddle',
			111 => 'Shanai',
			112 => 'Tinkle Bell',
			113 => 'Agogo',
			114 => 'Steel Drums',
			115 => 'Woodblock',
			116 => 'Taiko Drum',
			117 => 'Melodic Tom',
			118 => 'Synth Drum',
			119 => 'Reverse Cymbal',
			120 => 'Guitar Fret Noise',
			121 => 'Breath Noise',
			122 => 'Seashore',
			123 => 'Bird Tweet',
			124 => 'Telephone Ring',
			125 => 'Helicopter',
			126 => 'Applause',
			127 => 'Gunshot',
		);

		/**
		 * Returns code number of Acoustic Grand Piano.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ACOUSTIC_GRAND_PIANO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x00));
		}

		/**
		 * Returns code number of Bright Acoustic Piano.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BRIGHT_ACOUSTIC_PIANO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x01));
		}

		/**
		 * Returns code number of Electric Grand Piano.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_GRAND_PIANO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x02));
		}

		/**
		 * Returns code number of Honky Tonk Piano.
		 * @return \php\util\midis\Instrument
		 */
		public static final function HONKY_TONK_PIANO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x03));
		}

		/**
		 * Returns code number of Electric Piano 1.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_PIANO_1() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x04));
		}

		/**
		 * Returns code number of Electric Piano 2.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_PIANO_2() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x05));
		}

		/**
		 * Returns code number of Harpsichord.
		 * @return \php\util\midis\Instrument
		 */
		public static final function HARPSICHORD() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x06));
		}

		/**
		 * Returns code number of Clavinet.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CLAVINET() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x07));
		}

		/**
		 * Returns code number of Celesta.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CELESTA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x08));
		}

		/**
		 * Returns code number of Glockenspiel.
		 * @return \php\util\midis\Instrument
		 */
		public static final function GLOCKENSPIEL() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x09));
		}

		/**
		 * Returns code number of Music Box.
		 * @return \php\util\midis\Instrument
		 */
		public static final function MUSIC_BOX() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x0A));
		}

		/**
		 * Returns code number of Vibraphone.
		 * @return \php\util\midis\Instrument
		 */
		public static final function VIBRAPHONE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x0B));
		}

		/**
		 * Returns code number of Marimba.
		 * @return \php\util\midis\Instrument
		 */
		public static final function MARIMBA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x0C));
		}

		/**
		 * Returns code number of Xylophone.
		 * @return \php\util\midis\Instrument
		 */
		public static final function XYLOPHONE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x0D));
		}

		/**
		 * Returns code number of Tubular Bells.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TUBULAR_BELLS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x0E));
		}

		/**
		 * Returns code number of Dulcimer.
		 * @return \php\util\midis\Instrument
		 */
		public static final function DULCIMER() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x0F));
		}

		/**
		 * Returns code number of Drawbar Organ.
		 * @return \php\util\midis\Instrument
		 */
		public static final function DRAWBAR_ORGAN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x10));
		}

		/**
		 * Returns code number of Percussive Organ.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PERCUSSIVE_ORGAN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x11));
		}

		/**
		 * Returns code number of Rock Organ.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ROCK_ORGAN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x12));
		}

		/**
		 * Returns code number of Church Organ.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CHURCH_ORGAN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x13));
		}

		/**
		 * Returns code number of Reed Organ.
		 * @return \php\util\midis\Instrument
		 */
		public static final function REED_ORGAN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x14));
		}

		/**
		 * Returns code number of Accordion.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ACCORDION() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x15));
		}

		/**
		 * Returns code number of Harmonica.
		 * @return \php\util\midis\Instrument
		 */
		public static final function HARMONICA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x16));
		}

		/**
		 * Returns code number of Tango Accordion.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TANGO_ACCORDION() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x17));
		}

		/**
		 * Returns code number of Acoustic Guitar Nylon.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ACOUSTIC_GUITAR_NYLON() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x18));
		}

		/**
		 * Returns code number of Acoustic Guitar Steel.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ACOUSTIC_GUITAR_STEEL() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x19));
		}

		/**
		 * Returns code number of Electric Guitar Jazz.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_GUITAR_JAZZ() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x1A));
		}

		/**
		 * Returns code number of Electric Guitar Clean.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_GUITAR_CLEAN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x1B));
		}

		/**
		 * Returns code number of Electric Guitar Muted.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_GUITAR_MUTED() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x1C));
		}

		/**
		 * Returns code number of Overdriven Guitar.
		 * @return \php\util\midis\Instrument
		 */
		public static final function OVERDRIVEN_GUITAR() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x1D));
		}

		/**
		 * Returns code number of Distortion Guitar.
		 * @return \php\util\midis\Instrument
		 */
		public static final function DISTORTION_GUITAR() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x1E));
		}

		/**
		 * Returns code number of Guitar Harmonics.
		 * @return \php\util\midis\Instrument
		 */
		public static final function GUITAR_HARMONICS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x1F));
		}

		/**
		 * Returns code number of Acoustic Bass.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ACOUSTIC_BASS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x20));
		}

		/**
		 * Returns code number of Electric Bass Finger.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_BASS_FINGER() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x21));
		}

		/**
		 * Returns code number of Electric Bass Pick.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ELECTRIC_BASS_PICK() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x22));
		}

		/**
		 * Returns code number of Fretless Bass.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FRETLESS_BASS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x23));
		}

		/**
		 * Returns code number of Slap Bass 1.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SLAP_BASS_1() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x24));
		}

		/**
		 * Returns code number of Slap Bass 2.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SLAP_BASS_2() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x25));
		}

		/**
		 * Returns code number of Synth Bass 1.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_BASS_1() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x26));
		}

		/**
		 * Returns code number of Synth Bass 2.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_BASS_2() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x27));
		}

		/**
		 * Returns code number of Violin.
		 * @return \php\util\midis\Instrument
		 */
		public static final function VIOLIN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x28));
		}

		/**
		 * Returns code number of Viola.
		 * @return \php\util\midis\Instrument
		 */
		public static final function VIOLA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x29));
		}

		/**
		 * Returns code number of Cello.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CELLO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x2A));
		}

		/**
		 * Returns code number of Contrabass.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CONTRABASS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x2B));
		}

		/**
		 * Returns code number of Tremolo Strings.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TREMOLO_STRINGS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x2C));
		}

		/**
		 * Returns code number of Pizzicato Strings.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PIZZICATO_STRINGS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x2D));
		}

		/**
		 * Returns code number of Orchestral Harp.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ORCHESTRAL_HARP() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x2E));
		}

		/**
		 * Returns code number of Timpani.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TIMPANI() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x2F));
		}

		/**
		 * Returns code number of String Ensemble 1.
		 * @return \php\util\midis\Instrument
		 */
		public static final function STRING_ENSEMBLE_1() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x30));
		}

		/**
		 * Returns code number of String Ensemble 2.
		 * @return \php\util\midis\Instrument
		 */
		public static final function STRING_ENSEMBLE_2() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x31));
		}

		/**
		 * Returns code number of Synth Strings 1.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_STRINGS_1() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x32));
		}

		/**
		 * Returns code number of Synth Strings 2.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_STRINGS_2() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x33));
		}

		/**
		 * Returns code number of Choir Aahs.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CHOIR_AAHS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x34));
		}

		/**
		 * Returns code number of Voice Oohs.
		 * @return \php\util\midis\Instrument
		 */
		public static final function VOICE_OOHS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x35));
		}

		/**
		 * Returns code number of Synth Choir.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_CHOIR() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x36));
		}

		/**
		 * Returns code number of Orchestra Hit.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ORCHESTRA_HIT() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x37));
		}

		/**
		 * Returns code number of Trumpet.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TRUMPET() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x38));
		}

		/**
		 * Returns code number of Trombone.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TROMBONE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x39));
		}

		/**
		 * Returns code number of Tuba.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TUBA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x3A));
		}

		/**
		 * Returns code number of Muted Trumpet.
		 * @return \php\util\midis\Instrument
		 */
		public static final function MUTED_TRUMPET() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x3B));
		}

		/**
		 * Returns code number of French Horn.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FRENCH_HORN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x3C));
		}

		/**
		 * Returns code number of Brass Section.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BRASS_SECTION() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x3D));
		}

		/**
		 * Returns code number of Synth Brass 1.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_BRASS_1() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x3E));
		}

		/**
		 * Returns code number of Synth Brass 2.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_BRASS_2() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x3F));
		}

		/**
		 * Returns code number of Soprano Sax.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SOPRANO_SAX() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x40));
		}

		/**
		 * Returns code number of Alto Sax.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ALTO_SAX() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x41));
		}

		/**
		 * Returns code number of Tenor Sax.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TENOR_SAX() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x42));
		}

		/**
		 * Returns code number of Baritone Sax.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BARITONE_SAX() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x43));
		}

		/**
		 * Returns code number of Oboe.
		 * @return \php\util\midis\Instrument
		 */
		public static final function OBOE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x44));
		}

		/**
		 * Returns code number of English Horn.
		 * @return \php\util\midis\Instrument
		 */
		public static final function ENGLISH_HORN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x45));
		}

		/**
		 * Returns code number of Bassoon.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BASSOON() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x46));
		}

		/**
		 * Returns code number of Clarinet.
		 * @return \php\util\midis\Instrument
		 */
		public static final function CLARINET() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x47));
		}

		/**
		 * Returns code number of Piccolo.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PICCOLO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x48));
		}

		/**
		 * Returns code number of Flute.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FLUTE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x49));
		}

		/**
		 * Returns code number of Recorder.
		 * @return \php\util\midis\Instrument
		 */
		public static final function RECORDER() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x4A));
		}

		/**
		 * Returns code number of Pan Flute.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAN_FLUTE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x4B));
		}

		/**
		 * Returns code number of Blown Bottle.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BLOWN_BOTTLE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x4C));
		}

		/**
		 * Returns code number of Shakuhachi.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SHAKUHACHI() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x4D));
		}

		/**
		 * Returns code number of Whistle.
		 * @return \php\util\midis\Instrument
		 */
		public static final function WHISTLE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x4E));
		}

		/**
		 * Returns code number of Ocarina.
		 * @return \php\util\midis\Instrument
		 */
		public static final function OCARINA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x4F));
		}

		/**
		 * Returns code number of Lead 1 Square.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_1_SQUARE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x50));
		}

		/**
		 * Returns code number of Lead 2 Sawtooth.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_2_SAWTOOTH() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x51));
		}

		/**
		 * Returns code number of Lead 3 Calliope.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_3_CALLIOPE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x52));
		}

		/**
		 * Returns code number of Lead 4 Chiff.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_4_CHIFF() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x53));
		}

		/**
		 * Returns code number of Lead 5 Charang.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_5_CHARANG() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x54));
		}

		/**
		 * Returns code number of Lead 6 Voice.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_6_VOICE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x55));
		}

		/**
		 * Returns code number of Lead 7 Fifths.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_7_FIFTHS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x56));
		}

		/**
		 * Returns code number of Lead 8 Bass + Lead.
		 * @return \php\util\midis\Instrument
		 */
		public static final function LEAD_8_BASS_LEAD() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x57));
		}

		/**
		 * Returns code number of Pad 1 New Age.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_1_NEW_AGE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x58));
		}

		/**
		 * Returns code number of Pad 2 Warm.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_2_WARM() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x59));
		}

		/**
		 * Returns code number of Pad 3 Polysynth.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_3_POLYSYNTH() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x5A));
		}

		/**
		 * Returns code number of Pad 4 Choir.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_4_CHOIR() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x5B));
		}

		/**
		 * Returns code number of Pad 5 Bowed.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_5_BOWED() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x5C));
		}

		/**
		 * Returns code number of Pad 6 Metallic.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_6_METALLIC() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x5D));
		}

		/**
		 * Returns code number of Pad 7 Halo.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_7_HALO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x5E));
		}

		/**
		 * Returns code number of Pad 8 Sweep.
		 * @return \php\util\midis\Instrument
		 */
		public static final function PAD_8_SWEEP() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x5F));
		}

		/**
		 * Returns code number of FX_1_RAIN.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_1_RAIN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x60));
		}

		/**
		 * Returns code number of Fx 2 Soundtrack.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_2_SOUNDTRACK() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x61));
		}

		/**
		 * Returns code number of Fx 3 Crystal.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_3_CRYSTAL() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x62));
		}

		/**
		 * Returns code number of Fx 4 Atmosphere.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_4_ATMOSPHERE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x63));
		}

		/**
		 * Returns code number of Fx 5 Brightness.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_5_BRIGHTNESS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x64));
		}

		/**
		 * Returns code number of Fx 6 Goblins.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_6_GOBLINS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x65));
		}

		/**
		 * Returns code number of Fx 7 Echoes.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_7_ECHOES() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x66));
		}

		/**
		 * Returns code number of Fx 8 Sci-Fi.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FX_8_SCI_FI() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x67));
		}

		/**
		 * Returns code number of Sitar.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SITAR() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x68));
		}

		/**
		 * Returns code number of Banjo.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BANJO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x69));
		}

		/**
		 * Returns code number of Shamisen.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SHAMISEN() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x6A));
		}

		/**
		 * Returns code number of Koto.
		 * @return \php\util\midis\Instrument
		 */
		public static final function KOTO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x6B));
		}

		/**
		 * Returns code number of Kalimba.
		 * @return \php\util\midis\Instrument
		 */
		public static final function KALIMBA() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x6C));
		}

		/**
		 * Returns code number of Bagpipe.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BAGPIPE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x6D));
		}

		/**
		 * Returns code number of Fiddle.
		 * @return \php\util\midis\Instrument
		 */
		public static final function FIDDLE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x6E));
		}

		/**
		 * Returns code number of Shanai.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SHANAI() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x6F));
		}

		/**
		 * Returns code number of Tinkle Bell.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TINKLE_BELL() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x70));
		}

		/**
		 * Returns code number of Agogo.
		 * @return \php\util\midis\Instrument
		 */
		public static final function AGOGO() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x71));
		}

		/**
		 * Returns code number of Steel Drums.
		 * @return \php\util\midis\Instrument
		 */
		public static final function STEEL_DRUMS() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x72));
		}

		/**
		 * Returns code number of Woodblock.
		 * @return \php\util\midis\Instrument
		 */
		public static final function WOODBLOCK() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x73));
		}

		/**
		 * Returns code number of Taiko Drum.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TAIKO_DRUM() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x74));
		}

		/**
		 * Returns code number of Melodich Tom.
		 * @return \php\util\midis\Instrument
		 */
		public static final function MELODIC_TOM() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x75));
		}

		/**
		 * Returns code number of Synth Drum.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SYNTH_DRUM() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x76));
		}

		/**
		 * Returns code number of Reverse Cymbal.
		 * @return \php\util\midis\Instrument
		 */
		public static final function REVERSE_CYMBAL() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x77));
		}

		/**
		 * Returns code number of Guitar Fret Noise.
		 * @return \php\util\midis\Instrument
		 */
		public static final function GUITAR_FRET_NOISE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x78));
		}

		/**
		 * Returns code number of Breath Noise.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BREATH_NOISE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x79));
		}

		/**
		 * Returns code number of Seashore.
		 * @return \php\util\midis\Instrument
		 */
		public static final function SEASHORE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x7A));
		}

		/**
		 * Returns code number of Bird Tweet.
		 * @return \php\util\midis\Instrument
		 */
		public static final function BIRD_TWEET() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x7B));
		}

		/**
		 * Returns code number of Telephone Ring.
		 * @return \php\util\midis\Instrument
		 */
		public static final function TELEPHONE_RING() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x7C));
		}

		/**
		 * Returns code number of Helicopter.
		 * @return \php\util\midis\Instrument
		 */
		public static final function HELICOPTER() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x7D));
		}

		/**
		 * Returns code number of Applause.
		 * @return \php\util\midis\Instrument
		 */
		public static final function APPLAUSE() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x7E));
		}

		/**
		 * Returns code number of Gunshot.
		 * @return \php\util\midis\Instrument
		 */
		public static final function GUNSHOT() {
			return new Instrument(\php\lang\PHPNumber::newInstance(0x7F));
		}

		private $instrument;

		/**
		 * Constructs the \php\util\midis\Instrument object.
		 * @param \php\lang\PHPNumber $instrument
		 */
		protected function __construct(\php\lang\PHPNumber $instrument) {
			parent::__construct();
			$this->instrument = $instrument->getInteger()->getNumber();
		}

		/**
		 * Returns the code number of Instrument.
		 * @return \php\lang\PHPNumber
		 */
		public function getInstrument() {
			return \php\lang\PHPNumber::newInstance($this->instrument);
		}

		/**
		 * Returns the name of Instrument.
		 * @return \php\lang\PHPString
		 */
		public function getInstrumentName() {
			return \php\lang\PHPString::newInstance(self::$instrumentNames[$this->instrument]);
		}

	}

}