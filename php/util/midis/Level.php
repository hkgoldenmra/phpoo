<?php

/**
 * Description of \php\util\midis\Level
 */

namespace php\util\midis {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');

	/**
	 * This class represents a level of a muscial note.
	 */
	class Level extends \php\lang\PHPObject {

		/**
		 * Returns C-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x00));
		}

		/**
		 * Returns C#-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x01));
		}

		/**
		 * Returns D-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x02));
		}

		/**
		 * Returns D#-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x03));
		}

		/**
		 * Returns E-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x04));
		}

		/**
		 * Returns F-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x05));
		}

		/**
		 * Returns F#-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x06));
		}

		/**
		 * Returns G-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x07));
		}

		/**
		 * Returns G#-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x08));
		}

		/**
		 * Returns A-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x09));
		}

		/**
		 * Returns A#-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x0A));
		}

		/**
		 * Returns B-1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B_1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x0B));
		}

		/**
		 * Returns C0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x0C));
		}

		/**
		 * Returns C#0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x0D));
		}

		/**
		 * Returns D0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x0E));
		}

		/**
		 * Returns D#0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x0F));
		}

		/**
		 * Returns E0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x10));
		}

		/**
		 * Returns F0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x11));
		}

		/**
		 * Returns F#0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x12));
		}

		/**
		 * Returns G0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x13));
		}

		/**
		 * Returns G#0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x14));
		}

		/**
		 * Returns A0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x15));
		}

		/**
		 * Returns A#0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x16));
		}

		/**
		 * Returns B0 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B0() {
			return new Level(\php\lang\PHPNumber::newInstance(0x17));
		}

		/**
		 * Returns C1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x18));
		}

		/**
		 * Returns C#1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x19));
		}

		/**
		 * Returns D1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x1A));
		}

		/**
		 * Returns D#1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x1B));
		}

		/**
		 * Returns E1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x1C));
		}

		/**
		 * Returns F1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x1D));
		}

		/**
		 * Returns F#1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x1E));
		}

		/**
		 * Returns G1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x1F));
		}

		/**
		 * Returns G#1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x20));
		}

		/**
		 * Returns A1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x21));
		}

		/**
		 * Returns A#1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x22));
		}

		/**
		 * Returns B1 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B1() {
			return new Level(\php\lang\PHPNumber::newInstance(0x23));
		}

		/**
		 * Returns C2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x24));
		}

		/**
		 * Returns C#2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x25));
		}

		/**
		 * Returns D2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x26));
		}

		/**
		 * Returns D#2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x27));
		}

		/**
		 * Returns E2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x28));
		}

		/**
		 * Returns F2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x29));
		}

		/**
		 * Returns F#2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x2A));
		}

		/**
		 * Returns G2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x2B));
		}

		/**
		 * Returns G#2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x2C));
		}

		/**
		 * Returns A2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x2D));
		}

		/**
		 * Returns A#2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x2E));
		}

		/**
		 * Returns B2 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B2() {
			return new Level(\php\lang\PHPNumber::newInstance(0x2F));
		}

		/**
		 * Returns C3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x30));
		}

		/**
		 * Returns C#3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x31));
		}

		/**
		 * Returns D3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x32));
		}

		/**
		 * Returns D#3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x33));
		}

		/**
		 * Returns E3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x34));
		}

		/**
		 * Returns F3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x35));
		}

		/**
		 * Returns F#3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x36));
		}

		/**
		 * Returns G3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x37));
		}

		/**
		 * Returns G#3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x38));
		}

		/**
		 * Returns A3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x39));
		}

		/**
		 * Returns A#3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x3A));
		}

		/**
		 * Returns B3 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B3() {
			return new Level(\php\lang\PHPNumber::newInstance(0x3B));
		}

		/**
		 * Returns C4 (Middle C) level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x3C));
		}

		/**
		 * Returns C4 (Middle C) level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function MIDDLE_C() {
			return self::C4();
		}

		/**
		 * Returns CS4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x3D));
		}

		/**
		 * Returns D4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x3E));
		}

		/**
		 * Returns D#4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x3F));
		}

		/**
		 * Returns E4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x40));
		}

		/**
		 * Returns F4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x41));
		}

		/**
		 * Returns F#4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x42));
		}

		/**
		 * Returns G4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x43));
		}

		/**
		 * Returns G#4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x44));
		}

		/**
		 * Returns A4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x45));
		}

		/**
		 * Returns A#4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x46));
		}

		/**
		 * Returns B4 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B4() {
			return new Level(\php\lang\PHPNumber::newInstance(0x47));
		}

		/**
		 * Returns C5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x48));
		}

		/**
		 * Returns C#5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x49));
		}

		/**
		 * Returns D5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x4A));
		}

		/**
		 * Returns D#5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x4B));
		}

		/**
		 * Returns E5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x4C));
		}

		/**
		 * Returns F5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x4D));
		}

		/**
		 * Returns F#5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x4E));
		}

		/**
		 * Returns G5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x4F));
		}

		/**
		 * Returns G#5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x50));
		}

		/**
		 * Returns A5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x51));
		}

		/**
		 * Returns A#5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x52));
		}

		/**
		 * Returns B5 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B5() {
			return new Level(\php\lang\PHPNumber::newInstance(0x53));
		}

		/**
		 * Returns C6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x54));
		}

		/**
		 * Returns C#6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x55));
		}

		/**
		 * Returns D6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x56));
		}

		/**
		 * Returns D#6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x57));
		}

		/**
		 * Returns E6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x58));
		}

		/**
		 * Returns F6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x59));
		}

		/**
		 * Returns F#6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x5A));
		}

		/**
		 * Returns G6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x5B));
		}

		/**
		 * Returns G#6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x5C));
		}

		/**
		 * Returns A6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x5D));
		}

		/**
		 * Returns A#6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x5E));
		}

		/**
		 * Returns B6 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B6() {
			return new Level(\php\lang\PHPNumber::newInstance(0x5F));
		}

		/**
		 * Returns C7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x60));
		}

		/**
		 * Returns C#7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x61));
		}

		/**
		 * Returns D7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x62));
		}

		/**
		 * Returns D#7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x63));
		}

		/**
		 * Returns E7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x64));
		}

		/**
		 * Returns F7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x65));
		}

		/**
		 * Returns F#7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x66));
		}

		/**
		 * Returns G7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x67));
		}

		/**
		 * Returns G#7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x68));
		}

		/**
		 * Returns A7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x69));
		}

		/**
		 * Returns A#7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x6A));
		}

		/**
		 * Returns B7 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B7() {
			return new Level(\php\lang\PHPNumber::newInstance(0x6B));
		}

		/**
		 * Returns C8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x6C));
		}

		/**
		 * Returns C#8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function CS8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x6D));
		}

		/**
		 * Returns D8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function D8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x6E));
		}

		/**
		 * Returns D#8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function DS8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x6F));
		}

		/**
		 * Returns E8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function E8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x70));
		}

		/**
		 * Returns F8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function F8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x71));
		}

		/**
		 * Returns F#8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function FS8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x72));
		}

		/**
		 * Returns G8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function G8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x73));
		}

		/**
		 * Returns G#8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function GS8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x74));
		}

		/**
		 * Returns A8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function A8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x75));
		}

		/**
		 * Returns A#8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function AS8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x76));
		}

		/**
		 * Returns B8 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function B8() {
			return new Level(\php\lang\PHPNumber::newInstance(0x77));
		}

		/**
		 * Returns C9 level of a note.
		 * @return \php\util\midis\Level
		 */
		public static final function C9() {
			return new Level(\php\lang\PHPNumber::newInstance(0x78));
		}

		private $level;

		/**
		 * Constructs the \php\util\midis\Level object.
		 * @param \php\lang\PHPNumber $level
		 */
		protected function __construct(\php\lang\PHPNumber $level) {
			parent::__construct();
			$this->level = $level->getInteger()->getNumber();
		}

		/**
		 * Returns the level of a note.
		 * @return \php\lang\PHPNumber
		 */
		public function getLevel() {
			return \php\lang\PHPNumber::newInstance($this->level);
		}

	}

}