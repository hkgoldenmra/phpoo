<?php

/**
 * Description of \php\util\midis\Midi
 */

namespace php\util\midis {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/midis/Track.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 * The class \php\util\midis\Midi represents a Midi file.
	 */
	class Midi extends \php\lang\PHPObject {

		/**
		 * Returns the header of the Midi Chunk.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HEADER() {
			return \php\lang\PHPString::newInstance("\x4D\x54\x68\x64");
		}

		/**
		 * Represent the length of a chunk in 4-byte hex value style.
		 * @param \php\lang\PHPNumber $length the length of a chunk.
		 * @return \php\lang\PHPString
		 */
		public static function toChunkLengthString(\php\lang\PHPNumber $length) {
			$length = $length->getInteger()->getNumber();
			return \php\lang\PHPString::newInstance(
					chr(($length >> 0x18) & 0xFF)
					. chr(($length >> 0x10) & 0xFF)
					. chr(($length >> 0x08) & 0xFF)
					. chr($length & 0xFF)
			);
		}

		/**
		 * Returns a \php\util\midis\Midi object.
		 */
		public static function newInstance() {
			return new Midi();
		}

		private $tracks;

		/**
		 * Construct a \php\util\midis\Midi object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->tracks = array();
		}

		/**
		 * Adds a track to this \php\util\midis\Midi object.
		 * @param \php\util\midis\Track $track a \php\util\midis\Track object.
		 */
		public function addTrack(Track $track) {
			array_push($this->tracks, $track);
		}

		private function dumpString($string) {
			$return = '';
			for ($i = 0; $i < strlen($string); $i++) {
				$return .= sprintf('%02X ', ord(substr($string, $i, 1)));
			}
			return trim($return);
		}

		/**
		 * Represents this object in debug mode.
		 * @return \php\lang\PHPString
		 */
		public function toDebugString() {
			$format = "\x00\x01";
			$trackCount = count($this->tracks);
			$trackCount = chr(($trackCount >> 0x08) & 0xFF) . chr($trackCount & 0xFF);
			$ppqn = Note::QUARTER()->getNode()->getNumber();
			$ppqn = chr(($ppqn >> 0x08) & 0xFF) . chr($ppqn & 0xFF);
			$return = '';
			$return .= $this->dumpString(self::HEADER()->getString());
			$return .= "\n";
			$return .= $this->dumpString(Midi::toChunkLengthString(\php\lang\PHPNumber::newInstance(strlen($format) + strlen($trackCount) + strlen($ppqn)))->getString());
			$return .= "\n";
			$return .= $this->dumpString($format);
			$return .= "\n";
			$return .= $this->dumpString($trackCount);
			$return .= "\n";
			$return .= $this->dumpString($ppqn);
			foreach ($this->tracks as $track) {
				$return .= "\n";
				$return .= $track->toDebugString()->getString();
			}
			return \php\lang\PHPString::newInstance($return);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = '';
			$bytes = explode(' ', str_replace("\n", ' ', $this->toDebugString()->getString()));
			foreach ($bytes as $byte) {
				$return .= chr(hexdec($byte));
			}
			return \php\lang\PHPString::newInstance($return);
		}

		/**
		 * Exports the data from this Midi.
		 * @param \php\lang\PHPString $filename The default export file name. Default &lt;current system date time&gt;.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			// **************************************************
			// export
			// **************************************************
			$extension = \php\lang\PHPString::newInstance('mid');
			if ($filename === null) {
				$datetime = \php\util\Calendar::newInstanceByTimestamp();
				$filename = $datetime->toString();
			}
			if ($filename->endsWith($extension)->not()->getBoolean()) {
				$filename = $filename->append(\php\lang\PHPString::newInstance('.'));
				$filename = $filename->append($extension);
			}
			\php\util\Exporter::export(\php\lang\PHPString::newInstance('audio/midi'), $filename);
			// **************************************************
			echo $this->toString()->getString();
		}

	}

}