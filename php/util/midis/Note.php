<?php

/**
 * Description of \php\util\midis\Note
 */

namespace php\util\midis {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');

	/**
	 * This class represents a duration of a muscial note.
	 */
	class Note extends \php\lang\PHPObject {

		/**
		 * Returns the value of Quater note. The default constant value is 96 (0x60) which can divided by 3, 32.
		 * @return \php\lang\PHPNumber
		 */
		public static final function QUARTER() {
			return new Note(\php\lang\PHPNumber::newInstance(0x60));
		}

		/**
		 * Returns the value of Whole note which equals Quarter note * 4.
		 * @return \php\lang\PHPNumber
		 */
		public static final function WHOLE() {
			return new Note(self::QUARTER()->getNode()->multiply(\php\lang\PHPNumber::newInstance(4)));
		}

		/**
		 * Returns the value of Half note which equals Quarter note * 2.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HALF() {
			return new Note(self::QUARTER()->getNode()->multiply(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of Eighth note which equals Quarter note / 2.
		 * @return \php\lang\PHPNumber
		 */
		public static final function EIGHTH() {
			return new Note(self::QUARTER()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of Sixteenth note which equals Quarter note / 4.
		 * @return \php\lang\PHPNumber
		 */
		public static final function SIXTEENTH() {
			return new Note(self::EIGHTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of Thirty-Second note which equals Quarter note / 8.
		 * @return \php\lang\PHPNumber
		 */
		public static final function THIRTY_SECOND() {
			return new Note(self::SIXTEENTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of Sixty-Fourth note which equals Quarter note / 16.
		 * @return \php\lang\PHPNumber
		 */
		public static final function SIXTY_FOURTH() {
			return new Note(self::THIRTY_SECOND()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of dotted Whole note which equals Quarter note * 6.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED_WHOLE() {
			return new Note(self::QUARTER()->getNode()->multiply(\php\lang\PHPNumber::newInstance(6)));
		}

		/**
		 * Returns the value of dotted Half note which equals Quarter note * 3.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED_HALF() {
			return new Note(self::DOTTED_WHOLE()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of dotted Quarter note which equals Quarter note * 3 / 2.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED_QUARTER() {
			return new Note(self::DOTTED_HALF()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of dotted Eighth note which equals Quarter note * 3 / 4.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED_EIGHTH() {
			return new Note(self::DOTTED_QUARTER()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of dotted Sixteenth note which equals Quarter note * 3 / 8.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED_SIXTEENTH() {
			return new Note(self::DOTTED_EIGHTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of dotted Sixteenth note which equals Quarter note * 3 / 16.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED_THIRTY_SECOND() {
			return new Note(self::DOTTED_SIXTEENTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(2)));
		}

		/**
		 * Returns the value of Triplet Half note which equals Quarter note * 4 / 3.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TRIPLET_HALF() {
			return new Note(self::WHOLE()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(3)));
		}

		/**
		 * Returns the value of Triplet Quarter note which equals Quarter note * 2 / 3.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TRIPLET_QUARTER() {
			return new Note(self::HALF()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(3)));
		}

		/**
		 * Returns the value of Triplet Eighth note which equals Quarter note / 3.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TRIPLET_EIGHTH() {
			return new Note(self::QUARTER()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(3)));
		}

		/**
		 * Returns the value of Triplet Sixteenth note which equals Quarter note / 6.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TRIPLET_SIXTEENTH() {
			return new Note(self::EIGHTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(3)));
		}

		/**
		 * Returns the value of Triplet Thirty-Second note which equals Quarter note / 12.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TRIPLET_THIRTY_SECOND() {
			return new Note(self::SIXTEENTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(3)));
		}

		/**
		 * Returns the value of Triplet Sixty-Fourth note which equals Quarter note / 24.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TRIPLET_SIXTY_FOURTH() {
			return new Note(self::EIGHTH()->getNode()->dividedBy(\php\lang\PHPNumber::newInstance(3)));
		}

		private $note;

		/**
		 * Constructs the \php\util\midis\Level object.
		 * @param \php\lang\PHPNumber $note
		 */
		protected function __construct(\php\lang\PHPNumber $note) {
			parent::__construct();
			$this->note = $note->getInteger()->getNumber();
		}

		/**
		 * Returns the duration of a note.
		 * @return \php\lang\PHPNumber
		 */
		public function getNode() {
			return \php\lang\PHPNumber::newInstance($this->note);
		}

	}

}