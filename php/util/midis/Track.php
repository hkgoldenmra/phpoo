<?php

/**
 * Description of \php\util\midis\Track
 */

namespace php\util\midis {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');
	include_once('php/util/midis/Instrument.php');
	include_once('php/util/midis/Level.php');
	include_once('php/util/midis/Note.php');
	include_once('php/util/midis/Midi.php');
	include_once('php/util/collections/LinkedList.php');

	/**
	 * A track is an independent standard Midi file of stream of Midi events.
	 */
	class Track extends \php\lang\PHPObject {

		/**
		 * Returns the header of the Midi Track Chunk.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HEADER() {
			return \php\lang\PHPString::newInstance("\x4D\x54\x72\x6B");
		}

		/**
		 * Returns the footer of the Midi Track Chunk.
		 * @return \php\lang\PHPNumber
		 */
		public static final function FOOTER() {
			return \php\lang\PHPString::newInstance("\x00\xFF\x2F\x00");
		}

		/**
		 * Returns a \php\util\midis\Track object.
		 */
		public static function newInstance() {
			return new Track();
		}

		private $fields = array();
		private $body = array();

		/**
		 * Construct a \php\util\midis\Track object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->fields['copyright'] = '';
			$this->fields['instrument_name'] = '';
			$this->fields['tempo'] = '';
			$this->fields['instrument'] = '';
			$this->setCopyright(\php\lang\PHPString::newInstance('Copyright (c) All Rights Reserved'));
			$this->setTempo(\php\lang\PHPNumber::newInstance(120));
			$this->setInstrument(Instrument::ACOUSTIC_GRAND_PIANO());
		}

		/**
		 * Sets the copyright of this Track object. Only the first available track can affect the copyright field.
		 * @param \php\lang\PHPString $copyright The copyright message.
		 */
		public function setCopyright(\php\lang\PHPString $copyright) {
			$string = '';
			$length = $copyright->length();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($length)->getBoolean(); $i = $i->increase()) {
				$char = $copyright->charAt($i);
				$urlencodes = $char->toURLEncodes();
				for ($j = \php\lang\PHPNumber::ZERO(); $urlencodes->isEmpty()->not()->getBoolean(); $j = $j->increase()) {
					$urlencode = $urlencodes->getFirst();
					$urlencodes->deleteFirst();
					$string .= chr($urlencode->getNumber());
				}
			}
			$this->fields['copyright'] = "\x00\xFF\x02" . chr(strlen($string)) . $string;
			//$this->fields['sequencer'] = "\x00\xFF\x00" . chr(strlen($string)) . $string;
			//$this->fields['event'] = "\x00\xFF\x01" . chr(strlen($string)) . $string;
			//$this->fields['track_name'] = "\x00\xFF\0x03" . chr(strlen($string)) . $string;
			//$this->fields['marker'] = "\x00\xFF\x06" . chr(strlen($string)) . $string;
			//$this->fields['cue_point'] = "\x00\xFF\x07" . chr(strlen($string)) . $string;
		}

		/**
		 * Sets the tempo of this Track object.
		 * @param \php\lang\PHPNumber $tempo The speed of track.
		 */
		public function setTempo(\php\lang\PHPNumber $tempo) {
			$tempo = intval(60000000 / $tempo->getInteger()->getNumber());
			$string = chr(($tempo >> 0x10) & 0xFF)
				. chr(($tempo >> 0x08) & 0xFF)
				. chr($tempo & 0xFF);
			$this->fields['tempo'] = "\x00\xFF\x51" . chr(strlen($string)) . $string;
		}

		/**
		 * Sets the instrument of this Track object.
		 * @param \php\util\midis\Instrument $instrument The code of instrument.
		 */
		public function setInstrument(Instrument $instrument) {
			/*
			  $instrumentName = $instrument->getInstrumentName()->getString();
			  $this->fields['instrument_name'] = chr(0x00)
			  . chr(0xFF)
			  . chr(0x04)
			  . chr(strlen($instrumentName))
			  . $instrumentName;
			 */
			$this->fields['instrument'] = "\x00\xC0" . chr($instrument->getInstrument()->getNumber());
		}

		private function noteOn($level, $volume) {
			return "\x00\x90" . chr($level) . chr($volume);
		}

		private function noteOff($duration, $level) {
			$initial = '';
			for ($i = 3; $i >= 1; $i--) {
				$offset = pow(0x80, $i);
				if ($duration >= $offset) {
					$quotient = floor($duration / $offset);
					$initial .= chr(0x80 + $quotient);
					$duration %= $offset;
				}
			}
			$initial .= chr($duration);
			return $initial . "\x80" . chr($level) . "\x00";
		}

		/**
		 * Adds a rest to this Track object.
		 * @param \php\util\midis\Note $note The duration of a note. References to the constant value of Quarter node.
		 */
		public function addRest(Note $note) {
			array_push($this->body,
				$this->noteOn(0x00, 0x00)
				. $this->noteOff($note->getNode()->getNumber(), 0x00)
			);
		}

		/**
		 * Adds a note to this Track object.
		 * @param \php\util\midis\Note $note The duration of a note. References to the constant value of Quarter node.
		 * @param \php\util\midis\Level $level The level of a note.
		 */
		public function addNote(Note $note, Level $level, \php\lang\PHPString $lyric = null) {
			$levels = \php\util\collections\LinkedList::newInstance();
			$levels->addLast($level);
			$this->addNotes($note, $levels, $lyric);
		}

		/**
		 * Adds a list of notes to this Track object.
		 * @param \php\util\midis\Note $note The duration of a note. References to the constant value of Quarter node.
		 * @param \php\util\collections\LinkedList $levels A list of levels of a note.
		 */
		public function addNotes(Note $note, \php\util\collections\LinkedList $levels, \php\lang\PHPString $lyric = null) {
			$on = '';
			$off = '';
			$string = '';
			while ($levels->isEmpty()->not()->getBoolean()) {
				$level = $levels->getFirst()->getLevel()->getNumber();
				$levels->deleteFirst();
				$on .= $this->noteOn($level, 0x7F);
				$off .= $this->noteOff((($levels->size()->isPositive()->getBoolean()) ? 0 : $note->getNode()->getNumber()), $level);
			}
			if ($lyric !== null) {
				$length = $lyric->length();
				for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($length)->getBoolean(); $i = $i->increase()) {
					$char = $lyric->charAt($i);
					$urlencodes = $char->toURLEncodes();
					for ($j = \php\lang\PHPNumber::ZERO(); $urlencodes->isEmpty()->not()->getBoolean(); $j = $j->increase()) {
						$urlencode = $urlencodes->getFirst();
						$urlencodes->deleteFirst();
						$string .= chr($urlencode->getNumber());
					}
				}
				$string = "\x00\xFF\x05" . chr(strlen($string)) . $string;
			}
			array_push($this->body, $string . $on . $off);
		}

		private function dumpString($string) {
			$return = '';
			for ($i = 0; $i < strlen($string); $i++) {
				$return .= sprintf('%02X ', ord(substr($string, $i, 1)));
			}
			return trim($return);
		}

		/**
		 * Represents this object in debug mode.
		 * @return \php\lang\PHPString
		 */
		public function toDebugString() {
			$length = 0;
			$body = '';
			$fields = array_merge($this->fields, $this->body);
			array_push($fields, self::FOOTER()->getString());
			foreach ($fields as $field) {
				if (strlen($field) > 0) {
					$length += strlen($field);
					$body .= "\n" . $this->dumpString($field);
				}
			}
			$body = $this->dumpString(self::HEADER()->getString())
				. "\n"
				. $this->dumpString(
					chr(($length >> 0x18) & 0xFF)
					. chr(($length >> 0x10) & 0xFF)
					. chr(($length >> 0x08) & 0xFF)
					. chr($length & 0xFF)
				)
				. $body;
			return \php\lang\PHPString::newInstance($body);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = '';
			$bytes = explode(' ', str_replace("\n", ' ', $this->toDebugString()->getString()));
			foreach ($bytes as $byte) {
				$return .= chr(hexdec($byte));
			}
			return \php\lang\PHPString::newInstance($return);
		}

	}

}