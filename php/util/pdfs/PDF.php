<?php

/**
 * Description of \php\util\pdfs\PDF
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/pdfs/PDFInfo.php');
	include_once('php/util/pdfs/PDFCatalog.php');

	/**
	 * The \php\util\pdfs\PDF class represents the PDF structure.
	 */
	class PDF extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\pdfs\PDF object.
		 * @return \php\util\pdfs\PDF
		 */
		public static function newInstance() {
			return new PDF();
		}

		/**
		 *
		 * @var \php\util\collections\Map
		 */
		private $pdfObjects;

		/**
		 * Constructs a \php\util\pdfs\PDF object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->pdfObjects = \php\util\collections\Map::newInstance();
		}

		/**
		 * Sets the catalog of this PDF object.
		 * @param \php\util\pdfs\pdfs\PDFCatalog $pdfCatalog The PDF catalog.
		 */
		public function setCatalog(PDFCatalog $pdfCatalog) {
			$this->pdfObjects->put(\php\lang\PHPString::newInstance('Root'), $pdfCatalog);
		}

		/**
		 * Sets the information of this PDF object.
		 * @param \php\util\pdfs\pdfs\PDFInfo $pdfInfo The PDF information.
		 */
		public function setInfo(PDFInfo $pdfInfo) {
			$this->pdfObjects->put(\php\lang\PHPString::newInstance('Info'), $pdfInfo);
		}

		/**
		 * Exports the PDF object to a PDF file.
		 * @param \php\lang\PHPString $filename The default export file name. Default &lt;current system date time&gt;.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			$extension = \php\lang\PHPString::newInstance('pdf');
			if ($filename === null) {
				$datetime = \php\util\Calendar::newInstanceByTimestamp();
				$filename = $datetime->toString();
			}
			if ($filename->endsWith($extension)->not()->getBoolean()) {
				$filename = $filename->append(\php\lang\PHPString::newInstance('.'));
				$filename = $filename->append($extension);
			}
			\php\util\Exporter::export(\php\lang\PHPString::newInstance('application/pdf'), $filename);
			echo "%PDF-1.7";
			$keys = $this->pdfObjects->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				if (($value = $this->pdfObjects->get($key)) !== null) {
					echo $value->toString()->getString();
				}
			}
			echo "\n\ntrailer <<\n";
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				if (($value = $this->pdfObjects->get($key)) !== null) {
					echo '/';
					echo $key->getstring();
					echo ' ';
					echo $value->getId()->getNumber();
					echo " 0 R\n";
				}
			}
			echo ">>\n";
			echo '%%EOF';
		}

	}

}