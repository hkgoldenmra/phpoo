<?php

/**
 * Description of \php\util\pdfs\PDF2Point
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDFShape.php');

	/**
	 * The \php\util\pdfs\PDF2Point class represents the PDF 2-point structure.
	 */
	abstract class PDF2Point extends PDFShape {

		private $x1;
		private $y1;
		private $x2;
		private $y2;

		/**
		 * Constructs a \php\util\pdfs\PDF2Point object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF 2-point.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF 2-point.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF 2-point.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF 2-point.
		 * @param \php\awt\Color $color The color of the PDF 2-point.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style of the PDF 2-point.
		 */
		protected function __construct(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\awt\Color $color = null, PDFLineStyle $style = null) {
			parent::__construct($color, $style);
			$this->x1 = $x1->getNumber();
			$this->y1 = $y1->negative()->getNumber();
			$this->x2 = $x2->getNumber();
			$this->y2 = $y2->negative()->getNumber();
		}

		/**
		 * Returns the first point of x of the PDF 2-point.
		 * @return \php\lang\PHPNumber
		 */
		public function getX1() {
			return \php\lang\PHPNumber::newInstance($this->x1);
		}

		/**
		 * Returns the first point of y of the PDF 2-point.
		 * @return \php\lang\PHPNumber
		 */
		public function getY1() {
			return \php\lang\PHPNumber::newInstance($this->y1);
		}

		/**
		 * Returns the second point of x of the PDF 2-point.
		 * @return \php\lang\PHPNumber
		 */
		public function getX2() {
			return \php\lang\PHPNumber::newInstance($this->x2);
		}

		/**
		 * Returns the second point of y of the PDF 2-point.
		 * @return \php\lang\PHPNumber
		 */
		public function getY2() {
			return \php\lang\PHPNumber::newInstance($this->y2);
		}

	}

}