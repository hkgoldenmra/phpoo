<?php

/**
 * Description of \php\util\pdfs\PDF2PointShape
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDF2Point.php');

	/**
	 * The \php\util\pdfs\PDF2PointShape class represents the PDF 2-Point Shape structure.
	 */
	abstract class PDF2PointShape extends PDF2Point {

		public static function calculateControlPoint($number) {
			return $number * (4 * (sqrt(2) - 1) / 3);
		}

		/**
		 * Constructs a \php\util\pdfs\PDF2PointShape object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF 2-point shape.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF 2-point shape.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF 2-point shape.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF 2-point shape.
		 * @param \php\awt\Color $color The color of the PDF 2-point shape.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style of the PDF 2-point shape.
		 */
		protected function __construct(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\awt\Color $color = null, PDFLineStyle $style = null) {
			parent::__construct($x1->min($x2), $y1->min($y2), $x2->max($x2), $y2->max($y1), $color, $style);
		}

	}

}