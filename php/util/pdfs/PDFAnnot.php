<?php

/**
 * Description of \php\util\pdfs\PDFAnnot
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/util/pdfs/PDFType.php');

	/**
	 * The \php\util\pdfs\PDFAnnot class represents the PDF annotation structure.
	 */
	abstract class PDFAnnot extends PDFType {

		/**
		 * Constructs a \php\util\pdfs\PDFAnnot object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('/Annot'));
		}

	}

}