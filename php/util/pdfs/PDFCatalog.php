<?php

/**
 * Description of \php\util\pdfs\PDFCatalog
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/pdfs/PDFType.php');
	include_once('php/util/pdfs/PDFPages.php');
	include_once('php/util/pdfs/PDFPage.php');

	/**
	 * The \php\util\pdfs\PDFCatalog class represents the PDF catalog structure.
	 */
	class PDFCatalog extends PDFType {

		private static $pageLayouts = array(
			1 => '/SinglePage',
			2 => '/TwoPageLeft',
			3 => '/TwoPageRight',
			4 => '/OneColumn',
			5 => '/TwoColumnLeft',
			6 => '/TwoColumnRight',
		);
		private static $pageModes = array(
			1 => '/UseNone',
			2 => '/UseOutlines',
			3 => '/UseThumbs',
			4 => '/FullScreen',
			5 => '/UseOC',
			6 => '/UseAttachments',
		);

		/**
		 * Returns the single page of page layout.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_LAYOUT_SINGLE_PAGE() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the two page left of page layout.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_LAYOUT_TWO_PAGE_LEFT() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the two page right of page layout.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_LAYOUT_TWO_PAGE_RIGHT() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the one column of page layout.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_LAYOUT_ONE_COLUMN() {
			return \php\lang\PHPNumber::newInstance(4);
		}

		/**
		 * Returns the two column left of page layout.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_LAYOUT_TWO_COLUMN_LEFT() {
			return \php\lang\PHPNumber::newInstance(5);
		}

		/**
		 * Returns the two column right of page layout.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_LAYOUT_TWO_COLUMN_RIGHT() {
			return \php\lang\PHPNumber::newInstance(6);
		}

		/**
		 * Returns the use none of page mode.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_MODE_USE_NONE() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the use outlines of page mode.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_MODE_USE_OUTLINES() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the use thumbs of page mode.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_MODE_USE_THUMBS() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the fullscreen of page mode.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_MODE_FULLSCREEN() {
			return \php\lang\PHPNumber::newInstance(4);
		}

		/**
		 * Returns the use OC of page mode.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_MODE_USE_OC() {
			return \php\lang\PHPNumber::newInstance(5);
		}

		/**
		 * Returns the use attachments of page mode.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAGE_MODE_USE_ATTACHMENTS() {
			return \php\lang\PHPNumber::newInstance(6);
		}

		private $pdfPages;

		/**
		 * Returns a \php\util\pdfs\PDFCatalog object.
		 * @return \php\util\pdfs\PDFCatalog
		 */
		public static function newInstance() {
			return new PDFCatalog();
		}

		/**
		 * Constructs a \php\util\pdfs\PDFCatalog object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('/Catalog'));
			$this->pdfPages = PDFPages::newInstance();
			$this->setAttribute(\php\lang\PHPString::newInstance('Pages'), $this->pdfPages->getId()->toString()->append(\php\lang\PHPString::newInstance(" 0 R")));
		}

		/**
		 * Sets the page layout of this PDF catalog.
		 * @param \php\lang\PHPNumber $pageLayout The page layout of this PDF catalog.
		 */
		public function setPageLayout(\php\lang\PHPNumber $pageLayout) {
			$this->setAttribute(\php\lang\PHPString::newInstance('PageLayout'), \php\lang\PHPString::newInstance(self::$pageLayouts[$pageLayout->getNumber()]));
		}

		/**
		 * Sets the page mode of this PDF catalog.
		 * @param \php\lang\PHPNumber $pageMode The page mode of this PDF catalog.
		 */
		public function setPageMode(\php\lang\PHPNumber $pageMode) {
			$this->setAttribute(\php\lang\PHPString::newInstance('PageMode'), \php\lang\PHPString::newInstance(self::$pageModes[$pageMode->getNumber()]));
		}

		/**
		 * Adds a PDF page to this PDF catalog.
		 * @param \php\util\pdfs\PDFPage $pdfPage A PDF page.
		 */
		public function addPage(PDFPage $pdfPage) {
			$this->pdfPages->addPage($pdfPage);
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($pdfPage->getContents()->size())->getBoolean(); $i = $i->increase()) {
				$pdfContent = $pdfPage->getContents()->get($i);
				for ($j = \php\lang\PHPNumber::newInstance(0); $j->isLessThan($pdfContent->getStreams()->size())->getBoolean(); $j = $j->increase()) {
					$pdfStream = $pdfContent->getStreams()->get($j);
					if ($pdfStream instanceof PDFText) {
						$this->pdfPages->addFont($pdfStream->getPDFFont());
					}
				}
			}
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			$return = $return->append($this->pdfPages->toString());
			return $return;
		}

	}

}