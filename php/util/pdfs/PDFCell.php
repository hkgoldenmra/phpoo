<?php

/**
 * Description of \php\util\pdfs\PDFCell
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/pdfs/PDFText.php');
	include_once('php/util/pdfs/PDFLine.php');
	include_once('php/util/pdfs/PDFFont.php');
	include_once('php/util/pdfs/PDFCustomFont.php');

	/**
	 * The \php\util\pdfs\PDFText class represents the PDF cell structure.
	 */
	class PDFCell extends PDFText {

		/**
		 * Returns the left horizontal alignment of the PDF cell.
		 * @return \php\lang\PHPNumber
		 */
		public static final function LEFT() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the center horizontal alignment of the PDF cell.
		 * @return \php\lang\PHPNumber
		 */
		public static final function CENTER() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the right horizontal alignment of the PDF cell.
		 * @return \php\lang\PHPNumber
		 */
		public static final function RIGHT() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the top vertical alignment of the PDF cell.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TOP() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the middle vertical alignment of the PDF cell.
		 * @return \php\lang\PHPNumber
		 */
		public static final function MIDDLE() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the middle vertical alignment of the PDF cell.
		 * @return \php\lang\PHPNumber
		 */
		public static final function BOTTOM() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		public static function newInstanceByParameters(\php\lang\PHPString $text, \php\lang\PHPNumber $x, \php\lang\PHPNumber $y, PDFFont $pdfFont, \php\lang\PHPNumber $size, \php\awt\Color $color = null, \php\lang\PHPNumber $width = null, \php\lang\PHPNumber $height = null, \php\lang\PHPNumber $horizontaolAlign = null, \php\lang\PHPNumber $verticalAlign = null, PDFLineStyle $top = null, PDFLineStyle $left = null, PDFLineStyle $right = null, PDFLineStyle $bottom = null) {
			return new PDFCell($text, $x, $y, $pdfFont, $size, $color, $width, $height, $horizontaolAlign, $verticalAlign, $top, $left, $right, $bottom);
		}

		private $top = null;
		private $left = null;
		private $right = null;
		private $bottom = null;

		/**
		 * Returns a \php\util\pdfs\PDFCell object.
		 * @param \php\lang\PHPString $text The text stream.
		 * @param \php\lang\PHPNumber $x The left coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the text from the first letter.
		 * @param \php\util\pdfs\PDFFont $pdfFont The PDF font of the text.
		 * @param \php\lang\PHPNumber $size The text size.
		 * @param \php\awt\Color $color The color of the text.
		 * @param \php\lang\PHPNumber $width The width of the PDF cell.
		 * @param \php\lang\PHPNumber $height The height of the PDF cell.
		 * @param \php\lang\PHPNumber $horizontaolAlign The horizontal alignment of the text in the PDF cell.
		 * @param \php\lang\PHPNumber $verticalAlign The vertical alignment of the text in the PDF cell.
		 * @param \php\util\pdfs\PDFLineStyle $top The top border style.
		 * @param \php\util\pdfs\PDFLineStyle $left The left border style.
		 * @param \php\util\pdfs\PDFLineStyle $right The right border style.
		 * @param \php\util\pdfs\PDFLineStyle $bottom The bottom border style.
		 */
		protected function __construct(\php\lang\PHPString $text, \php\lang\PHPNumber $x, \php\lang\PHPNumber $y, PDFFont $pdfFont, \php\lang\PHPNumber $size, \php\awt\Color $color = null, \php\lang\PHPNumber $width = null, \php\lang\PHPNumber $height = null, \php\lang\PHPNumber $horizontaolAlign = null, \php\lang\PHPNumber $verticalAlign = null, PDFLineStyle $top = null, PDFLineStyle $left = null, PDFLineStyle $right = null, PDFLineStyle $bottom = null) {
			parent::__construct($text, $x, $y, $pdfFont, $size, $color);
			$textWidth = PDFCustomFont::getTextWidth($text, $pdfFont, $size);
			$textHeight = PDFCustomFont::getTextHeight($size);
			if ($width === null) {
				$width = $textWidth;
			}
			if ($height === null) {
				$height = $textHeight;
			}
			if ($horizontaolAlign === null) {
				$horizontaolAlign = self::LEFT();
			}
			if ($verticalAlign === null) {
				$verticalAlign = self::TOP();
			}
			switch ($horizontaolAlign->getNumber()) {
				case self::CENTER()->getNumber(): {
						$this->setX($x->addition($width->subtraction($textWidth)->dividedBy(\php\lang\PHPNumber::newInstance(2))));
					} break;
				case self::RIGHT()->getNumber(): {
						$this->setX($x->addition($width->subtraction($textWidth)));
					} break;
			}
			switch ($verticalAlign->getNumber()) {
				case self::MIDDLE()->getNumber(): {
						$this->setY($y->addition($height)->addition($y)->subtraction($textHeight)->dividedBy(\php\lang\PHPNumber::newInstance(2)));
					} break;
				case self::BOTTOM()->getNumber(): {
						$this->setY($y->addition($height)->subtraction($textHeight));
					} break;
			}
			if ($top !== null) {
				$this->top = PDFLine::newInstanceByParameters($x, $y, $x->addition($width), $y, $top);
			}
			if ($left !== null) {
				$this->left = PDFLine::newInstanceByParameters($x, $y->addition($height), $x, $y, $left);
			}
			if ($right !== null) {
				$this->right = PDFLine::newInstanceByParameters($x->addition($width), $y, $x->addition($width), $y->addition($height), $right);
			}
			if ($bottom !== null) {
				$this->bottom = PDFLine::newInstanceByParameters($x->addition($width), $y->addition($height), $x, $y->addition($height), $bottom);
			}
		}

		/**
		 * Returns the top border style of this PDF cell.
		 * @return \php\util\pdfs\PDFLineStyle
		 */
		public function getTop() {
			return $this->top;
		}

		/**
		 * Returns the left border style of this PDF cell.
		 * @return \php\util\pdfs\PDFLineStyle
		 */
		public function getLeft() {
			return $this->left;
		}

		/**
		 * Returns the right border style of this PDF cell.
		 * @return \php\util\pdfs\PDFLineStyle
		 */
		public function getRight() {
			return $this->right;
		}

		/**
		 * Returns the bottom border style of this PDF cell.
		 * @return \php\util\pdfs\PDFLineStyle
		 */
		public function getBottom() {
			return $this->bottom;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			$lines = array(
				$this->getTop()
				, $this->getRight()
				, $this->getBottom()
				, $this->getLeft()
			);
			foreach ($lines as $k => $v) {
				if ($v !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance("\n"));
					$return = $return->append($v->toString());
				}
			}
			return $return;
		}

	}

}