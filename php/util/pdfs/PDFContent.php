<?php

/**
 * Description of \php\util\pdfs\PDFContent
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/pdfs/PDFObject.php');
	include_once('php/util/pdfs/PDFStream.php');

	/**
	 * The \php\util\pdfs\PDFContent class represents the PDF content structure.
	 */
	class PDFContent extends PDFObject {

		/**
		 * Returns a \php\util\pdfs\PDFContent object.
		 * @return \php\util\pdfs\PDFContent
		 */
		public static function newInstance() {
			return new PDFContent();
		}

		private $streams;

		/**
		 * Constructs a \php\util\pdfs\PDFContent object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->setAttribute(\php\lang\PHPString::newInstance('Type'), \php\lang\PHPString::newInstance('/Content'));
			$this->streams = \php\util\collections\Vector::newInstance();
		}

		/**
		 * Adds a PDF stream for this PDF content.
		 * @param \php\util\pdfs\PDFStream $stream A PDF stream for this PDF content.
		 */
		public function addStream(PDFStream $stream) {
			$this->streams->add($stream);
		}

		/**
		 * Returns a list of PDF streams in this PDF content.
		 * @return \php\util\collections\Vector
		 */
		public function getStreams() {
			return $this->streams;
		}

		private function getContents() {
			$size = $this->getStreams()->size();
			$return = \php\lang\PHPString::newInstance('');
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
				$return = $return->append($this->getStreams()->get($i)->toString());
				$return = $return->append(\php\lang\PHPString::newInstance("\n"));
			}
			return $return;
		}

		/**
		 * Returns the represented string of streams of this PDF content.
		 * @return \php\lang\PHPString
		 */
		public function toStreamString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance($this->getContents()));
			if ($return->length()->isPositive()->getBoolean()) {
				$return = $return->insert(\php\lang\PHPString::newInstance("stream\n"));
				$return = $return->append(\php\lang\PHPString::newInstance("endstream\n"));
			}
			return $return;
		}

	}

}