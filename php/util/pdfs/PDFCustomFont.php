<?php

/**
 * Description of \php\util\pdfs\PDFCustomFont
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/pdfs/PDFFont.php');

	/**
	 * The \php\util\pdfs\PDFCustomFont class represents the PDF default font structure.
	 */
	class PDFCustomFont extends PDFFont {

		/**
		 * Returns the custom PDF font - MSungStd Light Acro.
		 * @return \php\util\pdfs\PDFCustomFont
		 */
		public static final function MSUNGSTD_LIGHT_ACRO() {
			$pdfCustomFont = new PDFCustomFont(\php\lang\PHPString::newInstance('/MSungStd-Light-Acro'), PDFFont::UNICNS_UTF16_H(), \php\lang\PHPNumber::newInstance(1000));
			$pdfCustomFont = self::MSUNGSTD_LIGHT_ACRO_CHAR_WIDTHS($pdfCustomFont);
			return $pdfCustomFont;
		}

		/**
		 * Returns the custom PDF font - MSungStd Light Acro with bold style.
		 * @return \php\util\pdfs\PDFCustomFont
		 */
		public static final function MSUNGSTD_LIGHT_ACRO_BOLD() {
			$pdfCustomFont = new PDFCustomFont(\php\lang\PHPString::newInstance('/MSungStd-Light-Acro,Bold'), PDFFont::UNICNS_UTF16_H(), \php\lang\PHPNumber::newInstance(1000));
			$pdfCustomFont = self::MSUNGSTD_LIGHT_ACRO_CHAR_WIDTHS($pdfCustomFont);
			return $pdfCustomFont;
		}

		/**
		 * Returns the custom PDF font - MSungStd Light Acro with italic style.
		 * @return \php\util\pdfs\PDFCustomFont
		 */
		public static final function MSUNGSTD_LIGHT_ACRO_ITALIC() {
			$pdfCustomFont = new PDFCustomFont(\php\lang\PHPString::newInstance('/MSungStd-Light-Acro,Italic'), PDFFont::UNICNS_UTF16_H(), \php\lang\PHPNumber::newInstance(1000));
			$pdfCustomFont = self::MSUNGSTD_LIGHT_ACRO_CHAR_WIDTHS($pdfCustomFont);
			return $pdfCustomFont;
		}

		/**
		 * Returns the custom PDF font - MSungStd Light Acro with bold and italic style.
		 * @return \php\util\pdfs\PDFCustomFont
		 */
		public static final function MSUNGSTD_LIGHT_ACRO_BOLDITALIC() {
			$pdfCustomFont = new PDFCustomFont(\php\lang\PHPString::newInstance('/MSungStd-Light-Acro,BoldItalic'), PDFFont::UNICNS_UTF16_H(), \php\lang\PHPNumber::newInstance(1000));
			$pdfCustomFont = self::MSUNGSTD_LIGHT_ACRO_CHAR_WIDTHS($pdfCustomFont);
			return $pdfCustomFont;
		}

		private static final function MSUNGSTD_LIGHT_ACRO_CHAR_WIDTHS(PDFCustomFont $pdfCustomFont) {
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter(' '), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('!'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('"'), \php\lang\PHPNumber::newInstance(408));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('#'), \php\lang\PHPNumber::newInstance(668));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('$'), \php\lang\PHPNumber::newInstance(490));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('%'), \php\lang\PHPNumber::newInstance(875));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('&'), \php\lang\PHPNumber::newInstance(698));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter("'"), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('('), \php\lang\PHPNumber::newInstance(240));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter(')'), \php\lang\PHPNumber::newInstance(240));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('*'), \php\lang\PHPNumber::newInstance(417));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('+'), \php\lang\PHPNumber::newInstance(667));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter(','), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('-'), \php\lang\PHPNumber::newInstance(313));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('.'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('/'), \php\lang\PHPNumber::newInstance(520));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('0'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('1'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('2'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('3'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('4'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('5'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('6'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('7'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('8'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('9'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter(':'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter(';'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('<'), \php\lang\PHPNumber::newInstance(667));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('='), \php\lang\PHPNumber::newInstance(667));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('>'), \php\lang\PHPNumber::newInstance(667));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('?'), \php\lang\PHPNumber::newInstance(396));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('@'), \php\lang\PHPNumber::newInstance(921));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('A'), \php\lang\PHPNumber::newInstance(677));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('B'), \php\lang\PHPNumber::newInstance(615));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('C'), \php\lang\PHPNumber::newInstance(719));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('D'), \php\lang\PHPNumber::newInstance(760));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('E'), \php\lang\PHPNumber::newInstance(625));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('F'), \php\lang\PHPNumber::newInstance(552));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('G'), \php\lang\PHPNumber::newInstance(771));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('H'), \php\lang\PHPNumber::newInstance(802));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('I'), \php\lang\PHPNumber::newInstance(354));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('J'), \php\lang\PHPNumber::newInstance(354));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('K'), \php\lang\PHPNumber::newInstance(781));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('L'), \php\lang\PHPNumber::newInstance(604));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('M'), \php\lang\PHPNumber::newInstance(927));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('N'), \php\lang\PHPNumber::newInstance(750));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('O'), \php\lang\PHPNumber::newInstance(823));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('P'), \php\lang\PHPNumber::newInstance(563));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('Q'), \php\lang\PHPNumber::newInstance(823));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('R'), \php\lang\PHPNumber::newInstance(729));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('S'), \php\lang\PHPNumber::newInstance(542));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('T'), \php\lang\PHPNumber::newInstance(698));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('U'), \php\lang\PHPNumber::newInstance(771));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('V'), \php\lang\PHPNumber::newInstance(729));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('W'), \php\lang\PHPNumber::newInstance(948));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('X'), \php\lang\PHPNumber::newInstance(771));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('Y'), \php\lang\PHPNumber::newInstance(677));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('Z'), \php\lang\PHPNumber::newInstance(635));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('['), \php\lang\PHPNumber::newInstance(344));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('\\'), \php\lang\PHPNumber::newInstance(520));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter(']'), \php\lang\PHPNumber::newInstance(344));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('^'), \php\lang\PHPNumber::newInstance(469));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('_'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('`'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('a'), \php\lang\PHPNumber::newInstance(469));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('b'), \php\lang\PHPNumber::newInstance(521));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('c'), \php\lang\PHPNumber::newInstance(427));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('d'), \php\lang\PHPNumber::newInstance(521));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('e'), \php\lang\PHPNumber::newInstance(438));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('f'), \php\lang\PHPNumber::newInstance(271));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('g'), \php\lang\PHPNumber::newInstance(469));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('h'), \php\lang\PHPNumber::newInstance(531));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('i'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('j'), \php\lang\PHPNumber::newInstance(250));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('k'), \php\lang\PHPNumber::newInstance(458));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('l'), \php\lang\PHPNumber::newInstance(240));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('m'), \php\lang\PHPNumber::newInstance(802));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('n'), \php\lang\PHPNumber::newInstance(531));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('o'), \php\lang\PHPNumber::newInstance(500));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('p'), \php\lang\PHPNumber::newInstance(521));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('q'), \php\lang\PHPNumber::newInstance(521));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('r'), \php\lang\PHPNumber::newInstance(365));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('s'), \php\lang\PHPNumber::newInstance(333));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('t'), \php\lang\PHPNumber::newInstance(292));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('u'), \php\lang\PHPNumber::newInstance(521));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('v'), \php\lang\PHPNumber::newInstance(458));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('w'), \php\lang\PHPNumber::newInstance(677));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('x'), \php\lang\PHPNumber::newInstance(479));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('y'), \php\lang\PHPNumber::newInstance(458));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('z'), \php\lang\PHPNumber::newInstance(427));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('{'), \php\lang\PHPNumber::newInstance(480));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('|'), \php\lang\PHPNumber::newInstance(496));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('}'), \php\lang\PHPNumber::newInstance(480));
			$pdfCustomFont->setCharWidth(\php\lang\PHPCharacter::newInstanceByCharacter('~'), \php\lang\PHPNumber::newInstance(667));
			return $pdfCustomFont;
		}

		/**
		 * Calculates the text width with specific text contents, font and font size.
		 * @param \php\lang\PHPString $text The text contents.
		 * @param \php\util\pdfs\PDFCustomFont $pdfCustomFont The PDF custom font.
		 * @param \php\lang\PHPNumber $size Font size.
		 * @return \php\lang\PHPNumber
		 */
		public static function getTextWidth(\php\lang\PHPString $text, PDFCustomFont $pdfCustomFont, \php\lang\PHPNumber $size) {
			$total = \php\lang\PHPNumber::newInstance(0);
			$defaultWidth = $pdfCustomFont->getDefaultWidth();
			$charWidths = $pdfCustomFont->getCharWidths();
			$length = $text->length();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($length)->getBoolean(); $i = $i->increase()) {
				$char = $text->charAt($i);
				if ($charWidths->containsKey($char)->getBoolean()) {
					$total = $total->addition($charWidths->get($char));
				} else {
					$total = $total->addition($defaultWidth);
				}
			}
			return $total->dividedBy(\php\lang\PHPNumber::newInstance(1000.0))->multiply($size);
		}

		private $defaultWidth;
		private $charWidths;

		/**
		 * Constructs a \php\util\pdfs\PDFFont object.
		 * @param \php\lang\PHPNumber $baseFont The base font of this PDF custom font.
		 * @param \php\lang\PHPNumber $encoding The encoding of this PDF custom font.
		 * @param \php\lang\PHPNumber $defaultWidth The default width of this PDF custom font.
		 */
		protected function __construct(\php\lang\PHPString $baseFont, \php\lang\PHPNumber $encoding, \php\lang\PHPNumber $defaultWidth = null) {
			parent::__construct($baseFont, $encoding);
			$this->setAttribute(\php\lang\PHPString::newInstance('Subtype'), \php\lang\PHPString::newInstance('/Type0'));
			if ($defaultWidth === null) {
				$defaultWidth = \php\lang\PHPNumber::newInstance(1000);
			}
			$this->defaultWidth = $defaultWidth->getNumber();
			$this->charWidths = \php\util\collections\Map::newInstance();
		}

		/**
		 * Returns the default width of this PDF custom font.
		 * @return \php\lang\PHPNumber
		 */
		public function getDefaultWidth() {
			return \php\lang\PHPNumber::newInstance($this->defaultWidth);
		}

		/**
		 * Sets the width of character override the default width.
		 * @param \php\lang\PHPCharacter $char The character to be set.
		 * @param \php\lang\PHPNumber $width The width of character.
		 */
		public function setCharWidth(\php\lang\PHPCharacter $char, \php\lang\PHPNumber $width) {
			$this->charWidths->put($char, $width);
		}

		/**
		 * Returns all width of characters.
		 * @return \php\util\collections\Map
		 */
		public function getCharWidths() {
			return $this->charWidths;
		}

		private function addFonts() {
			$children = \php\lang\PHPString::newInstance('');
			$children = $children->append(\php\lang\PHPString::newInstance("[\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("<<\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Type /Font\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Subtype /CIDFontType0\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/BaseFont " . $this->getAttributes()->get(\php\lang\PHPString::newInstance('BaseFont'))->getString() . "\n"));
			$children = $children->append(\php\lang\PHPString::newInstance(sprintf("/DW %.2f\n", $this->getDefaultWidth()->getNumber())));
			$children = $children->append(\php\lang\PHPString::newInstance("/W ["));
			$keys = $this->getCharWidths()->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->getCharWidths()->get($key);
				$children = $children->append(\php\lang\PHPString::newInstance(' '));
				$children = $children->append(\php\lang\PHPString::newInstance($key->toUnicode()->getNumber() - ord(' ') + 1));
				$children = $children->append(\php\lang\PHPString::newInstance(' [ '));
				$children = $children->append(\php\lang\PHPString::newInstance(sprintf('%.2f', $value->getNumber())));
				$children = $children->append(\php\lang\PHPString::newInstance(' ]'));
			}
			$children = $children->append(\php\lang\PHPString::newInstance(" ]\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/CIDSystemInfo <<\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Registry (Adobe)\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Ordering (CNS1)\n"));
			$children = $children->append(\php\lang\PHPString::newInstance(">>\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/FontDescriptor <<\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Type /FontDescriptor\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/FontName " . $this->getAttributes()->get(\php\lang\PHPString::newInstance('BaseFont'))->getString() . "\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/FontBBox [ 0.00 0.00 0.00 0.00 ]\n"));
			$children = $children->append(\php\lang\PHPString::newInstance(">>\n"));
			$children = $children->append(\php\lang\PHPString::newInstance(">>\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("]"));
			$this->setAttribute(\php\lang\PHPString::newInstance('DescendantFonts'), $children);
		}

		protected function toAttributeString() {
			$this->addFonts();
			return parent::toAttributeString();
		}

	}

}