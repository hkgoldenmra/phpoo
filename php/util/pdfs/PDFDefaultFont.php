<?php

/**
 * Description of \php\util\pdfs\PDFDefaultFont
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDFFont.php');

	/**
	 * The \php\util\pdfs\PDFDefaultFont class represents the PDF default font structure.
	 */
	class PDFDefaultFont extends PDFFont {

		/**
		 * Returns a \php\util\pdfs\PDFFont object.
		 * @param \php\lang\PHPNumber $baseFont The base font of this PDF default font.
		 * @param \php\lang\PHPNumber $encoding The encoding of this PDF default font.
		 * @return \php\util\pdfs\PDFDefaultFont
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $baseFont, \php\lang\PHPNumber $encoding) {
			return new PDFDefaultFont($baseFont, $encoding);
		}

		/**
		 * Constructs a \php\util\pdfs\PDFFont object.
		 * @param \php\lang\PHPNumber $baseFont The base font of this PDF default font.
		 * @param \php\lang\PHPNumber $encoding The encoding of this PDF default font.
		 */
		protected function __construct(\php\lang\PHPNumber $baseFont, \php\lang\PHPNumber $encoding) {
			parent::__construct($baseFont, $encoding);
		}

	}

}