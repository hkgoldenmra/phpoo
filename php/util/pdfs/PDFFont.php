<?php

/**
 * Description of \php\util\pdfs\PDFFont
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/pdfs/PDFType.php');

	/**
	 * The \php\util\pdfs\PDFFont class represents the PDF font structure.
	 */
	class PDFFont extends PDFType {

		/**
		 * Returns the default PDF font - Courier.
		 * @return \php\lang\PHPNumber
		 */
		public static final function COURIER() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the default PDF font - Courier with bold style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function COURIER_BOLD() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the default PDF font - Courier with italic style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function COURIER_OBLIQUE() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the default PDF font - Courier with bold and italic style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function COURIER_BOLDOBLIQUE() {
			return \php\lang\PHPNumber::newInstance(4);
		}

		/**
		 * Returns the default PDF font - Helvetica.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HELVETICA() {
			return \php\lang\PHPNumber::newInstance(5);
		}

		/**
		 * Returns the default PDF font - Helvetica with bold style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HELVETICA_BOLD() {
			return \php\lang\PHPNumber::newInstance(6);
		}

		/**
		 * Returns the default PDF font - Helvetica with italic style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HELVETICA_OBLIQUE() {
			return \php\lang\PHPNumber::newInstance(7);
		}

		/**
		 * Returns the default PDF font - Helvetica with bold and italic style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function HELVETICA_BOLDOBLIQUE() {
			return \php\lang\PHPNumber::newInstance(8);
		}

		/**
		 * Returns the default PDF font - Times new Roman.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TIMES_ROMAN() {
			return \php\lang\PHPNumber::newInstance(9);
		}

		/**
		 * Returns the default PDF font - Times new Roman with bold style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TIMES_BOLD() {
			return \php\lang\PHPNumber::newInstance(10);
		}

		/**
		 * Returns the default PDF font - Times new Roman with italic style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TIMES_ITALIC() {
			return \php\lang\PHPNumber::newInstance(11);
		}

		/**
		 * Returns the default PDF font - Times new Roman with bold and italic style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TIMES_BOLDITALIC() {
			return \php\lang\PHPNumber::newInstance(12);
		}

		/**
		 * Returns the default PDF font - Symbol.
		 * @return \php\lang\PHPNumber
		 */
		public static final function SYMBOL() {
			return \php\lang\PHPNumber::newInstance(13);
		}

		/**
		 * Returns the default PDF font - Zapfdingbats.
		 * @return \php\lang\PHPNumber
		 */
		public static final function ZAPFDINGBATS() {
			return \php\lang\PHPNumber::newInstance(14);
		}

		/**
		 * Returns the default PDF encoding - Unicode GB UCS2 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIGB_UCS2_H() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the default PDF encoding - Unicode GB UCS2 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIGB_UCS2_V() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the default PDF encoding - Unicode GB UTF16 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIGB_UTF16_H() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the default PDF encoding - Unicode GB UTF16 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIGB_UTF16_V() {
			return \php\lang\PHPNumber::newInstance(4);
		}

		/**
		 * Returns the default PDF encoding - Unicode CNS UCS2 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNICNS_UCS2_H() {
			return \php\lang\PHPNumber::newInstance(5);
		}

		/**
		 * Returns the default PDF encoding - Unicode CNS UCS2 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNICNS_UCS2_V() {
			return \php\lang\PHPNumber::newInstance(6);
		}

		/**
		 * Returns the default PDF encoding - Unicode CNS UTF16 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNICNS_UTF16_H() {
			return \php\lang\PHPNumber::newInstance(7);
		}

		/**
		 * Returns the default PDF encoding - Unicode CNS UTF16 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNICNS_UTF16_V() {
			return \php\lang\PHPNumber::newInstance(8);
		}

		/**
		 * Returns the default PDF encoding - Unicode JIS UCS2 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIJIS_UCS2_H() {
			return \php\lang\PHPNumber::newInstance(9);
		}

		/**
		 * Returns the default PDF encoding - Unicode JIS UCS2 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIJIS_UCS2_V() {
			return \php\lang\PHPNumber::newInstance(10);
		}

		/**
		 * Returns the default PDF encoding - Unicode JIS UTF16 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIJIS_UTF16_H() {
			return \php\lang\PHPNumber::newInstance(11);
		}

		/**
		 * Returns the default PDF encoding - Unicode JIS UTF16 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIJIS_UTF16_V() {
			return \php\lang\PHPNumber::newInstance(12);
		}

		/**
		 * Returns the default PDF encoding - Unicode KS UCS2 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIKS_UCS2_H() {
			return \php\lang\PHPNumber::newInstance(13);
		}

		/**
		 * Returns the default PDF encoding - Unicode KS UCS2 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIKS_UCS2_V() {
			return \php\lang\PHPNumber::newInstance(14);
		}

		/**
		 * Returns the default PDF encoding - Unicode KS UTF16 horizontal.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIKS_UTF16_H() {
			return \php\lang\PHPNumber::newInstance(15);
		}

		/**
		 * Returns the default PDF encoding - Unicode KS UTF16 vertical.
		 * @return \php\lang\PHPNumber
		 */
		public static final function UNIKS_UTF16_V() {
			return \php\lang\PHPNumber::newInstance(16);
		}

		private static $baseFonts = array(
			1 => '/Courier',
			2 => '/Courier-Bold',
			3 => '/Courier-Oblique',
			4 => '/Courier-BoldOblique',
			5 => '/Helvetica',
			6 => '/Helvetica-Bold',
			7 => '/Helvetica-Oblique',
			8 => '/Helvetica-BoldOblique',
			9 => '/Times-Roman',
			10 => '/Times-Bold',
			11 => '/Times-Italic',
			12 => '/Times-BoldItalic',
			13 => '/Symbol',
			14 => '/ZapfDingbats',
		);
		private static $encodings = array(
			1 => '/UniGB-UCS2-H',
			2 => '/UniGB-UCS2-V',
			3 => '/UniGB-UTF16-H',
			4 => '/UniGB-UTF16-V',
			5 => '/UniCNS-UCS2-H',
			6 => '/UniCNS-UCS2-V',
			7 => '/UniCNS-UTF16-H',
			8 => '/UniCNS-UTF16-V',
			9 => '/UniJIS-UCS2-H',
			10 => '/UniJIS-UCS2-V',
			11 => '/UniJIS-UTF16-H',
			12 => '/UniJIS-UTF16-V',
			13 => '/UniKS-UCS2-H',
			14 => '/UniKS-UCS2-V',
			15 => '/UniKS-UTF16-H',
			16 => '/UniKS-UTF16-V',
		);

		/**
		 * Calculates the text height with specific font size.
		 * @param \php\lang\PHPNumber $size Font size.
		 * @return \php\lang\PHPNumber
		 */
		public static function getTextHeight(\php\lang\PHPNumber $size) {
			return $size->multiply(\php\lang\PHPNumber::newInstance(1.25));
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\util\pdfs\PDFFont object.
		 * @param \php\lang\PHPNumber $baseFont The base font of this PDF font.
		 * @param \php\lang\PHPNumber $encoding The encoding of this PDF font.
		 * @return \php\util\pdfs\PDFFont
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $baseFont, \php\lang\PHPNumber $encoding) {
			$baseFont = \php\lang\PHPString::newInstance(self::$baseFonts[$baseFont->getNumber()]);
			return new PDFFont($baseFont, $encoding);
		}

		/**
		 * Constructs a \php\util\pdfs\PDFFont object.
		 * @param \php\lang\PHPNumber $baseFont The base font of this PDF font.
		 * @param \php\lang\PHPNumber $encoding The encoding of this PDF font.
		 */
		protected function __construct(\php\lang\PHPString $baseFont, \php\lang\PHPNumber $encoding) {
			parent::__construct(\php\lang\PHPString::newInstance('/Font'));
			$this->setAttribute(\php\lang\PHPString::newInstance('Subtype'), \php\lang\PHPString::newInstance('/Type1'));
			$this->setBaseFont($baseFont);
			$this->setEncoding($encoding);
		}

		private function setBaseFont(\php\lang\PHPString $baseFont) {
			$this->setAttribute(\php\lang\PHPString::newInstance('BaseFont'), $baseFont);
		}

		private function setEncoding(\php\lang\PHPNumber $encoding) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Encoding'), \php\lang\PHPString::newInstance(self::$encodings[$encoding->getNumber()]));
		}

	}

}