<?php

/**
 * Description of \php\util\pdfs\PDFImage
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Image.php');
	include_once('php/util/pdfs/PDFStream.php');

	/**
	 * The \php\util\pdfs\PDFImage class represents the PDF image structure.
	 */
	class PDFImage extends PDFStream {

		/**
		 * Returns a \php\util\pdfs\PDFImage object.
		 * @param \php\awt\Image $image The image object.
		 * @param \php\lang\PHPNumber $x The left coordination of the image from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the image from the first letter.
		 * @return \php\util\pdfs\PDFImage
		 */
		public static function newInstanceByParameters(\php\awt\Image $image, \php\lang\PHPNumber $x, \php\lang\PHPNumber $y) {
			return new PDFImage($image, $x, $y);
		}

		private $image;
		private $x;
		private $y;

		/**
		 * Constructs a \php\util\pdfs\PDFImage object.
		 * @param \php\awt\Image $image The image object.
		 * @param \php\lang\PHPNumber $x The left coordination of the image from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the image from the first letter.
		 */
		protected function __construct(\php\awt\Image $image, \php\lang\PHPNumber $x, \php\lang\PHPNumber $y) {
			parent::__construct();
			$this->image = $image;
			$this->x = $x->getNumber();
			$this->y = $y->negative()->getNumber();
		}

		public function getImage() {
			return $this->image;
		}

		/**
		 * Sets the left coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $x The left coordination of the text from the first letter.
		 */
		public function getX() {
			return \php\lang\PHPNumber::newInstance($this->x);
		}

		/**
		 * Sets the bottom coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the text from the first letter.
		 */
		public function getY() {
			return \php\lang\PHPNumber::newInstance($this->y);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$image = $this->getImage();
			$width = $image->getWidth();
			$height = $image->getHeight();
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance('q'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $width->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' 0.00 0.00'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $height->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getX()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getY()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' cm'));
			$return = $return->append(\php\lang\PHPString::newInstance(' BI'));
			$return = $return->append(\php\lang\PHPString::newInstance(' /W'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $width->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' /H'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $height->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(" /CS /RGB /BPC 8 /F [ /AHx ] ID\n"));
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($width)->getBoolean(); $i = $i->increase()) {
				for ($j = \php\lang\PHPNumber::newInstance(0); $j->isLessThan($height)->getBoolean(); $j = $j->increase()) {
					$return = $return->append(\php\lang\PHPString::newInstance(sprintf('%06x ', $image->getColorAt($j, $i)->getRGB()->getNumber())));
				}
				$return = $return->append(\php\lang\PHPString::newInstance("\n"));
			}
			$return = $return->append(\php\lang\PHPString::newInstance('> EI Q'));
			return $return;
		}

	}

}