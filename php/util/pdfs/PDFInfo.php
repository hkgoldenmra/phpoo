<?php

/**
 * Description of \php\util\pdfs\PDFInfo
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/pdfs/PDFObject.php');

	/**
	 * The \php\util\pdfs\PDFInfo class represents the PDF info structure.
	 */
	class PDFInfo extends PDFObject {

		/**
		 * Returns a \php\util\pdfs\PDFInfo object.
		 * @return \php\util\pdfs\PDFInfo
		 */
		public static function newInstance() {
			return new PDFInfo();
		}

		private $utc = 0;

		/**
		 * Constructs a \php\util\pdfs\PDFInfo object.
		 */
		protected function __construct() {
			parent::__construct();
			$data = array('Mr.A', 'PHP', 'PDF');
			$this->setAuthor(\php\lang\PHPString::newInstance($data[0]));
			$this->setCreator(\php\lang\PHPString::newInstance(implode(' ', $data) . 'Creator'));
			$this->setProducer(\php\lang\PHPString::newInstance(implode(' ', $data) . 'Producer'));
			$this->setKeywords(\php\lang\PHPString::newInstance(implode(', ', $data)));
			$calendar = \php\util\Calendar::newInstanceByDateTime();
			$this->setCreationDate($calendar);
			$this->setModificationDate($calendar);
			$dateTimeZone = new \DateTimeZone(date_default_timezone_get());
			$dateTime = new \DateTime("now", $dateTimeZone);
			$this->setUTC(\php\lang\PHPNumber::newInstance($dateTimeZone->getOffset($dateTime) / 3600));
		}

		/**
		 * Sets the title of this PDF info.
		 * @param \php\lang\PHPString $title The title of this PDF info.
		 */
		public function setTitle(\php\lang\PHPString $title) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Title'), $title);
		}

		/**
		 * Sets the subject of this PDF info.
		 * @param \php\lang\PHPString $subject The subject of this PDF info.
		 */
		public function setSubject(\php\lang\PHPString $subject) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Subject'), $subject);
		}

		/**
		 * Sets the author of this PDF info.
		 * @param \php\lang\PHPString $author The author of this PDF info.
		 */
		public function setAuthor(\php\lang\PHPString $author) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Author'), $author);
		}

		/**
		 * Sets the keywords of this PDF info.
		 * @param \php\lang\PHPString $keywords The keywords of this PDF info.
		 */
		public function setKeywords(\php\lang\PHPString $keywords) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Keywords'), $keywords);
		}

		/**
		 * Sets the creator of this PDF info.
		 * @param \php\lang\PHPString $creator The creator of this PDF info.
		 */
		public function setCreator(\php\lang\PHPString $creator) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Creator'), $creator);
		}

		/**
		 * Sets the producer of this PDF info.
		 * @param \php\lang\PHPString $producer The producer of this PDF info.
		 */
		public function setProducer(\php\lang\PHPString $producer) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Producer'), $producer);
		}

		/**
		 * Sets the creation date of this PDF info.
		 * @param \php\lang\PHPString $creationDate The creation date of this PDF info.
		 */
		public function setCreationDate(\php\util\Calendar $creationDate) {
			$this->setAttribute(\php\lang\PHPString::newInstance('CreationDate'), $creationDate);
		}

		/**
		 * Sets the modification date of this PDF info.
		 * @param \php\lang\PHPString $modDate The modification date of this PDF info.
		 */
		public function setModificationDate(\php\util\Calendar $modDate) {
			$this->setAttribute(\php\lang\PHPString::newInstance('ModDate'), $modDate);
		}

		/**
		 * Sets the value of UTC time zone.
		 * @param \php\lang\PHPNumber $utc The value of UTC time zone.
		 */
		public function setUTC(\php\lang\PHPNumber $utc) {
			$this->utc = $utc->getNumber();
		}

		protected function toAttributeString() {
			$return = \php\lang\PHPString::newInstance('');
			$keys = $this->attributes->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->attributes->get($key);
				if ($value instanceof \php\lang\PHPString) {
					$return = $return->append(\php\lang\PHPString::newInstance('/'));
					$return = $return->append($key);
					$return = $return->append(\php\lang\PHPString::newInstance(' < feff'));
					for ($j = \php\lang\PHPNumber::newInstance(0); $j->isLessThan($value->length())->getBoolean(); $j = $j->increase()) {
						$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %04x', $value->charAt($j)->toUnicode()->getNumber())));
					}
					$return = $return->append(\php\lang\PHPString::newInstance(" > %%% "));
					$return = $return->append($value);
					$return = $return->append(\php\lang\PHPString::newInstance("\n"));
				} else if ($value instanceof \php\util\Calendar) {
					$return = $return->append(\php\lang\PHPString::newInstance('/'));
					$return = $return->append($key);
					$return = $return->append(\php\lang\PHPString::newInstance(' (D:'));
					$return = $return->append($value->toDateString()->replaceAll(\php\lang\PHPString::newInstance('-'), \php\lang\PHPString::newInstance('')));
					$return = $return->append($value->toTimeString()->replaceAll(\php\lang\PHPString::newInstance('-'), \php\lang\PHPString::newInstance('')));
					$utc = \php\lang\PHPString::newInstance('');
					$utc = $utc->append(\php\lang\PHPString::newInstance(($this->utc < 0) ? '-' : '+'));
					$utc = $utc->append(\php\lang\PHPString::newInstance(abs(intval($this->utc)))->padChar(\php\lang\PHPCharacter::newInstanceByCharacter('0'), \php\lang\PHPNumber::newInstance(2)));
					$utc = $utc->append(\php\lang\PHPString::newInstance("'"));
					$utc = $utc->append(\php\lang\PHPString::newInstance((abs($this->utc) - abs(intval($this->utc))) * 60)->padChar(\php\lang\PHPCharacter::newInstanceByCharacter('0'), \php\lang\PHPNumber::newInstance(2)));
					$return = $return->append($utc);
					$return = $return->append(\php\lang\PHPString::newInstance("') %%% "));
					$return = $return->append($value->toDateString());
					$return = $return->append(\php\lang\PHPString::newInstance(" "));
					$return = $return->append($value->toTimeString()->replaceAll(\php\lang\PHPString::newInstance('-'), \php\lang\PHPString::newInstance(':')));
					$return = $return->append(\php\lang\PHPString::newInstance(" UTC"));
					$return = $return->append($utc->replaceAll(\php\lang\PHPString::newInstance("'"), \php\lang\PHPString::newInstance(':')));
					$return = $return->append(\php\lang\PHPString::newInstance("\n"));
				}
			}
			return $return;
		}

	}

}
