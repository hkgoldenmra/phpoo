<?php

/**
 * Description of \php\util\pdfs\PDFLine
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDF2Point.php');

	/**
	 * The \php\util\pdfs\PDFLine class represents the PDF line structure.
	 */
	class PDFLine extends PDF2Point {

		/**
		 * Returns a \php\util\pdfs\PDFLine object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF line.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF line.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF line.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF line.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style.
		 * @return \php\util\pdfs\PDFLine
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, PDFLineStyle $style) {
			return new PDFLine($x1, $y1, $x2, $y2, $style);
		}

		/**
		 * Returns a \php\util\pdfs\PDFLine object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF line.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF line.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF line.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF line.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style.
		 */
		protected function __construct(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, PDFLineStyle $style = null) {
			parent::__construct($x1, $y1, $x2, $y2, null, $style);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance('q'));
			$return = $return->append(parent::toString());
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getX1()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getY1()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' m'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getX2()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getY2()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' l'));
			$return = $return->append(\php\lang\PHPString::newInstance(' S'));
			$return = $return->append(\php\lang\PHPString::newInstance(' Q'));
			return $return;
		}

	}

}