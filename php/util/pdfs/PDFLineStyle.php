<?php

/**
 * Description of \php\util\pdfs\PDFLineStyle
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');

	/**
	 * The \php\util\pdfs\PDFLineStyle class represents the PDF line style structure.
	 */
	class PDFLineStyle extends \php\lang\PHPObject {

		/**
		 * Returns the normal end style for PDF line.
		 * @return \php\lang\PHPNumber
		 */
		public static final function END_NORMAL() {
			return \php\lang\PHPNumber::newInstance(0);
		}

		/**
		 * Returns the rounded end style for PDF line.
		 * @return \php\lang\PHPNumber
		 */
		public static final function END_ROUNDED() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the complement end style for PDF line.
		 * @return \php\lang\PHPNumber
		 */
		public static final function END_COMPLEMENT() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the normal corner style for PDF line.
		 * @return \php\lang\PHPNumber
		 */
		public static final function CORNER_NORMAL() {
			return \php\lang\PHPNumber::newInstance(0);
		}

		/**
		 * Returns the rounded corner style for PDF line.
		 * @return \php\lang\PHPNumber
		 */
		public static final function CORNER_ROUNDED() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the cutoff corner style for PDF line.
		 * @return \php\lang\PHPNumber
		 */
		public static final function CORNER_CUTOFF() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns a \php\util\pdfs\PDFLineStyle object.
		 * @param \php\lang\PHPNumber $thick The thickness of the PDF line style.
		 * @param \php\awt\Color $color The color of the PDF line style.
		 * @param \php\lang\PHPNumber $end The end style of the PDF line style.
		 * @param \php\lang\PHPNumber $corner The corner style of the PDF line style.
		 * @return \php\util\pdfs\PDFLineStyle
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $thick, \php\awt\Color $color = null, \php\lang\PHPNumber $end = null, \php\lang\PHPNumber $corner = null) {
			return new PDFLineStyle($thick, $color, $end, $corner);
		}

		private $thick;
		private $color;
		private $end;
		private $corner;

		/**
		 * Constructs a \php\util\pdfs\PDFLineStyle object.
		 * @param \php\lang\PHPNumber $thick The thickness of the PDF line style.
		 * @param \php\awt\Color $color The color of the PDF line style.
		 * @param \php\lang\PHPNumber $end The end style of the PDF line style.
		 * @param \php\lang\PHPNumber $corner The corner style of the PDF line style.
		 */
		protected function __construct(\php\lang\PHPNumber $thick, \php\awt\Color $color = null, \php\lang\PHPNumber $end = null, \php\lang\PHPNumber $corner = null) {
			parent::__construct();
			$this->thick = $thick->getNumber();
			if ($color === null) {
				$color = \php\awt\Color::BLACK();
			}
			$this->color = $color;
			if ($end === null) {
				$end = self::END_NORMAL();
			}
			$this->end = $end->getNumber();
			if ($corner === null) {
				$corner = self::CORNER_NORMAL();
			}
			$this->corner = $corner->getNumber();
		}

		/**
		 * Returns the thickness of this PDF line style.
		 * @return \php\lang\PHPNumber
		 */
		public function getThick() {
			return \php\lang\PHPNumber::newInstance($this->thick);
		}

		/**
		 * Returns the color of this PDF line style.
		 * @return \php\awt\Color
		 */
		public function getColor() {
			return $this->color;
		}

		/**
		 * Returns the end style of this PDF line style.
		 * @return \php\lang\PHPNumber
		 */
		public function getEnd() {
			return \php\lang\PHPNumber::newInstance($this->end);
		}

		/**
		 * Returns the corner style of this PDF line style.
		 * @return \php\lang\PHPNumber
		 */
		public function getCorner() {
			return \php\lang\PHPNumber::newInstance($this->corner);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$thick = $this->getThick();
			if ($thick->isPositive()->getBoolean()) {
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getThick()->getNumber())));
				$return = $return->append(\php\lang\PHPString::newInstance(' w'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %d', $this->getEnd()->getNumber())));
				$return = $return->append(\php\lang\PHPString::newInstance(' J'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %d', $this->getCorner()->getNumber())));
				$return = $return->append(\php\lang\PHPString::newInstance(' j'));
				if (($color = $this->getColor()) !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getColor()->getRed()->getNumber() / 255)));
					$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getColor()->getGreen()->getNumber() / 255)));
					$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getColor()->getBlue()->getNumber() / 255)));
					$return = $return->append(\php\lang\PHPString::newInstance(' RG'));
				}
			}
			return $return;
		}

	}

}