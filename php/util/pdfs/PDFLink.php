<?php

/**
 * Description of \php\util\pdfs\PDFLink
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/net/URI.php');
	include_once('php/util/pdfs/PDFAnnot.php');

	/**
	 * The \php\util\pdfs\PDFImage class represents the PDF link structure.
	 */
	class PDFLink extends PDFAnnot {

		/**
		 * Returns a \php\util\pdfs\PDFLink object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF line.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF line.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF line.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF line.
		 * @param \php\net\URI $uri The URI will visit to of the PDF line.
		 * @return \php\util\pdfs\PDFLink The URI visit link.
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\net\URI $uri) {
			return new PDFLink($x1, $y1, $x2, $y2, $uri);
		}

		private $x1;
		private $y1;
		private $x2;
		private $y2;
		private $uri;

		/**
		 * Constructs a \php\util\pdfs\PDFLink object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF link.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF link.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF link.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF link.
		 * @param \php\net\URI $uri The URI will the link visit to.
		 */
		protected function __construct(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\net\URI $uri) {
			parent::__construct();
			$this->setAttribute(\php\lang\PHPString::newInstance('Type'), \php\lang\PHPString::newInstance('/Annot'));
			$this->setAttribute(\php\lang\PHPString::newInstance('Subtype'), \php\lang\PHPString::newInstance('/Link'));
			$this->x1 = $x1->getNumber();
			$this->y1 = $y1->negative()->getNumber();
			$this->x2 = $x2->getNumber();
			$this->y2 = $y2->negative()->getNumber();
			$this->uri = $uri;
		}

		/**
		 * Returns the first point of x of the PDF link.
		 * @return \php\lang\PHPNumber
		 */
		public function getX1() {
			return \php\lang\PHPNumber::newInstance($this->x1);
		}

		/**
		 * Returns the first point of y of the PDF link.
		 * @return \php\lang\PHPNumber
		 */
		public function getY1() {
			return \php\lang\PHPNumber::newInstance($this->y1);
		}

		/**
		 * Returns the second point of x of the PDF link.
		 * @return \php\lang\PHPNumber
		 */
		public function getX2() {
			return \php\lang\PHPNumber::newInstance($this->x2);
		}

		/**
		 * Returns the second point of y of the PDF link.
		 * @return \php\lang\PHPNumber
		 */
		public function getY2() {
			return \php\lang\PHPNumber::newInstance($this->y2);
		}

		/**
		 * Returns the URI of the PDF link.
		 * @return \php\net\URI
		 */
		public function getURI() {
			return $this->uri;
		}

		protected function toAttributeString() {
			$children = \php\lang\PHPString::newInstance('');
			$children = $children->append(\php\lang\PHPString::newInstance(sprintf('[ %.2f %.2f %.2f %.2f ]', $this->getX1()->getNumber(), $this->getY1()->getNumber(), $this->getX2()->getNumber(), $this->getY2()->getNumber())));
			$this->setAttribute(\php\lang\PHPString::newInstance('Rect'), $children);
			$children = \php\lang\PHPString::newInstance('');
			$children = $children->append(\php\lang\PHPString::newInstance("<<\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Type /Action\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/S /URI\n"));
			$children = $children->append(\php\lang\PHPString::newInstance(sprintf("/URI (%s)\n", $this->getURI()->toString()->getString())));
			$children = $children->append(\php\lang\PHPString::newInstance('>>'));
			$this->setAttribute(\php\lang\PHPString::newInstance('A'), $children);
			$return = \php\lang\PHPString::newInstance('');
			$keys = $this->attributes->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->attributes->get($key);
				$return = $return->append(\php\lang\PHPString::newInstance('/'));
				$return = $return->append($key);
				$return = $return->append(\php\lang\PHPString::newInstance(' '));
				$return = $return->append($value->toString());
				$return = $return->append(\php\lang\PHPString::newInstance("\n"));
			}
			return $return;
		}

	}

}