<?php

/**
 * Description of \php\util\pdfs\PDFObject
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/pdfs/PDFStream.php');

	/**
	 * The \php\util\pdfs\PDFObject class represents the PDF object structure.
	 */
	abstract class PDFObject extends \php\lang\PHPObject {

		private static $oid = 0;
		private $id;
		protected $attributes;

		/**
		 * Constructs a \php\util\pdfs\PDFFont object.
		 */
		protected function __construct() {
			parent::__construct();
			self::$oid++;
			$this->id = self::$oid;
			$this->attributes = \php\util\collections\Map::newInstance();
		}

		/**
		 * Sets attribute pair to this PDF object.
		 * @param \php\lang\PHPString $key The key of attribute.
		 * @param \php\lang\PHPObject $value The value of attribute.
		 */
		protected function setAttribute(\php\lang\PHPString $key = null, \php\lang\PHPObject $value = null) {
			$this->attributes->put($key, $value);
		}

		/**
		 * Returns all attributes in this PDF object.
		 * @return \php\util\collections\Map
		 */
		public function getAttributes() {
			return $this->attributes;
		}

		/**
		 * Return the ID of this PDF object.
		 * @return \php\lang\PHPNumber
		 */
		public function getId() {
			return \php\lang\PHPNumber::newInstance($this->id);
		}

		/**
		 * Represents the attributes of this PDF object.
		 * @return \php\lang\PHPString
		 */
		protected function toAttributeString() {
			$return = \php\lang\PHPString::newInstance('');
			$keys = $this->getAttributes()->keySet();
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($keys->size())->getBoolean(); $i = $i->increase()) {
				$key = $keys->get($i);
				$value = $this->getAttributes()->get($key);
				$return = $return->append(\php\lang\PHPString::newInstance('/'));
				$return = $return->append($key);
				$return = $return->append(\php\lang\PHPString::newInstance(' '));
				$return = $return->append($value->toString());
				$return = $return->append(\php\lang\PHPString::newInstance("\n"));
			}
			return $return;
		}

		protected function toStreamString() {
			return null;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance("\n\n"));
			$return = $return->append($this->getId()->toString());
			$return = $return->append(\php\lang\PHPString::newInstance(" 0 obj <<\n"));
			if (($string = $this->toAttributeString()) !== null) {
				$return = $return->append($string);
			}
			$return = $return->append(\php\lang\PHPString::newInstance(">>\n"));
			if (($string = $this->toStreamString()) !== null) {
				$return = $return->append($string);
			}
			$return = $return->append(\php\lang\PHPString::newInstance("endobj"));
			return $return;
		}

	}

}