<?php

/**
 * Description of \php\util\pdfs\PDFOval
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDF2PointShape.php');

	/**
	 * The \php\util\pdfs\PDFOval class represents the PDF oval structure.
	 */
	class PDFOval extends PDF2PointShape {

		/**
		 * Returns a \php\util\pdfs\PDFOval object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF oval.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF oval.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF oval.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF oval.
		 * @param \php\awt\Color $color The color of the PDF oval.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style of the PDF oval.
		 * @return \php\util\pdfs\PDFOval
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\awt\Color $color = null, PDFLineStyle $style = null) {
			return new PDFOval($x1, $y1, $x2, $y2, $color, $style);
		}

		/**
		 * Constructs a \php\util\pdfs\PDFOval object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF oval.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF oval.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF oval.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF oval.
		 * @param \php\awt\Color $color The color of the PDF oval.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style of the PDF oval.
		 */
		protected function __construct(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\awt\Color $color = null, PDFLineStyle $style = null) {
			parent::__construct($x1, $y1, $x2, $y2, $color, $style);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$color = $this->getColor();
			$style = $this->getStyle();
			if ($color !== null || $style !== null) {
				$x1 = $this->getX1()->getNumber();
				$y1 = $this->getY1()->getNumber();
				$x2 = $this->getX2()->getNumber();
				$y2 = $this->getY2()->getNumber();
				$w = abs($x2 - $x1);
				$h = abs($y2 - $y1);
				$x3 = ($x2 + $x1) / 2;
				$y3 = ($y2 + $y1) / 2;
				$return = $return->append(\php\lang\PHPString::newInstance('q'));
				$return = $return->append(parent::toString());
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y1)));
				$return = $return->append(\php\lang\PHPString::newInstance(' m'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3 + PDF2PointShape::calculateControlPoint($w / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y3 + PDF2PointShape::calculateControlPoint($h / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y3)));
				$return = $return->append(\php\lang\PHPString::newInstance(' c'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y3 - PDF2PointShape::calculateControlPoint($h / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3 + PDF2PointShape::calculateControlPoint($w / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y2)));
				$return = $return->append(\php\lang\PHPString::newInstance(' c'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3 - PDF2PointShape::calculateControlPoint($w / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y3 - PDF2PointShape::calculateControlPoint($h / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y3)));
				$return = $return->append(\php\lang\PHPString::newInstance(' c'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y3 + PDF2PointShape::calculateControlPoint($h / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3 - PDF2PointShape::calculateControlPoint($w / 2))));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x3)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y1)));
				$return = $return->append(\php\lang\PHPString::newInstance(' c'));
				if ($color !== null && $style !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(' b'));
				} else if ($color !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(' f'));
				} else if ($style !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(' s'));
				}
				$return = $return->append(\php\lang\PHPString::newInstance(' Q'));
			}
			return $return;
		}

	}

}