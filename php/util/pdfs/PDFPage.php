<?php

/**
 * Description of \php\util\pdfs\PDFPage
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/pdfs/PDFType.php');
	include_once('php/util/pdfs/PDFPage.php');
	include_once('php/util/pdfs/PDFContent.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/collections/Set.php');

	/**
	 * The \php\util\pdfs\PDFPage class represents the PDF page structure.
	 */
	class PDFPage extends PDFType {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\util\pdfs\PDFPage object.
		 * @param \php\lang\PHPNumber $width The width of PDF page.
		 * @param \php\lang\PHPNumber $height The height of PDF page.
		 * @return \php\util\pdfs\PDFPage
		 */
		public static function newInstanceBySize(\php\lang\PHPNumber $width, \php\lang\PHPNumber $height) {
			return new PDFPage($width, $height);
		}

		/**
		 * Returns a langscape A4 paper size PDF page.
		 * @return \php\util\pdfs\PDFPage
		 */
		public static final function A4_LANDSCOPE() {
			return PDFPage::newInstanceBySize(\php\lang\PHPNumber::newInstance(595), \php\lang\PHPNumber::newInstance(842));
		}

		/**
		 * Returns a portrait A4 paper size PDF page.
		 * @return \php\util\pdfs\PDFPage
		 */
		public static final function A4_PORTRAIT() {
			return PDFPage::newInstanceBySize(\php\lang\PHPNumber::newInstance(842), \php\lang\PHPNumber::newInstance(595));
		}

		private $contents;
		private $annotations;

		/**
		 * Constructs a \php\util\pdfs\PDFPage object.
		 * @param \php\lang\PHPNumber $width The width of PDF page.
		 * @param \php\lang\PHPNumber $height The height of PDF page.
		 */
		protected function __construct(\php\lang\PHPNumber $width, \php\lang\PHPNumber $height) {
			parent::__construct(\php\lang\PHPString::newInstance('/Page'));
			$this->contents = \php\util\collections\Set::newInstance();
			$this->annotations = \php\util\collections\Set::newInstance();
			$mediaBox = \php\lang\PHPString::newInstance('');
			$mediaBox = $mediaBox->append(\php\lang\PHPString::newInstance(sprintf('[ 0.00 0.00 %.2f %.2f ]', $width->getNumber(), $height->negative()->getNumber())));
			$this->setAttribute(\php\lang\PHPString::newInstance('MediaBox'), $mediaBox);
		}

		/**
		 * Adds a content to this PDF page.
		 * @param \php\util\pdfs\PDFContent $content
		 */
		public function addContent(PDFContent $content) {
			$this->contents->put($content);
		}

		/**
		 * Returns all contents from this PDF page.
		 * @return \php\util\collections\Set
		 */
		public function getContents() {
			return $this->contents;
		}

		/**
		 * Adds an annotation to this PDF page.
		 * @param \php\util\pdfs\PDFAnnot $annotation
		 */
		public function addAnnotation(PDFAnnot $annotation) {
			$this->annotations->put($annotation);
		}

		/**
		 * Returns all annotations from this PDF page.
		 * @return \php\util\collections\Set
		 */
		public function getAnnotations() {
			return $this->annotations;
		}

		/**
		 * Sets the page container of this PDF page.
		 * @param \php\util\pdfs\PDFPages $pdfPages The page container.
		 */
		public function setParent(PDFPages $pdfPages) {
			$this->setAttribute(\php\lang\PHPString::newInstance('Parent'), $pdfPages->getId()->toString()->append(\php\lang\PHPString::newInstance(' 0 R')));
		}

		private function add(\php\lang\PHPString $key, \php\util\collections\Set $data) {
			$children = \php\lang\PHPString::newInstance('');
			$size = $data->size();
			if ($size->isPositive()->getBoolean()) {
				$children = $children->append(\php\lang\PHPString::newInstance("["));
				for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($size)->getBoolean(); $i = $i->increase()) {
					$children = $children->append(\php\lang\PHPString::newInstance(" "));
					$children = $children->append($data->get($i)->getId()->toString());
					$children = $children->append(\php\lang\PHPString::newInstance(" 0 R"));
				}
				$children = $children->append(\php\lang\PHPString::newInstance(" ]"));
				$this->setAttribute($key, $children);
			}
		}

		protected function toAttributeString() {
			$this->add(\php\lang\PHPString::newInstance("Contents"), $this->getContents());
			$this->add(\php\lang\PHPString::newInstance("Annots"), $this->getAnnotations());
			return parent::toAttributeString();
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($this->getContents()->size())->getBoolean(); $i = $i->increase()) {
				$return = $return->append($this->getContents()->get($i)->toString());
			}
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($this->getAnnotations()->size())->getBoolean(); $i = $i->increase()) {
				$return = $return->append($this->getAnnotations()->get($i)->toString());
			}
			return $return;
		}

	}

}