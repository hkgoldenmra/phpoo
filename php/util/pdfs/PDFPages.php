<?php

/**
 * Description of \php\util\pdfs\PDFPages
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/collections/Set.php');
	include_once('php/util/pdfs/PDFType.php');

	/**
	 * The \php\util\pdfs\PDFPages class represents the PDF pages structure.
	 */
	class PDFPages extends PDFType {

		/**
		 * Returns a \php\util\pdfs\PDFPages object.
		 * @return \php\util\pdfs\PDFPages
		 */
		public static function newInstance() {
			return new PDFPages();
		}

		private $pdfPages;
		private $pdfFonts;

		/**
		 * Constructs a \php\util\pdfs\PDFPages object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('/Pages'));
			$this->pdfPages = \php\util\collections\Vector::newInstance();
			$this->pdfFonts = \php\util\collections\Set::newInstance();
		}

		/**
		 * Adds a PDF page to this PDF pages.
		 * @param \php\util\pdfs\PDFPage $pdfPage A PDF page.
		 */
		public function addPage(PDFPage $pdfPage) {
			$this->pdfPages->add($pdfPage);
			$pdfPage->setParent($this);
		}

		/**
		 * Adds a PDF font to this PDF pages.
		 * @param \php\util\pdfs\PDFFont $pdfFont A PDF font.
		 */
		public function addFont(PDFFont $pdfFont) {
			$this->pdfFonts->put($pdfFont);
		}

		private function addKids() {
			$children = \php\lang\PHPString::newInstance('');
			if ($this->pdfPages->size()->isPositive()->getBoolean()) {
				$children = $children->append(\php\lang\PHPString::newInstance("[ "));
				for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($this->pdfPages->size())->getBoolean(); $i = $i->increase()) {
					$children = $children->append($this->pdfPages->get($i)->getId()->toString());
					$children = $children->append(\php\lang\PHPString::newInstance(" 0 R "));
				}
				$children = $children->append(\php\lang\PHPString::newInstance("]"));
				$this->setAttribute(\php\lang\PHPString::newInstance('Count'), $this->pdfPages->size()->toString());
				$this->setAttribute(\php\lang\PHPString::newInstance('Kids'), $children);
			}
		}

		private function addResources() {
			$children = \php\lang\PHPString::newInstance('');
			$children = $children->append(\php\lang\PHPString::newInstance("<<\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/ProcSet [ /PDF /Text /ImageB /ImageC /ImageI ]\n"));
			$children = $children->append(\php\lang\PHPString::newInstance("/Font <<\n"));
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($this->pdfFonts->size())->getBoolean(); $i = $i->increase()) {
				$children = $children->append(\php\lang\PHPString::newInstance("/F"));
				$children = $children->append($this->pdfFonts->get($i)->getId()->toString());
				$children = $children->append(\php\lang\PHPString::newInstance(" "));
				$children = $children->append($this->pdfFonts->get($i)->getId()->toString());
				$children = $children->append(\php\lang\PHPString::newInstance(" 0 R\n"));
			}
			$children = $children->append(\php\lang\PHPString::newInstance(">>\n"));
			$children = $children->append(\php\lang\PHPString::newInstance(">>"));
			$this->setAttribute(\php\lang\PHPString::newInstance('Resources'), $children);
		}

		protected function toAttributeString() {
			$this->addKids();
			$this->addResources();
			return parent::toAttributeString();
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(parent::toString());
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($this->pdfFonts->size())->getBoolean(); $i = $i->increase()) {
				$return = $return->append($this->pdfFonts->get($i)->toString());
			}
			for ($i = \php\lang\PHPNumber::newInstance(0); $i->isLessThan($this->pdfPages->size())->getBoolean(); $i = $i->increase()) {
				$return = $return->append($this->pdfPages->get($i)->toString());
			}
			return $return;
		}

	}

}