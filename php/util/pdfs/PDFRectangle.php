<?php

/**
 * Description of \php\util\pdfs\PDFRectangle
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDF2PointShape.php');

	/**
	 * The \php\util\pdfs\PDFRectangle class represents the PDF oval structure.
	 */
	class PDFRectangle extends PDF2PointShape {

		/**
		 * Returns a \php\util\pdfs\PDFRectangle object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF rectangle.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF rectangle.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF rectangle.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF rectangle.
		 * @param \php\awt\Color $color The color of the PDF rectangle.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style of the PDF rectangle.
		 * @return \php\util\pdfs\PDFRectangle
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\awt\Color $color = null, PDFLineStyle $style = null) {
			return new PDFRectangle($x1, $y1, $x2, $y2, $color, $style);
		}

		/**
		 * Constructs a \php\util\pdfs\PDFRectangle object.
		 * @param \php\lang\PHPNumber $x1 The first point of x of the PDF rectangle.
		 * @param \php\lang\PHPNumber $y1 The first point of y of the PDF rectangle.
		 * @param \php\lang\PHPNumber $x2 The second point of x of the PDF rectangle.
		 * @param \php\lang\PHPNumber $y2 The second point of y of the PDF rectangle.
		 * @param \php\awt\Color $color The color of the PDF rectangle.
		 * @param \php\util\pdfs\PDFLineStyle $style The line style of the PDF rectangle.
		 */
		protected function __construct(\php\lang\PHPNumber $x1, \php\lang\PHPNumber $y1, \php\lang\PHPNumber $x2, \php\lang\PHPNumber $y2, \php\awt\Color $color = null, PDFLineStyle $style = null) {
			parent::__construct($x1, $y1, $x2, $y2, $color, $style);
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			$color = $this->getColor();
			$style = $this->getStyle();
			if ($color !== null || $style !== null) {
				$x1 = $this->getX1()->getNumber();
				$y1 = $this->getY1()->getNumber();
				$x2 = $this->getX2()->getNumber();
				$y2 = $this->getY2()->getNumber();
				$return = $return->append(\php\lang\PHPString::newInstance('q'));
				$return = $return->append(parent::toString());
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y1)));
				$return = $return->append(\php\lang\PHPString::newInstance(' m'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y1)));
				$return = $return->append(\php\lang\PHPString::newInstance(' l'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x2)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y2)));
				$return = $return->append(\php\lang\PHPString::newInstance(' l'));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $x1)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $y2)));
				$return = $return->append(\php\lang\PHPString::newInstance(' l'));
				if ($color !== null && $style !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(' b'));
				} else if ($color !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(' f'));
				} else if ($style !== null) {
					$return = $return->append(\php\lang\PHPString::newInstance(' s'));
				}
				$return = $return->append(\php\lang\PHPString::newInstance(' Q'));
			}
			return $return;
		}

	}

}