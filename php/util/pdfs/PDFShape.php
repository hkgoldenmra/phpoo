<?php

/**
 * Description of \php\util\pdfs\PDFShape
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDFStream.php');
	include_once('php/util/pdfs/PDFLineStyle.php');

	/**
	 * The \php\util\pdfs\PDFShape class represents the PDF shape structure.
	 */
	abstract class PDFShape extends PDFStream {

		private $color;
		private $style;

		/**
		 * Constructs a \php\util\pdfs\PDFShape object.
		 * @param \php\awt\Color $color The color of the shape.
		 * @param \php\util\pdfs\PDFLineStyle $style The style of the shape.
		 */
		protected function __construct(\php\awt\Color $color = null, PDFLineStyle $style = null) {
			parent::__construct();
			$this->color = $color;
			$this->style = $style;
		}

		/**
		 * Returns the color of the shape.
		 * @return \php\awt\Color
		 */
		public function getColor() {
			return $this->color;
		}

		/**
		 * Returns the style of the shape.
		 * @return \php\awt\Color
		 */
		public function getStyle() {
			return $this->style;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$return = \php\lang\PHPString::newInstance('');
			if (($style = $this->getStyle()) !== null) {
				$return = $return->append($style->toString());
			}
			if (($color = $this->getColor()) !== null) {
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $color->getRed()->getNumber() / 255)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $color->getGreen()->getNumber() / 255)));
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $color->getBlue()->getNumber() / 255)));
				$return = $return->append(\php\lang\PHPString::newInstance(' rg'));
			}
			return $return;
		}

	}

}