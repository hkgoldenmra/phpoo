<?php

/**
 * Description of \php\util\pdfs\PDFStream
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPObject.php');

	/**
	 * The \php\util\pdfs\PDFStream class represents the PDF stream structure.
	 */
	abstract class PDFStream extends \php\lang\PHPObject {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\util\pdfs\PDFStream object.
		 */
		protected function __construct() {
			parent::__construct();
		}

	}

}