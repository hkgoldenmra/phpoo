<?php

/**
 * Description of \php\util\pdfs\PDFText
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/awt/Color.php');
	include_once('php/util/pdfs/PDFStream.php');
	include_once('php/util/pdfs/PDFFont.php');
	include_once('php/util/pdfs/PDFCustomFont.php');

	/**
	 * The \php\util\pdfs\PDFText class represents the PDF text structure.
	 */
	class PDFText extends PDFStream {

		/**
		 * Returns a \php\util\pdfs\PDFText object.
		 * @param \php\lang\PHPString $text The text stream.
		 * @param \php\lang\PHPNumber $x The left coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the text from the first letter.
		 * @param \php\util\pdfs\PDFFont $pdfFont The PDF font of the text.
		 * @param \php\lang\PHPNumber $size The text size.
		 * @param \php\awt\Color $color The color of the text.
		 * @return \php\util\pdfs\PDFText
		 */
		public static function newInstanceByParameters(\php\lang\PHPString $text, \php\lang\PHPNumber $x, \php\lang\PHPNumber $y, PDFFont $pdfFont, \php\lang\PHPNumber $size, \php\awt\Color $color = null) {
			return new PDFText($text, $x, $y, $pdfFont, $size, $color);
		}

		private $text;
		private $x;
		private $y;
		private $pdfFont;
		private $size;
		private $color;

		/**
		 * Constructs a \php\util\pdfs\PDFText object.
		 * @param \php\lang\PHPString $text The text stream.
		 * @param \php\lang\PHPNumber $x The left coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the text from the first letter.
		 * @param \php\util\pdfs\PDFFont $pdfFont The PDF font of the text.
		 * @param \php\lang\PHPNumber $size The text size.
		 * @param \php\awt\Color $color The color of the text.
		 */
		protected function __construct(\php\lang\PHPString $text, \php\lang\PHPNumber $x, \php\lang\PHPNumber $y, PDFFont $pdfFont, \php\lang\PHPNumber $size, \php\awt\Color $color = null) {
			parent::__construct();
			if ($color === null) {
				$color = \php\awt\Color::BLACK();
			}
			$this->text = $text->getString();
			$this->setX($x);
			$this->setY($y);
			$this->pdfFont = $pdfFont;
			$this->size = $size->getNumber();
			$this->color = $color;
		}

		/**
		 * Sets the left coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $x The left coordination of the text from the first letter.
		 */
		protected function setX(\php\lang\PHPNumber $x) {
			$this->x = $x->getNumber();
		}

		/**
		 * Sets the bottom coordination of the text from the first letter.
		 * @param \php\lang\PHPNumber $y The bottom coordination of the text from the first letter.
		 */
		protected function setY(\php\lang\PHPNumber $y) {
			$this->y = $y->negative()->getNumber();
		}

		/**
		 * Returns the text stream.
		 * @return \php\lang\PHPString
		 */
		public function getText() {
			return \php\lang\PHPString::newInstance($this->text);
		}

		/**
		 * Returns the left coordination of the text from the first letter.
		 * @return \php\lang\PHPNumber
		 */
		public function getX() {
			return \php\lang\PHPNumber::newInstance($this->x);
		}

		/**
		 * Returns the bottom coordination of the text from the first letter.
		 * @return \php\lang\PHPNumber
		 */
		public function getY() {
			return \php\lang\PHPNumber::newInstance($this->y);
		}

		/**
		 * Return the PDF font of the text.
		 * @return \php\util\pdfs\PDFFont
		 */
		public function getPDFFont() {
			return $this->pdfFont;
		}

		/**
		 * Returns the text size.
		 * @return  \php\lang\PHPNumber
		 */
		public function getSize() {
			return \php\lang\PHPNumber::newInstance($this->size);
		}

		/**
		 * Returns the color of the text.
		 * @return \php\awt\Color
		 */
		public function getColor() {
			return $this->color;
		}

		/**
		 * Represents this object.
		 * @return \php\lang\PHPString
		 */
		public function toString() {
			$size = $this->getSize();
			$return = \php\lang\PHPString::newInstance('');
			$return = $return->append(\php\lang\PHPString::newInstance('BT'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getColor()->getRed()->getNumber() / 255)));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getColor()->getGreen()->getNumber() / 255)));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getColor()->getBlue()->getNumber() / 255)));
			$return = $return->append(\php\lang\PHPString::newInstance(' rg'));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getX()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getY()->subtraction(PDFFont::getTextHeight($size))->addition($size->dividedBy(\php\lang\PHPNumber::newInstance(4)))->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' TD'));
			$return = $return->append(\php\lang\PHPString::newInstance(' /F' . $this->getPDFFont()->getId()->getNumber()));
			$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %.2f', $this->getSize()->getNumber())));
			$return = $return->append(\php\lang\PHPString::newInstance(' Tf'));
			$return = $return->append(\php\lang\PHPString::newInstance(' <'));
			$text = $this->getText();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($text->length())->getBoolean(); $i = $i->increase()) {
				$return = $return->append(\php\lang\PHPString::newInstance(sprintf(' %04x', $text->charAt($i)->toUnicode()->getNumber())));
			}
			$return = $return->append(\php\lang\PHPString::newInstance(' > Tj ET %%% '));
			$return = $return->append($text);
			return $return;
		}

	}

}