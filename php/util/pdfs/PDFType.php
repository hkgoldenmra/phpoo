<?php

/**
 * Description of \php\util\pdfs\PDFType
 */

namespace php\util\pdfs {
	include_once('php/lang/PHPString.php');
	include_once('php/util/pdfs/PDFObject.php');

	/**
	 * The \php\util\pdfs\PDFType class represents the PDF type structure.
	 */
	abstract class PDFType extends PDFObject {

		/**
		 * Constructs a \php\util\pdfs\PDFType object.
		 */
		protected function __construct(\php\lang\PHPString $type) {
			parent::__construct();
			$this->setAttribute(\php\lang\PHPString::newInstance('Type'), \php\lang\PHPString::newInstance($type));
		}

		protected function toStreamString() {
			return null;
		}

	}

}