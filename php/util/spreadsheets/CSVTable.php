<?php

/**
 * Description of \php\util\spreadsheets\CSVTable
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPString.php');
	include_once('php/util/spreadsheets/DelimitedTable.php');

	/**
	 * The \php\util\spreadsheets\CSVTable class represents the csv table.
	 */
	class CSVTable extends DelimitedTable {

		/**
		 * Returns a \php\util\spreadsheets\CSVTable object.
		 */
		public static function newInstance() {
			return new CSVTable();
		}

		/**
		 * Constructs a \php\util\spreadsheets\CSVTable object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance(','), \php\lang\PHPString::newInstance('text/csv'), \php\lang\PHPString::newInstance('csv'));
		}

	}

}