<?php

/**
 * Description of \php\util\spreadsheets\DelimitedTable
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/spreadsheets/Table.php');

	/**
	 * The \php\util\spreadsheets\DelimitedTable class represents the delimited table.
	 */
	abstract class DelimitedTable extends Table {

		protected $delimiter;
		protected $mimeType;
		protected $extension;

		/**
		 * Constructs a \php\util\spreadsheets\DelimitedTable object.
		 * @param \php\lang\PHPString $delimiter The delimiter between data field of this delimited table.
		 * @param \php\lang\PHPString $mimeType The MIME type of this delimited table.
		 * @param \php\lang\PHPString $extension The file extension of this delimited table.
		 */
		protected function __construct(\php\lang\PHPString $delimiter, \php\lang\PHPString $mimeType, \php\lang\PHPString $extension) {
			parent::__construct();
			$this->delimiter = $delimiter->getString();
			$this->mimeType = $mimeType->getString();
			$this->extension = $extension->getString();
		}

		/**
		 * Exports the data from this delimited table.
		 * @param \php\lang\PHPString $filename The default export file name. Default &lt;current system date time&gt;.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			// **************************************************
			// export
			// **************************************************
			$extension = \php\lang\PHPString::newInstance($this->extension);
			if ($filename === null) {
				$datetime = \php\util\Calendar::newInstanceByTimestamp();
				$filename = $datetime->toString();
			}
			if ($filename->endsWith($extension)->not()->getBoolean()) {
				$filename = $filename->append(\php\lang\PHPString::newInstance('.'));
				$filename = $filename->append($extension);
			}
			\php\util\Exporter::export(\php\lang\PHPString::newInstance($this->mimeType), $filename);
			// **************************************************
			$tableRows = $this->getTableRows();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($tableRows->size())->getBoolean(); $i = $i->increase()) {
				if ($i->isPositive()->getBoolean()) {
					echo "\n";
				}
				if (($tableRow = $tableRows->get($i)) !== null) {
					$tableCells = $tableRow->getTableCells();
					for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($tableCells->size())->getBoolean(); $j = $j->increase()) {
						if ($j->isPositive()->getBoolean()) {
							echo $this->delimiter;
						}
						if (($tableCell = $tableCells->get($j)) !== null && ($data = $tableCell->getData()) !== null) {
							echo sprintf('"%s"', $data->toString()->getString());
						} else {
							echo '';
						}
					}
				} else {
					echo '';
				}
			}
		}

	}

}