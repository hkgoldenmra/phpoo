<?php

/**
 * Description of \php\util\spreadsheets\HTMLSpreadsheet
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/spreadsheets/Table.php');
	include_once('php/util/spreadsheets/Spreadsheet.php');

	/**
	 * The \php\util\spreadsheets\HTMLSpreadsheet class represents the HTML spreadsheet.
	 */
	class HTMLSpreadsheet extends Spreadsheet {

		private static $papers = array(
			0 => array('width' => '842px', 'height' => '1191px'), // A3
			1 => array('width' => '595px', 'height' => '842px'), // A4
			2 => array('width' => '420px', 'height' => '595px'), // A5
		);
		private static $borderTypes = array(
			1 => 'solid',
			2 => 'dashed',
			3 => 'dotted',
		);
		private static $horizontalAligns = array(
			1 => 'left',
			2 => 'center',
			3 => 'right',
		);
		private static $verticalAligns = array(
			1 => 'top',
			2 => 'middle',
			3 => 'bottom',
		);

		/**
		 * Returns a \php\util\spreadsheets\HTMLSpreadsheet object.
		 */
		public static function newInstance() {
			return new HTMLSpreadsheet();
		}

		/**
		 * Contructs a \php\util\spreadsheets\HTMLSpreadsheet object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('text/html'), \php\lang\PHPString::newInstance('html'));
		}

		/**
		 * Exports the data from this HTML spreadsheet.
		 * @param \php\lang\PHPString $filename The default export file name. Default &lt;current system date time&gt;.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			$extension = \php\lang\PHPString::newInstance($this->extension);
			if ($filename === null) {
				$datetime = \php\util\Calendar::newInstanceByTimestamp();
				$filename = $datetime->toString();
			}
			if ($filename->endsWith($extension)->not()->getBoolean()) {
				$filename = $filename->append(\php\lang\PHPString::newInstance('.'));
				$filename = $filename->append($extension);
			}
			\php\util\Exporter::export(\php\lang\PHPString::newInstance($this->mimeType), $filename);
			// **************************************************
			// initial sets to avoid duplicated entries
			// **************************************************
			$tableStyleSet = \php\util\collections\Set::newInstance();
			$tableColumnStyleSet = \php\util\collections\Set::newInstance();
			$tableRowStyleSet = \php\util\collections\Set::newInstance();
			$tableCellStyleSet = \php\util\collections\Set::newInstance();
			// **************************************************
			// initial DOM document
			// **************************************************
			$htmlDocument = new \DOMDocument('1.0', 'UTF-8');
			$htmlDocument->preserveWhiteSpace = false;
			$htmlDocument->formatOutput = true;
			$htmlImplementation = new \DOMImplementation();
			$htmlDocument->appendChild($htmlImplementation->createDocumentType('html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"'));
			$htmlHtml = $htmlDocument->createElement('html');
			$htmlHtml->setAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
			$htmlHead = $htmlDocument->createElement('head');
			$htmlMeta = $htmlDocument->createElement('meta');
			$htmlMeta->setAttribute('http-equiv', 'Content-Type');
			$htmlMeta->setAttribute('content', 'text/html; charset=UTF-8');
			$htmlHead->appendChild($htmlMeta);
			$htmlTitle = $htmlDocument->createElement('title', $filename->getString());
			$htmlHead->appendChild($htmlTitle);
			// **************************************************
			// initial CSS
			// **************************************************
			$htmlStyle = $htmlDocument->createElement('style', "html, body { margin: 0px; padding: 0px; }");
			$htmlStyle->setAttribute('type', 'text/css');
			$htmlHead->appendChild($htmlStyle);
			$htmlStyle = $htmlDocument->createElement('style', "hr { width: 99%; height: 1px; color: #000000; }");
			$htmlStyle->setAttribute('type', 'text/css');
			$htmlHead->appendChild($htmlStyle);
			$htmlStyle = $htmlDocument->createElement('style', "table { border-collapse: collapse; }");
			$htmlStyle->setAttribute('type', 'text/css');
			$htmlHead->appendChild($htmlStyle);
			$htmlStyle = $htmlDocument->createElement('style', "caption { border: 1px solid #000000; }");
			$htmlStyle->setAttribute('type', 'text/css');
			$htmlHead->appendChild($htmlStyle);
			$htmlHtml->appendChild($htmlHead);
			$htmlBody = $htmlDocument->createElement('body');
			// **************************************************
			$names = $this->tables->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($names->size())->getBoolean(); $i = $i->increase()) {
				$name = $names->get($i);
				$table = $this->tables->get($name);
				$rowCount = $table->rowCount();
				$columnCount = $table->columnCount();
				// **************************************************
				// assume the table cells all are available
				// **************************************************
				$availableCells = Table::newInstance();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
						$availableCells->setData($r, $c, \php\lang\PHPBoolean::newInstance(true));
					}
				}
				// **************************************************
				$htmlDiv = $htmlDocument->createElement('div');
				$htmlDivContainer = $htmlDocument->createElement('div');
				$htmlDivContainer->setAttribute('style', 'border: 1px solid #000000; display: table-cell;');
				$htmlDivPaper = $htmlDocument->createElement('div');
				$htmlTable = $htmlDocument->createElement('table');
				// **************************************************
				// store the style which is not exist in the set
				// **************************************************
				if (($style = $table->getTableStyle()) !== null) {
					if ($tableStyleSet->contains($style)->not()->getBoolean()) {
						$tableStyleSet->put($style);
						$css = sprintf(".ct-%s {", $style->getHashCode()->getString());
						$paper = $style->getPaper();
						if (($isLandscape = $style->isLandscape()) !== null) {
							if ($isLandscape->getBoolean()) {
								if ($paper !== null) {
									$css .= sprintf(' width: %s; height: %s;', self::$papers[$paper->getNumber()]['height'], self::$papers[$paper->getNumber()]['width']);
								}
							} else {
								if ($paper !== null) {
									$css .= sprintf(' width: %s; height: %s;', self::$papers[$paper->getNumber()]['width'], self::$papers[$paper->getNumber()]['height']);
								}
							}
						}
						if (($alignment = $style->getVerticalCentered()) !== null && $alignment->getBoolean()) {
							$css .= " vertical-align: middle;";
						}
						$css .= " }";
						$htmlStyle = $htmlDocument->createElement('style', $css);
						$htmlStyle->setAttribute('type', 'text/css');
						$htmlHead->appendChild($htmlStyle);
						$css = sprintf(".pa-%s {", $style->getHashCode()->getString());
						$margins = array(
							'top' => $style->getTopMargin(),
							'left' => $style->getLeftMargin(),
							'right' => $style->getRightMargin(),
							'bottom' => $style->getBottomMargin(),
						);
						foreach ($margins as $k => $v) {
							if ($v !== null && $v->getSize() !== null) {
								$css .= sprintf(' margin-%s: %s;', $k, $v->toString()->getString());
							}
						}
						$css .= " }";
						$htmlStyle = $htmlDocument->createElement('style', $css);
						$htmlStyle->setAttribute('type', 'text/css');
						$htmlHead->appendChild($htmlStyle);
						$css = sprintf(".ta-%s {", $style->getHashCode()->getString());
						if (($alignment = $style->getHorizontalCentered()) !== null && $alignment->getBoolean()) {
							$css .= " margin: auto;";
						}
						$css .= " }";
						$htmlStyle = $htmlDocument->createElement('style', $css);
						$htmlStyle->setAttribute('type', 'text/css');
						$htmlHead->appendChild($htmlStyle);
					}
					$htmlDivContainer->setAttribute('class', 'ct-' . $style->getHashCode()->getString());
					$htmlDivPaper->setAttribute('class', 'pa-' . $style->getHashCode()->getString());
					$htmlTable->setAttribute('class', 'ta-' . $style->getHashCode()->getString());
				}
				$htmlColgroup = $htmlDocument->createElement('colgroup');
				$tableRowColumnStyles = $table->getTableRowColumnStyles();
				for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
					$htmlCol = $htmlDocument->createElement('col');
					if ($c->isLessThan($tableRowColumnStyles->size())->getBoolean() && ($style = $tableRowColumnStyles->get($c)) !== null) {
						// **************************************************
						// store the style which is not exist in the set
						// **************************************************
						if ($tableColumnStyleSet->contains($style)->not()->getBoolean()) {
							$tableColumnStyleSet->put($style);
							$css = sprintf(".co-%s {", $style->getHashCode()->getString());
							if (($size = $style->getSize()) !== null) {
								$css .= sprintf(' width: %s;', $style->toString()->getString());
							}
							$css .= " }";
							$htmlStyle = $htmlDocument->createElement('style', $css);
							$htmlStyle->setAttribute('type', 'text/css');
							$htmlHead->appendChild($htmlStyle);
						}
						// **************************************************
						// apply the style
						// **************************************************
						$htmlCol->setAttribute('class', 'co-' . $style->getHashCode()->getString());
					}
					$htmlColgroup->appendChild($htmlCol);
				}
				$htmlTable->appendChild($htmlColgroup);
				$htmlTbody = $htmlDocument->createElement('tbody');
				$tableRows = $table->getTableRows();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					$htmlTr = $htmlDocument->createElement('tr');
					if ($r->isLessThan($tableRows->size())->getBoolean() && ($tableRow = $tableRows->get($r)) !== null) {
						if (($style = $tableRow->getTableRowColumnStyle()) !== null) {
							// **************************************************
							// store the style which is not exist in the set
							// **************************************************
							if ($tableRowStyleSet->contains($style)->not()->getBoolean()) {
								$tableRowStyleSet->put($style);
								$css = sprintf(".ro-%s {", $style->getHashCode()->getString());
								if (($size = $style->getSize()) !== null) {
									$css .= sprintf(' height: %s;', $style->toString()->getString());
								}
								$css .= " }";
								$htmlStyle = $htmlDocument->createElement('style', $css);
								$htmlStyle->setAttribute('type', 'text/css');
								$htmlHead->appendChild($htmlStyle);
							}
							// **************************************************
							// apply the style
							// **************************************************
							$htmlTr->setAttribute('class', 'ro-' . $style->getHashCode()->getString());
						}
						$tableCells = $tableRow->getTableCells();
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$htmlTd = $htmlDocument->createElement('td');
							$htmlTd->nodeValue = '';
							if ($availableCells->getData($r, $c)->getBoolean()) {
								if ($c->isLessThan($tableCells->size())->getBoolean() && ($tableCell = $tableCells->get($c)) !== null) {
									if (($style = $tableCell->getTableCellStyle()) !== null) {
										// **************************************************
										// store the style which is not exist in the set
										// **************************************************
										if ($tableCellStyleSet->contains($style)->not()->getBoolean()) {
											$tableCellStyleSet->put($style);
											$css = sprintf(".ce-%s {", $style->getHashCode()->getString());
											if (($font = $style->getFont()) !== null) {
												if (($size = $font->getSize()) !== null) {
													$css .= sprintf(' font-size: %s;', $size->toString()->getString());
												}
												if (($name = $font->getName()) !== null) {
													$css .= sprintf(" font-family: '%s';", $name->getString());
												}
												if (($type = $font->getType()) !== null) {
													if ($type->bitwiseAnd(\php\awt\Font::BOLD())->isPositive()->getBoolean()) {
														$css .= ' font-weight: bold;';
													}
													if ($type->bitwiseAnd(\php\awt\Font::ITALIC())->isPositive()->getBoolean()) {
														$css .= ' font-style: italic;';
													}
												}
											}
											if (($color = $style->getTextColor()) !== null) {
												$css .= sprintf(' color: #%06x;', $color->getRGB()->getNumber());
											}
											if (($color = $style->getBackgroundColor()) !== null) {
												$css .= sprintf(' background-color: #%06x;', $color->getRGB()->getNumber());
											}
											if (($align = $style->getHorizontalAlign()) !== null && array_key_exists($align->getNumber(), self::$horizontalAligns)) {
												$css .= sprintf(' text-align: %s;', self::$horizontalAligns[$align->getNumber()]);
											}
											if (($align = $style->getVerticalAlign()) !== null && array_key_exists($align->getNumber(), self::$verticalAligns)) {
												$css .= sprintf(' vertical-align: %s;', self::$verticalAligns[$align->getNumber()]);
											}
											$borders = array(
												'top' => $style->getTopBorder(),
												'left' => $style->getLeftBorder(),
												'right' => $style->getRightBorder(),
												'bottom' => $style->getBottomBorder(),
											);
											foreach ($borders as $k => $v) {
												if ($v !== null) {
													if (($size = $v->getSize()) !== null) {
														$css .= sprintf(' border-%s-width: %s;', $k, $size->toString()->getString());
													}
													if (($type = $v->getType()) !== null && array_key_exists($type->getNumber(), self::$borderTypes)) {
														$css .= sprintf(' border-%s-style: %s;', $k, self::$borderTypes[$type->getNumber()]);
													}
													if (($color = $v->getColor()) !== null) {
														$css .= sprintf(' border-%s-color: #%06x;', $k, $color->getRGB()->getNumber());
													}
												}
											}
											$css .= " }";
											$htmlStyle = $htmlDocument->createElement('style', $css);
											$htmlStyle->setAttribute('type', 'text/css');
											$htmlHead->appendChild($htmlStyle);
										}
										// **************************************************
										// apply the style
										// **************************************************
										$htmlTd->setAttribute('class', 'ce-' . $style->getHashCode()->getString());
									}
									$bottom = $r->increase();
									$right = $c->increase();
									if (($span = $tableCell->getRowspan()) !== null) {
										$htmlTd->setAttribute('rowspan', $span->getNumber());
										$bottom = $r->addition($span);
									}
									if (($span = $tableCell->getColspan()) !== null) {
										$htmlTd->setAttribute('colspan', $span->getNumber());
										$right = $c->addition($span);
									}
									for ($y = $r; $y->isLessThan($bottom)->getBoolean(); $y = $y->increase()) {
										for ($x = $c; $x->isLessThan($right)->getBoolean(); $x = $x->increase()) {
											$availableCells->setData($y, $x, \php\lang\PHPBoolean::newInstance(false));
										}
									}
									// **************************************************
									if (($data = $tableCell->getData()) !== null) {
										$htmlTd->textContent = $data->getString();
									}
									// **************************************************
								}
								$htmlTr->appendChild($htmlTd);
							}
						}
					} else {
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$htmlTr->appendChild($htmlDocument->createElement('td'));
						}
					}
					$htmlTbody->appendChild($htmlTr);
				}
				$htmlTable->appendChild($htmlTbody);
				$htmlDivPaper->appendChild($htmlTable);
				$htmlDivContainer->appendChild($htmlDivPaper);
				$htmlDiv->appendChild($htmlDivContainer);
				$htmlBody->appendChild($htmlDiv);
			}
			$htmlHtml->appendChild($htmlBody);
			$htmlDocument->appendChild($htmlHtml);
			// **************************************************
			// export
			// **************************************************
			echo trim($htmlDocument->saveXML());
		}

	}

}