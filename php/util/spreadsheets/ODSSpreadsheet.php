<?php

/**
 * Description of \php\util\spreadsheets\ODSSpreadsheet
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/Zip.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/spreadsheets/Table.php');
	include_once('php/util/spreadsheets/Spreadsheet.php');

	/**
	 * The \php\util\spreadsheets\ODSSpreadsheet class represents the ODS spreadsheet.
	 */
	class ODSSpreadsheet extends Spreadsheet {

		private static $papers = array(
			0 => array('width' => '297mm', 'height' => '420mm'), // A3
			1 => array('width' => '210mm', 'height' => '297mm'), // A4
			2 => array('width' => '148mm', 'height' => '210mm'), // A5
		);
		private static $contentFile = 'content.xml';
		private static $settingsFile = 'settings.xml';
		private static $stylesFile = 'styles.xml';
		private static $borderTypes = array(
			1 => 'solid',
			2 => 'dashed',
			3 => 'dotted',
		);
		private static $horizontalAligns = array(
			1 => 'left',
			2 => 'center',
			3 => 'right',
		);
		private static $verticalAligns = array(
			1 => 'top',
			2 => 'middle',
			3 => 'bottom',
		);

		/**
		 * Returns a \php\util\spreadsheets\ODSSpreadsheet object.
		 */
		public static function newInstance() {
			return new ODSSpreadsheet();
		}

		/**
		 * Contructs a \php\util\spreadsheets\ODSSpreadsheet object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('application/vnd.oasis.opendocument.spreadsheet'), \php\lang\PHPString::newInstance('ods'));
		}

		/**
		 * Exports the data from this ODS spreadsheet.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			// **************************************************
			// initial sets to avoid duplicated entries
			// **************************************************
			$tableStyleSet = \php\util\collections\Set::newInstance();
			$tableColumnStyleSet = \php\util\collections\Set::newInstance();
			$tableRowStyleSet = \php\util\collections\Set::newInstance();
			$tableCellStyleSet = \php\util\collections\Set::newInstance();
			$fontSet = \php\util\collections\Set::newInstance();
			$zip = \php\util\Zip::newInstance();
			// **************************************************
			// initial DOM document for META-INF/manifest.xml
			// **************************************************
			$manifestDocument = new \DOMDocument('1.0', 'UTF-8');
			$manifestDocument->preserveWhiteSpace = false;
			$manifestDocument->formatOutput = true;
			$manifestManifest = $manifestDocument->createElement('manifest:manifest');
			$manifestManifest->setAttribute('xmlns:manifest', 'urn:oasis:names:tc:opendocument:xmlns:manifest:1.0');
			$manifestManifest->setAttribute('manifest:version', '1.2');
			$fileEntries = array(
				self::$contentFile,
				self::$settingsFile,
				self::$stylesFile,
			);
			$manifestFileEntry = $manifestDocument->createElement('manifest:file-entry');
			$manifestFileEntry->setAttribute('manifest:full-path', '/');
			$manifestFileEntry->setAttribute('manifest:media-type', 'application/vnd.oasis.opendocument.spreadsheet');
			$manifestManifest->appendChild($manifestFileEntry);
			foreach ($fileEntries as $fileEntry) {
				$manifestFileEntry = $manifestDocument->createElement('manifest:file-entry');
				$manifestFileEntry->setAttribute('manifest:full-path', $fileEntry);
				$manifestFileEntry->setAttribute('manifest:media-type', 'text/xml');
				$manifestManifest->appendChild($manifestFileEntry);
			}
			$manifestDocument->appendChild($manifestManifest);
			// **************************************************
			// initial DOM document for content.xml
			// **************************************************
			$stylesDocument = new \DOMDocument('1.0', 'UTF-8');
			$stylesDocument->preserveWhiteSpace = false;
			$stylesDocument->formatOutput = true;
			$stylesDocumentStyles = $stylesDocument->createElement('office:document-styles');
			$stylesDocumentStyles->setAttribute('xmlns:office', 'urn:oasis:names:tc:opendocument:xmlns:office:1.0');
			$stylesDocumentStyles->setAttribute('xmlns:style', 'urn:oasis:names:tc:opendocument:xmlns:style:1.0');
			$stylesDocumentStyles->setAttribute('office:version', '1.2');
			$stylesAutomaticStyles = $stylesDocument->createElement('office:automatic-styles');
			$stylesAutomaticStyles->setAttribute('xmlns:fo', 'urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0');
			$stylesMasterStyles = $stylesDocument->createElement('office:master-styles');
			// **************************************************
			// initial DOM document for settings.xml
			// **************************************************
			$settingsDocument = new \DOMDocument('1.0', 'UTF-8');
			$settingsDocument->preserveWhiteSpace = false;
			$settingsDocument->formatOutput = true;
			$settingsOfficeDocumentSettings = $settingsDocument->createElement('office:document-settings');
			$settingsOfficeDocumentSettings->setAttribute('xmlns:office', 'urn:oasis:names:tc:opendocument:xmlns:office:1.0');
			$settingsOfficeDocumentSettings->setAttribute('office:version', '1.2');
			$settingsOfficeSettings = $settingsDocument->createElement('office:settings');
			$settingsOfficeSettings->setAttribute('xmlns:config', 'urn:oasis:names:tc:opendocument:xmlns:config:1.0');
			$settingsOfficeSettings->setAttribute('xmlns:ooo', 'http://openoffice.org/2004/office');
			$settingsConfigItemSet = $settingsDocument->createElement('config:config-item-set');
			$settingsConfigItemSet->setAttribute('config:name', 'ooo:view-settings');
			$settingsConfigItemMapIndexed = $settingsDocument->createElement('config:config-item-map-indexed');
			$settingsConfigItemMapIndexed->setAttribute('config:name', 'Views');
			$settingsConfigItemMapEntry = $settingsDocument->createElement('config:config-item-map-entry');
			$settingsConfigItemMapNamed = $settingsDocument->createElement('config:config-item-map-named');
			$settingsConfigItemMapNamed->setAttribute('config:name', 'Tables');
			// **************************************************
			// initial DOM document for styles.xml
			// **************************************************
			$contentDocument = new \DOMDocument('1.0', 'UTF-8');
			$contentDocument->preserveWhiteSpace = false;
			$contentDocument->formatOutput = true;
			$contentDocumentContent = $contentDocument->createElement('office:document-content');
			$contentDocumentContent->setAttribute('xmlns:office', 'urn:oasis:names:tc:opendocument:xmlns:office:1.0');
			$contentDocumentContent->setAttribute('xmlns:style', 'urn:oasis:names:tc:opendocument:xmlns:style:1.0');
			$contentDocumentContent->setAttribute('office:version', '1.2');
			$contentFontFaceDecls = $contentDocument->createElement('office:font-face-decls');
			$contentFontFaceDecls->setAttribute('xmlns:svg', 'urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0');
			$contentAutomaticStyles = $contentDocument->createElement('office:automatic-styles');
			$contentAutomaticStyles->setAttribute('xmlns:fo', 'urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0');
			$contentBody = $contentDocument->createElement('office:body');
			$contentSpreadsheet = $contentDocument->createElement('office:spreadsheet');
			$contentSpreadsheet->setAttribute('xmlns:table', 'urn:oasis:names:tc:opendocument:xmlns:table:1.0');
			$contentSpreadsheet->setAttribute('xmlns:text', 'urn:oasis:names:tc:opendocument:xmlns:text:1.0');
			// **************************************************
			$names = $this->tables->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($names->size())->getBoolean(); $i = $i->increase()) {
				$name = $names->get($i);
				$table = $this->tables->get($name);
				$rowCount = $table->rowCount();
				$columnCount = $table->columnCount();
				// **************************************************
				// assume the table cells all are available
				// **************************************************
				$availableCells = Table::newInstance();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
						$availableCells->setData($r, $c, \php\lang\PHPBoolean::newInstance(true));
					}
				}
				$contentTable = $contentDocument->createElement('table:table');
				if (($style = $table->getTableStyle()) !== null) {
					if ($tableStyleSet->contains($style)->not()->getBoolean()) {
						// **************************************************
						// store the style which is not exist in the set
						// **************************************************
						$tableStyleSet->put($style);
						$contentStyleStyle = $contentDocument->createElement('style:style');
						$contentStyleStyle->setAttribute('style:name', 'ta-' . $style->getHashCode()->getString());
						$contentStyleStyle->setAttribute('style:family', 'table');
						$contentStyleStyle->setAttribute('style:master-page-name', 'mp-' . $style->getHashCode()->getString());
						$contentAutomaticStyles->appendChild($contentStyleStyle);
						// **************************************************
						$stylesStylePageLayout = $stylesDocument->createElement('style:page-layout');
						$stylesStylePageLayout->setAttribute('style:name', 'pl-' . $style->getHashCode()->getString());
						$stylesStylePageLayoutProperties = $stylesDocument->createElement('style:page-layout-properties');
						$paper = $style->getPaper();
						if (($isLandscape = $style->isLandscape()) !== null) {
							if ($isLandscape->getBoolean()) {
								if ($paper !== null) {
									$stylesStylePageLayoutProperties->setAttribute('fo:page-width', self::$papers[$paper->getNumber()]['height']);
									$stylesStylePageLayoutProperties->setAttribute('fo:page-height', self::$papers[$paper->getNumber()]['width']);
								}
								$stylesStylePageLayoutProperties->setAttribute('style:print-orientation', 'landscape');
							} else {
								if ($paper !== null) {
									$stylesStylePageLayoutProperties->setAttribute('fo:page-width', self::$papers[$paper->getNumber()]['width']);
									$stylesStylePageLayoutProperties->setAttribute('fo:page-height', self::$papers[$paper->getNumber()]['height']);
								}
								$stylesStylePageLayoutProperties->setAttribute('style:print-orientation', 'portrait');
							}
						}
						$margins = array(
							'top' => $style->getTopMargin(),
							'left' => $style->getLeftMargin(),
							'right' => $style->getRightMargin(),
							'bottom' => $style->getBottomMargin()
						);
						foreach ($margins as $k => $v) {
							if ($v !== null) {
								$stylesStylePageLayoutProperties->setAttribute('fo:margin-' . $k, $v->toString()->getString());
							}
						}
						$horizontalCentered = $style->getHorizontalCentered();
						$horizontalCentered = $horizontalCentered !== null && $horizontalCentered->getBoolean();
						$verticalCentered = $style->getVerticalCentered();
						$verticalCentered = $verticalCentered !== null && $verticalCentered->getBoolean();
						if ($horizontalCentered && $verticalCentered) {
							$stylesStylePageLayoutProperties->setAttribute('style:table-centering', 'both');
						} else if ($horizontalCentered) {
							$stylesStylePageLayoutProperties->setAttribute('style:table-centering', 'horizontal');
						} else if ($verticalCentered) {
							$stylesStylePageLayoutProperties->setAttribute('style:table-centering', 'vertical');
						}
						if ($stylesStylePageLayoutProperties->hasAttributes()) {
							$stylesStylePageLayout->appendChild($stylesStylePageLayoutProperties);
						}
						if ($stylesStylePageLayout->hasChildNodes()) {
							$stylesAutomaticStyles->appendChild($stylesStylePageLayout);
						}
						$stylesStyleMasterPage = $stylesDocument->createElement('style:master-page');
						$stylesStyleMasterPage->setAttribute('style:name', 'mp-' . $style->getHashCode()->getString());
						$stylesStyleMasterPage->setAttribute('style:page-layout-name', 'pl-' . $style->getHashCode()->getString());
						foreach (array('header', 'footer') as $v) {
							$stylesStyteHf = $stylesDocument->createElement('style:' . $v);
							$stylesStyteHf->setAttribute('style:display', 'false');
							$stylesStyleMasterPage->appendChild($stylesStyteHf);
						}
						$stylesMasterStyles->appendChild($stylesStyleMasterPage);
					}
					// **************************************************
					$settingsConfigItemMapEntryTable = $settingsDocument->createElement('config:config-item-map-entry');
					$settingsConfigItemMapEntryTable->setAttribute('config:name', $name->getString());
					if (($row = $style->getFreezeRow()) !== null) {
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', 2);
						$settingsConfigItem->setAttribute('config:name', 'VerticalSplitMode');
						$settingsConfigItem->setAttribute('config:type', 'short');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', $row->getNumber());
						$settingsConfigItem->setAttribute('config:name', 'VerticalSplitPosition');
						$settingsConfigItem->setAttribute('config:type', 'int');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', 0);
						$settingsConfigItem->setAttribute('config:name', 'PositionTop');
						$settingsConfigItem->setAttribute('config:type', 'int');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', $row->getNumber());
						$settingsConfigItem->setAttribute('config:name', 'PositionBottom');
						$settingsConfigItem->setAttribute('config:type', 'int');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
					}
					if (($column = $style->getFreezeColumn()) !== null) {
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', 2);
						$settingsConfigItem->setAttribute('config:name', 'HorizontalSplitMode');
						$settingsConfigItem->setAttribute('config:type', 'short');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', $column->getNumber());
						$settingsConfigItem->setAttribute('config:name', 'HorizontalSplitPosition');
						$settingsConfigItem->setAttribute('config:type', 'int');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', 0);
						$settingsConfigItem->setAttribute('config:name', 'PositionLeft');
						$settingsConfigItem->setAttribute('config:type', 'int');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
						$settingsConfigItem = $settingsDocument->createElement('config:config-item', $column->getNumber());
						$settingsConfigItem->setAttribute('config:name', 'PositionRight');
						$settingsConfigItem->setAttribute('config:type', 'int');
						$settingsConfigItemMapEntryTable->appendChild($settingsConfigItem);
					}
					$settingsConfigItemMapNamed->appendChild($settingsConfigItemMapEntryTable);
					// **************************************************
					// apply the style
					// **************************************************
					$contentTable->setAttribute('table:style-name', 'ta-' . $style->getHashCode()->getString());
				}
				$contentTable->setAttribute('table:name', $name->getString());
				$tableRowColumnStyles = $table->getTableRowColumnStyles();
				for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
					$contentTableColumn = $contentDocument->createElement('table:table-column');
					if ($c->isLessThan($tableRowColumnStyles->size())->getBoolean() && ($style = $tableRowColumnStyles->get($c)) !== null) {
						// **************************************************
						// store the style which is not exist in the set
						// **************************************************
						if ($tableColumnStyleSet->contains($style)->not()->getBoolean()) {
							$tableColumnStyleSet->put($style);
							$contentStyleStyle = $contentDocument->createElement('style:style');
							$contentStyleStyle->setAttribute('style:name', 'co-' . $style->getHashCode()->getString());
							$contentStyleStyle->setAttribute('style:family', 'table-column');
							$contentStyleTableColumnProperties = $contentDocument->createElement('style:table-column-properties');
							$contentStyleTableColumnProperties->setAttribute('style:column-width', $style->toString()->getString());
							$contentStyleTableColumnProperties->setAttribute('fo:break-before', 'auto');
							$contentStyleTableColumnProperties->setAttribute('style:use-optimal-column-width', 'false');
							$contentStyleStyle->appendChild($contentStyleTableColumnProperties);
							$contentAutomaticStyles->appendChild($contentStyleStyle);
						}
						// **************************************************
						// apply the style
						// **************************************************
						$contentTableColumn->setAttribute('table:style-name', 'co-' . $style->getHashCode()->getString());
					}
					$contentTable->appendChild($contentTableColumn);
				}
				$tableRows = $table->getTableRows();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					$contentTableRow = $contentDocument->createElement('table:table-row');
					if ($r->isLessThan($tableRows->size())->getBoolean() && ($tableRow = $tableRows->get($r)) !== null) {
						if (($style = $tableRow->getTableRowColumnStyle()) !== null) {
							// **************************************************
							// store the style which is not exist in the set
							// **************************************************
							if ($tableRowStyleSet->contains($style)->not()->getBoolean()) {
								$tableRowStyleSet->put($style);
								$contentStyleStyle = $contentDocument->createElement('style:style');
								$contentStyleStyle->setAttribute('style:name', 'ro-' . $style->getHashCode()->getString());
								$contentStyleStyle->setAttribute('style:family', 'table-row');
								$contentStyleTableRowProperties = $contentDocument->createElement('style:table-row-properties');
								$contentStyleTableRowProperties->setAttribute('style:row-height', $style->toString()->getString());
								$contentStyleTableRowProperties->setAttribute('fo:break-before', 'auto');
								$contentStyleTableRowProperties->setAttribute('style:use-optimal-row-height', 'false');
								$contentStyleStyle->appendChild($contentStyleTableRowProperties);
								$contentAutomaticStyles->appendChild($contentStyleStyle);
							}
							// **************************************************
							// apply the style
							// **************************************************
							$contentTableRow->setAttribute('table:style-name', 'ro-' . $style->getHashCode()->getString());
						}
						$tableCells = $tableRow->getTableCells();
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$contentTableCell = $contentDocument->createElement('table:table-cell');
							if ($availableCells->getData($r, $c)->getBoolean()) {
								if ($c->isLessThan($tableCells->size())->getBoolean() && ($tableCell = $tableCells->get($c)) !== null) {
									if (($style = $tableCell->getTableCellStyle()) !== null) {
										// **************************************************
										// store the style which is not exist in the set
										// **************************************************
										if (($font = $style->getFont()) !== null) {
											if ($fontSet->contains($font)->not()->getBoolean()) {
												$fontSet->put($font);
												$contentStyleFontFace = $contentDocument->createElement('style:font-face');
												$contentStyleFontFace->setAttribute('style:name', $font->getName()->getString());
												$contentStyleFontFace->setAttribute('svg:font-family', $font->getName()->getString());
												$contentFontFaceDecls->appendChild($contentStyleFontFace);
											}
										}
										// **************************************************
										// store the style which is not exist in the set
										// **************************************************
										if ($tableCellStyleSet->contains($style)->not()->getBoolean()) {
											$tableCellStyleSet->put($style);
											$contentStyleStyle = $contentDocument->createElement('style:style');
											$contentStyleStyle->setAttribute('style:name', 'ce-' . $style->getHashCode()->getString());
											$contentStyleStyle->setAttribute('style:family', 'table-cell');
											$contentStyleTableCellProperties = $contentDocument->createElement('style:table-cell-properties');
											if (($align = $style->getVerticalAlign()) !== null && array_key_exists($align->getNumber(), self::$verticalAligns)) {
												$contentStyleTableCellProperties->setAttribute('style:vertical-align', self::$verticalAligns[$align->getNumber()]);
											}
											$borders = array(
												'top' => $style->getTopBorder(),
												'left' => $style->getLeftBorder(),
												'right' => $style->getRightBorder(),
												'bottom' => $style->getBottomBorder(),
											);
											foreach ($borders as $k => $v) {
												if ($v !== null) {
													$size = $v->getSize()->toString()->getString();
													$type = 0;
													if ($v->getType() !== null) {
														$type = $v->getType()->getNumber();
													}
													$color = 0;
													if ($v->getColor() !== null) {
														$color = $v->getColor()->getRGB()->getNumber();
													}
													$contentStyleTableCellProperties->setAttribute('fo:border-' . $k, sprintf('%s %s #%06x', $size, self::$borderTypes[$type], $color));
												}
											}
											if (($color = $style->getBackgroundColor()) !== null) {
												$contentStyleTableCellProperties->setAttribute('fo:background-color', sprintf('#%06x', $color->getRGB()->getNumber()));
											}
											$contentStyleTableCellProperties->setAttribute('style:text-align-source', 'fix');
											$contentStyleTableCellProperties->setAttribute('style:repeat-content', 'false');
											$contentStyleStyle->appendChild($contentStyleTableCellProperties);
											if (($align = $style->getHorizontalAlign()) !== null && array_key_exists($align->getNumber(), self::$horizontalAligns)) {
												$contentStyleParagraphProperties = $contentDocument->createElement('style:paragraph-properties');
												$contentStyleParagraphProperties->setAttribute('fo:text-align', self::$horizontalAligns[$align->getNumber()]);
												$contentStyleStyle->appendChild($contentStyleParagraphProperties);
											}
											if (($font = $style->getFont()) !== null || ($color = $style->getTextColor()) !== null) {
												$contentStyleTextProperties = $contentDocument->createElement('style:text-properties');
												if (($font = $style->getFont()) !== null) {
													$contentStyleTextProperties->setAttribute('fo:font-size', $font->getSize()->toString()->getString());
													if (($name = $font->getName()) !== null) {
														$contentStyleTextProperties->setAttribute('style:font-name', $name->getString());
													}
													if (($type = $font->getType()) !== null) {
														if ($type->bitwiseAnd(\php\awt\Font::BOLD())->isPositive()->getBoolean()) {
															$contentStyleTextProperties->setAttribute('fo:font-weight', 'bold');
														}
														if ($type->bitwiseAnd(\php\awt\Font::ITALIC())->isPositive()->getBoolean()) {
															$contentStyleTextProperties->setAttribute('fo:font-style', 'italic');
														}
													}
												}
												if (($color = $style->getTextColor()) !== null) {
													$contentStyleTextProperties->setAttribute('fo:color', sprintf('#%06x', $color->getRGB()->getNumber()));
												}
												$contentStyleStyle->appendChild($contentStyleTextProperties);
											}
											$contentAutomaticStyles->appendChild($contentStyleStyle);
										}
										// **************************************************
										// apply the style
										// **************************************************
										$contentTableCell->setAttribute('table:style-name', 'ce-' . $style->getHashCode()->getString());
									}
									$bottom = $r->increase();
									$right = $c->increase();
									if (($span = $tableCell->getRowspan()) !== null) {
										$contentTableCell->setAttribute('table:number-rows-spanned', $span->getNumber());
										$bottom = $r->addition($span);
									}
									if (($span = $tableCell->getColspan()) !== null) {
										$contentTableCell->setAttribute('table:number-columns-spanned', $span->getNumber());
										$right = $c->addition($span);
									}
									for ($y = $r; $y->isLessThan($bottom)->getBoolean(); $y = $y->increase()) {
										for ($x = $c; $x->isLessThan($right)->getBoolean(); $x = $x->increase()) {
											$availableCells->setData($y, $x, \php\lang\PHPBoolean::newInstance(false));
										}
									}
									// **************************************************
									if (($data = $tableCell->getData()) !== null) {
										$contentTableCell->appendChild($contentDocument->createElement('text:p', $data->getString()));
									}
								}
							} else {
								// **************************************************
								// cell not available
								// **************************************************
								$contentTableCell = $contentDocument->createElement('table:covered-table-cell');
							}
							$contentTableRow->appendChild($contentTableCell);
						}
					} else {
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$contentTableRow->appendChild($contentDocument->createElement('table:table-cell'));
						}
					}
					$contentTable->appendChild($contentTableRow);
				}
				$contentSpreadsheet->appendChild($contentTable);
			}
			$contentBody->appendChild($contentSpreadsheet);
			if ($contentFontFaceDecls->hasChildNodes()) {
				$contentDocumentContent->appendChild($contentFontFaceDecls);
			}
			if ($contentAutomaticStyles->hasChildNodes()) {
				$contentDocumentContent->appendChild($contentAutomaticStyles);
			}
			$contentDocumentContent->appendChild($contentBody);
			$contentDocument->appendChild($contentDocumentContent);
			// **************************************************
			$settingsConfigItemMapEntry->appendChild($settingsConfigItemMapNamed);
			$settingsConfigItemMapIndexed->appendChild($settingsConfigItemMapEntry);
			$settingsConfigItemSet->appendChild($settingsConfigItemMapIndexed);
			$settingsOfficeSettings->appendChild($settingsConfigItemSet);
			$settingsOfficeDocumentSettings->appendChild($settingsOfficeSettings);
			$settingsDocument->appendChild($settingsOfficeDocumentSettings);
			// **************************************************
			if ($stylesAutomaticStyles->hasChildNodes()) {
				$stylesDocumentStyles->appendChild($stylesAutomaticStyles);
			}
			if ($stylesMasterStyles->hasChildNodes()) {
				$stylesDocumentStyles->appendChild($stylesMasterStyles);
			}
			$stylesDocument->appendChild($stylesDocumentStyles);
			// **************************************************
			// write in zip
			// **************************************************
			$zip->addFile(\php\lang\PHPString::newInstance('META-INF/manifest.xml'), \php\lang\PHPString::newInstance(trim($manifestDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance(self::$contentFile), \php\lang\PHPString::newInstance(trim($contentDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance(self::$settingsFile), \php\lang\PHPString::newInstance(trim($settingsDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance(self::$stylesFile), \php\lang\PHPString::newInstance(trim($stylesDocument->saveXML())));
			// **************************************************
			// export the zip
			// **************************************************
			$extension = \php\lang\PHPString::newInstance($this->extension);
			$mimeType = \php\lang\PHPString::newInstance($this->mimeType);
			$zip->export($filename, $extension, $mimeType);
		}

	}

}