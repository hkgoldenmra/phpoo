<?php

/**
 * Description of \php\util\spreadsheets\Spreadsheet
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/spreadsheets/Table.php');

	/**
	 * The \php\util\spreadsheets\Spreadsheet class represents the spreadsheet.
	 */
	abstract class Spreadsheet extends \php\lang\PHPObject {

		protected $tables;
		protected $mimeType;
		protected $extension;

		/**
		 * Contructs a \php\util\spreadsheets\Spreadsheet object.
		 * @param \php\lang\PHPString $mimeType The MIME type of this spreadsheet.
		 * @param \php\lang\PHPString $extension The file extension of this spreadsheet.
		 */
		protected function __construct(\php\lang\PHPString $mimeType, \php\lang\PHPString $extension) {
			parent::__construct();
			$this->tables = \php\util\collections\Map::newInstance();
			$this->mimeType = $mimeType->getString();
			$this->extension = $extension->getString();
		}

		/**
		 * Sets a table with an unique name for this spreadsheet.
		 * @param \php\lang\PHPString $name The unique name of the table.
		 * @param \php\util\spreadsheets\Table $table The data of table.
		 */
		public function setTable(\php\lang\PHPString $name, Table $table) {
			$this->tables->put($name, $table);
		}

		/**
		 * Exports the data from this spreadsheet.
		 */
		protected abstract function export(\php\lang\PHPString $filename = null);
	}

}