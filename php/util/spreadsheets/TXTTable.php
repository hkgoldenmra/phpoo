<?php

/**
 * Description of \php\util\spreadsheets\TXTTable
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPString.php');
	include_once('php/util/spreadsheets/DelimitedTable.php');

	/**
	 * The \php\util\spreadsheets\TXTTable class represents the txt table.
	 */
	class TXTTable extends DelimitedTable {

		/**
		 * Returns a \php\util\spreadsheets\TXTTable object.
		 */
		public static function newInstance() {
			return new TXTTable();
		}

		/**
		 * Constructs a \php\util\spreadsheets\TXTTable object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance("\t"), \php\lang\PHPString::newInstance('text/plain'), \php\lang\PHPString::newInstance('txt'));
		}

	}

}