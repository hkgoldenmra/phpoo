<?php

/**
 * Description of \php\util\spreadsheets\Table
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/spreadsheets/TableRow.php');
	include_once('php/util/spreadsheets/TableStyle.php');

	/**
	 * The \php\util\spreadsheets\Table class represents the table row.
	 */
	class Table extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\spreadsheets\Table object.
		 * @return \php\util\spreadsheets\Table
		 */
		public static function newInstance() {
			return new Table();
		}

		/**
		 *
		 * @var \php\util\spreadsheets\TableStyle
		 */
		private $tableStyle;

		/**
		 *
		 * @var \php\util\collections\Vector
		 */
		private $tableRows;

		/**
		 *
		 * @var \php\util\collections\Vector
		 */
		private $tableRowColumnStyles;

		/**
		 * Constructs a \php\util\spreadsheets\Table object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->tableRows = \php\util\collections\Vector::newInstance();
			$this->tableRowColumnStyles = \php\util\collections\Vector::newInstance();
		}

		/**
		 * Sets table style for this table.
		 * @param \php\util\spreadsheets\TableStyle $tableStyle
		 */
		public function setTableStyle(TableStyle $tableStyle = null) {
			$this->tableStyle = $tableStyle;
		}

		/**
		 * Sets table row for this table.
		 * @param \php\lang\PHPNumber $index Position of this table row.
		 * @param \php\util\spreadsheets\TableRow $tableRow The table row.
		 */
		public function setTableRow(\php\lang\PHPNumber $index, TableRow $tableRow = null) {
			$this->tableRows->set($index, $tableRow);
		}

		/**
		 * Sets table row column style for this table.
		 * @param \php\lang\PHPNumber $index Position of this table column.
		 * @param \php\util\spreadsheets\TableRowColumnStyle $tableRowColumnStyle The table column style.
		 */
		public function setTableRowStyle(\php\lang\PHPNumber $index, TableRowColumnStyle $tableRowColumnStyle = null) {
			$this->tableRows->get($index)->setTableRowColumnStyle($tableRowColumnStyle);
		}

		/**
		 * Sets table row column style for this table.
		 * @param \php\lang\PHPNumber $index Position of this table column.
		 * @param \php\util\spreadsheets\TableRowColumnStyle $tableRowColumnStyle The table column style.
		 */
		public function setTableColumnStyle(\php\lang\PHPNumber $index, TableRowColumnStyle $tableRowColumnStyle = null) {
			$this->tableRowColumnStyles->set($index, $tableRowColumnStyle);
		}

		/**
		 * Sets data for this table.
		 * @param \php\lang\PHPNumber $row The row position of the selected table cell.
		 * @param \php\lang\PHPNumber $column The column position of the selected table cell.
		 * @param \php\lang\PHPObject $data The data for the selected table cell.
		 * @param \php\util\spreadsheets\TableCellStyle $tableCellStyle The table cell style for the selected table cell.
		 * @param \php\lang\PHPNumber $rowspan The rowspan size for the selected table cell.
		 * @param \php\lang\PHPNumber $colspan The colspan size for the selected table cell.
		 */
		public function setData(\php\lang\PHPNumber $row, \php\lang\PHPNumber $column, \php\lang\PHPObject $data = null, TableCellStyle $tableCellStyle = null, \php\lang\PHPNumber $rowspan = null, \php\lang\PHPNumber $colspan = null) {
			$tableRow = null;
			if ($row->isLessThan($this->getTableRows()->size())->getBoolean()) {
				$tableRow = $this->getTableRows()->get($row);
			}
			if ($tableRow === null) {
				$tableRow = TableRow::newInstance();
			}
			$tableRow->setData($column, $data, $tableCellStyle, $rowspan, $colspan);
			$this->setTableRow($row, $tableRow);
		}

		/**
		 * Returns the data of the selected table cell.
		 * @param \php\lang\PHPNumber $row The row position of the selected table cell.
		 * @param \php\lang\PHPNumber $column The column position of the selected table cell.
		 * @return \php\lang\PHPObject
		 */
		public function getData(\php\lang\PHPNumber $row, \php\lang\PHPNumber $column) {
			return $this->tableRows->get($row)->getData($column);
		}

		/**
		 * Returns the table style of this table.
		 * @return \php\util\spreadsheets\TableStyle
		 */
		public function getTableStyle() {
			return $this->tableStyle;
		}

		/**
		 * Returns all table rows of this table.
		 * @return \php\util\collections\Vector
		 */
		public function getTableRows() {
			return $this->tableRows;
		}

		/**
		 * Returns all table row column styles of this table.
		 * @return \php\util\collections\Vector
		 */
		public function getTableRowColumnStyles() {
			return $this->tableRowColumnStyles;
		}

		/**
		 * Returns the number of table rows of this table.
		 * @return \php\lang\PHPNumber
		 */
		public function rowCount() {
			return $this->tableRows->size();
		}

		/**
		 * Returns the number of table column of this table.
		 * @return \php\lang\PHPNumber
		 */
		public function columnCount() {
			$columns = $this->tableRowColumnStyles->size();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($this->rowCount())->getBoolean(); $i = $i->increase()) {
				$tableRow = $this->tableRows->get($i);
				$columns = $columns->max($tableRow->count());
			}
			return $columns;
		}

		/**
		 * Swaps the table rows and table columns.
		 * @return \php\util\spreadsheets\Table
		 */
		public function swap() {
			$table = Table::newInstance();
			$tableRows = $this->getTableRows();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($tableRows->size())->getBoolean(); $i = $i->increase()) {
				if (($tableRow = $tableRows->get($i)) !== null) {
					$tableRowColumnStyle = $tableRow->getTableRowColumnStyle();
					$table->setTableColumnStyle($i, $tableRowColumnStyle);
					$tableCells = $tableRow->getTableCells();
					for ($j = \php\lang\PHPNumber::ZERO(); $j->isLessThan($tableCells->size())->getBoolean(); $j = $j->increase()) {
						if (($tableCell = $tableCells->get($j)) !== null) {
							$data = $tableCell->getData();
							$tableCellStyle = $tableCell->getTableCellStyle();
							$rowspan = $tableCell->getRowspan();
							$colspan = $tableCell->getColspan();
							$table->setData($j, $i, $data, $tableCellStyle, $colspan, $rowspan);
						}
					}
				}
			}
			$tableRows = $table->getTableRows();
			$tableRowColumnStyles = $this->getTableRowColumnStyles();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($tableRows->size())->getBoolean(); $i = $i->increase()) {
				$tableRowColumnStyle = $tableRowColumnStyles->get($i);
				$tableRows->get($i)->setTableColumnStyle($tableRowColumnStyle);
			}
			$tableStyle = $this->getTableStyle();
			$tableStyle = TableStyle::newInstanceByParameters(
					$tableStyle->getPaper()
					, $tableStyle->isLandscape()->not()
					, $tableStyle->getFreezeColumn()
					, $tableStyle->getFreezeRow()
					, $tableStyle->getLeftMargin()
					, $tableStyle->getBottomMargin()
					, $tableStyle->getTopMargin()
					, $tableStyle->getRightMargin()
					, $tableStyle->getHorizontalCentered()
					, $tableStyle->getVerticalCentered()
			);
			$table->setTableStyle($tableStyle);
			return $table;
		}

	}

}