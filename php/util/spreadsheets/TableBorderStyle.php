<?php

/**
 * Description of \php\util\spreadsheets\TableBorderStyle
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');

	/**
	 * The \php\util\spreadsheets\TableBorderStyle class represents the table borde style.
	 */
	class TableBorderStyle extends \php\lang\PHPObject {

		/**
		 * Returns no table border style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function NONE() {
			return \php\lang\PHPNumber::newInstance(0);
		}

		/**
		 * Returns the solid style of table border style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function SOLID() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the dashed style of table border style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DASHED() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the dotted style of table border style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function DOTTED() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\util\spreadsheets\TableBorderStyle object.
		 * @param \php\lang\PHPNumber $type The type of table border style. Default TableBorderStyle::NONE().
		 * @param \php\awt\Size $size The thickness of table border style. Default &lt;null&gt;.
		 * @param \php\awt\Color $color The color of table border style. Default &lt;null&gt;.
		 * @return \php\util\spreadsheets\TableBorderStyle
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $type = null, \php\awt\Size $size = null, \php\awt\Color $color = null) {
			return new TableBorderStyle($type, $size, $color);
		}

		private $type;
		private $size;
		private $color;

		/**
		 * Contructs a \php\util\spreadsheets\TableBorderStyle object.
		 * @param \php\lang\PHPNumber $type The type of table border style. Default TableBorderStyle::NONE().
		 * @param \php\awt\Size $size The thickness of table border style. Default &lt;null&gt;.
		 * @param \php\awt\Color $color The color of table border style. Default &lt;null&gt;.
		 */
		protected function __construct(\php\lang\PHPNumber $type = null, \php\awt\Size $size = null, \php\awt\Color $color = null) {
			parent::__construct();
			if ($type === null) {
				$type = TableBorderStyle::NONE();
			}
			$this->type = $type->getNumber();
			$this->size = $size;
			$this->color = $color;
		}

		/**
		 * Returns the type of table border style.
		 * @return \php\lang\PHPNumber
		 */
		public function getType() {
			return \php\lang\PHPNumber::newInstance($this->type);
		}

		/**
		 * Returns the size of table border style.
		 * @return \php\awt\Size
		 */
		public function getSize() {
			return $this->size;
		}

		/**
		 * Returns the color of table border style.
		 * @return \php\awt\Color
		 */
		public function getColor() {
			return $this->color;
		}

	}

}