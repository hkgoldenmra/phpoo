<?php

/**
 * Description of \php\util\spreadsheets\TableCell
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/spreadsheets/TableBorderStyle.php');
	include_once('php/util/spreadsheets/TableCellStyle.php');

	/**
	 * The \php\util\spreadsheets\TableCell class represents the table cell.
	 */
	class TableCell extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\spreadsheets\TableCell object.
		 * @return \php\util\spreadsheets\TableCell
		 */
		public static function newInstance() {
			return new TableCell();
		}

		/**
		 *
		 * @var \php\lang\PHPObject
		 */
		private $data;
		private $rowspan;
		private $colspan;

		/**
		 *
		 * @var \php\util\spreadsheets\TableCellStyle
		 */
		private $tableCellStyle;

		/**
		 * Constructs a \php\util\spreadsheets\TableCell object.
		 */
		protected function __construct() {
			parent::__construct();
		}

		/**
		 * Sets data for this table cell.
		 * @param \php\lang\PHPObject $data The data in this table cell.
		 */
		public function setData(\php\lang\PHPObject $data = null) {
			$this->data = $data;
		}

		/**
		 * Sets table cell style for this table cell.
		 * @param \php\util\spreadsheets\TableCellStyle $tableCellStyle
		 */
		public function setTableCellStyle(TableCellStyle $tableCellStyle = null) {
			$this->tableCellStyle = $tableCellStyle;
		}

		/**
		 * Sets rowspan size for this table cell.
		 * @param \php\lang\PHPNumber $rowspan The rowspan size.
		 */
		public function setRowspan(\php\lang\PHPNumber $rowspan = null) {
			$this->rowspan = (($rowspan === null) ? null : $rowspan->getInteger()->getNumber());
		}

		/**
		 * Sets colspan size for this table cell.
		 * @param \php\lang\PHPNumber $colspan The colspan size.
		 */
		public function setColspan(\php\lang\PHPNumber $colspan = null) {
			$this->colspan = (($colspan === null) ? null : $colspan->getInteger()->getNumber());
		}

		/**
		 * Returns the data of this cell.
		 * @return \php\lang\PHPObject
		 */
		public function getData() {
			return $this->data;
		}

		/**
		 * Returns the table cell style of this cell.
		 * @return \php\util\spreadsheets\TableCellStyle
		 */
		public function getTableCellStyle() {
			return $this->tableCellStyle;
		}

		/**
		 * Returns the rowspan size of this cell.
		 * @return \php\lang\PHPNumber
		 */
		public function getRowspan() {
			return (($this->rowspan === null) ? null : \php\lang\PHPNumber::newInstance($this->rowspan));
		}

		/**
		 * Returns the colspan size of this cell.
		 * @return \php\lang\PHPNumber
		 */
		public function getColspan() {
			return (($this->colspan === null) ? null : \php\lang\PHPNumber::newInstance($this->colspan));
		}

	}

}