<?php

/**
 * Description of \php\util\spreadsheets\TableCellStyle
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');
	include_once('php/awt/Color.php');
	include_once('php/awt/Font.php');
	include_once('php/util/spreadsheets/TableBorderStyle.php');

	/**
	 * The \php\util\spreadsheets\TableCellStyle class represents the table cell style.
	 */
	class TableCellStyle extends \php\lang\PHPObject {

		/**
		 * Returns the value of left horizontal alignment.
		 * @return \php\lang\PHPNumber
		 */
		public static final function LEFT() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the value of center horizontal alignment.
		 * @return \php\lang\PHPNumber
		 */
		public static final function CENTER() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the value of right horizontal alignment.
		 * @return \php\lang\PHPNumber
		 */
		public static final function RIGHT() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		/**
		 * Returns the value of top vertical alignment.
		 * @return \php\lang\PHPNumber
		 */
		public static final function TOP() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the value of middle vertical alignment.
		 * @return \php\lang\PHPNumber
		 */
		public static final function MIDDLE() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		/**
		 * Returns the value of bottom vertical alignment.
		 * @return \php\lang\PHPNumber
		 */
		public static final function BOTTOM() {
			return \php\lang\PHPNumber::newInstance(3);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Constructs a \php\util\spreadsheets\TableCellStyle object.
		 * @param \php\awt\Font $font
		 * @param \php\awt\Color $textColor
		 * @param \php\awt\Color $backgroundColor
		 * @param \php\lang\PHPNumber $horizontaolAlign
		 * @param \php\lang\PHPNumber $verticalAlign
		 * @param \php\util\spreadsheets\TableBorderStyle $topBorder
		 * @param \php\util\spreadsheets\TableBorderStyle $leftBorder
		 * @param \php\util\spreadsheets\TableBorderStyle $rightBorder
		 * @param \php\util\spreadsheets\TableBorderStyle $bottomBorder
		 * @return \php\util\spreadsheets\TableCellStyle
		 */
		public static function newInstanceByParameters(\php\awt\Font $font = null, \php\awt\Color $textColor = null, \php\awt\Color $backgroundColor = null, \php\lang\PHPNumber $horizontaolAlign = null, \php\lang\PHPNumber $verticalAlign = null, TableBorderStyle $topBorder = null, TableBorderStyle $leftBorder = null, TableBorderStyle $rightBorder = null, TableBorderStyle $bottomBorder = null) {
			return new TableCellStyle($font, $textColor, $backgroundColor, $horizontaolAlign, $verticalAlign, $topBorder, $leftBorder, $rightBorder, $bottomBorder);
		}

		/**
		 *
		 * @var \php\awt\Font 
		 */
		private $font;

		/**
		 *
		 * @var \php\awt\Color
		 */
		private $textColor;

		/**
		 *
		 * @var \php\awt\Color
		 */
		private $backgroundColor;

		/**
		 *
		 * @var \php\lang\PHPNumber
		 */
		private $horizontalAlign;

		/**
		 *
		 * @var \php\lang\PHPNumber
		 */
		private $verticalAlign;

		/**
		 *
		 * @var \php\util\spreadsheets\TableBorderStyle
		 */
		private $topBorder;

		/**
		 *
		 * @var \php\util\spreadsheets\TableBorderStyle
		 */
		private $leftBorder;

		/**
		 *
		 * @var \php\util\spreadsheets\TableBorderStyle
		 */
		private $rightBorder;

		/**
		 *
		 * @var \php\util\spreadsheets\TableBorderStyle
		 */
		private $bottomBorder;

		protected function __construct(\php\awt\Font $font = null, \php\awt\Color $textColor = null, \php\awt\Color $backgroundColor = null, \php\lang\PHPNumber $horizontaolAlign = null, \php\lang\PHPNumber $verticalAlign = null, TableBorderStyle $topBorder = null, TableBorderStyle $leftBorder = null, TableBorderStyle $rightBorder = null, TableBorderStyle $bottomBorder = null) {
			parent::__construct();
			if ($horizontaolAlign === null) {
				$horizontaolAlign = TableCellStyle::LEFT();
			}
			if ($verticalAlign === null) {
				$verticalAlign = TableCellStyle::TOP();
			}
			$this->font = $font;
			$this->textColor = $textColor;
			$this->backgroundColor = $backgroundColor;
			$this->horizontalAlign = $horizontaolAlign->getInteger()->getNumber();
			$this->verticalAlign = $verticalAlign->getInteger()->getNumber();
			$this->topBorder = $topBorder;
			$this->leftBorder = $leftBorder;
			$this->rightBorder = $rightBorder;
			$this->bottomBorder = $bottomBorder;
		}

		/**
		 * Returns the font of this table cell style.
		 * @return \php\awt\Font
		 */
		public function getFont() {
			return $this->font;
		}

		/**
		 * Returns the text color of this table cell style.
		 * @return \php\awt\Color
		 */
		public function getTextColor() {
			return $this->textColor;
		}

		/**
		 * Returns the background color of this table cell style.
		 * @return \php\awt\Color
		 */
		public function getBackgroundColor() {
			return $this->backgroundColor;
		}

		/**
		 * Returns the horizontal alignment of this table cell style.
		 * @return \php\lang\PHPNumber
		 */
		public function getHorizontalAlign() {
			return \php\lang\PHPNumber::newInstance($this->horizontalAlign);
		}

		/**
		 * Returns the vertical alignment of this table cell style.
		 * @return \php\lang\PHPNumber
		 */
		public function getVerticalAlign() {
			return \php\lang\PHPNumber::newInstance($this->verticalAlign);
		}

		/**
		 * Returns the top border style of this table cell style.
		 * @return \php\util\spreadsheets\TableBorderStyle
		 */
		public function getTopBorder() {
			return $this->topBorder;
		}

		/**
		 * Returns the left border style of this table cell style.
		 * @return \php\util\spreadsheets\TableBorderStyle
		 */
		public function getLeftBorder() {
			return $this->leftBorder;
		}

		/**
		 * Returns the right border style of this table cell style.
		 * @return \php\util\spreadsheets\TableBorderStyle
		 */
		public function getRightBorder() {
			return $this->rightBorder;
		}

		/**
		 * Returns the bottom border style of this table cell style.
		 * @return \php\util\spreadsheets\TableBorderStyle
		 */
		public function getBottomBorder() {
			return $this->bottomBorder;
		}

	}

}