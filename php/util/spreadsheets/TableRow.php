<?php

/**
 * Description of \php\util\spreadsheets\TableRow
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/util/collections/Vector.php');
	include_once('php/util/spreadsheets/TableCell.php');
	include_once('php/util/spreadsheets/TableRowColumnStyle.php');

	/**
	 * The \php\util\spreadsheets\TableRow class represents the table row.
	 */
	class TableRow extends \php\lang\PHPObject {

		/**
		 * Returns a \php\util\spreadsheets\TableRow object.
		 * @return \php\util\spreadsheets\TableRow
		 */
		public static function newInstance() {
			return new TableRow();
		}

		/**
		 *
		 * @var \php\util\collections\Vector
		 */
		private $tableCells;

		/**
		 *
		 * @var \php\util\spreadsheets\TableRowColumnStyle
		 */
		private $tableRowColumnStyle;

		/**
		 * Constructs a \php\util\spreadsheets\TableRow object.
		 */
		protected function __construct() {
			parent::__construct();
			$this->tableCells = \php\util\collections\Vector::newInstance();
		}

		/**
		 * Sets table cell for this table row.
		 * @param \php\lang\PHPNumber $index Position in this table row.
		 * @param \php\util\spreadsheets\TableCell $tableCell
		 */
		public function setTableCell(\php\lang\PHPNumber $index, TableCell $tableCell = null) {
			$this->tableCells->set($index, $tableCell);
		}

		/**
		 * Sets table row column style for this table row.
		 * @param \php\util\spreadsheets\TableRowColumnStyle $tableRowColumnStyle
		 */
		public function setTableRowColumnStyle(TableRowColumnStyle $tableRowColumnStyle = null) {
			$this->tableRowColumnStyle = $tableRowColumnStyle;
		}

		/**
		 * Sets data for this table row.
		 * @param \php\lang\PHPNumber $index Position of the selected table cell.
		 * @param \php\lang\PHPObject $data The data for the selected table cell.
		 * @param \php\util\spreadsheets\TableCellStyle $tableCellStyle The table cell style for the selected table cell.
		 * @param \php\lang\PHPNumber $rowspan The rowspan size for the selected table cell.
		 * @param \php\lang\PHPNumber $colspan The colspan size for the selected table cell.
		 */
		public function setData(\php\lang\PHPNumber $index, \php\lang\PHPObject $data = null, TableCellStyle $tableCellStyle = null, \php\lang\PHPNumber $rowspan = null, \php\lang\PHPNumber $colspan = null) {
			$tableCell = null;
			if ($index->isLessThan($this->getTableCells()->size())->getBoolean()) {
				$tableCell = $this->getTableCells()->get($index);
			}
			if ($tableCell === null) {
				$tableCell = TableCell::newInstance();
			}
			$tableCell->setData($data);
			$tableCell->setTableCellStyle($tableCellStyle);
			$tableCell->setRowspan($rowspan);
			$tableCell->setColspan($colspan);
			$this->setTableCell($index, $tableCell);
		}

		/**
		 * Returns the data of the selected table cell.
		 * @param \php\lang\PHPNumber $index Position of the selected table cell.
		 * @return \php\lang\PHPObject
		 */
		public function getData(\php\lang\PHPNumber $index) {
			return $this->tableCells->get($index)->getData();
		}

		/**
		 * Returns all table cells of this table row.
		 * @return \php\util\collections\Vector
		 */
		public function getTableCells() {
			return $this->tableCells;
		}

		/**
		 * Returns the table row column style of this table row.
		 * @return \php\util\spreadsheets\TableRowColumnStyle
		 */
		public function getTableRowColumnStyle() {
			return $this->tableRowColumnStyle;
		}

		/**
		 * Returns the number of table cells of this table row.
		 * @return \php\lang\PHPNumber
		 */
		public function count() {
			return $this->tableCells->size();
		}

	}

}