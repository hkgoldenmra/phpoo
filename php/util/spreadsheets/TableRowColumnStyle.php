<?php

/**
 * Description of \php\util\spreadsheets\TableRowColumnStyle
 */

namespace php\util\spreadsheets {
	include_once('php/awt/Size.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPString.php');

	/**
	 * The \php\util\spreadsheets\TableRowColumnStyle class represents the table row style and table column style.
	 */
	class TableRowColumnStyle extends \php\awt\Size {

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\util\spreadsheets\TableRowColumnStyle object.
		 * @param \php\lang\PHPNumber $size The size data.
		 * @param \php\lang\PHPString $unit The unit data.
		 * @return \php\util\spreadsheets\TableRowColumnStyle
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $size, \php\lang\PHPString $unit = null) {
			return new TableRowColumnStyle($size, $unit);
		}

		/**
		 * Constructs a \php\util\spreadsheets\TableRowColumnStyle object.
		 * @param \php\lang\PHPNumber $size The size data.
		 * @param \php\lang\PHPString $unit The unit data.
		 */
		protected function __construct(\php\lang\PHPNumber $size, \php\lang\PHPString $unit = null) {
			parent::__construct($size, $unit);
		}

	}

}