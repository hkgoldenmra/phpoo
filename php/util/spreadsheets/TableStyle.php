<?php

/**
 * Description of \php\util\spreadsheets\TableStyle
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPNumber.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/awt/Size.php');

	/**
	 * The \php\util\spreadsheets\TableStyle class represents the table style.
	 */
	class TableStyle extends \php\lang\PHPObject {

		/**
		 * Returns the A3 paper size value of table style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAPER_A3() {
			return \php\lang\PHPNumber::newInstance(0);
		}

		/**
		 * Returns the A4 paper size value of table style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAPER_A4() {
			return \php\lang\PHPNumber::newInstance(1);
		}

		/**
		 * Returns the A5 paper size value of table style.
		 * @return \php\lang\PHPNumber
		 */
		public static final function PAPER_A5() {
			return \php\lang\PHPNumber::newInstance(2);
		}

		public static function newInstance() {
			parent::unsupportedFunction();
		}

		/**
		 * Returns a \php\util\spreadsheets\TableStyle object.
		 * @param \php\lang\PHPNumber $paper The paper size value. Default PAPER_A4().
		 * @param \php\lang\PHPBoolean $isLandscape Paper orientation. Default &lt;true&gt;.
		 * @param \php\lang\PHPNumber $freezeRow The freezed row index. Default 0.
		 * @param \php\lang\PHPNumber $freezeColumn The freezed column index. Default 0.
		 * @param \php\awt\Size $topMargin The top margin size. Default &lt;null&gt;
		 * @param \php\awt\Size $leftMargin The left margin size. Default &lt;null&gt;
		 * @param \php\awt\Size $rightMargin The right margin size. Default &lt;null&gt;
		 * @param \php\awt\Size $bottomMargin The bottom margin size. Default &lt;null&gt;
		 * @param \php\lang\PHPBoolean $horizontalCentered The horizontal centered of the data. Default &lt;false&gt;
		 * @param \php\lang\PHPBoolean $verticalCentered The vertical centered of the data. Default &lt;false&gt;
		 * @return \php\util\spreadsheets\TableStyle
		 */
		public static function newInstanceByParameters(\php\lang\PHPNumber $paper = null, \php\lang\PHPBoolean $isLandscape = null, \php\lang\PHPNumber $freezeRow = null, \php\lang\PHPNumber $freezeColumn = null, \php\awt\Size $topMargin = null, \php\awt\Size $leftMargin = null, \php\awt\Size $rightMargin = null, \php\awt\Size $bottomMargin = null, \php\lang\PHPBoolean $horizontalCentered = null, \php\lang\PHPBoolean $verticalCentered = null) {
			return new TableStyle($paper, $isLandscape, $freezeRow, $freezeColumn, $topMargin, $leftMargin, $rightMargin, $bottomMargin, $horizontalCentered, $verticalCentered);
		}

		private $paper;
		private $isLandscape;
		private $freezeRow;
		private $freezeColumn;
		private $topMargin;
		private $leftMargin;
		private $rightMargin;
		private $bottomMargin;
		private $horizontalCentered;
		private $verticalCentered;

		/**
		 * Returns a \php\util\spreadsheets\TableStyle object.
		 * @param \php\lang\PHPNumber $paper The paper size value. Default PAPER_A4().
		 * @param \php\lang\PHPBoolean $isLandscape Paper orientation. Default &lt;true&gt;.
		 * @param \php\lang\PHPNumber $freezeRow The freezed row index. Default 0.
		 * @param \php\lang\PHPNumber $freezeColumn The freezed column index. Default 0.
		 * @param \php\awt\Size $topMargin The top margin size. Default &lt;null&gt;
		 * @param \php\awt\Size $leftMargin The left margin size. Default &lt;null&gt;
		 * @param \php\awt\Size $rightMargin The right margin size. Default &lt;null&gt;
		 * @param \php\awt\Size $bottomMargin The bottom margin size. Default &lt;null&gt;
		 * @param \php\lang\PHPBoolean $horizontalCentered The horizontal centered of the data. Default &lt;false&gt;
		 * @param \php\lang\PHPBoolean $verticalCentered The vertical centered of the data. Default &lt;false&gt;
		 */
		protected function __construct(\php\lang\PHPNumber $paper = null, \php\lang\PHPBoolean $isLandscape = null, \php\lang\PHPNumber $freezeRow = null, \php\lang\PHPNumber $freezeColumn = null, \php\awt\Size $topMargin = null, \php\awt\Size $leftMargin = null, \php\awt\Size $rightMargin = null, \php\awt\Size $bottomMargin = null, \php\lang\PHPBoolean $horizontalCentered = null, \php\lang\PHPBoolean $verticalCentered = null) {
			parent::__construct();
			if ($paper === null) {
				$paper = TableStyle::PAPER_A4();
			}
			if ($isLandscape === null) {
				$isLandscape = \php\lang\PHPBoolean::newInstance(true);
			}
			if ($freezeRow === null) {
				$freezeRow = \php\lang\PHPNumber::ZERO();
			}
			if ($freezeColumn === null) {
				$freezeColumn = \php\lang\PHPNumber::ZERO();
			}
			if ($horizontalCentered === null) {
				$horizontalCentered = \php\lang\PHPBoolean::newInstance(false);
			}
			if ($verticalCentered === null) {
				$verticalCentered = \php\lang\PHPBoolean::newInstance(false);
			}
			$this->paper = $paper->getNumber();
			$this->isLandscape = $isLandscape->getBoolean();
			$this->freezeRow = $freezeRow->getNumber();
			$this->freezeColumn = $freezeColumn->getNumber();
			$this->topMargin = $topMargin;
			$this->leftMargin = $leftMargin;
			$this->rightMargin = $rightMargin;
			$this->bottomMargin = $bottomMargin;
			$this->horizontalCentered = $horizontalCentered->getBoolean();
			$this->verticalCentered = $verticalCentered->getBoolean();
		}

		/**
		 * Returns the paper size index of this table style.
		 * @return \php\lang\PHPNumber
		 */
		public function getPaper() {
			return \php\lang\PHPNumber::newInstance($this->paper);
		}

		/**
		 * Returns the orientation of this table style.
		 * @return \php\lang\PHPBoolean
		 */
		public function isLandscape() {
			return \php\lang\PHPBoolean::newInstance($this->isLandscape);
		}

		/**
		 * Returns the freezed row index of this table style.
		 * @return \php\lang\PHPNumber
		 */
		public function getFreezeRow() {
			return \php\lang\PHPNumber::newInstance($this->freezeRow);
		}

		/**
		 * Returns the freezed column index of this table style.
		 * @return \php\lang\PHPNumber
		 */
		public function getFreezeColumn() {
			return \php\lang\PHPNumber::newInstance($this->freezeColumn);
		}

		/**
		 * The top margin size of this table style.
		 * @return \php\awt\Size
		 */
		public function getTopMargin() {
			return $this->topMargin;
		}

		/**
		 * The left margin size of this table style.
		 * @return \php\awt\Size
		 */
		public function getLeftMargin() {
			return $this->leftMargin;
		}

		/**
		 * The right margin size of this table style.
		 * @return \php\awt\Size
		 */
		public function getRightMargin() {
			return $this->rightMargin;
		}

		/**
		 * The bottom margin size of this table style.
		 * @return \php\awt\Size
		 */
		public function getBottomMargin() {
			return $this->bottomMargin;
		}

		/**
		 * Returns is horizontal centered of this table style.
		 * @return \php\lang\PHPBoolean
		 */
		public function getHorizontalCentered() {
			return \php\lang\PHPBoolean::newInstance($this->horizontalCentered);
		}

		/**
		 * Returns is vertical centered of this table style.
		 * @return \php\lang\PHPBoolean
		 */
		public function getVerticalCentered() {
			return \php\lang\PHPBoolean::newInstance($this->verticalCentered);
		}

	}

}