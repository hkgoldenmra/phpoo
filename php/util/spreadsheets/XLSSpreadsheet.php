<?php

/**
 * Description of \php\util\spreadsheets\XLSSpreadsheet
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/Calendar.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/spreadsheets/Table.php');
	include_once('php/util/spreadsheets/Spreadsheet.php');

	/**
	 * The \php\util\spreadsheets\XLSSpreadsheet class represents the XLS spreadsheet.
	 */
	class XLSSpreadsheet extends Spreadsheet {

		private static $papers = array(
			0 => 8, // A3
			1 => 9, // A4
			2 => 11, // A5
		);
		private static $borderTypes = array(
			1 => 'Continuous',
			2 => 'Dash',
			3 => 'Dot',
		);
		private static $horizontalAligns = array(
			1 => 'Left',
			2 => 'Center',
			3 => 'Right',
		);
		private static $verticalAligns = array(
			1 => 'Top',
			2 => 'Center',
			3 => 'Bottom',
		);

		/**
		 * Returns a \php\util\spreadsheets\XLSSpreadsheet object.
		 */
		public static function newInstance() {
			return new XLSSpreadsheet();
		}

		/**
		 * Contructs a \php\util\spreadsheets\XLSSpreadsheet object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('application/xml'), \php\lang\PHPString::newInstance('xls'));
			//parent::__construct(\php\lang\PHPString::newInstance('application/vnd.ms-excel'), \php\lang\PHPString::newInstance('xls'));
		}

		/**
		 * Exports the data from this XLS spreadsheet.
		 * @param \php\lang\PHPString $filename The default export file name. Default &lt;current system date time&gt;.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			// **************************************************
			// export
			// **************************************************
			$extension = \php\lang\PHPString::newInstance($this->extension);
			if ($filename === null) {
				$datetime = \php\util\Calendar::newInstanceByTimestamp();
				$filename = $datetime->toString();
			}
			if ($filename->endsWith($extension)->not()->getBoolean()) {
				$filename = $filename->append(\php\lang\PHPString::newInstance('.'));
				$filename = $filename->append($extension);
			}
			\php\util\Exporter::export(\php\lang\PHPString::newInstance($this->mimeType), $filename);
			// **************************************************
			// initial sets to avoid duplicated entries
			// **************************************************
			$tableCellStyleSet = \php\util\collections\Set::newInstance();
			// **************************************************
			// initial DOM document for to META-INF/manifest.xml
			// **************************************************
			$xlsDocument = new \DOMDocument('1.0', 'UTF-8');
			$xlsDocument->preserveWhiteSpace = false;
			$xlsDocument->formatOutput = true;
			$xlsWorkbook = $xlsDocument->createElement('Workbook');
			$xlsWorkbook->setAttribute('xmlns', 'urn:schemas-microsoft-com:office:spreadsheet');
			$xlsWorkbook->setAttribute('xmlns:ss', 'urn:schemas-microsoft-com:office:spreadsheet');
			$xlsWorkbook->setAttribute('xmlns:x', 'urn:schemas-microsoft-com:office:excel');
			$xlsExcelWorkbook = $xlsDocument->createElement('ExcelWorkbook');
			$xlsExcelWorkbook->setAttribute('xmlns', 'urn:schemas-microsoft-com:office:excel');
			$xlsWorkbook->appendChild($xlsExcelWorkbook);
			$xlsStyles = $xlsDocument->createElement('Styles');
			$xlsWorkbook->appendChild($xlsStyles);
			$names = $this->tables->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($names->size())->getBoolean(); $i = $i->increase()) {
				$name = $names->get($i);
				$table = $this->tables->get($name);
				$rowCount = $table->rowCount();
				$columnCount = $table->columnCount();
				// **************************************************
				// assume the table cells all are available
				// **************************************************
				$availableCells = Table::newInstance();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
						$availableCells->setData($r, $c, \php\lang\PHPBoolean::newInstance(true));
					}
				}
				// **************************************************
				$xlsWorksheet = $xlsDocument->createElement('Worksheet');
				$xlsWorksheet->setAttribute('ss:Name', $name->getString());
				$tableRowColumnStyles = $table->getTableRowColumnStyles();
				if (($style = $table->getTableStyle()) !== null) {
					$xlsWorksheetOptions = $xlsDocument->createElement('WorksheetOptions');
					$xlsWorksheetOptions->setAttribute('xmlns', 'urn:schemas-microsoft-com:office:excel');
					$xlsPageSetup = $xlsDocument->createElement('PageSetup');
					$xlsLayout = $xlsDocument->createElement('Layout');
					if (($isLandscape = $style->isLandscape()) !== null) {
						if ($isLandscape->getBoolean()) {
							$xlsLayout->setAttribute('x:Orientation', 'Landscape');
						} else {
							$xlsLayout->setAttribute('x:Orientation', 'Portrait');
						}
					}
					$centered = array(
						'x:CenterHorizontal' => $style->getHorizontalCentered(),
						'x:CenterVertical' => $style->getVerticalCentered()
					);
					foreach ($centered as $k => $v) {
						if ($v !== null && $v->getBoolean()) {
							$xlsLayout->setAttribute($k, 1);
						}
					}
					if ($xlsLayout->hasAttributes()) {
						$xlsPageSetup->appendChild($xlsLayout);
					}
					$xlsPageMargins = $xlsDocument->createElement('PageMargins');
					$margins = array(
						'Top' => $style->getTopMargin(),
						'Left' => $style->getLeftMargin(),
						'Right' => $style->getRightMargin(),
						'Bottom' => $style->getBottomMargin()
					);
					foreach ($margins as $k => $v) {
						if ($v !== null) {
							$xlsPageMargins->setAttribute('x:' . $k, $v->getSize()->getNumber());
						}
					}
					if ($xlsPageMargins->hasAttributes()) {
						$xlsPageSetup->appendChild($xlsPageMargins);
					}
					if ($xlsPageSetup->hasChildNodes()) {
						$xlsWorksheetOptions->appendChild($xlsPageSetup);
					}
					if (($paper = $style->getPaper()) !== null) {
						$xlsPrint = $xlsDocument->createElement('Print');
						$xlsPrint->appendChild($xlsDocument->createElement('PaperSizeIndex', self::$papers[$paper->getNumber()]));
						$xlsWorksheetOptions->appendChild($xlsPrint);
					}
					if (($row = $style->getFreezeRow()) !== null) {
						$xlsWorksheetOptions->appendChild($xlsDocument->createElement('SplitHorizontal', $row->getNumber()));
						$xlsWorksheetOptions->appendChild($xlsDocument->createElement('TopRowBottomPane', $row->getNumber()));
					}
					if (($column = $style->getFreezeColumn()) !== null) {
						$xlsWorksheetOptions->appendChild($xlsDocument->createElement('SplitVertical', $column->getNumber()));
						$xlsWorksheetOptions->appendChild($xlsDocument->createElement('LeftColumnRightPane', $column->getNumber()));
					}
					if ($xlsWorksheetOptions->hasChildNodes()) {
						$xlsWorksheetOptions->appendChild($xlsDocument->createElement('FreezePanes'));
						$xlsWorksheet->appendChild($xlsWorksheetOptions);
					}
				}
				$xlsTable = $xlsDocument->createElement('Table');
				for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($tableRowColumnStyles->size())->getBoolean(); $c = $c->increase()) {
					$Column = $xlsDocument->createElement('Column');
					if (($style = $tableRowColumnStyles->get($c)) !== null) {
						// **************************************************
						$Column->setAttribute('ss:Width', sprintf('%.2f', $style->getSize()->getNumber()));
					}
					$xlsTable->appendChild($Column);
				}
				$tableRows = $table->getTableRows();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					$xlsRow = $xlsDocument->createElement('Row');
					if ($r->isLessThan($tableRows->size())->getBoolean() && ($tableRow = $tableRows->get($r)) !== null) {
						$tableCells = $tableRow->getTableCells();
						if (($style = $tableRow->getTableRowColumnStyle()) !== null && ($size = $style->getSize()) != null) {
							// **************************************************
							$xlsRow->setAttribute('ss:Height', sprintf('%.2f', $size->getNumber()));
						}
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							if ($availableCells->getData($r, $c)->getBoolean()) {
								if ($c->isLessThan($tableCells->size())->getBoolean() && ($tableCell = $tableCells->get($c)) !== null) {
									$xlsCell = $xlsDocument->createElement('Cell');
									$xlsCell->setAttribute('ss:Index', $c->getNumber() + 1);
									if (($style = $tableCell->getTableCellStyle()) !== null) {
										// **************************************************
										// store the style which is not exist in the set
										// **************************************************
										if ($tableCellStyleSet->contains($style)->not()->getBoolean()) {
											$tableCellStyleSet->put($style);
											$xlsStyle = $xlsDocument->createElement('Style');
											$xlsStyle->setAttribute('ss:ID', 'ce-' . $style->getHashCode()->getString());
											$xlsAlignment = $xlsDocument->createElement('Alignment');
											if (($align = $style->getHorizontalAlign()) !== null && array_key_exists($align->getNumber(), self::$horizontalAligns)) {
												$xlsAlignment->setAttribute('ss:Horizontal', self::$horizontalAligns[$align->getNumber()]);
											}
											if (($align = $style->getVerticalAlign()) !== null && array_key_exists($align->getNumber(), self::$verticalAligns)) {
												$xlsAlignment->setAttribute('ss:Vertical', self::$verticalAligns[$align->getNumber()]);
											}
											if ($xlsAlignment->hasAttributes()) {
												$xlsStyle->appendChild($xlsAlignment);
											}
											$xlsBorders = $xlsDocument->createElement('Borders');
											$borders = array(
												'Top' => $style->getTopBorder(),
												'Left' => $style->getLeftBorder(),
												'Right' => $style->getRightBorder(),
												'Bottom' => $style->getBottomBorder(),
											);
											foreach ($borders as $k => $v) {
												if ($v !== null) {
													$xlsBorder = $xlsDocument->createElement('Border');
													$xlsBorder->setAttribute('ss:Position', $k);
													$xlsBorder->setAttribute('ss:Weight', $v->getSize()->getSize()->getNumber());
													if (($type = $v->getType()) !== null && array_key_exists($type->getNumber(), self::$borderTypes)) {
														$xlsBorder->setAttribute('ss:LineStyle', self::$borderTypes[$type->getNumber()]);
													}
													if (($color = $v->getColor()) !== null) {
														$xlsBorder->setAttribute('ss:Color', sprintf('#%06x', $color->getRGB()->getNumber()));
													}
													$xlsBorders->appendChild($xlsBorder);
												}
											}
											if ($xlsBorders->hasChildNodes()) {
												$xlsStyle->appendChild($xlsBorders);
											}
											if (($color = $style->getBackgroundColor()) !== null) {
												$xlsInterior = $xlsDocument->createElement('Interior');
												$xlsInterior->setAttribute('ss:Color', sprintf('#%06x', $color->getRGB()->getNumber()));
												$xlsInterior->setAttribute('ss:Pattern', 'Solid');
												$xlsStyle->appendChild($xlsInterior);
											}
											$xlsFont = $xlsDocument->createElement('Font');
											$xlsFont->setAttribute('x:CharSet', 136);
											if (($font = $style->getFont()) !== null) {
												$xlsFont->setAttribute('ss:Size', $font->getSize()->getSize()->getNumber());
												if (($name = $font->getName()) !== null) {
													$xlsFont->setAttribute('ss:FontName', $name->getString());
												}
												if (($type = $font->getType()) !== null) {
													if ($type->bitwiseAnd(\php\awt\Font::BOLD())->isPositive()->getBoolean()) {
														$xlsFont->setAttribute('ss:Bold', 1);
													}
													if ($type->bitwiseAnd(\php\awt\Font::ITALIC())->isPositive()->getBoolean()) {
														$xlsFont->setAttribute('ss:Italic', 1);
													}
												}
											}
											if (($color = $style->getTextColor()) !== null) {
												$xlsFont->setAttribute('ss:Color', sprintf('#%06x', $color->getRGB()->getNumber()));
											}
											$xlsStyle->appendChild($xlsFont);
											$xlsStyles->appendChild($xlsStyle);
										}
										// **************************************************
										// apply the style
										// **************************************************
										$xlsCell->setAttribute('ss:StyleID', 'ce-' . $style->getHashCode()->getString());
									}
									$bottom = $r->increase();
									$right = $c->increase();
									if (($span = $tableCell->getRowspan()) !== null) {
										if (($number = $span->getNumber()) > 1) {
											$xlsCell->setAttribute('ss:MergeDown', $number - 1);
										}
										$bottom = $r->addition($span);
									}
									if (($span = $tableCell->getColspan()) !== null) {
										if (($number = $span->getNumber()) > 1) {
											$xlsCell->setAttribute('ss:MergeAcross', $number - 1);
										}
										$right = $c->addition($span);
									}
									for ($y = $r; $y->isLessThan($bottom)->getBoolean(); $y = $y->increase()) {
										for ($x = $c; $x->isLessThan($right)->getBoolean(); $x = $x->increase()) {
											$availableCells->setData($y, $x, \php\lang\PHPBoolean::newInstance(false));
										}
									}
									// **************************************************
									if (($data = $tableCell->getData()) !== null) {
										$xlsData = $xlsDocument->createElement('Data', $data->getString());
										$xlsData->setAttribute('ss:Type', 'String');
										$xlsCell->appendChild($xlsData);
									}
									$xlsRow->appendChild($xlsCell);
								}
							}
						}
					} else {
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$xlsRow->appendChild($xlsDocument->createElement('Cell'));
						}
					}
					$xlsTable->appendChild($xlsRow);
				}
				$xlsWorksheet->appendChild($xlsTable);
				$xlsWorkbook->appendChild($xlsWorksheet);
			}
			$xlsDocument->appendChild($xlsWorkbook);
			echo trim($xlsDocument->saveXML());
		}

	}

}