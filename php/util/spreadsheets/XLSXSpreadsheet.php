<?php

/**
 * Description of \php\util\spreadsheets\XLSXSpreadsheet
 */

namespace php\util\spreadsheets {
	include_once('php/lang/PHPObject.php');
	include_once('php/lang/PHPString.php');
	include_once('php/lang/PHPBoolean.php');
	include_once('php/util/Exporter.php');
	include_once('php/util/Zip.php');
	include_once('php/util/collections/Map.php');
	include_once('php/util/spreadsheets/Table.php');
	include_once('php/util/spreadsheets/Spreadsheet.php');

	/**
	 * The \php\util\spreadsheets\XLSXSpreadsheet class represents the XLSX spreadsheet.
	 */
	class XLSXSpreadsheet extends Spreadsheet {

		private static $papers = array(
			0 => 8, // A3
			1 => 9, // A4
			2 => 11, // A5
		);
		private static $borderTypes = array(
			1 => 'thin',
			2 => 'hair',
			3 => 'dotted',
		);
		private static $horizontalAligns = array(
			1 => 'left',
			2 => 'center',
			3 => 'right',
		);
		private static $verticalAligns = array(
			1 => 'top',
			2 => 'center',
			3 => 'bottom',
		);

		/**
		 * Returns a \php\util\spreadsheets\XLSXSpreadsheet object.
		 */
		public static function newInstance() {
			return new XLSXSpreadsheet();
		}

		/**
		 * Contructs a \php\util\spreadsheets\XLSXSpreadsheet object.
		 */
		protected function __construct() {
			parent::__construct(\php\lang\PHPString::newInstance('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'), \php\lang\PHPString::newInstance('xlsx'));
		}

		private function valueToAlphabet($value) {
			$minAlphabet = ord('A');
			$maxAlphabet = ord('Z');
			$maxRange = $maxAlphabet - $minAlphabet + 1;
			$alphabet = '';
			while ($value >= 0) {
				$alphabet = chr(($value % $maxRange) + $minAlphabet) . $alphabet;
				$value = floor($value / $maxRange) - 1;
			}
			return $alphabet;
		}

		/**
		 * Exports the data from this XLSX spreadsheet.
		 * @param \php\lang\PHPString $filename The default export file name. Default &lt;current system date time&gt;.
		 */
		public function export(\php\lang\PHPString $filename = null) {
			// **************************************************
			// initial sets to avoid duplicated entries
			// **************************************************
			$sharedStrings = \php\util\collections\Set::newInstance();
			$fontSet = \php\util\collections\Set::newInstance();
			$fontSet->put(\php\lang\PHPObject::newInstance());
			$fillSet = \php\util\collections\Set::newInstance();
			$fillSet->put(\php\lang\PHPObject::newInstance());
			$fillSet->put(\php\lang\PHPObject::newInstance());
			$borderSet = \php\util\collections\Set::newInstance();
			$borderSet->put(\php\lang\PHPObject::newInstance());
			$cellXfSet = \php\util\collections\Set::newInstance();
			$cellXfSet->put(\php\lang\PHPObject::newInstance());
			$zip = \php\util\Zip::newInstance();
			// **************************************************
			// initial DOM document for to [Content_Types].xml
			// **************************************************
			$contentTypeDocument = new \DOMDocument('1.0', 'UTF-8');
			$contentTypeDocument->preserveWhiteSpace = false;
			$contentTypeDocument->formatOutput = true;
			$contentTypeTypes = $contentTypeDocument->createElement('Types');
			$contentTypeTypes->setAttribute('xmlns', 'http://schemas.openxmlformats.org/package/2006/content-types');
			$contentTypeDefault = $contentTypeDocument->createElement('Default');
			$contentTypeDefault->setAttribute('Extension', 'rels');
			$contentTypeDefault->setAttribute('ContentType', 'application/vnd.openxmlformats-package.relationships+xml');
			$contentTypeTypes->appendChild($contentTypeDefault);
			$contentTypeDefault = $contentTypeDocument->createElement('Default');
			$contentTypeDefault->setAttribute('Extension', 'xml');
			$contentTypeDefault->setAttribute('ContentType', 'application/xml');
			$contentTypeTypes->appendChild($contentTypeDefault);
			$contentTypeOverride = $contentTypeDocument->createElement('Override');
			$contentTypeOverride->setAttribute('PartName', '/xl/workbook.xml');
			$contentTypeOverride->setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml');
			$contentTypeTypes->appendChild($contentTypeOverride);
			$contentTypeOverride = $contentTypeDocument->createElement('Override');
			$contentTypeOverride->setAttribute('PartName', '/xl/styles.xml');
			$contentTypeOverride->setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml');
			$contentTypeTypes->appendChild($contentTypeOverride);
			$contentTypeOverride = $contentTypeDocument->createElement('Override');
			$contentTypeOverride->setAttribute('PartName', '/xl/sharedStrings.xml');
			$contentTypeOverride->setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml');
			$contentTypeTypes->appendChild($contentTypeOverride);
			// **************************************************
			// initial DOM document for to _rels/.rels
			// **************************************************
			$relsDocument = new \DOMDocument('1.0', 'UTF-8');
			$relsDocument->preserveWhiteSpace = false;
			$relsDocument->formatOutput = true;
			$relsRelationships = $relsDocument->createElement('Relationships');
			$relsRelationships->setAttribute('xmlns', 'http://schemas.openxmlformats.org/package/2006/relationships');
			$relsRelationship = $relsDocument->createElement('Relationship');
			$relsRelationship->setAttribute('Id', 'workbook');
			$relsRelationship->setAttribute('Target', '/xl/workbook.xml');
			$relsRelationship->setAttribute('Type', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument');
			$relsRelationships->appendChild($relsRelationship);
			$relsDocument->appendChild($relsRelationships);
			// **************************************************
			// initial DOM document for to xl/workbook.xml
			// **************************************************
			$workbookDocument = new \DOMDocument('1.0', 'UTF-8');
			$workbookDocument->preserveWhiteSpace = false;
			$workbookDocument->formatOutput = true;
			$workbookWorkbook = $workbookDocument->createElement('workbook');
			$workbookWorkbook->setAttribute('xmlns', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main');
			$workbookSheets = $workbookDocument->createElement('sheets');
			$workbookSheets->setAttribute('xmlns:r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
			// **************************************************
			// initial DOM document for to xl/_rels/workbook.xml.rels
			// **************************************************
			$workbookRelsDocument = new \DOMDocument('1.0', 'UTF-8');
			$workbookRelsDocument->preserveWhiteSpace = false;
			$workbookRelsDocument->formatOutput = true;
			$workbookRelsRelationships = $workbookRelsDocument->createElement('Relationships');
			$workbookRelsRelationships->setAttribute('xmlns', 'http://schemas.openxmlformats.org/package/2006/relationships');
			$workbookRelsRelationship = $workbookRelsDocument->createElement('Relationship');
			$workbookRelsRelationship->setAttribute('Id', 'style');
			$workbookRelsRelationship->setAttribute('Target', '/xl/styles.xml');
			$workbookRelsRelationship->setAttribute('Type', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles');
			$workbookRelsRelationships->appendChild($workbookRelsRelationship);
			$workbookRelsRelationship = $workbookRelsDocument->createElement('Relationship');
			$workbookRelsRelationship->setAttribute('Id', 'shared-strings');
			$workbookRelsRelationship->setAttribute('Target', '/xl/sharedStrings.xml');
			$workbookRelsRelationship->setAttribute('Type', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings');
			$workbookRelsRelationships->appendChild($workbookRelsRelationship);
			// **************************************************
			// initial DOM document for to xl/sharedStrings.xml
			// **************************************************
			$sharedStringsDocument = new \DOMDocument('1.0', 'UTF-8');
			$sharedStringsDocument->preserveWhiteSpace = false;
			$sharedStringsDocument->formatOutput = true;
			$sharedStringsSst = $sharedStringsDocument->createElement('sst');
			$sharedStringsSst->setAttribute('xmlns', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main');
			// **************************************************
			// initial DOM document for to xl/styles.xml
			// **************************************************
			$stylesDocument = new \DOMDocument('1.0', 'UTF-8');
			$stylesDocument->preserveWhiteSpace = false;
			$stylesDocument->formatOutput = true;
			$stylesStyleSheet = $stylesDocument->createElement('styleSheet');
			$stylesStyleSheet->setAttribute('xmlns', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main');
			$stylesFonts = $stylesDocument->createElement('fonts');
			$stylesFonts->appendChild($stylesDocument->createElement('font'));
			$stylesFills = $stylesDocument->createElement('fills');
			$stylesFills->appendChild($stylesDocument->createElement('fill'));
			$stylesFills->appendChild($stylesDocument->createElement('fill'));
			$stylesBorders = $stylesDocument->createElement('borders');
			$stylesBorders->appendChild($stylesDocument->createElement('border'));
			$stylesCellXfs = $stylesDocument->createElement('cellXfs');
			$stylesCellXfs->appendChild($stylesDocument->createElement('xf'));
			// **************************************************
			$names = $this->tables->keySet();
			for ($i = \php\lang\PHPNumber::ZERO(); $i->isLessThan($names->size())->getBoolean(); $i = $i->increase()) {
				$name = $names->get($i);
				$table = $this->tables->get($name);
				$rowCount = $table->rowCount();
				$columnCount = $table->columnCount();
				// **************************************************
				// assume the table cells all are null
				// **************************************************
				$availableCells = Table::newInstance();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
						$availableCells->setData($r, $c, null);
					}
				}
				// **************************************************
				// initial DOM document for to xl/worksheet/sheet-*.xml
				// **************************************************
				$sheetDocument = new \DOMDocument('1.0', 'UTF-8');
				$sheetDocument->preserveWhiteSpace = false;
				$sheetDocument->formatOutput = true;
				$sheetWorksheet = $sheetDocument->createElement('worksheet');
				$sheetWorksheet->setAttribute('xmlns', 'http://schemas.openxmlformats.org/spreadsheetml/2006/main');
//				$sheetWorksheet->setAttribute('xmlns:r', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships');
				if (($style = $table->getTableStyle()) !== null) {
					$sheetSheetViews = $sheetDocument->createElement('sheetViews');
					$sheetSheetView = $sheetDocument->createElement('sheetView');
					$sheetSheetView->setAttribute('workbookViewId', 0);
					$sheetPane = $sheetDocument->createElement('pane');
					$column = $style->getFreezeColumn();
					$row = $style->getFreezeRow();
					if ($column !== null) {
						$sheetPane->setAttribute('xSplit', $column->getNumber());
					} else {
						$column = \php\lang\PHPNumber::ZERO();
					}
					if ($row !== null) {
						$sheetPane->setAttribute('ySplit', $row->getNumber());
					} else {
						$row = \php\lang\PHPNumber::ZERO();
					}
					if ($sheetPane->hasAttributes()) {
						$column = $this->valueToAlphabet($column->getNumber());
						$row = $row->increase()->getNumber();
						$sheetPane->setAttribute('topLeftCell', $column . $row);
						$sheetPane->setAttribute('state', 'frozen');
						$sheetSheetView->appendChild($sheetPane);
						$sheetSheetViews->appendChild($sheetSheetView);
						$sheetWorksheet->appendChild($sheetSheetViews);
					}
				}
				$sheetCols = $sheetDocument->createElement('cols');
				$tableRowColumnStyles = $table->getTableRowColumnStyles();
				for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
					$sheetCol = $sheetDocument->createElement('col');
					$sheetCol->setAttribute('min', $c->increase()->getNumber());
					$sheetCol->setAttribute('max', $c->increase()->getNumber());
					if ($c->isLessThan($tableRowColumnStyles->size())->getBoolean() && ($style = $tableRowColumnStyles->get($c)) !== null) {
						$sheetCol->setAttribute('width', $style->getSize()->getNumber() / 8.0);
						$sheetCol->setAttribute('customWidth', 1);
					}
					$sheetCols->appendChild($sheetCol);
				}
				$sheetWorksheet->appendChild($sheetCols);
				$sheetSheetData = $sheetDocument->createElement('sheetData');
				$sheetMergeCells = $sheetDocument->createElement('mergeCells');
				$tableRows = $table->getTableRows();
				for ($r = \php\lang\PHPNumber::ZERO(); $r->isLessThan($rowCount)->getBoolean(); $r = $r->increase()) {
					$sheetRow = $sheetDocument->createElement('row');
					$sheetRow->setAttribute('r', $r->increase()->getNumber());
					if ($r->isLessThan($tableRows->size())->getBoolean() && ($tableRow = $tableRows->get($r)) !== null) {
						if (($style = $tableRow->getTableRowColumnStyle()) !== null) {
							$sheetRow->setAttribute('ht', $style->getSize()->getNumber() * 0.75);
							$sheetRow->setAttribute('customHeight', 1);
						}
						$tableCells = $tableRow->getTableCells();
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$sheetC = $sheetDocument->createElement('c');
							$sheetC->setAttribute('r', $this->valueToAlphabet($c->getNumber()) . $r->increase()->getNumber());
							if (($xf = $availableCells->getData($r, $c)) !== null) {
								$sheetC->setAttribute('t', 's');
								$sheetC->setAttribute('s', $cellXfSet->indexOf($xf)->getNumber());
							} else {
								if ($c->isLessThan($tableCells->size())->getBoolean() && ($tableCell = $tableCells->get($c)) !== null) {
									if (($style = $tableCell->getTableCellStyle()) !== null) {
										$stylesFont = $stylesDocument->createElement('font');
										$stylesFill = $stylesDocument->createElement('fill');
										$stylesBorder = $stylesDocument->createElement('border');
										$stylesXf = $stylesDocument->createElement('xf');
										$stylesXf->setAttribute('quotePrefix', 1);
										// **************************************************
										$font = $style->getFont();
										if ($font !== null) {
											$stylesCharset = $stylesDocument->createElement('charset');
											$stylesCharset->setAttribute('val', 136);
											$stylesFont->appendChild($stylesCharset);
											if (($type = $font->getType()) !== null) {
												if ($type->bitwiseAnd(\php\awt\Font::BOLD())->isPositive()->getBoolean()) {
													$stylesFont->appendChild($stylesDocument->createElement('b'));
												}
												if ($type->bitwiseAnd(\php\awt\Font::ITALIC())->isPositive()->getBoolean()) {
													$stylesFont->appendChild($stylesDocument->createElement('i'));
												}
											}
											if (($val = $font->getName()) !== null) {
												$stylesName = $stylesDocument->createElement('name');
												$stylesName->setAttribute('val', $val->getString());
												$stylesFont->appendChild($stylesName);
											}
											if (($val = $font->getSize()) !== null) {
												$stylesSz = $stylesDocument->createElement('sz');
												$stylesSz->setAttribute('val', $val->getSize()->getNumber());
												$stylesFont->appendChild($stylesSz);
											}
											if (($val = $style->getTextColor()) !== null) {
												$stylesColor = $stylesDocument->createElement('color');
												$stylesColor->setAttribute('rgb', sprintf('ff%06x', $val->getRGB()->getNumber()));
												$stylesFont->appendChild($stylesColor);
											}
										}
										$font = \php\lang\PHPString::newInstance(trim($stylesDocument->saveXML($stylesFont)));
										if ($fontSet->contains($font)->not()->getBoolean()) {
											$fontSet->put($font);
											$stylesFonts->appendChild($stylesFont);
										}
										// **************************************************
										$fill = $style->getBackgroundColor();
										if ($fill !== null && $fillSet->contains($fill)->not()->getBoolean()) {
											$stylesPatternFill = $stylesDocument->createElement('patternFill');
											$stylesPatternFill->setAttribute('patternType', 'solid');
											$stylesColor = $stylesDocument->createElement('fgColor');
											$stylesColor->setAttribute('rgb', sprintf('ff%06x', $fill->getRGB()->getNumber()));
											$stylesPatternFill->appendChild($stylesColor);
											$stylesFill->appendChild($stylesPatternFill);
										}
										$fill = \php\lang\PHPString::newInstance(trim($stylesDocument->saveXML($stylesFill)));
										if ($fillSet->contains($fill)->not()->getBoolean()) {
											$fillSet->put($fill);
											$stylesFills->appendChild($stylesFill);
										}
										// **************************************************
										$borders = array(
											'left' => $style->getLeftBorder(),
											'right' => $style->getRightBorder(),
											'top' => $style->getTopBorder(),
											'bottom' => $style->getBottomBorder()
										);
										foreach ($borders as $k => $v) {
											$_styles[$k] = $stylesDocument->createElement($k);
											if ($v !== null) {
												if (($type = $v->getType()) !== null && array_key_exists($type->getNumber(), self::$borderTypes)) {
													$_styles[$k]->setAttribute('style', self::$borderTypes[$type->getNumber()]);
												}
												if (($val = $v->getColor()) !== null) {
													$stylesColor = $stylesDocument->createElement('color');
													$stylesColor->setAttribute('rgb', sprintf('ff%06x', $val->getRGB()->getNumber()));
													$_styles[$k]->appendChild($stylesColor);
												}
											}
											$stylesBorder->appendChild($_styles[$k]);
										}
										$border = \php\lang\PHPString::newInstance(trim($stylesDocument->saveXML($stylesBorder)));
										if ($borderSet->contains($border)->not()->getBoolean()) {
											$borderSet->put($border);
											$stylesBorders->appendChild($stylesBorder);
										}
										// **************************************************
										if (($index = $fontSet->indexOf($font))) {
											$stylesXf->setAttribute('fontId', $index->getNumber());
											$stylesXf->setAttribute('applyFont', 1);
										}
										// **************************************************
										if (($index = $fillSet->indexOf($fill))) {
											$stylesXf->setAttribute('fillId', $index->getNumber());
											$stylesXf->setAttribute('applyFill', 1);
										}
										// **************************************************
										if (($index = $borderSet->indexOf($border))) {
											$stylesXf->setAttribute('borderId', $index->getNumber());
											$stylesXf->setAttribute('applyBorder', 1);
										}
										// **************************************************
										$_alignment = $stylesDocument->createElement('alignment');
										if (($align = $style->getHorizontalAlign()) !== null && array_key_exists($align->getNumber(), self::$horizontalAligns)) {
											$_alignment->setAttribute('horizontal', self::$horizontalAligns[$align->getNumber()]);
										}
										if (($align = $style->getVerticalAlign()) !== null && array_key_exists($align->getNumber(), self::$verticalAligns)) {
											$_alignment->setAttribute('vertical', self::$verticalAligns[$align->getNumber()]);
										}
										// **************************************************
										if ($_alignment->hasAttributes()) {
											$stylesXf->setAttribute('applyAlignment', 1);
											$stylesXf->appendChild($_alignment);
										}
										// **************************************************
										$xf = \php\lang\PHPString::newInstance(trim($stylesDocument->saveXML($stylesXf)));
										if ($cellXfSet->contains($xf)->not()->getBoolean()) {
											$cellXfSet->put($xf);
											$stylesCellXfs->appendChild($stylesXf);
										}
									}
									$data = $tableCell->getData();
									$data = (($data === null) ? \php\lang\PHPString::newInstance('') : $data->toString());
									// **************************************************
									if ($sharedStrings->contains($data)->not()->getBoolean()) {
										$sharedStrings->put($data);
										$sharedStringsSi = $sharedStringsDocument->createElement('si');
										$sharedStringsT = $sharedStringsDocument->createElement('t', $data->getString());
										$sharedStringsSi->appendChild($sharedStringsT);
										$sharedStringsSst->appendChild($sharedStringsSi);
									}
									// **************************************************
									if (($style = $tableCell->getTableCellStyle()) !== null) {
										$sheetC->setAttribute('t', 's');
										$sheetC->setAttribute('s', $cellXfSet->indexOf($xf)->getNumber());
									}
									$v = $sheetDocument->createElement('v', $sharedStrings->indexOf($data));
									$sheetC->appendChild($v);
									// **************************************************
									$sheetMergeCell = $sheetDocument->createElement('mergeCell');
									// **************************************************
									$bottom = $r->increase();
									$right = $c->increase();
									if (($span = $tableCell->getRowspan()) !== null) {
										$bottom = $r->addition($span);
									}
									if (($span = $tableCell->getColspan()) !== null) {
										$right = $c->addition($span);
									}
									for ($y = $r; $y->isLessThan($bottom)->getBoolean(); $y = $y->increase()) {
										for ($x = $c; $x->isLessThan($right)->getBoolean(); $x = $x->increase()) {
											$availableCells->setData($y, $x, $xf);
										}
									}
									$leftTop = $this->valueToAlphabet($c->getNumber())
										. $r->increase()->getNumber();
									$rightBottom = $this->valueToAlphabet($right->descrease()->getNumber())
										. $bottom->getNumber();
									if ($leftTop != $rightBottom) {
										$sheetMergeCell->setAttribute('ref', sprintf('%s:%s'
												, $leftTop
												, $rightBottom
										));
										$sheetMergeCells->appendChild($sheetMergeCell);
									}
								}
							}
							$sheetRow->appendChild($sheetC);
						}
					} else {
						for ($c = \php\lang\PHPNumber::ZERO(); $c->isLessThan($columnCount)->getBoolean(); $c = $c->increase()) {
							$sheetRow->appendChild($sheetDocument->createElement('c'));
						}
					}
					$sheetSheetData->appendChild($sheetRow);
				}
				$sheetWorksheet->appendChild($sheetSheetData);
				$sheetWorksheet->appendChild($sheetMergeCells);
				if (($style = $table->getTableStyle()) !== null) {
					$sheetPrintOptions = $sheetDocument->createElement('printOptions');
					$centered = array(
						'horizontalCentered' => $style->getHorizontalCentered(),
						'verticalCentered' => $style->getVerticalCentered()
					);
					foreach ($centered as $k => $v) {
						if ($v !== null && $v->getBoolean()) {
							$sheetPrintOptions->setAttribute($k, 1);
						}
					}
					if ($sheetPrintOptions->hasAttributes()) {
						$sheetWorksheet->appendChild($sheetPrintOptions);
					}
					// **************************************************
					$sheetPageMargins = $sheetDocument->createElement('pageMargins');
					$margins = array(
						'left' => $style->getTopMargin(),
						'right' => $style->getRightMargin(),
						'top' => $style->getTopMargin(),
						'bottom' => $style->getBottomMargin()
					);
					foreach ($margins as $k => $v) {
						if ($v !== null) {
							$sheetPageMargins->setAttribute($k, $v->getSize()->getNumber());
						}
					}
					$sheetPageMargins->setAttribute('header', 0);
					$sheetPageMargins->setAttribute('footer', 0);
					$sheetWorksheet->appendChild($sheetPageMargins);
					// **************************************************
					$sheetPageSetup = $sheetDocument->createElement('pageSetup');
					if (($paper = $style->getPaper()) !== null) {
						$sheetPageSetup->setAttribute('paperSize', self::$papers[$paper->getNumber()]);
					}
					if (($isLandscape = $style->isLandscape()) !== null) {
						if ($isLandscape->getBoolean()) {
							$sheetPageSetup->setAttribute('orientation', 'landscape');
						} else {
							$sheetPageSetup->setAttribute('orientation', 'portrait');
						}
					}
					if ($sheetPageSetup->hasAttributes()) {
						$sheetWorksheet->appendChild($sheetPageSetup);
					}
				}
				// **************************************************
				$contentTypeOverride = $contentTypeDocument->createElement('Override');
				$contentTypeOverride->setAttribute('PartName', sprintf('/xl/worksheets/sheet-%d.xml', $i->getNumber()));
				$contentTypeOverride->setAttribute('ContentType', 'application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml');
				$contentTypeTypes->appendChild($contentTypeOverride);
				// **************************************************
				$sheet = $workbookDocument->createElement('sheet');
				$sheet->setAttribute('name', $name->getString());
				$sheet->setAttribute('sheetId', $i->increase()->getNumber());
				$sheet->setAttribute('r:id', 'sheet-' . $i->getNumber());
				$workbookSheets->appendChild($sheet);
				// **************************************************
				$workbookRelsRelationship = $workbookRelsDocument->createElement('Relationship');
				$workbookRelsRelationship->setAttribute('Id', 'sheet-' . $i->getNumber());
				$workbookRelsRelationship->setAttribute('Target', sprintf('/xl/worksheets/sheet-%s.xml', $i->getNumber()));
				$workbookRelsRelationship->setAttribute('Type', 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet');
				$workbookRelsRelationships->appendChild($workbookRelsRelationship);
				// **************************************************
				$sheetDocument->appendChild($sheetWorksheet);
				$zip->addFile(\php\lang\PHPString::newInstance(sprintf('xl/worksheets/sheet-%d.xml', $i->getNumber())), \php\lang\PHPString::newInstance(trim($sheetDocument->saveXML())));
			}
			// **************************************************
			$contentTypeDocument->appendChild($contentTypeTypes);
			// **************************************************
			$workbookWorkbook->appendChild($workbookSheets);
			$workbookDocument->appendChild($workbookWorkbook);
			// **************************************************
			$workbookRelsDocument->appendChild($workbookRelsRelationships);
			// **************************************************
			$sharedStringsDocument->appendChild($sharedStringsSst);
			// **************************************************
			$stylesStyleSheet->appendChild($stylesFonts);
			$stylesStyleSheet->appendChild($stylesFills);
			$stylesStyleSheet->appendChild($stylesBorders);
			$stylesStyleSheet->appendChild($stylesCellXfs);
			$stylesDocument->appendChild($stylesStyleSheet);
			// **************************************************
			$zip->addFile(\php\lang\PHPString::newInstance('[Content_Types].xml'), \php\lang\PHPString::newInstance(trim($contentTypeDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance('_rels/.rels'), \php\lang\PHPString::newInstance(trim($relsDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance('xl/workbook.xml'), \php\lang\PHPString::newInstance(trim($workbookDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance('xl/_rels/workbook.xml.rels'), \php\lang\PHPString::newInstance(trim($workbookRelsDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance('xl/sharedStrings.xml'), \php\lang\PHPString::newInstance(trim($sharedStringsDocument->saveXML())));
			$zip->addFile(\php\lang\PHPString::newInstance('xl/styles.xml'), \php\lang\PHPString::newInstance(trim($stylesDocument->saveXML())));
			// **************************************************
			$extension = \php\lang\PHPString::newInstance($this->extension);
			$mimeType = \php\lang\PHPString::newInstance($this->mimeType);
			$zip->export($filename, $extension, $mimeType);
		}

	}

}